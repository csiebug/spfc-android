package tw.org.spfc.android;

import com.google.analytics.tracking.android.EasyTracker;

import android.webkit.WebView;
import csiebug.android.AbstractDefaultWebActivity;
import csiebug.android.ContextMethod;

public abstract class BaseWebActivity extends AbstractDefaultWebActivity {
	
	@Override
	protected void debugLogic(Exception e, Object[] args) {
		ContextMethod.debugLogic(this, e, args, getString(R.string.exception), getString(R.string.send_mail_to_author), getString(R.string.close), getString(R.string.authorEmail));
	}

	@Override
	protected int getWebViewId() {
		return R.id.web;
	}
	
	@Override
	protected int getLayout() {
		return R.layout.web;
	}
	
	@Override
	protected void customWebViewSettings(WebView webView) {
		//讓網頁可以zoom-in,zoom-out
		webView.getSettings().setBuiltInZoomControls(true);
	}
	
	protected String getBaseURL() {
		return null;
	}
	
	protected String getHTML() {
		return null;
	}
	
	protected String getEncoding() {
		return null;
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}
	
	@Override
	protected void onStopLogic() {
		super.onStopLogic();
		EasyTracker.getInstance().activityStop(this);
	}
}
