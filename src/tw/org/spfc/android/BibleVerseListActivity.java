package tw.org.spfc.android;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.google.analytics.tracking.android.EasyTracker;

import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;
import csiebug.util.StringUtility;
import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.service.FacebookService;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

public class BibleVerseListActivity extends BaseListActivity {
	private String keyword;
	private String[] originalVerses;
	private String[] indexes;
	
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private final String PENDING_ACTION_BUNDLE_KEY = "tw.org.spfc.android:PendingAction";
	private PendingAction pendingAction = PendingAction.NONE;
	private GraphUser user;
	
	private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
    
    protected GraphUser getUser() {
    	return user;
    }
    
    protected void setUser(GraphUser user) {
    	this.user = user;
    }
    
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (pendingAction != PendingAction.NONE &&
                (exception instanceof FacebookOperationCanceledException ||
                exception instanceof FacebookAuthorizationException)) {
        	pendingAction = PendingAction.NONE;
            handleFacebookPermissionNotGranted();
        } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
            handlePendingAction();
        }
        updateUI();
    }
    
    protected void handleFacebookPermissionNotGranted() {
		alert(getString(R.string.warning), getString(R.string.facebook_permission_not_granted), getString(R.string.ok));
	};
	
	@SuppressWarnings("incomplete-switch")
    protected void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

    }
	
	protected void updateUI() {
        Session session = Session.getActiveSession();
        boolean enableButtons = (session != null && session.isOpened());
        
        if (enableButtons && user != null) {
            enableUI(user);
        } else {
            disableUI();
        }
    }
	
	protected void enableUI(GraphUser user) {
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}
	
	protected void disableUI() {
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}
	
	private boolean hasPublishPermission() {
        Session session = Session.getActiveSession();
        return session != null && session.getPermissions().contains("publish_actions");
    }
	
    protected void performPublish(PendingAction action) {
        Session session = Session.getActiveSession();
        if (session != null) {
            pendingAction = action;
            if (hasPublishPermission()) {
                // We can do the action right away.
                handlePendingAction();
            } else {
                // We need to get new permissions, then complete the action when we get called back.
                session.requestNewPublishPermissions(new Session.NewPermissionsRequest(this, PERMISSIONS));
            }
        }
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
		try {
			//必要設定
	        super.onCreate(savedInstanceState);
	        uiHelper = new UiLifecycleHelper(this, callback);
	        uiHelper.onCreate(savedInstanceState);

	        if (savedInstanceState != null) {
	            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
	            pendingAction = PendingAction.valueOf(name);
	        }
	        
	        setListContentView();
	        
	        //客製設定
	        //無資料時,替代顯示的UI
	        if(getEmptyView() != null) {
	        	getListView().setEmptyView(findViewById(getEmptyView()));
	        }
	        
	        customSettings();
	        
	        //實作onCreate時的邏輯行為
	        restorePreference();
	        findViews();
	        onCreateLogic();
	        
	        //Binding UI的邏輯行為
	        setListeners();
	        if(getRegisterViewForContextMenu() != null) {
	        	registerForContextMenu(getRegisterViewForContextMenu());
	        }
	        
	        //Binding UI的資料
	        setAdapters();
	    } catch(Exception e) {
			debugLogic(e);
		}
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();

        updateUI();
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);

        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		uiHelper.onActivityResult(requestCode, resultCode, data);
		
		try {
			onActivityResultLogic(requestCode, resultCode, data);
		} catch(Exception e) {
			Iterator<String> keys = data.getExtras().keySet().iterator();
			StringBuffer bundle = new StringBuffer("[");
			int i = 0;
			while(keys.hasNext()) {
				String key = keys.next();
				if(i == 0) {
					i = 1;
				} else {
					bundle.append(", ");
				}
				bundle.append(key + " = " + data.getExtras().get(key));
			}
			bundle.append("]");
			debugLogic(e, new Object[]{"requestCode = " + requestCode, "resultCode = " + resultCode, "data.getExtras = " + bundle.toString()});
		}
	}
    
    @Override
	protected void onPause() {
		super.onPause();
		
		uiHelper.onPause();
		
		try {
			//實作onPause時的邏輯行為
			savePreferenceOnPause();
			onPauseLogic();
		} catch(Exception e) {
			debugLogic(e);
		}
	}
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }
    
    protected void publishFeedDialog(final String name, String caption,
			String description, String link, String picture) {
		//覆寫此功能，為了加上Google Analytics
		EasyTracker.getTracker().sendEvent("facebook", "click", "", (long)0);
		
		Bundle params = new Bundle();
        if(AssertUtility.isNotNullAndNotSpace(name)) {
        	params.putString("name", name);
        }
        if(AssertUtility.isNotNullAndNotSpace(caption)) {
        	params.putString("caption", caption);
        }
        if(AssertUtility.isNotNullAndNotSpace(description)) {
        	params.putString("description", description);
        }
        if(AssertUtility.isNotNullAndNotSpace(link)) {
        	params.putString("link", link);
        }
        if(AssertUtility.isNotNullAndNotSpace(picture)) {
        	params.putString("picture", picture);
        }

        WebDialog feedDialog = (
            new WebDialog.FeedDialogBuilder(this,
                Session.getActiveSession(),
                params))
            .setOnCompleteListener(new OnCompleteListener() {

                public void onComplete(Bundle values,
                    FacebookException error) {
                    if (error == null) {
                        // When the story is posted, echo the success
                        // and the post Id.
                        final String postId = values.getString("post_id");
                        if (postId != null) {
                        	//toast("Posted story, id: "+postId);
                        	EasyTracker.getTracker().sendEvent(name, "share", "", (long)0);
                        } else {
                        	//toast("Publish cancelled");
                        	EasyTracker.getTracker().sendEvent("facebook", "cancel", "", (long)0);
                        }
                    } else if (error instanceof FacebookOperationCanceledException) {
                    	//toast("Publish cancelled");
                    	EasyTracker.getTracker().sendEvent("facebook", "cancel", "", (long)0);
                    } else {
                    	//toast("Error posting story");
                    }
                }

            })
            .build();
        feedDialog.show();
	}
    
    /**
	 * 判斷是否已登入Twitter
	 * @return
	 */
	protected boolean isLoggedTwitter() {
		SPFCPreference preference = new SPFCPreference(this);
		return !preference.getTwitterToken().trim().equals("");
	}
	
	@Override
	protected void setAdapters() {
		super.setAdapters();
		
		((LoginButton) findViewById(R.id.login_button)).setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            public void onUserInfoFetched(GraphUser user) {
            	setUser(user);
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
	}
	
	@Override
	protected String[] getListItemTexts() {
		return null;
	}
	
	@Override
	protected Spanned[] getListItemSpanneds() {
		Spanned[] highlineVerses = new Spanned[originalVerses.length];
		for(int i = 0; i < originalVerses.length; i++) {
			//允許使用者輸入空白來隔開多個keyword
			String highlineVerse = originalVerses[i];
			List<String> keywords = StringUtility.parseKeyword(keyword, " ");
			for(int j = 0; j < keywords.size(); j++) {
				highlineVerse = highlineVerse.replaceAll(keywords.get(j), "<font color='red'>" + keywords.get(j) + "</font>");
			}
			
			highlineVerses[i] = Html.fromHtml(highlineVerse);
		}
		
		return highlineVerses;
	}

	@Override
	protected String[] getContextMenuOptionsByArray() {
		return null;
	}

	@Override
	protected List<String> getContextMenuOptionsByList() {
		return null;
	}

	@Override
	protected Map<Integer, String> getContextMenuOptionsByMap() {
		return null;
	}

	@Override
	protected void contextItemSelectedLogic(MenuItem item) {
	}

	@Override
	protected String getContextMenuHeaderTitle() {
		return null;
	}

	@Override
	protected int getContextMenuHeaderIcon() {
		return 0;
	}

	@Override
	protected void putListItemToBundle(Bundle bundle, int position) {
	}

	@Override
	protected Class<?> getListItemEditActivityClass() {
		return null;
	}

	@Override
	protected int getRequestCode() {
		return 0;
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
	}

	@Override
	protected void setListeners() {
		getListView().setOnItemLongClickListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					final int arg2, long arg3) {
				AlertDialog.Builder builder = new AlertDialog.Builder(BibleVerseListActivity.this);
				
				final Bundle bundle = new Bundle();
				Session session = Session.getActiveSession();
		        boolean enableButtons = (session != null && session.isOpened());
		        boolean facebookEnable = enableButtons && getUser() != null;
				bundle.putBoolean("facebookEnable", facebookEnable);
				
				String[] options = new String[3];
				if(facebookEnable && isLoggedTwitter()) {
					options = new String[5];
					options[0] = getString(R.string.reading);
					options[1] = getString(R.string.copy_to_clipboard);
					options[2] = getString(R.string.make_golden_word_card);
					options[3] = getString(R.string.share_to_facebook);
					options[4] = getString(R.string.share_to_twitter);
				} else if(facebookEnable) {
					options = new String[4];
					options[0] = getString(R.string.reading);
					options[1] = getString(R.string.copy_to_clipboard);
					options[2] = getString(R.string.make_golden_word_card);
					options[3] = getString(R.string.share_to_facebook);
				} else if(isLoggedTwitter()) {
					options = new String[4];
					options[0] = getString(R.string.reading);
					options[1] = getString(R.string.copy_to_clipboard);
					options[2] = getString(R.string.make_golden_word_card);
					options[3] = getString(R.string.share_to_twitter);
				} else {
					options[0] = getString(R.string.reading);
					options[1] = getString(R.string.copy_to_clipboard);
					options[2] = getString(R.string.make_golden_word_card);
				}
				final int optionLength = options.length;
				
				builder.setItems(options, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						int book = Integer.parseInt(indexes[arg2].split(":")[0]);
						int chapter = Integer.parseInt(indexes[arg2].split(":")[1]);
						int sec = Integer.parseInt(indexes[arg2].split(":")[2]);
						String verse = originalVerses[arg2];
						
						if(which == 0) {
							SPFCPreference preference = new SPFCPreference(BibleVerseListActivity.this);
							preference.setBibleReading(book, chapter, sec);
							setResult(1);
							finish();
						} else if(which == 1) {
							EasyTracker.getTracker().sendEvent("copyVerseToClipboard", "use", "", (long)0);
							
							if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
								ClipboardManager cliboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
								cliboard.setText(verse);
							} else {
								android.text.ClipboardManager cliboard = (android.text.ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
								cliboard.setText(verse);
							}
						} else if(which == 2) {
							Intent card = new Intent(BibleVerseListActivity.this, GoldenWordCardActivity.class);
							bundle.putInt("book", book);
							bundle.putInt("chap", chapter);
							bundle.putInt("sec", sec);
							bundle.putString("verse", verse);
							card.putExtras(bundle);
							startActivity(card);
						} else if(which == 3) {
							if(optionLength == 4 && isLoggedTwitter()) {
								SPFCPreference preference = new SPFCPreference(BibleVerseListActivity.this);
								String oauthAccessToken = preference.getTwitterToken();
								String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

								ConfigurationBuilder confbuilder = new ConfigurationBuilder();
								Configuration conf = confbuilder
													.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
													.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
													.setOAuthAccessToken(oauthAccessToken)
													.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
													.setUseSSL(true)
													.build();
								
								Twitter twitter = new TwitterFactory(conf).getInstance();
								
								try {
									twitter.updateStatus(verse);
									toast(getString(R.string.twitter_success));
								} catch (TwitterException e) {
									Log.e(getClass().getName(), "TwitterException", e);
								}
							} else {
								String pictureURL = getString(R.string.spfcAppIcon);
								try {
									pictureURL = ServiceProxyFactory.createFacebookService(BibleVerseListActivity.this).getPicture(FacebookService.FACEBOOK_MY_GOLDEN_WORD);
								} catch (ServiceException e) {
									Log.e(getClass().getName(), "Get picture URL error!", e);
								}
								
								String url = getString(R.string.spfcHomepage);
								
								publishFeedDialog(getString(R.string.facebook_my_golden_word), "", verse, url, pictureURL);
							}
						} else if(which == 4) {
							SPFCPreference preference = new SPFCPreference(BibleVerseListActivity.this);
							String oauthAccessToken = preference.getTwitterToken();
							String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

							ConfigurationBuilder confbuilder = new ConfigurationBuilder();
							Configuration conf = confbuilder
												.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
												.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
												.setOAuthAccessToken(oauthAccessToken)
												.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
												.setUseSSL(true)
												.build();
							
							Twitter twitter = new TwitterFactory(conf).getInstance();
							
							try {
								twitter.updateStatus(verse);
								toast(getString(R.string.twitter_success));
							} catch (TwitterException e) {
								Log.e(getClass().getName(), "TwitterException", e);
							}
						}
					}
			    });
				
				
			    builder.create();
			    
			    builder.show();
			    
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
	}
	
	@Override
	protected Integer getLayout() {
		return R.layout.result_list;
	}
	
	@Override
	protected void onCreateLogic() {
		keyword = getIntent().getExtras().getString("keyword");
		originalVerses = getIntent().getExtras().getStringArray("verses");
		indexes = getIntent().getExtras().getStringArray("indexes");
		
		int count = 0;
		if(StringUtility.parseKeyword(keyword, " ").size() > 1) {
			//如果是複合搜尋，則就搜尋結果有幾筆
			count = originalVerses.length;
			((TextView)findViewById(R.id.function_name)).setText(Html.fromHtml("<font color='red'>'" + getIntent().getExtras().getString("keyword") + "'</font>" + getString(R.string.query_result) + getString(R.string.query_result_count_2).replaceAll("var.count", "" + count) + "<br>" + getString(R.string.verse_hint)));
		} else {
			//如果是單詞搜尋，就計算這個詞出現幾次
			for(int i = 0; i < originalVerses.length; i++) {
				count = count + StringUtility.countKeyword(originalVerses[i], StringUtility.ltrim(keyword.trim()));
				((TextView)findViewById(R.id.function_name)).setText(Html.fromHtml("<font color='red'>'" + getIntent().getExtras().getString("keyword") + "'</font>" + getString(R.string.query_result) + getString(R.string.query_result_count_1).replaceAll("var.count", "" + count)  + "<br>" + getString(R.string.verse_hint)));
			}
		}
	}
	
	@Override
	protected Integer getListItemLayout() {
		int layout = FontSizeLayout.getLayoutForList(new SPFCPreference(this));
		
		return layout;
	}
	
	@Override
	protected Integer getListItemTextViewID() {
		return R.id.itemText;
	}
}
