package tw.org.spfc.android;

import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.widget.TextView;

public class BookListActivity extends BaseListActivity {
	private String[] books;
	
	@Override
	protected String[] getListItemTexts() {
		return books;
	}
	
	@Override
	protected Spanned[] getListItemSpanneds() {
		return null;
	}

	@Override
	protected String[] getContextMenuOptionsByArray() {
		return null;
	}

	@Override
	protected List<String> getContextMenuOptionsByList() {
		return null;
	}

	@Override
	protected Map<Integer, String> getContextMenuOptionsByMap() {
		return null;
	}

	@Override
	protected void contextItemSelectedLogic(MenuItem item) {
	}

	@Override
	protected String getContextMenuHeaderTitle() {
		return null;
	}

	@Override
	protected int getContextMenuHeaderIcon() {
		return 0;
	}

	@Override
	protected void putListItemToBundle(Bundle bundle, int position) {
		bundle.putString("name", getIntent().getExtras().getStringArray("bookNames")[position]);
		bundle.putString("link", getIntent().getExtras().getStringArray("bookLinks")[position]);
	}

	@Override
	protected Class<?> getListItemEditActivityClass() {
		return BookDetailActivity.class;
	}

	@Override
	protected int getRequestCode() {
		return 0;
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
	}

	@Override
	protected void setListeners() {
	}
	
	@Override
	protected Integer getLayout() {
		return R.layout.result_list;
	}
	
	@Override
	protected void onCreateLogic() {
		books = getIntent().getExtras().getStringArray("bookNames");
		((TextView)findViewById(R.id.function_name)).setText(Html.fromHtml("<font color='red'>'" + getIntent().getExtras().getString("keyword") + "'</font>" + getString(R.string.query_result) + getString(R.string.query_result_count).replaceAll("var.count", "" + books.length) + " " + getString(R.string.query_book_result)));
	}
}
