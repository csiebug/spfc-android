package tw.org.spfc.android;

import com.google.analytics.tracking.android.EasyTracker;

import csiebug.android.AbstractDefaultListActivity;
import csiebug.android.ContextMethod;

public abstract class BaseListActivity extends AbstractDefaultListActivity {
	@Override
	protected void customSettings() {
		getListView().setFastScrollEnabled(true);
	}
	
	
	@Override
	protected void debugLogic(Exception e, Object[] args) {
		ContextMethod.debugLogic(this, e, args, getString(R.string.exception), getString(R.string.send_mail_to_author), getString(R.string.close), getString(R.string.authorEmail));
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}
	
	@Override
	protected void onStopLogic() {
		super.onStopLogic();
		EasyTracker.getInstance().activityStop(this);
	}
}
