package tw.org.spfc.android;

import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

import com.facebook.FacebookRequestError;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.analytics.tracking.android.EasyTracker;

import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;
import csiebug.util.StringUtility;
import bible.domain.BibleBook;
import bible.domain.BibleChapter;
import bible.domain.BibleVerse;
import bible.domain.pojoImpl.BibleBookImpl;
import bible.domain.pojoImpl.BibleChapterImpl;
import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.BibleGameService;
import tw.org.spfc.android.service.FacebookService;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.domain.BibleReadingPlan;
import tw.org.spfc.domain.BibleReadingPlanChapterInterval;
import tw.org.spfc.service.BibleService;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.TextView;

public class BibleReadingActivity extends BaseFacebookActivity {
	private BibleService service;
	private SPFCPreference preference;
	private Gallery gallery;
	private BibleReadingPlan plan;
	private TextToSpeech player;
	private PaintView paint;
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		try {
			String[] optionSource1 = getContextMenuOptionsByArray();
			List<String> optionSource2 = getContextMenuOptionsByList();
			Map<Integer, String> optionSource3 = getContextMenuOptionsByMap();
			
			if(optionSource1 != null) {
				for(int i = 0; i < optionSource1.length; i++) {
					menu.add(0, i, 0, optionSource1[i]);
				}
			} else if(optionSource2 != null) {
				for(int i = 0; i < optionSource2.size(); i++) {
					menu.add(0, i, 0, optionSource2.get(i));
				}
			} else if(optionSource3 != null) {
				Iterator<Entry<Integer, String>> iterator = optionSource3.entrySet().iterator();
				while(iterator.hasNext()) {
					Entry<Integer, String> entry = iterator.next();
					menu.add(0, entry.getKey(), 0, entry.getValue());
				}
			}
			
			menu.setHeaderTitle(getContextMenuHeaderTitle());
			menu.setHeaderIcon(getContextMenuHeaderIcon());
		} catch(Exception e) {
			debugLogic(e);
		}
		
		super.onCreateContextMenu(menu, v, menuInfo);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		try {
			contextItemSelectedLogic(item);
		} catch(Exception e) {
			debugLogic(e);
		}
		
		return super.onContextItemSelected(item);
	}
	
	@Override
	protected Map<Integer, String> getContextMenuOptionsByMap() {
		Map<Integer, String> menu = new LinkedHashMap<Integer, String>();
		menu.put(1, getString(R.string.read_note));
		menu.put(2, getString(R.string.bible_map));
		menu.put(3, getString(R.string.bible_person));
		menu.put(4, getString(R.string.bible_reference));
		menu.put(5, getString(R.string.copy_to_clipboard));
		menu.put(6, getString(R.string.note_fhl_3));
		menu.put(7, getString(R.string.note_fhl_4));
		menu.put(8, getString(R.string.note_fhl_whparsing));
		menu.put(9, getString(R.string.note_fhl_strong_number));
		menu.put(10, getString(R.string.writing_enable));
		menu.put(11, getString(R.string.make_golden_word_card));
		
		Session session = Session.getActiveSession();
        boolean enableButtons = (session != null && session.isOpened());
        
        if (enableButtons && getUser() != null) {
        	menu.put(12, getString(R.string.share_to_facebook));
        }
        
        if(isLoggedTwitter()) {
        	menu.put(13, getString(R.string.twitter) + getString(R.string.share_to_twitter));
        }
        
		return menu;
	}
	
	@Override
	protected void contextItemSelectedLogic(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		try {
			BibleChapter bibleChapter = service.getChapter(info.position);
			int book = bibleChapter.getBook().getBookId();
			int chapter = bibleChapter.getChapterId();
			int sec = preference.getBibleReadingReferenceVerse();
			
			Intent fhlNote = new Intent(this, FHLActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("engs", service.getBookFHLAbbrNames().get(book));
			bundle.putInt("chap", chapter);
			bundle.putInt("sec", sec);
			
			Session session = Session.getActiveSession();
	        boolean enableButtons = (session != null && session.isOpened());
	        boolean facebookEnable = enableButtons && getUser() != null;
	        bundle.putBoolean("facebookEnable", facebookEnable);
			
			switch (item.getItemId()) {
				case 1:
					Intent note = new Intent(this, BibleNoteActivity.class);
					bundle.putString("chapterName", bibleChapter.getName());
					bundle.putInt("book", book);
					note.putExtras(bundle);
					startActivity(note);
					break;
				case 2:
					Intent map = new Intent(this, BibleMapActivity.class);
					bundle.putString("chapterName", bibleChapter.getName());
					bundle.putInt("book", book);
					map.putExtras(bundle);
					startActivity(map);
					break;
				case 3:
					Intent person = new Intent(this, BiblePersonListActivity.class);
					bundle.putInt("book", book);
					person.putExtras(bundle);
					startActivity(person);
					break;
				case 6:
					if(!AndroidUtility.isNetworkAvailable(BibleReadingActivity.this)) {
						toast(getString(R.string.network_unavailable));
					} else {
						bundle.putString("book", "3");
						fhlNote.putExtras(bundle);
						startActivity(fhlNote);
					}
					break;
				case 7:
					if(!AndroidUtility.isNetworkAvailable(BibleReadingActivity.this)) {
						toast(getString(R.string.network_unavailable));
					} else {
						bundle.putString("book", "4");
						fhlNote.putExtras(bundle);
						startActivity(fhlNote);
					}
					break;
				case 8:
					if(!AndroidUtility.isNetworkAvailable(BibleReadingActivity.this)) {
						toast(getString(R.string.network_unavailable));
					} else {
						if(book < BibleService.OLD_TESTAMENT_BOOK_COUNT) {
							bundle.putString("book", "parsing");
						} else {
							bundle.putString("book", "whparsing");
						}
						fhlNote.putExtras(bundle);
						startActivity(fhlNote);
					}
					break;
				case 9:
					if(!AndroidUtility.isNetworkAvailable(BibleReadingActivity.this)) {
						toast(getString(R.string.network_unavailable));
					} else {
						int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
						int version = preference.getBibleVersion();
						
						Locale phoneLocale = getResources().getConfiguration().locale;
						
						if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
							language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
						} else {
							language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
						}
						
						try {
							//信望愛那邊chineses這個參數一定要傳中文簡寫,所以這邊需要重新取得BibleService強制使用中文目錄
							service = ServiceProxyFactory.createBibleService(this, version, BibleCatagoryDAOImpl.CHINESE_BIBLE);
							if(version == 1) {
								bundle.putString("version", "kjv");
							} else {
								bundle.putString("version", "unv");
							}
							bundle.putString("chineses", service.getBookAbbrNames().get(book));
							bundle.putInt("book", book);
							Intent fhlStrongNumber = new Intent(this, FHLStrongNumberListActivity.class);
							fhlStrongNumber.putExtras(bundle);
							startActivity(fhlStrongNumber);
						} catch (ServiceException e) {
							throw new RuntimeException(e);
						} finally {
							service = ServiceProxyFactory.createBibleService(this, version, language);
						}
					}
					break;
				case 12:
					String pictureURL = getString(R.string.spfcAppIcon);
					try {
						pictureURL = ServiceProxyFactory.createFacebookService(BibleReadingActivity.this).getPicture(FacebookService.FACEBOOK_MY_GOLDEN_WORD);
					} catch (ServiceException e) {
						Log.e(getClass().getName(), "Get picture URL error!", e);
					}
					
					String url = getString(R.string.spfcHomepage);
					String name = service.getVerse(book, chapter, sec).getVerse();
					
					publishFeedDialog(getString(R.string.facebook_my_golden_word), "", name, url, pictureURL);
					break;
				case 13:
					SPFCPreference preference = new SPFCPreference(BibleReadingActivity.this);
					String oauthAccessToken = preference.getTwitterToken();
					String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

					ConfigurationBuilder confbuilder = new ConfigurationBuilder();
					Configuration conf = confbuilder
										.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
										.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
										.setOAuthAccessToken(oauthAccessToken)
										.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
										.setUseSSL(true)
										.build();
					
					Twitter twitter = new TwitterFactory(conf).getInstance();
					
					name = service.getVerse(book, chapter, sec).getVerse();
					
					try {
						twitter.updateStatus(name);
						toast(getString(R.string.twitter_success));
					} catch (TwitterException e) {
						Log.e(getClass().getName(), "TwitterException", e);
					}
					break;
				case 4:
					if(!AndroidUtility.isNetworkAvailable(BibleReadingActivity.this)) {
						toast(getString(R.string.network_unavailable));
					} else {
						BibleGameService bibleGameService = ServiceProxyFactory.createBibleGameService(this);
				        try {
							JSONArray games = bibleGameService.getBibleGames(book, chapter, sec);
							if(games.length() > 0) {
								JSONObject jsonGame = games.getJSONObject(games.length() - 1);
								
								if(jsonGame.getString("type").equals("Swipe")) {
									Intent game = new Intent(this, BibleGameActivity.class);
									bundle.putInt("book", book);
									String[] images = new String[jsonGame.getJSONArray("images").length()];
									for(int i = 0; i < images.length; i++) {
										images[i] = jsonGame.getJSONArray("images").getString(i);
									}
									bundle.putStringArray("images", images);
									game.putExtras(bundle);
									startActivity(game);
								} else {
									Intent game = new Intent(this, BiblePhotoActivity.class);
									bundle.putInt("book", book);
									bundle.putString("gameCompleteURL", jsonGame.getString("complete"));
									game.putExtras(bundle);
									startActivity(game);
								}
							} else {
								toast(getString(R.string.no_data));
							}
						} catch (Exception e) {
							//輔助功能,不處理exception
							Log.e(getClass().getName(), "Get picture URL error!", e);
							toast(getString(R.string.no_data));
						}
					}
					break;
				case 10:
					toast(getString(R.string.writing_hint));
					LayoutParams params = paint.getLayoutParams();
					params.height = LayoutParams.FILL_PARENT;
					params.width = LayoutParams.FILL_PARENT;
					paint.setLayoutParams(params);
					paint.setExportFileName(service.getBookFHLAbbrNames().get(book) + StringUtility.addZero(chapter, 3) + StringUtility.addZero(sec, 3) + Calendar.getInstance().getTimeInMillis() + ".png");
					paint.setContentView(gallery);
					paint.setFacebookEnable(facebookEnable);
			        
//					int height = AndroidUtility.getResolution(this).getHeight();
					int width = AndroidUtility.getResolution(this).getWidth();
					params = gallery.getLayoutParams();
					
//					if(AndroidUtility.getOrientation(this, params) == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
						params.height = LayoutParams.FILL_PARENT;
						params.width = width / 2;
//					} else {
//						params.height = height / 2 - 96;
//						params.width = LayoutParams.FILL_PARENT;
//					}
					
					gallery.setLayoutParams(params);
					
					break;
				case 11:
					Intent card = new Intent(this, GoldenWordCardActivity.class);
					bundle.putString("chapterName", bibleChapter.getName());
					bundle.putInt("book", book);
					card.putExtras(bundle);
					startActivity(card);
					break;
				case 5:
					EasyTracker.getTracker().sendEvent("copyVerseToClipboard", "use", "", (long)0);
					
					name = service.getVerse(book, chapter, sec).getVerse();
					
					if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
						ClipboardManager cliboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
						cliboard.setText(name);
					} else {
						android.text.ClipboardManager cliboard = (android.text.ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
						cliboard.setText(name);
					}
					break;
				default:
					break;
			}
		} catch(ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		switch (resultCode) {
			case 1:
				refreshListView();
				break;
			case 2:
				setTodayPlan();
				break;
			case 3:
				setReadedButton();
				break;
			default:
				break;
		}
	}
	
	@Override
	protected Integer getOptionMenuLayout() {
		return R.menu.bible_reading_plan_menu;
	}
	
	@Override
	protected void optionsItemSelectedLogic(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.version:
				AlertDialog.Builder builder = new AlertDialog.Builder(BibleReadingActivity.this);
			    
				String[] versionNames = {"AA" + "(Portuguese)", "CUVT" + "(Chinese Traditional)", "CUVTH" + "(加上難字注音)", "CUVS" + "(Chinese Simplified)", "CUVSH" + "(加上难字汉语拼音)", "DN1933" + "(Danish)", "KAR" + "(Hungarian)", "KJV" + "(English)", "LSG" + "(French)", "LUTH1545" + "(German)", "R1933" + "(Finnish)", "RMNN" + "(Romanian)", "RVA" + "(Spanish)", "SV1917" + "(Swedish)"};
				
				builder.setItems(versionNames, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						int version = 0;
						
						if(which == 0) {
							version = 9;
						} else if(which == 1) {
							version = 0;
						} else if(which == 2) {
							version = 15;
						} else if(which == 3) {
							version = 2;
						} else if(which == 4) {
							version = 16;
						} else if(which == 5) {
							version = 3;
						} else if(which == 6) {
							version = 7;
						} else if(which == 7) {
							version = 1;
						} else if(which == 8) {
							version = 5;
						} else if(which == 9) {
							version = 6;
						} else if(which == 10) {
							version = 4;
						} else if(which == 11) {
							version = 10;
						} else if(which == 12) {
							version = 12;
						} else if(which == 13) {
							version = 13;
						}
						
						EasyTracker.getTracker().sendEvent("bibleReadingVersion - " + BibleDAOImpl.getVersionName(version), "use", "", (long)0);
						
						preference.setBibleVersion(version);
						
						setupService();
						
						setAdapters();
					}
			    });
			    builder.create();
			    
			    builder.show();
				break;
			case R.id.vs_version:
				builder = new AlertDialog.Builder(BibleReadingActivity.this);
			    
				String[] vsVersionNames = {getString(R.string.off), "AA" + "(Portuguese)", "CUVT" + "(Chinese Traditional)", "CUVTH" + "(加上難字注音)", "CUVS" + "(Chinese Simplified)", "CUVSH" + "(加上难字汉语拼音)", "DN1933" + "(Danish)", "KAR" + "(Hungarian)", "KJV" + "(English)", "LSG" + "(French)", "LUTH1545" + "(German)", "R1933" + "(Finnish)", "RMNN" + "(Romanian)", "RVA" + "(Spanish)", "SV1917" + "(Swedish)"};
				
				builder.setItems(vsVersionNames, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						int version = 0;
						
						if(which == 0) {
							version = -1;
						} else if(which == 1) {
							version = 9;
						} else if(which == 2) {
							version = 0;
						} else if(which == 3) {
							version = 15;
						} else if(which == 4) {
							version = 2;
						} else if(which == 5) {
							version = 16;
						} else if(which == 6) {
							version = 3;
						} else if(which == 7) {
							version = 7;
						} else if(which == 8) {
							version = 1;
						} else if(which == 9) {
							version = 5;
						} else if(which == 10) {
							version = 6;
						} else if(which == 11) {
							version = 4;
						} else if(which == 12) {
							version = 10;
						} else if(which == 13) {
							version = 12;
						} else if(which == 14) {
							version = 13;
						}
						
						if(version != -1) {
							EasyTracker.getTracker().sendEvent("bibleReadingVSVersion - " + BibleDAOImpl.getVersionName(version), "use", "", (long)0);
						}
						
						preference.setBibleVS((version != -1), version);
						
						setupService();
						
						setAdapters();
					}
			    });
			    builder.create();
			    
			    builder.show();
				break;
			case R.id.query:
				Intent searchKeyword = new Intent(this, BibleSearchActivity.class);
//				startActivity(searchKeyword);
				startActivityForResult(searchKeyword, 1);
				break;
			case R.id.bible_reading_plan:
				Intent bibleReadingPlan = new Intent(this, BibleReadingPlanActivity.class);
				startActivityForResult(bibleReadingPlan, 2);
				break;
			case R.id.bible_reading_style:
				builder = new AlertDialog.Builder(BibleReadingActivity.this);
			    
				String[] styles = {getString(R.string.bible_reading_style_original), getString(R.string.bible_reading_style_bw)};
				
				builder.setItems(styles, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if(which != -1) {
							EasyTracker.getTracker().sendEvent("bibleReadingStyle - " + which, "use", "", (long)0);
						}
						
						preference.setBibleReadingStyle(which);
						
						setupService();
						
						setAdapters();
					}
			    });
			    builder.create();
			    
			    builder.show();
				break;
			default:
				break;
		}
	}

	@Override
	protected void setListeners() {
		findViewById(R.id.menu).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				BibleReadingActivity.this.openOptionsMenu();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		findViewById(R.id.addBookmark).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("addBibleBookmark", "use", "", (long)0);
				preference.addBibleBookMark(preference.getBibleReadingBook(), preference.getBibleReadingChapter());
				toast(getString(R.string.add_success));
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		findViewById(R.id.chooseChapter).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent chooseChapterIntent = new Intent(BibleReadingActivity.this, BibleChapterChooseActivity.class);
				startActivityForResult(chooseChapterIntent, 1);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.readed).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("bibleReadingRecord", "use", "", (long)0);
				
				int book = preference.getBibleReadingBook();
				int chapter = preference.getBibleReadingChapter();
				BibleChapter voBibleChapter = new BibleChapterImpl();
				BibleBook voBibleBook = new BibleBookImpl();
				voBibleBook.setId("" + book);
				voBibleBook.setBookId(book);
				voBibleChapter.setBook(voBibleBook);
				voBibleChapter.setChapterId(chapter);
				
				try {
					//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
					service.readedChapter(null, plan, voBibleChapter);
					Button readed = (Button)findViewById(R.id.readed);
					LayoutParams params = readed.getLayoutParams();
					params.height = 0;
					params.width = 0;
					readed.setLayoutParams(params);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.play).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				if(player.isSpeaking()) {
					//player暫停,把按鈕改成停止狀態
					player.stop();
//					findViewById(R.id.play).setBackgroundResource(android.R.drawable.ic_media_play);
					findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
				} else {
					try {
						readBibleByListening();
					} catch (Exception e) {
						//輔助功能,產生錯誤就不丟出exception了
						Log.e(getClass().getName(), "Play mp3 file error!", e);
						
//						findViewById(R.id.play).setBackgroundResource(android.R.drawable.ic_media_play);
						findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
					}
				}
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
		});
	}
	
	@Override
	protected void setAdapters() {
		int book = preference.getBibleReadingBook();
		int chapter = preference.getBibleReadingChapter();
		
		gallery.setAdapter(new BibleChapterAdapter(this, service, preference));
		
		try {
			gallery.setSelection(service.getChapterIndex(book, chapter));
			registerForContextMenu(gallery);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		((LoginButton) findViewById(R.id.login_button)).setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            public void onUserInfoFetched(GraphUser user) {
            	setUser(user);
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
	}
	
	private void refreshListView() {
		int book = preference.getBibleReadingBook();
		int chapter = preference.getBibleReadingChapter();
		try {
			gallery.setSelection(service.getChapterIndex(book, chapter));
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		setReadedButton();
		setPlayButton(true);
	}
	
	@Override
	protected int getLayout() {
		return R.layout.bible_reading;
	}
	
	@Override
	protected void findViews() {
		gallery = (Gallery)findViewById(R.id.bible_chapter);
		paint = (PaintView)findViewById(R.id.paint_view);
	}
	
	@Override
	protected void onCreateLogic() {
		preference = new SPFCPreference(this);
		
		setupService();
		
		setTodayPlan();
		
		setPlayButton(false);
	}
	
	@Override
	protected void onStopLogic() {
		if(player != null) {
			player.stop();
			player.shutdown();
		}
	}
	
	private void setupService() {
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		int version = preference.getBibleVersion();
		
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//		}
		player = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
			
			public void onInit(int status) {
				if(status != TextToSpeech.ERROR){
					player.setLanguage(Locale.US);
				}
			}
		});
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
			player.setLanguage(Locale.CHINA);
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
			player.setLanguage(Locale.TAIWAN);
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
			player.setLanguage(Locale.FRANCE);
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
			player.setLanguage(Locale.GERMANY);
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
			player.setLanguage(Locale.UK);
		}
		
		try {
			service = ServiceProxyFactory.createBibleService(this, version, language);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void setPlayButton(boolean changeChapter) {
		View play = findViewById(R.id.play);
		LayoutParams params = play.getLayoutParams();
		
		//TODO 先把此功能關掉
//		params.height = LayoutParams.WRAP_CONTENT;
//		params.width = LayoutParams.WRAP_CONTENT;
		params.height = 0;
		params.width = 0;
		
		if(changeChapter && player.isSpeaking()) {
			try {
				readBibleByListening();
			} catch (Exception e) {
				//輔助功能,產生錯誤就不丟出exception了
				Log.e(getClass().getName(), "Listening error!", e);
				params.height = 0;
				params.width = 0;
			}
		}
	}
	
	private void readBibleByListening() throws Exception {
		EasyTracker.getTracker().sendEvent("chapterListening", "play", "", (long)0);
		
		if(!player.isSpeaking()) {
			int book = preference.getBibleReadingBook();
			int chapter = preference.getBibleReadingChapter();
			
			BibleChapter bibleCapter = service.getChapter(book, chapter);
			List<BibleVerse> verses = bibleCapter.getVerses();
			StringBuffer text = new StringBuffer();
			for(int i = 0; i < verses.size(); i++) {
				text.append(verses.get(i).getVerse());
			}
			
			player.speak(text.toString(), TextToSpeech.QUEUE_FLUSH, null);
			
			findViewById(R.id.play).setBackgroundResource(R.drawable.play1_hot);
		} else {
			findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
		}
	}
	
	public void setReadedButton() {
		Button readed = (Button)findViewById(R.id.readed);
		LayoutParams params = readed.getLayoutParams();
		
		if(AssertUtility.isNotNullAndNotSpace(preference.getBibleReadingPlan())) {
			try {
				if(plan != null) {
					int book = preference.getBibleReadingBook();
					int chapter = preference.getBibleReadingChapter();
					BibleChapter voBibleChapter = new BibleChapterImpl();
					BibleBook voBibleBook = new BibleBookImpl();
					voBibleBook.setId("" + book);
					voBibleBook.setBookId(book);
					voBibleChapter.setBook(voBibleBook);
					voBibleChapter.setChapterId(chapter);
					if(plan.isInInterval(voBibleChapter)) {
						//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
						boolean showFlag = !service.isReaded(null, plan, book, chapter);
						
						if(showFlag) {
							params.height = LayoutParams.WRAP_CONTENT;
							params.width = LayoutParams.WRAP_CONTENT;
						} else {
							params.height = 0;
							params.width = 0;
						}
					} else {
						params.height = 0;
						params.width = 0;
					}
				} else {
					params.height = 0;
					params.width = 0;
				}
			} catch (Exception e) {
				//輔助功能,產生錯誤就不丟出exception了
				Log.e(getClass().getName(), "Read plan error!", e);
				params.height = 0;
				params.width = 0;
			}
		} else {
			params.height = 0;
			params.width = 0;
		}
		
		readed.setLayoutParams(params);
	}
	
	private void setTodayPlan() {
		TextView status = (TextView)findViewById(R.id.status);
		LayoutParams statusParams = status.getLayoutParams();
		Button readed = (Button)findViewById(R.id.readed);
		LayoutParams readedParams = readed.getLayoutParams();
		
		if(AssertUtility.isNotNullAndNotSpace(preference.getBibleReadingPlan())) {
			try {
				plan = service.getBibleReadingPlan(preference.getBibleReadingPlan());
				
				if(plan != null) {
					BibleReadingPlanChapterInterval interval = service.getTodayPlan(plan);
					if(interval != null) {
						//產生今日進度
						String verse1 = interval.getStartChapter().getVerses().get(0).getVerse();
						String verse2 = interval.getEndChapter().getVerses().get(0).getVerse();
						
						String startText = verse1.substring(0, verse1.indexOf(":"));
						String endText = verse2.substring(0, verse2.indexOf(":"));
						
						//如果是同一卷書，則省略書名
						StringBuffer prefix = new StringBuffer();
						for(int i = 0; i < startText.length(); i++) {
							try {
								Integer.parseInt(startText.substring(i, i + 1));
								break;
							} catch(Exception e) {
								prefix.append(startText.substring(i, i + 1));
							}
						}
						endText = endText.replaceFirst(prefix.toString(), "");
						
						status.setText(getString(R.string.today_bible_reading_plan) + ": " + startText + " - " + endText);
						statusParams.height = LayoutParams.WRAP_CONTENT;
						
						//產生讀取按鈕
						int book = preference.getBibleReadingBook();
						int chapter = preference.getBibleReadingChapter();
						BibleChapter voBibleChapter = new BibleChapterImpl();
						BibleBook voBibleBook = new BibleBookImpl();
						voBibleBook.setId("" + book);
						voBibleBook.setBookId(book);
						voBibleChapter.setBook(voBibleBook);
						voBibleChapter.setChapterId(chapter);
						if(plan.isInInterval(voBibleChapter)) {
							//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
							boolean showFlag = !service.isReaded(null, plan, book, chapter);
							
							if(showFlag) {
								readedParams.height = LayoutParams.WRAP_CONTENT;
								readedParams.width = LayoutParams.WRAP_CONTENT;
							} else {
								readedParams.height = 0;
								readedParams.width = 0;
							}
						} else {
							readedParams.height = 0;
							readedParams.width = 0;
						}
					}
				} else {
					status.setText("");
					statusParams.height = 0;
					readedParams.height = 0;
					readedParams.width = 0;
				}
			} catch (Exception e) {
				//輔助功能,產生錯誤就不丟出exception了
				Log.e(getClass().getName(), "Read plan error!", e);
				status.setText("");
				statusParams.height = 0;
				readedParams.height = 0;
				readedParams.width = 0;
			}
		} else {
			status.setText("");
			statusParams.height = 0;
			readedParams.height = 0;
			readedParams.width = 0;
		}
		
		status.setLayoutParams(statusParams);
		readed.setLayoutParams(readedParams);
	}

	@Override
	protected void enableUI(GraphUser user) {
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void disableUI() {
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void showPublishResult(GraphObject result,
			FacebookRequestError error) {}

	@Override
	protected String getUserStatusMessage() {
		return null;
	}

	@Override
	protected Bitmap getUserPhoto() {
		return null;
	}
}
