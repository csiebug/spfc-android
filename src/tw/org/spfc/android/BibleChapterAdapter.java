package tw.org.spfc.android;

import java.util.List;

import csiebug.service.ServiceException;
import bible.domain.BibleChapter;
import bible.domain.BibleVerse;
import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.listener.AbstractDefaultScrollListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.service.BibleService;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;

public class BibleChapterAdapter extends BaseAdapter implements SpinnerAdapter {
	private Context context;
	private BibleService service;
	private BibleService vsService;
	private SPFCPreference preference;
	private BibleChapter chapter;
	private BibleChapter vsChapter;
	private BibleChapterListView bibleChapter;
	private String[] verses;
	private Integer[] realIndex;
	
	private int preferenceVerse;
	private boolean firstLoadFlag;
	
	public BibleChapterAdapter(Context context, BibleService service, SPFCPreference preference) {
		this.context = context;
		this.service = service;
		this.preference = preference;
		
		preferenceVerse = preference.getBibleReadingVerse();
		firstLoadFlag = true;
		
		setupService();
	}
	
	private void setupService() {
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		int version = preference.getBibleVSVersion();
		
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//		}
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
		}
		
		try {
			vsService = ServiceProxyFactory.createBibleService(context, version, language);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	public int getCount() {
		return 1189;
	}

	public Object getItem(int position) {
		try {
			chapter = service.getChapter(position);
			return chapter;
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	public long getItemId(int position) {
		return position;
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		chapter = (BibleChapter)getItem(position);
		List<BibleVerse> list = chapter.getVerses();
		
		boolean vsFlag = false;
		if(preference.isBibleVSEnable()) {
			try {
				vsChapter = vsService.getChapter(position);
				List<BibleVerse> vsList = vsChapter.getVerses();
				verses = new String[list.size() + vsList.size()];
				realIndex = new Integer[list.size() + vsList.size()];
				
				int a = 0;
				int b = 0;
				int i = 0;
				while(i < verses.length) {
					if(a < list.size()) {
						verses[i] = list.get(a).getVerse();
						realIndex[i] = a;
						a++;
						i++;
					}
					
					if(b < vsList.size()) {
						verses[i] = vsList.get(b).getVerse();
						realIndex[i] = b;
						b++;
						i++;
					}
				}
				
				vsFlag = true;
				preference.setBibleVS(true, preference.getBibleVSVersion());
			} catch (ServiceException e) {
				//輔助功能，不要讓他當掉
				preference.setBibleVS(false, preference.getBibleVSVersion());
			}
		}
		
		if(!vsFlag) {
			verses = new String[list.size()];
			
			for(int i = 0; i < list.size(); i++) {
				verses[i] = list.get(i).getVerse();
			}
		}
		
		bibleChapter = new BibleChapterListView(context, preference, verses);
		bibleChapter.setCacheColorHint(0);
		bibleChapter.setOnTouchListener(bibleChapter);
		bibleChapter.setOnItemLongClickListener(new AbstractDefaultListItemListener(context) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				if(!preference.isBibleVSEnable()) {
					preference.setBibleReadingReferenceVerse(arg2 + 1);
				} else {
					preference.setBibleReadingReferenceVerse(realIndex[arg2] + 1);
				}
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
		bibleChapter.setOnScrollListener(new AbstractDefaultScrollListener(context) {
			
			@Override
			protected void onScrollStateChangedLogic(AbsListView view, int scrollState) {
				if(!preference.isBibleVSEnable()) {
					preference.setBibleReading(chapter.getBook().getBookId(), chapter.getChapterId(), view.getFirstVisiblePosition() + 1);
				} else {
					preference.setBibleReading(chapter.getBook().getBookId(), chapter.getChapterId(), realIndex[view.getFirstVisiblePosition()] + 1);
				}
			}
			
			@Override
			protected void onScrollLogic(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {}
		});
		
		//表示這是一開始啟動讀經程式,還要幫忙跳到上次讀經的節
		if(firstLoadFlag) {
			if(!preference.isBibleVSEnable()) {
				bibleChapter.setSelection(preferenceVerse - 1);
			} else {
				int index = 0;
				for(int i = 0; i < realIndex.length; i++) {
					if(realIndex[i] == preferenceVerse - 1) {
						index = i;
						break;
					}
				}
				bibleChapter.setSelection(index);
			}
			firstLoadFlag = false;
		} else {
			if(!(preference.getBibleReadingBook() == chapter.getBook().getBookId() &&
			   preference.getBibleReadingChapter() == chapter.getChapterId())) {
				preference.setBibleReading(chapter.getBook().getBookId(), chapter.getChapterId(), preference.getBibleReadingVerse());
			}
			
			if(!preference.isBibleVSEnable()) {
				bibleChapter.setSelection(preference.getBibleReadingVerse() - 1);
			} else {
				int index = 0;
				for(int i = 0; i < realIndex.length; i++) {
					if(realIndex[i] == preference.getBibleReadingVerse() - 1) {
						index = i;
						break;
					}
				}
				bibleChapter.setSelection(index);
			}
		}
		
		((BibleReadingActivity)context).setReadedButton();
		((BibleReadingActivity)context).setPlayButton(true);
		
		return bibleChapter;
	}

}
