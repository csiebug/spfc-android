package tw.org.spfc.android.listener;

import tw.org.spfc.android.R;
import android.content.Context;
import csiebug.android.ContextMethod;
import csiebug.android.listener.AbstractLongClickListener;

public abstract class AbstractDefaultLongClickListener extends AbstractLongClickListener {

	public AbstractDefaultLongClickListener(Context context) {
		super(context);
	}

	@Override
	protected void debugLogic(Exception e, Object[] args) {
		ContextMethod.debugLogic(getContext(), e, args, getContext().getString(R.string.exception), getContext().getString(R.string.send_mail_to_author), getContext().getString(R.string.close), getContext().getString(R.string.authorEmail));
	}
	
	
}
