package tw.org.spfc.android.listener;

import tw.org.spfc.android.R;
import android.content.Context;
import android.os.Handler;
import csiebug.android.ContextMethod;
import csiebug.android.MediaPlayerController;
import csiebug.android.listener.AbstractMediaPlayerListener;

public abstract class AbstractDefaultMediaPlayerListener extends AbstractMediaPlayerListener {

	public AbstractDefaultMediaPlayerListener(Context context,
			MediaPlayerController mp, Handler seekbarHandler,
			Runnable seekbarRunnable) {
		super(context, mp, seekbarHandler, seekbarRunnable);
	}
	
	@Override
	protected void debugLogic(Exception e, Object[] args) {
		ContextMethod.debugLogic(getContext(), e, args, getContext().getString(R.string.exception), getContext().getString(R.string.send_mail_to_author), getContext().getString(R.string.close), getContext().getString(R.string.authorEmail));
	}
}
