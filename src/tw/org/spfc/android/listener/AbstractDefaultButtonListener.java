package tw.org.spfc.android.listener;

import tw.org.spfc.android.BaseProgressDialogRunnable;
import tw.org.spfc.android.R;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Message;
import android.view.View;
import csiebug.android.ContextMethod;
import csiebug.android.listener.AbstractButtonListener;

public abstract class AbstractDefaultButtonListener extends AbstractButtonListener {
	
	public AbstractDefaultButtonListener(Context context) {
		super(context);
	}

	@Override
	protected void debugLogic(Exception e, Object[] args) {
		ContextMethod.debugLogic(getContext(), e, args, getContext().getString(R.string.exception), getContext().getString(R.string.send_mail_to_author), getContext().getString(R.string.close), getContext().getString(R.string.authorEmail));
	}
	
	public void onClick(final DialogInterface dialog, final int which) {
		Thread thread = new Thread(new BaseProgressDialogRunnable(getContext(), null) {
			
			@Override
			protected void runLogic() {
				sendEmptyMessage(0);
			}
			
			@Override
			protected void handlerLogic(Message msg) {
				try {
					onClickLogic(dialog, which);
				} catch(Exception e) {
					debugLogic(e);
				}
			}
		});
		
		thread.start();
	}
	
	public void onClick(final View v) {
		Thread thread = new Thread(new BaseProgressDialogRunnable(getContext(), null) {
			
			@Override
			protected void runLogic() {
				sendEmptyMessage(0);
			}
			
			@Override
			protected void handlerLogic(Message msg) {
				try {
					onClickLogic(v);
				} catch(Exception e) {
					debugLogic(e);
				}
			}
		});
		
		thread.start();
	}
}
