package tw.org.spfc.android;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import csiebug.service.ServiceException;
import tw.org.spfc.android.service.TrainingService;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class SundaySchoolActivity extends BaseListActivity {
	private TrainingService service;
	private List<tw.org.spfc.domain.Class> classes = new ArrayList<tw.org.spfc.domain.Class>(); 
	
	@Override
	protected Spanned[] getListItemSpanneds() {
		return null;
	}

	@Override
	protected String[] getListItemTexts() {
		String[] items = new String[classes.size()];
		
		for(int i = 0; i < classes.size(); i++) {
			items[i] = classes.get(i).getName();
		}
		
		return items;
	}

	@Override
	protected void contextItemSelectedLogic(MenuItem arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected int getContextMenuHeaderIcon() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected String getContextMenuHeaderTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String[] getContextMenuOptionsByArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected List<String> getContextMenuOptionsByList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Map<Integer, String> getContextMenuOptionsByMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?> getListItemEditActivityClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected int getRequestCode() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected void onActivityResultLogic(int arg0, int arg1, Intent arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void putListItemToBundle(Bundle arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setListeners() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected Integer getLayout() {
		return R.layout.result_list;
	}
	
	@Override
	protected Integer getListItemTextViewID() {
		return R.id.itemText;
	}
	
	@Override
	protected void onCreateLogic() {
		((TextView)findViewById(R.id.function_name)).setText(R.string.sunday_school);
		
		SPFCPreference preference = new SPFCPreference(this);
		
		if(preference.isSPFCAvailable()) {
			try {
				service = ServiceProxyFactory.createTrainingService(this);
				classes = service.getClasses(Integer.MAX_VALUE);
			} catch (ServiceException e) {
				//throw new RuntimeException(e);
				Log.e(getClass().getName(), "Get class error!", e);
				classes = new ArrayList<tw.org.spfc.domain.Class>();
				toast(getString(R.string.spfc_unavailable));
			}
		} else {
			classes = new ArrayList<tw.org.spfc.domain.Class>();
			toast(getString(R.string.spfc_unavailable));
		}
	}
	
	@Override
	protected void onListItemClickLogic(ListView l, View v, final int position,
			long id) {
		Thread thread = new Thread(new BaseProgressDialogRunnable(this, null) {
			
			@Override
			protected void runLogic() {
				try {
					tw.org.spfc.domain.Class clazz = service.getClassDetail(classes.get(position));
					
					ArrayList<String> images = new ArrayList<String>();
					if(!clazz.getReferences().get(3).trim().equals("")) {
						images = (ArrayList<String>)service.getClassPPT(clazz);
					}
					
					Intent intent = new Intent(SundaySchoolActivity.this, ClassDetailActivity.class);
					Bundle bundle = new Bundle();
					bundle.putString("name", clazz.getName());
					bundle.putString("referenceLink", clazz.getReferences().get(0));
					bundle.putString("htmlPart", clazz.getReferences().get(1));
					bundle.putString("audioURL", clazz.getReferences().get(2));
					bundle.putStringArrayList("ppt", images);
					intent.putExtras(bundle);
					startActivity(intent);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}

			@Override
			protected String getTitle() {
				return getString(R.string.loading);
			}
		});
		
		thread.start();
	}
}
