package tw.org.spfc.android.service;

import csiebug.service.ServiceException;

public interface RobotService {
	/**
	 * 與亞比該說話
	 * @param message
	 * @return
	 * @throws ServiceException
	 */
	String talk(String message) throws ServiceException;
}
