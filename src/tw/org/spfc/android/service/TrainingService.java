package tw.org.spfc.android.service;

import java.util.List;

import csiebug.service.ServiceException;

import tw.org.spfc.domain.Class;

public interface TrainingService extends tw.org.spfc.service.TrainingService {
	/**
	 * 取得班級列表
	 * @param maxLength
	 * @return
	 * @throws ServiceException
	 */
	List<Class> getClasses(int maxLength) throws ServiceException;
	
	/**
	 * 搜尋班級
	 * @param keywords
	 * @return
	 * @throws ServiceException
	 */
	List<Class> searchClasses(List<String> keywords) throws ServiceException;
	
	/**
	 * 取得此班的細部資料,放入reference裡面
	 * @param clazz
	 * @return
	 * @throws ServiceException
	 */
	Class getClassDetail(Class clazz) throws ServiceException;
	
	/**
	 * 取得此班的投影片,放入reference裡面
	 * @param clazz
	 * @return
	 * @throws ServiceException
	 */
	List<String> getClassPPT(Class clazz) throws ServiceException;
}
