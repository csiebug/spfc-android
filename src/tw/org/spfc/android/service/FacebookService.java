package tw.org.spfc.android.service;

import csiebug.service.ServiceException;

public interface FacebookService {
	final static String FACEBOOK_MY_GOLDEN_WORD = "FACEBOOK_MY_GOLDEN_WORD";
	final static String FACEBOOK_MY_BIBLE_NOTE = "FACEBOOK_MY_BIBLE_NOTE";
	final static String FACEBOOK_MY_BIBLE_MAP_NOTE = "FACEBOOK_MY_BIBLE_MAP_NOTE";
	final static String FACEBOOK_MY_FAVOR_BOOK = "FACEBOOK_MY_FAVOR_BOOK";
	final static String FACEBOOK_MY_FAVOR_SERMON = "FACEBOOK_MY_FAVOR_SERMON";
	final static String FACEBOOK_MY_FAVOR_CLASS = "FACEBOOK_MY_FAVOR_CLASS";
	final static String FACEBOOK_MY_BIBLE_READING = "FACEBOOK_MY_BIBLE_READING";
	final static String FACEBOOK_MY_BIBLE_READING_START = "FACEBOOK_MY_BIBLE_READING_START";
	final static String FACEBOOK_MY_BIBLE_READING_COMPELETE = "FACEBOOK_MY_BIBLE_READING_COMPELETE";
	final static String FACEBOOK_MY_BIBLE_READING_TODAY = "FACEBOOK_MY_BIBLE_READING_TODAY";
	final static String FACEBOOK_SPFC_NEWS = "FACEBOOK_SPFC_NEWS";
	final static String FACEBOOK_SHARE_IOS_APP = "FACEBOOK_SHARE_IOS_APP";
	final static String FACEBOOK_SHARE_ANDROID_APP = "FACEBOOK_SHARE_ANDROID_APP";
	
	/**
	 * 取得facebook對應的縮圖
	 * @param facebookType
	 * @return
	 * @throws ServiceException
	 */
	String getPicture(String facebookType) throws ServiceException;
}
