package tw.org.spfc.android.service;

import org.json.JSONArray;

import csiebug.service.ServiceException;

public interface BibleGameService {
	/**
	 * 取得遊戲
	 * @param book
	 * @param chapter
	 * @param verse
	 * @return
	 * @throws ServiceException
	 */
	JSONArray getBibleGames(int book, int  chapter, int verse) throws ServiceException;
}
