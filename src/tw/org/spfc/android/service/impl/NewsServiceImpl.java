package tw.org.spfc.android.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.service.ServiceException;
import tw.org.spfc.android.R;
import tw.org.spfc.android.service.NewsService;
import tw.org.spfc.domain.News;
import tw.org.spfc.domain.pojoImpl.NewsImpl;

public class NewsServiceImpl extends BaseServiceImpl implements NewsService {
	private Context context;
	
	public NewsServiceImpl(Context context) {
		this.context = context;
	}
	
	public List<News> getNews() throws ServiceException {
		//TODO: 教會網站改版時必須修改
		List<News> news = new ArrayList<News>();
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String homeURL = context.getString(R.string.spfcHomepage);
		String[] htmlLines = doPostForTextContent(homeURL, params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout))).split("\n");
		boolean addFlag = false;
		for(int i = 0; i < htmlLines.length; i++) {
			if(addFlag) {
				News message = new NewsImpl();
				message.setContent(htmlLines[i]);
				news.add(message);
			}
			
			if(htmlLines[i].indexOf("<div") != -1 && htmlLines[i].indexOf("vmarquee") != -1) {
				addFlag = true;
			}
			
			if(addFlag == true && htmlLines[i].startsWith("</div>")) {
				addFlag = false;
			}
		}
		
		return news;
	}

	public boolean postNews(News message) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public List<News> getPromotedNews() throws ServiceException {
		// TODO description目前放的是facebook所用的縮圖
		List<News> list = new ArrayList<News>();
		
		String json = doPostForJSON(context.getString(R.string.promotedNewsURL), null, Integer.parseInt(context.getString(R.string.httpTimeout)));
		
		try {
			JSONObject jsonResult = new JSONObject(json);
			JSONArray jsonPromotedNews = jsonResult.getJSONArray("news");
			JSONArray jsonFacebookPictureURLs = jsonResult.getJSONArray("facebookPictureURLs");
			for(int i = 0; i < jsonPromotedNews.length(); i++) {
				News news = new NewsImpl();
				news.setContent(jsonPromotedNews.getJSONObject(i).getString("content"));
				news.setURL(jsonPromotedNews.getJSONObject(i).getString("URL"));
				news.setDescription(jsonFacebookPictureURLs.getString(i));
				list.add(news);
			}
		} catch (JSONException e) {
			throw new ServiceException(e);
		}
		
		return list;
	}

	public List<News> getEmergencyNews() throws ServiceException {
		List<News> list = new ArrayList<News>();
		
		String json = doPostForJSON(context.getString(R.string.emergencyNewsURL), null, Integer.parseInt(context.getString(R.string.httpTimeout)));
		
		try {
			JSONObject jsonResult = new JSONObject(json);
			JSONArray jsonNews = jsonResult.getJSONArray("news");
			for(int i = 0; i < jsonNews.length(); i++) {
				News news = new NewsImpl();
				news.setContent(new String(jsonNews.getJSONObject(i).getString("content").getBytes("ISO-8859-1"), "UTF-8"));
				list.add(news);
			}
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		
		return list;
	}
}
