package tw.org.spfc.android.service.impl;

import tw.org.spfc.android.persistence.impl.BibleReadingRecordDAOImpl;
import tw.org.spfc.android.service.BibleGameService;
import tw.org.spfc.android.service.FacebookService;
import tw.org.spfc.android.service.NewsService;
import tw.org.spfc.android.service.RobotService;
import tw.org.spfc.android.service.TrainingService;
import tw.org.spfc.persistence.BibleReadingRecordDAO;
import tw.org.spfc.service.BibleService;
import tw.org.spfc.service.LibraryService;
import tw.org.spfc.service.PhotoService;
import tw.org.spfc.service.SermonService;
import csiebug.service.VersionControlService;
import android.content.Context;
import csiebug.android.aop.ExceptionHandler;
import csiebug.aop.AuthorizationHandler;
import csiebug.aop.TransactionHandler;
import csiebug.domain.User;
import csiebug.service.ServiceException;


/**
 * 用此建立Service的Proxy物件
 * @author George_Tsai
 *
 */
public class ServiceProxyFactory {
	public static VersionControlService createVersionControlService(Context context) {
		return (VersionControlService)new ExceptionHandler(context).createProxy(new VersionControlServiceImpl(context));
	}
	
	public static SermonService createSermonService(Context context) {
		return (SermonService)new ExceptionHandler(context).createProxy(new SermonServiceImpl(context));
	}
	
	public static LibraryService createLibraryService(Context context) {
		return (LibraryService)new ExceptionHandler(context).createProxy(new LibraryServiceImpl(context));
	}
	
	public static BibleService createBibleService(Context context, int version, int language) throws ServiceException {
		BibleReadingRecordDAO dao = new BibleReadingRecordDAOImpl(context);
		BibleServiceImpl service = new BibleServiceImpl(context, version, language);
		service.setBibleReadingRecordDAO((BibleReadingRecordDAO)new TransactionHandler(dao).createProxy(dao));
		return (BibleService)new ExceptionHandler(context).createProxy(service);
	}
	
	public static PhotoService createPhotoService(Context context) {
		return (PhotoService)new ExceptionHandler(context).createProxy(new PhotoServiceImpl(context));
	}
	
	public static NewsService createNewsService(Context context, User user, String sessionId) {
		return (NewsService)new ExceptionHandler(context).createProxy(new AuthorizationHandler(sessionId, new AuthorizationServiceImpl()).createProxy(new NewsServiceImpl(context)));
	}
	
	public static FacebookService createFacebookService(Context context) {
		return (FacebookService)new ExceptionHandler(context).createProxy(new FacebookServiceImpl(context));
	}
	
	public static TrainingService createTrainingService(Context context) {
		return (TrainingService)new ExceptionHandler(context).createProxy(new TrainingServiceImpl(context));
	}
	
	public static BibleGameService createBibleGameService(Context context) {
		return (BibleGameService)new ExceptionHandler(context).createProxy(new BibleGameServiceImpl(context));
	}
	
	public static RobotService createRobotService(Context context) {
		return (RobotService)new ExceptionHandler(context).createProxy(new RobotServiceImpl(context));
	}
}
