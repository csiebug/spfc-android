package tw.org.spfc.android.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.service.ServiceException;
import tw.org.spfc.android.R;
import tw.org.spfc.domain.Book;
import tw.org.spfc.domain.pojoImpl.BookImpl;
import tw.org.spfc.service.LibraryService;

public class LibraryServiceImpl extends BaseServiceImpl implements LibraryService {
	private Context context;
	
	public LibraryServiceImpl(Context context) {
		this.context = context;
	}
	
	public List<Book> queryBook(Book valueObject) throws ServiceException {
		List<Book> list = new ArrayList<Book>();
		
		String html = doGetForTextContent(context.getString(R.string.libraryQueryURL) + valueObject.getName(), null, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout)));
		String[] htmlLines = html.split("\n");
		for(int i = 0; i < htmlLines.length; i++) {
			if(htmlLines[i].startsWith("<li><a href=")) {
				String[] bookDatas = htmlLines[i].split("\">");
				String link = context.getString(R.string.libraryHostURL) + bookDatas[0].replace("<li><a href=\"", "");
				String name = bookDatas[1].replace("</a></li>", "");
				
				Book book = new BookImpl();
				book.setName(name);
				book.setReferenceLink(link);
				list.add(book);
			} else if(htmlLines[i].startsWith("<div class=\"pagination\">")) {
				//表示資料太多有分頁,不能把全部資料抓回來,免得loading太大,必須提示使用者縮小查詢範圍
				//故意塞最後一筆資料為null來作此判斷
				list.add(null);
			}
		}
		
		return list;
	}

	public Book getBookDetail(Book valueObject) throws ServiceException {
		Book book = new BookImpl();
		
		String html = doGetForTextContent(valueObject.getReferenceLink(), null, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout)));
		String[] htmlLines = html.split("\n");
		for(int i = 0; i < htmlLines.length; i++) {
			if(htmlLines[i].startsWith("<li>作者：")) {
				book.setAuthorName(htmlLines[i].replace("<li>作者：", "").replace("</li>", ""));
			}
			
			if(htmlLines[i].startsWith("<li>出版：")) {
				book.setPublisherName(htmlLines[i].replace("<li>出版：", "").replace("</li>", ""));
			}
			
			if(htmlLines[i].startsWith("<li>ISBN：")) {
				book.setId(htmlLines[i].replace("<li>ISBN：", "").replace("</li>", ""));
			}
			
			if(htmlLines[i].startsWith("<li>類別：")) {
				book.setClassName(htmlLines[i].split("\">")[1].replace("</a></li>", ""));
			}
		}
		
		return book;
	}

	public List<Book> getNewBooks() throws ServiceException {
		// TODO 待實作,目前先取得spfc-service的資料
		List<Book> list = new ArrayList<Book>();
		
		String json = doPostForJSON(context.getString(R.string.libraryNewBooksURL), null, Integer.parseInt(context.getString(R.string.httpTimeout)));
		
		try {
			JSONObject jsonResult = new JSONObject(json);
			JSONArray jsonNewBooks = jsonResult.getJSONArray("result");
			for(int i = 0; i < jsonNewBooks.length(); i++) {
				Book tempBook = new BookImpl();
				tempBook.setCoverLink(jsonNewBooks.getJSONObject(i).getString("coverLink"));
				
				String description = jsonNewBooks.getJSONObject(i).getString("description");
				tempBook.setDescription(new String(description.getBytes("ISO-8859-1"), "UTF-8"));
				list.add(tempBook);
			}
		} catch (JSONException e) {
			throw new ServiceException(e);
		} catch (UnsupportedEncodingException e) {
			throw new ServiceException(e);
		}
		
//		// TODO 待實作,目前先取得以琳書房首頁的書籍來替代
//		String html = doGetForTextContent(context.getString(R.string.libraryNewBooksURL), null, "Big5", Integer.parseInt(context.getString(R.string.httpTimeout)));
//		String[] htmlLines = html.split("\n");
//		Book tempBook = new BookImpl();
//		StringBuffer tempDescription = new StringBuffer();
//		for(int i = 0; i < htmlLines.length; i++) {
//			if(htmlLines[i].indexOf("#FFFFFF") != -1 && htmlLines[i].indexOf("shop.php?html=category8_2&lefthtml=left_category1&Fid=17023&Aid=") != -1) {
//				tempBook = new BookImpl();
//				tempBook.setCoverLink(htmlLines[i].substring(htmlLines[i].indexOf("src=") + 5, htmlLines[i].indexOf("   /></a></td>") - 1));
//				
//				boolean flag = htmlLines[i + 6].indexOf("</td>") != -1;
//				tempDescription.append(replaceCharacter(htmlLines[i + 6].substring(htmlLines[i + 6].indexOf(">") + 1, htmlLines[i + 6].length())));
//				i = i + 6;
//				
//				if(flag) {
//					tempBook.setDescription(tempDescription.toString());
//					list.add(tempBook);
//					tempDescription = new StringBuffer();
//				}
//			} else if(!tempDescription.toString().equals("")) {
//				tempDescription.append(replaceCharacter(htmlLines[i]));
//				
//				if(htmlLines[i].indexOf("</td>") != -1) {
//					tempBook.setDescription(tempDescription.toString());
//					list.add(tempBook);
//					tempDescription = new StringBuffer();
//				}
//			}
//		}
		
		return list;
	}
	
//	private String replaceCharacter(String html) {
//		return html.replace("<span class=\"explain\" align=\"left\">", "").replace("</span>", "").replaceAll("&hellip;", "...").replace("<br />", "\n").replace("<p>", "").replace("</p>", "").replace("</td>", "");
//	};
}
