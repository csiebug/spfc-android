package tw.org.spfc.android.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.persistence.DAOException;
import csiebug.service.ServiceException;
import csiebug.util.DateFormatUtility;
import csiebug.util.StringUtility;
import bible.domain.BibleBook;
import bible.domain.BibleChapter;
import bible.domain.BibleLocation;
import bible.domain.BiblePerson;
import bible.domain.BibleVerse;
import bible.domain.BibleWord;
import bible.domain.StrongNumber;
import bible.domain.pojoImpl.BibleBookImpl;
import bible.domain.pojoImpl.BibleChapterImpl;
import bible.domain.pojoImpl.BibleVerseImpl;
import bible.domain.pojoImpl.BibleWordImpl;
import bible.domain.pojoImpl.StrongNumberImpl;
import bible.persistence.BibleCatagoryDAO;
import bible.persistence.BibleDAO;
import bible.persistence.BibleLocationDAO;
import bible.persistence.BiblePersonDAO;
import tw.org.spfc.android.R;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleLocationDAOImpl;
import tw.org.spfc.android.persistence.impl.BiblePersonDAOImpl;
import tw.org.spfc.domain.BibleBookMark;
import tw.org.spfc.domain.BibleReadingPlan;
import tw.org.spfc.domain.BibleReadingPlanChapterInterval;
import tw.org.spfc.domain.BibleSearchResult;
import tw.org.spfc.domain.TimeSchedule;
import tw.org.spfc.domain.User;
import tw.org.spfc.domain.pojoImpl.BibleBookMarkImpl;
import tw.org.spfc.domain.pojoImpl.BibleReadingPlanChapterIntervalImpl;
import tw.org.spfc.domain.pojoImpl.BibleReadingPlanImpl;
import tw.org.spfc.domain.pojoImpl.BibleSearchResultImpl;
import tw.org.spfc.persistence.BibleReadingRecordDAO;
import tw.org.spfc.service.BibleService;

public class BibleServiceImpl extends BaseServiceImpl implements BibleService {
	private BibleDAO bibleDAO;
	private BibleCatagoryDAO bibleCatagoryDAO;
	private BibleLocationDAO bibleLocationDAO;
	private BiblePersonDAO biblePersonDAO;
	private BibleReadingRecordDAO bibleReadingRecordDAO;
	private Context context;
	private int version;
	
	public BibleServiceImpl(Context context, int version, int language) throws ServiceException {
		try {
			this.bibleDAO = new BibleDAOImpl(context, version);
			this.bibleCatagoryDAO = new BibleCatagoryDAOImpl(context, language);
			this.bibleLocationDAO = new BibleLocationDAOImpl(context);
			this.biblePersonDAO = new BiblePersonDAOImpl(context);
			this.context = context;
			this.version = version;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public void setBibleReadingRecordDAO(BibleReadingRecordDAO dao) {
		this.bibleReadingRecordDAO = dao;
	}
	
	public int getChapterIndex(int book, int chapter) throws ServiceException {
		if(isValidChapter(book, chapter)) {
			return bookChapters[book] + chapter - 1;
		} else {
			Log.d(getId(), "book = " + book + "; chapter = " + chapter);
			throw new ServiceException("Chapter not found!");
		}
	}
	
	public boolean isValidChapter(BibleChapter bibleChapter) {
		return isValidChapter(bibleChapter.getBook().getBookId(), bibleChapter.getChapterId());
	}
	
	public boolean isValidChapter(int book, int chapter) {
		if(book < 0 || book > (BOOK_COUNT - 1)) {
			return false;
		}
		
		if(chapter <= 0) {
			return false;
		}
		
		int baseChapterIndex = bookChapters[book];
		int nextBaseChapterIndex = bookChapters[book + 1];
		
		return ((baseChapterIndex + chapter - 1) < nextBaseChapterIndex);
	}
	
	/**
	 * 取得從聖經第一章第一節(0開始)累計的節數
	 * @param book
	 * @param chapter
	 * @param verse
	 * @return
	 * @throws ServiceException
	 */
	private int getVerseIndex(int book, int chapter, int verse) throws ServiceException {
		if(isValidVerse(book, chapter, verse)) {
			try {
				return bibleDAO.getChapterStartVerseIndex()[getChapterIndex(book, chapter)] + verse - 1;
			} catch (DAOException e) {
				Log.d(getId(), "book = " + book + "; chapter = " + chapter + "; verse = " + verse);
				throw new ServiceException("Verse not found!", e);
			}
		} else {
			Log.d(getId(), "book = " + book + "; chapter = " + chapter + "; verse = " + verse);
			throw new ServiceException("Verse not found!");
		}
	}
	
	public boolean isValidVerse(int book, int chapter, int verse) throws ServiceException {
		if(!isValidChapter(book, chapter)) {
			return false;
		}
		
		if(verse <= 0) {
			return false;
		}
		
		int chapterIndex = getChapterIndex(book, chapter);
		
		try {
			int baseVerseIndex = bibleDAO.getChapterStartVerseIndex()[chapterIndex];
			int nextBaseVerseIndex = bibleDAO.getChapterStartVerseIndex()[chapterIndex + 1];
			
			return ((baseVerseIndex + verse - 1) < nextBaseVerseIndex);
		} catch(DAOException e) {
			Log.d(getId(), "book = " + book + "; chapter = " + chapter + "; verse = " + verse);
			throw new ServiceException(e);
		}
	}
	
	public int getBookChapters(int book) throws ServiceException {
		return bookChapters[book + 1] - bookChapters[book];
	}

	public int getChapterVerses(int book, int chapter) throws ServiceException {
		if(isValidChapter(book, chapter)) {
			int chapterIndex = getChapterIndex(book, chapter);
			try {
				return bibleDAO.getChapterStartVerseIndex()[chapterIndex + 1] - bibleDAO.getChapterStartVerseIndex()[chapterIndex];
			} catch (DAOException e) {
				Log.d(getId(), "book = " + book + "; chapter = " + chapter);
				throw new ServiceException("Chapter not found!", e);
			}
		} else {
			Log.d(getId(), "book = " + book + "; chapter = " + chapter);
			throw new ServiceException("Chapter not found!");
		}
	}
	
	public BibleVerse getVerse(int book, int chapter, int verse) throws ServiceException {
		try {
			BibleBook voBook = new BibleBookImpl();
			voBook.setBookId(book);
			BibleChapter voChapter = new BibleChapterImpl();
			voChapter.setChapterId(chapter);
			voChapter.setBook(voBook);
			
			BibleVerse voVerse = bibleDAO.getVerse(getVerseIndex(book, chapter, verse));
			voVerse.setVerseId(verse);
			voVerse.setChapter(voChapter);
			
			return voVerse;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public BibleChapter getChapter(int chapterIndex) throws ServiceException {
		for(int i = 0; i < bookChapters.length; i++) {
			if(chapterIndex < bookChapters[i]) {
				return getChapter(i - 1, chapterIndex - bookChapters[i - 1] + 1);
			}
		}
		
		Log.d(getId(), "chapterIndex = " + chapterIndex);
		throw new ServiceException("Chapter not found!");
	}
	
	public BibleChapter getChapter(int book, int chapter) throws ServiceException {
		try {
			int startIndex = getVerseIndex(book, chapter, 1);
			int endIndex = startIndex + getChapterVerses(book, chapter);
			
			BibleChapter bibleChapter = new BibleChapterImpl();
			bibleChapter.setId(StringUtility.addZero(book, 2) + StringUtility.addZero(chapter, 3));
			bibleChapter.setName(bibleCatagoryDAO.getBookName(book) + chapter);
			BibleBook bibleBook = new BibleBookImpl();
			bibleBook.setId("" + book);
			bibleBook.setBookId(book);
			bibleChapter.setBook(bibleBook);
			bibleChapter.setChapterId(chapter);
			bibleChapter.setVerses(bibleDAO.getVerses(startIndex, endIndex));
			
			return bibleChapter;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * 傳入整本聖經的經文index，取得它是第幾卷書的第幾章第幾節
	 * @param index
	 * @return
	 * @throws ServiceException
	 */
	private Integer[] getBookChapterVerseIndex(int index) throws ServiceException {
		Integer[] result = new Integer[3];
		int book = 0;
		int chapter = 1;
		int verse = 1;
		
		for(int i = 0; i < BOOK_COUNT; i++) {
			if(i == BOOK_COUNT - 1 || index < getVerseIndex(i + 1, 1, 1)) {
				book = i;
				break;
			}
		}
		
		int chapterCount = getBookChapters(book);
		for(int i = 1; i <= chapterCount; i++) {
			if(i == chapterCount || index < getVerseIndex(book, i + 1, 1)) {
				chapter = i;
				break;
			}
		}
		
		int verseCount = getChapterVerses(book, chapter);
		for(int i = 1; i <= verseCount; i++) {
			if(index == getVerseIndex(book, chapter, i)) {
				verse = i;
				break;
			}
		}
		
		result[0] = book;
		result[1] = chapter;
		result[2] = verse;
		
		return result;
	}
	
	private BibleSearchResult searchVersesWithIndex(List<String> keywords, int startIndex, int endIndex) throws ServiceException {
		try {
			List<BibleVerse> verses = new ArrayList<BibleVerse>();
			
			if(version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
				bibleDAO.setDatasource(BibleDAOImpl.CHINESE_UNION_VERSION);
			} else if(version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
				bibleDAO.setDatasource(BibleDAOImpl.CHINA_UNION_VERSION);;
			}
			List<BibleVerse> holeBible = bibleDAO.getVerses(startIndex, endIndex);
			for(int i = 0; i < holeBible.size(); i++) {
				boolean addFlag = true;
				for(int j = 0; j < keywords.size(); j++) {
					if(holeBible.get(i).getVerse().indexOf(keywords.get(j)) == -1) {
						addFlag = false;
						break;
					}
				}
				
				if(addFlag) {
					Integer[] index = getBookChapterVerseIndex(startIndex + i);
					BibleBook book = new BibleBookImpl();
					book.setBookId(index[0]);
					
					BibleChapter chapter = new BibleChapterImpl();
					chapter.setBook(book);
					chapter.setChapterId(index[1]);
					
					BibleVerse verse = holeBible.get(i);
					verse.setChapter(chapter);
					verse.setVerseId(index[2]);
					
					verses.add(verse);
				}
			}
			if(version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
				bibleDAO.setDatasource(version);;
			}
			
			BibleSearchResult result = new BibleSearchResultImpl();
			StringBuffer keyword = new StringBuffer();
			for(int i = 0; i < keywords.size(); i++) {
				if(i != 0) {
					keyword.append(" ");
				}
				keyword.append(keywords.get(i));
			}
			result.setKeyword(keyword.toString());
			result.setVerses(verses);
			
			return result;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public BibleSearchResult searchVerses(String keyword) throws ServiceException {
		int startIndex = getVerseIndex(0, 1, 1);
		int lastBookChapters = getBookChapters(BOOK_COUNT - 1);
		int endIndex = getVerseIndex(BOOK_COUNT - 1, lastBookChapters, getChapterVerses(BOOK_COUNT - 1, lastBookChapters));
		List<String> keywords = new ArrayList<String>();
		keywords.add(keyword);
		
		return searchVersesWithIndex(keywords, startIndex, endIndex);
	}

	public BibleSearchResult searchVerses(String keyword, int testament) throws ServiceException {
		switch (testament) {
			case 0:
				int startIndex = getVerseIndex(0, 1, 1);
				int lastBookChapters = getBookChapters(OLD_TESTAMENT_BOOK_COUNT - 1);
				int endIndex = getVerseIndex(OLD_TESTAMENT_BOOK_COUNT - 1, lastBookChapters, getChapterVerses(OLD_TESTAMENT_BOOK_COUNT - 1, lastBookChapters));
				List<String> keywords = new ArrayList<String>();
				keywords.add(keyword);
				
				return searchVersesWithIndex(keywords, startIndex, endIndex);
			case 1:
				startIndex = getVerseIndex(OLD_TESTAMENT_BOOK_COUNT, 1, 1);
				lastBookChapters = getBookChapters(BOOK_COUNT - 1);
				endIndex = getVerseIndex(BOOK_COUNT - 1, lastBookChapters, getChapterVerses(BOOK_COUNT - 1, lastBookChapters));
				keywords = new ArrayList<String>();
				keywords.add(keyword);
				
				return searchVersesWithIndex(keywords, startIndex, endIndex);
			default:
				return null;
		}
	}

	public BibleSearchResult searchVerses(String keyword, int testament, int book) throws ServiceException {
		int startIndex = getVerseIndex(book, 1, 1);
		int bookChapters = getBookChapters(book);
		int endIndex = getVerseIndex(book, bookChapters, getChapterVerses(book, bookChapters));
		List<String> keywords = new ArrayList<String>();
		keywords.add(keyword);
		
		return searchVersesWithIndex(keywords, startIndex, endIndex);
	}

	public BibleSearchResult searchVerses(String keyword, int testament, int book,
			int chapter) throws ServiceException {
		int startIndex = getVerseIndex(book, chapter, 1);
		int endIndex = getVerseIndex(book, chapter, getChapterVerses(book, chapter));
		List<String> keywords = new ArrayList<String>();
		keywords.add(keyword);
		
		return searchVersesWithIndex(keywords, startIndex, endIndex);
	}
	
	public BibleSearchResult searchVerses(String keyword, int startBook, int startChapter, int endBook, int endChapter) throws ServiceException {
		int startIndex = getVerseIndex(startBook, startChapter, 1);
		int endIndex = getVerseIndex(endBook, endChapter, getChapterVerses(endBook, endChapter));
		List<String> keywords = new ArrayList<String>();
		keywords.add(keyword);
		
		return searchVersesWithIndex(keywords, startIndex, endIndex);
	}
	
	public BibleSearchResult searchVerses(List<String> keywords)
			throws ServiceException {
		int startIndex = getVerseIndex(0, 1, 1);
		int lastBookChapters = getBookChapters(BOOK_COUNT - 1);
		int endIndex = getVerseIndex(BOOK_COUNT - 1, lastBookChapters, getChapterVerses(BOOK_COUNT - 1, lastBookChapters));
		
		return searchVersesWithIndex(keywords, startIndex, endIndex);
	}

	public BibleSearchResult searchVerses(List<String> keywords, int testament)
			throws ServiceException {
		switch (testament) {
			case 0:
				int startIndex = getVerseIndex(0, 1, 1);
				int lastBookChapters = getBookChapters(OLD_TESTAMENT_BOOK_COUNT - 1);
				int endIndex = getVerseIndex(OLD_TESTAMENT_BOOK_COUNT - 1, lastBookChapters, getChapterVerses(OLD_TESTAMENT_BOOK_COUNT - 1, lastBookChapters));

				return searchVersesWithIndex(keywords, startIndex, endIndex);
			case 1:
				startIndex = getVerseIndex(OLD_TESTAMENT_BOOK_COUNT, 1, 1);
				lastBookChapters = getBookChapters(BOOK_COUNT - 1);
				endIndex = getVerseIndex(BOOK_COUNT - 1, lastBookChapters, getChapterVerses(BOOK_COUNT - 1, lastBookChapters));

				return searchVersesWithIndex(keywords, startIndex, endIndex);
			default:
				return null;
		}
	}

	public BibleSearchResult searchVerses(List<String> keywords, int testament,
			int book) throws ServiceException {
		int startIndex = getVerseIndex(book, 1, 1);
		int bookChapters = getBookChapters(book);
		int endIndex = getVerseIndex(book, bookChapters, getChapterVerses(book, bookChapters));

		return searchVersesWithIndex(keywords, startIndex, endIndex);
	}

	public BibleSearchResult searchVerses(List<String> keywords, int testament,
			int book, int chapter) throws ServiceException {
		int startIndex = getVerseIndex(book, chapter, 1);
		int endIndex = getVerseIndex(book, chapter, getChapterVerses(book, chapter));

		return searchVersesWithIndex(keywords, startIndex, endIndex);
	}
	
	public BibleSearchResult searchVerses(List<String> keywords, int startBook,
			int startChapter, int endBook, int endChapter) throws ServiceException {
		int startIndex = getVerseIndex(startBook, startChapter, 1);
		int endIndex = getVerseIndex(endBook, endChapter, getChapterVerses(endBook, endChapter));

		return searchVersesWithIndex(keywords, startIndex, endIndex);
	}

	public List<String> getBookNames() throws ServiceException {
		try {
			return bibleCatagoryDAO.getBookNames(0, BOOK_COUNT);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getOldTestamentBookNames() throws ServiceException {
		try {
			return bibleCatagoryDAO.getBookNames(0, OLD_TESTAMENT_BOOK_COUNT);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getNewTestamentBookNames() throws ServiceException {
		try {
			return bibleCatagoryDAO.getBookNames(OLD_TESTAMENT_BOOK_COUNT, BOOK_COUNT);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getBookAbbrNames() throws ServiceException {
		try {
			return bibleCatagoryDAO.getBookAbbrNames(0, BOOK_COUNT);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getOldTestamentBookAbbrNames() throws ServiceException {
		try {
			return bibleCatagoryDAO.getBookAbbrNames(0, OLD_TESTAMENT_BOOK_COUNT);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getNewTestamentBookAbbrNames() throws ServiceException {
		try {
			return bibleCatagoryDAO.getBookAbbrNames(OLD_TESTAMENT_BOOK_COUNT, BOOK_COUNT);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getBookFHLAbbrNames() throws ServiceException {
		try {
			return bibleCatagoryDAO.getBookFHLAbbrNames(0, BOOK_COUNT);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public Boolean addBibleReadingPlan(BibleReadingPlan plan)
			throws ServiceException {
		try {
			return bibleReadingRecordDAO.addBibleReadingPlan(plan);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public Boolean updateBibleReadingPlan(BibleReadingPlan plan)
			throws ServiceException {
		try {
			return bibleReadingRecordDAO.modifyBibleReadingPlan(plan);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public Boolean deleteBibleReadingPlan(BibleReadingPlan plan)
			throws ServiceException {
		try {
			return bibleReadingRecordDAO.deleteBibleReadingPlan(plan);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public BibleReadingPlan getBibleReadingPlan(String id)
			throws ServiceException {
		BibleReadingPlan vo = new BibleReadingPlanImpl();
		vo.setId(id);
		
		try {
			List<BibleReadingPlan> list = bibleReadingRecordDAO.searchBibleReadingPlans(vo);
			
			if(list.size() > 0) {
				BibleReadingPlan plan = list.get(0);
				
				//靠這個method載入size資訊
				getPlanChapterSize(plan);
				
				return plan;
			} else {
				return null;
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * 要將讀經進度全部load進來會佔太多記憶體,使app執行緩慢,所以需要實作此method
	 * 如果不得已真的需要load進來的時候，再獨立呼叫此method
	 * @param plan
	 * @throws ServiceException
	 */
	public void bindingReadingRecord(BibleReadingPlan plan) throws ServiceException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		try {
			bibleReadingRecordDAO.open();
			
			//去資料庫載入讀經紀錄資訊到plan物件
			bibleReadingRecordDAO.loadRecords(null, plan);
		} catch (DAOException e) {
			throw new ServiceException(e);
		} finally {
			bibleReadingRecordDAO.close();
		}
	}
	
	public Boolean isReaded(User user, BibleReadingPlan plan, int book, int chapter) throws ServiceException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		try {
			return bibleReadingRecordDAO.isReaded(null, plan, book, chapter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public int readedSize(User user, BibleReadingPlan plan) throws ServiceException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		try {
			return bibleReadingRecordDAO.readedSize(null, plan);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<BibleReadingPlan> getAllBibleReadingPlans() throws ServiceException {
		try {
			List<BibleReadingPlan> list = bibleReadingRecordDAO.searchBibleReadingPlans(null);
			for(int i = 0; i < list.size(); i++) {
				//靠這個method載入size資訊
				getPlanChapterSize(list.get(i));
			}
			return list;
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public BibleReadingPlanChapterInterval getTodayPlan(BibleReadingPlan plan)
			throws ServiceException {
		BibleReadingPlanChapterInterval result = null;
		
		List<BibleReadingPlanChapterInterval> readings = plan.getReadings();
		int chapters = getPlanChapterSize(plan);
		int daysWithBreak = getPlanDaySizeWithBreak(plan);
		if(daysWithBreak != 0) {
			int chaptersPerDay =  chapters / daysWithBreak;
			int remainChapters = chapters % daysWithBreak;
			
			BibleReadingPlan tempPlan1 = new BibleReadingPlanImpl();
			TimeSchedule schedule = new TimeSchedule();
			schedule.setStartDatetime(plan.getSchedule().getStartDatetime());
			schedule.setEndDatetime(Calendar.getInstance());
			schedule.setMondayBreak(plan.getSchedule().isMondayBreak());
			schedule.setTuesdayBreak(plan.getSchedule().isTuesdayBreak());
			schedule.setWednesdayBreak(plan.getSchedule().isWednesdayBreak());
			schedule.setThursdayBreak(plan.getSchedule().isThursdayBreak());
			schedule.setFridayBreak(plan.getSchedule().isFridayBreak());
			schedule.setSaturdayBreak(plan.getSchedule().isSaturdayBreak());
			schedule.setSundayBreak(plan.getSchedule().isSundayBreak());
			tempPlan1.setSchedule(schedule);
			int shouldReadDays = getPlanDaySizeWithBreak(tempPlan1);
			int shouldReadChapters = 0;
			boolean remainFlag = true;
			if(shouldReadDays >= remainChapters) {
				remainFlag = false;
				shouldReadChapters = chaptersPerDay * shouldReadDays + remainChapters;
			} else {
				shouldReadChapters = (chaptersPerDay + 1) * shouldReadDays;
			}
			
			//避免時間超過還繼續累加,最大應讀經文應該設為總共經文數
			if(shouldReadChapters >= chapters) {
				shouldReadChapters = chapters;
			}
			
			int readedChapters = 0;
			BibleReadingPlanChapterInterval currentReading = null;
			for(int i = 0; i < readings.size(); i++) {
				readedChapters = readedChapters + readings.get(i).getSize();
				if(readedChapters >= shouldReadChapters) {
					currentReading = readings.get(i);
					readedChapters = readedChapters - readings.get(i).getSize();
					break;
				}
			}
			int remainShouldReadChaptersInCurrentReading = shouldReadChapters - readedChapters;
			
			if(currentReading != null) {
				int addChapters = remainShouldReadChaptersInCurrentReading - 1;
				if(remainShouldReadChaptersInCurrentReading == 0) {
					addChapters = chaptersPerDay;
				}
				BibleChapter startChapter = null;
				BibleChapter endChapter = addChapters(currentReading.getStartChapter(), addChapters);
				
				if(remainFlag) {
					addChapters = addChapters - (chaptersPerDay + 1) + 1;
				} else {
					addChapters = addChapters - (chaptersPerDay) + 1;
				}
				startChapter = addChapters(currentReading.getStartChapter(), addChapters);
				
				result = new BibleReadingPlanChapterIntervalImpl(startChapter, endChapter);
			}
		} else {
			try {
				int startIndex = getVerseIndex(0, 1, 1);
				int endIndex = startIndex + getChapterVerses(0, 1);
				
				BibleChapter startChapter = new BibleChapterImpl();
				startChapter.setId(StringUtility.addZero(0, 2) + StringUtility.addZero(1, 3));
				startChapter.setName(bibleCatagoryDAO.getBookName(0) + 1);
				BibleBook startBibleBook = new BibleBookImpl();
				startBibleBook.setId("" + 0);
				startBibleBook.setBookId(0);
				startChapter.setBook(startBibleBook);
				startChapter.setChapterId(1);
				startChapter.setVerses(bibleDAO.getVerses(startIndex, endIndex));
				
				startIndex = getVerseIndex(65, 22, 1);
				endIndex = startIndex + getChapterVerses(65, 22);
				
				BibleChapter endChapter = new BibleChapterImpl();
				endChapter.setId(StringUtility.addZero(65, 2) + StringUtility.addZero(22, 3));
				endChapter.setName(bibleCatagoryDAO.getBookName(65) + 22);
				BibleBook endBibleBook = new BibleBookImpl();
				endBibleBook.setId("" + 65);
				endBibleBook.setBookId(65);
				endChapter.setBook(endBibleBook);
				endChapter.setChapterId(22);
				endChapter.setVerses(bibleDAO.getVerses(startIndex, endIndex));
				
				result = new BibleReadingPlanChapterIntervalImpl(startChapter, endChapter);
			} catch(DAOException e) {
				throw new ServiceException(e);
			}
		}
		
		return result;
	}
	
	public int getShouldReadChapterSize(BibleReadingPlan plan) throws ServiceException {
		int chapters = getPlanChapterSize(plan);
		int daysWithBreak = getPlanDaySizeWithBreak(plan);
		if(daysWithBreak != 0) {
			int chaptersPerDay =  chapters / daysWithBreak;
			int remainChapters = chapters % daysWithBreak;
			
			BibleReadingPlan tempPlan1 = new BibleReadingPlanImpl();
			TimeSchedule schedule = new TimeSchedule();
			schedule.setStartDatetime(plan.getSchedule().getStartDatetime());
			schedule.setEndDatetime(Calendar.getInstance());
			schedule.setMondayBreak(plan.getSchedule().isMondayBreak());
			schedule.setTuesdayBreak(plan.getSchedule().isTuesdayBreak());
			schedule.setWednesdayBreak(plan.getSchedule().isWednesdayBreak());
			schedule.setThursdayBreak(plan.getSchedule().isThursdayBreak());
			schedule.setFridayBreak(plan.getSchedule().isFridayBreak());
			schedule.setSaturdayBreak(plan.getSchedule().isSaturdayBreak());
			schedule.setSundayBreak(plan.getSchedule().isSundayBreak());
			tempPlan1.setSchedule(schedule);
			int shouldReadDays = getPlanDaySizeWithBreak(tempPlan1);
			int shouldReadChapters = 0;
			if(shouldReadDays >= remainChapters) {
				shouldReadChapters = chaptersPerDay * shouldReadDays + remainChapters;
			} else {
				shouldReadChapters = (chaptersPerDay + 1) * shouldReadDays;
			}
			
			//避免時間超過還繼續累加,最大應讀經文應該設為總共經文數
			if(shouldReadChapters >= chapters) {
				shouldReadChapters = chapters;
			}
			
			return shouldReadChapters;
		} else {
			return chapters;
		}
	}

	public int getPlanChapterSize(BibleReadingPlan plan)
			throws ServiceException {
		int size = 0;
		
		List<BibleReadingPlanChapterInterval> readings = plan.getReadings();
		for(int i = 0; i < readings.size(); i++) {
			BibleReadingPlanChapterInterval reading = readings.get(i);
			int startBook = reading.getStartChapter().getBook().getBookId();
			int startChapter = reading.getStartChapter().getChapterId();
			int endBook = reading.getEndChapter().getBook().getBookId();
			int endChapter = reading.getEndChapter().getChapterId();
			
			int readingSize = 0;
			if(startBook == endBook) {
				readingSize = endChapter - startChapter + 1;
				size = size + readingSize;
			} else {
				int books = endBook - startBook + 1;
				for(int j = 0; j < books; j++) {
					if(j == 0) {
						readingSize = readingSize + (getBookChapters(startBook) - startChapter + 1);
					} else if(j == books - 1) {
						readingSize = readingSize + endChapter;
					} else {
						readingSize = readingSize + getBookChapters(startBook + j);
					}
				}
				
				size = size + readingSize;
			}
			reading.setSize(readingSize);
		}
		
		return size;
	}

	public int getPlanDaySizeWithBreak(BibleReadingPlan plan)
			throws ServiceException {
		int days = 0;
		
		Calendar startDate = plan.getSchedule().getStartDatetime();
		Calendar endDate = plan.getSchedule().getEndDatetime();
		
		int startYear = startDate.get(Calendar.YEAR);
		int endYear = endDate.get(Calendar.YEAR);
		
		int startDaysOfYear = startDate.get(Calendar.DAY_OF_YEAR);
		
		if(startYear == endYear) {
			int allDays = endDate.get(Calendar.DAY_OF_YEAR) - startDate.get(Calendar.DAY_OF_YEAR) + 1;
			days = currentYearDaysWithBreak(allDays, startYear, startDaysOfYear, plan);
		} else {
			int years = endYear - startYear + 1;
			for(int i = 0; i < years; i++) {
				if(i == 0) {
					if(DateFormatUtility.isBissextile(startYear)) {
						days = days + currentYearDaysWithBreak((366 - startDate.get(Calendar.DAY_OF_YEAR) + 1), startYear, startDaysOfYear, plan);
					} else {
						days = days + currentYearDaysWithBreak((365 - startDate.get(Calendar.DAY_OF_YEAR) + 1), startYear, startDaysOfYear, plan);
					}
				} else if(i == years - 1) {
					days = days + currentYearDaysWithBreak(endDate.get(Calendar.DAY_OF_YEAR), endYear, 1, plan);
				} else {
					if(DateFormatUtility.isBissextile(startYear + i)) {
						days = days + currentYearDaysWithBreak(366, startYear + i, 1, plan);
					} else {
						days = days + currentYearDaysWithBreak(365, startYear + i, 1, plan);
					}
				}
			}
		}
		
		return days;
	}
	
	private int currentYearDaysWithBreak(int allDays, int startYear, int startDaysOfYear, BibleReadingPlan plan) {
		int days = 0;
		for(int i = 0; i < allDays; i++) {
			Calendar cursor = Calendar.getInstance();
			cursor.set(Calendar.YEAR, startYear);
			cursor.set(Calendar.DAY_OF_YEAR, startDaysOfYear + i);
			
			if(!isBreakDay(cursor, plan)) {
				days++;
			}
		}
		return days;
	}
	
	public boolean isBreakDay(Calendar calendar, BibleReadingPlan plan) {
		switch (calendar.get(Calendar.DAY_OF_WEEK)) {
			case Calendar.MONDAY:
				return plan.getSchedule().isMondayBreak();
			case Calendar.TUESDAY:
				return plan.getSchedule().isTuesdayBreak();
			case Calendar.WEDNESDAY:
				return plan.getSchedule().isWednesdayBreak();
			case Calendar.THURSDAY:
				return plan.getSchedule().isThursdayBreak();
			case Calendar.FRIDAY:
				return plan.getSchedule().isFridayBreak();
			case Calendar.SATURDAY:
				return plan.getSchedule().isSaturdayBreak();
			case Calendar.SUNDAY:
				return plan.getSchedule().isSundayBreak();
			default:
				return false;
		}
	}

	public int getPlanDaySize(BibleReadingPlan plan) throws ServiceException {
		int days = 0;
		
		Calendar startDate = plan.getSchedule().getStartDatetime();
		Calendar endDate = plan.getSchedule().getEndDatetime();
		
		int startYear = startDate.get(Calendar.YEAR);
		int endYear = endDate.get(Calendar.YEAR);
		
		if(startYear == endYear) {
			days = endDate.get(Calendar.DAY_OF_YEAR) - startDate.get(Calendar.DAY_OF_YEAR) + 1;
		} else {
			int years = endYear - startYear + 1;
			for(int i = 0; i < years; i++) {
				if(i == 0) {
					if(DateFormatUtility.isBissextile(startYear)) {
						days = days + (366 - startDate.get(Calendar.DAY_OF_YEAR) + 1);
					} else {
						days = days + (365 - startDate.get(Calendar.DAY_OF_YEAR) + 1);
					}
				} else if(i == years - 1) {
					days = days + endDate.get(Calendar.DAY_OF_YEAR);
				} else {
					if(DateFormatUtility.isBissextile(startYear + i)) {
						days = days + 366;
					} else {
						days = days + 365;
					}
				}
			}
		}
		
		return days;
	}
	
	public void readedChapter(User user, BibleReadingPlan plan, BibleChapter chapter)
			throws ServiceException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		try {
			bibleReadingRecordDAO.insertRecord(plan.getOwner(), plan, chapter);
			chapter.setId(StringUtility.addZero(chapter.getBook().getBookId(), 2) + StringUtility.addZero(chapter.getChapterId(), 3));
			chapter.setName(bibleCatagoryDAO.getBookName(chapter.getBook().getBookId()));
			plan.getOwner().addReadedChapter(chapter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public void clearReadedRecord(User user, BibleReadingPlan plan)
			throws ServiceException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		try {
			bibleReadingRecordDAO.deleteRecords(plan.getOwner(), plan);
			plan.getOwner().setReadedChapters(new ArrayList<BibleChapter>());
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	public BibleChapter addChapters(BibleChapter bibleChapter, int addChapter) throws ServiceException {
		return addChapters(bibleChapter.getBook().getBookId(), bibleChapter.getChapterId(), addChapter);
	}

	public BibleChapter addChapters(int book, int chapter, int addChapter)
			throws ServiceException {
		if(isValidChapter(book, chapter)) {
			int book2 = book;
			int chapter2 = chapter;
			int addChapter2 = addChapter;
			
			if(addChapter == 0) {
				return getChapter(book, chapter);
			} else if(addChapter > 0) {
				while(addChapter2 > 0) {
					if(isValidChapter(book2, chapter2 + addChapter2)) {
						return getChapter(book2, chapter2 + addChapter2);
					} else {
						if(book2 < 65) {
							addChapter2 = addChapter2 - (getBookChapters(book2) - chapter2);
							book2++;
							chapter2 = 0;
						} else {
							Log.d(getId(), "book = " + book + "; chapter = " + chapter + "; addChapter = " + addChapter);
							throw new ServiceException("Chapter not found!");
						}
					}
				}
			} else if(addChapter < 0) {
				while(addChapter2 <= 0) {
					if(isValidChapter(book2, chapter2 + addChapter2)) {
						return getChapter(book2, chapter2 + addChapter2);
					} else {
						if(book2 > 0) {
							addChapter2 = addChapter2 + chapter2;
							book2--;
							chapter2 = getBookChapters(book2);
						} else {
							Log.d(getId(), "book = " + book + "; chapter = " + chapter + "; addChapter = " + addChapter);
							throw new ServiceException("Chapter not found!");
						}
					}
				}
			}
			
			Log.d(getId(), "book = " + book + "; chapter = " + chapter + "; addChapter = " + addChapter);
			throw new ServiceException("Chapter computing error!");
		} else {
			Log.d(getId(), "book = " + book + "; chapter = " + chapter);
			throw new ServiceException("Chapter not found!");
		}
	}

	public String getGoldenWord() throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
			
		String[] htmlLines = doPostForTextContent(context.getString(R.string.goldenWordURL), params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout))).split("\n");
		for(int i = 0; i < htmlLines.length; i++) {
			if(htmlLines[i].startsWith("<body")) {
				String part = htmlLines[i].split("<font size=\"2\"> ")[1];
				String verse = part.split("<a href=\"http://bible.fhl.net/new/read.php")[0];
				String part2 = part.split("<a href=\"http://bible.fhl.net/new/read.php")[1];
				String part3 = part2.split("target=\"bible\">")[1];
				String bookChapter = part3.split("</a>")[0];
				
				return verse + " " + bookChapter;
			}
		}
		
		return "";
	}

	public List<BibleLocation> getLocations(String name) throws ServiceException {
		try {
			return bibleLocationDAO.getLocations(name);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<BibleLocation> getLocations(BibleVerse verse)
			throws ServiceException {
		try {
			return bibleLocationDAO.getLocations(verse);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<BibleLocation> getLocations(BibleChapter chapter)
			throws ServiceException {
		try {
			return bibleLocationDAO.getLocations(chapter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<BibleLocation> getLocations(BibleBook book)
			throws ServiceException {
		try {
			return bibleLocationDAO.getLocations(book);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public Boolean participatePlan(User user, BibleReadingPlan plan)
			throws ServiceException {
		//TODO: 參加一個讀經計畫,目前都是本機端,所以暫不實作
		return null;
	}

	public List<BibleBook> getUnCompletedBooks(User user, BibleReadingPlan plan)
			throws ServiceException {
		List<BibleBook> books = new ArrayList<BibleBook>();
		
		//這個演算法在最差的情況下,必須跑完所有的章
		//而且這個最差狀況是會發生的(隨著讀經進度進行會越來越慢)
		//必須想更好的做法來來判斷一卷書是否讀完
//		List<BibleReadingPlanChapterInterval> readings = plan.getReadings();
//		for(int i = 0; i < readings.size(); i++) {
//			BibleReadingPlanChapterInterval interval = readings.get(i);
//			BibleChapter cursor = interval.getStartChapter();
//			Integer unCompletedBookId = null;
//			while(!(cursor.getBook().getBookId().equals(interval.getEndChapter().getBook().getBookId()) && cursor.getChapterId().equals(interval.getEndChapter().getChapterId()))) {
//				if(unCompletedBookId == null || !cursor.getBook().getBookId().equals(unCompletedBookId)) {
//					if(!isReaded(user, plan, cursor.getBook().getBookId(), cursor.getChapterId())) {
//						books.add(cursor.getBook());
//						unCompletedBookId = cursor.getBook().getBookId();
//					}
//				}
//				
//				cursor = addChapters(cursor, 1);
//			}
//		}
		
		//這個演算法已大幅改善上一個演算法的計算量(sql次數)
		//sql的次數減少為與讀經進度的書卷數正相關
		//以讀完整卷書來說,要下sql 66次,速度雖可接受,但應該還可以更快
//		List<BibleReadingPlanChapterInterval> readings = plan.getReadings();
//		for(int i = 0; i < readings.size(); i++) {
//			BibleReadingPlanChapterInterval interval = readings.get(i);
//			BibleChapter cursor = interval.getStartChapter();
//			
//			Integer currentBookId = null;
//			Integer startChapterId = null;
//			Integer endChapterId = null;
//			BibleBook preBook = null;
//			while(!(cursor.getBook().getBookId().equals(interval.getEndChapter().getBook().getBookId()) && cursor.getChapterId().equals(interval.getEndChapter().getChapterId()))) {
//				if(currentBookId == null || !cursor.getBook().getBookId().equals(currentBookId)) {
//					try {
//						if(currentBookId != null && !bibleReadingRecordDAO.isCompletedBook(user, plan, currentBookId, startChapterId, endChapterId)) {
//							books.add(preBook);
//						}
//					} catch (DAOException e) {
//						throw new ServiceException(e);
//					}
//					
//					currentBookId = cursor.getBook().getBookId();
//					startChapterId = cursor.getChapterId();
//				}
//				
//				preBook = cursor.getBook();
//				endChapterId = cursor.getChapterId();
//				cursor = addChapters(cursor, 1);
//			}
//		}
		
		//這個演算法是把所有閱讀紀錄一次撈出來
		//再用程式邏輯去判斷有哪些書卷未讀完
		//此法保證只有一次sql
		//經過實機測試的結果,速度上也感覺不出跟上一個演算法有差異
		//可見在縮小到一定範圍內的sql次數,想在sql這邊改善整個method的速度應該已經到極限了
		try {
			List<BibleChapter> readedChapters = bibleReadingRecordDAO.getReadedChapters(null, plan);
			List<BibleReadingPlanChapterInterval> readings = plan.getReadings();
			for(int i = 0; i < readings.size(); i++) {
				BibleReadingPlanChapterInterval interval = readings.get(i);
				BibleChapter cursor = interval.getStartChapter();
				
				Integer currentBookId = null;
				Integer startChapterId = null;
				Integer endChapterId = null;
				BibleBook preBook = null;
				while(!(cursor.getBook().getBookId().equals(interval.getEndChapter().getBook().getBookId()) && cursor.getChapterId().equals(interval.getEndChapter().getChapterId()))) {
					if(currentBookId == null || !cursor.getBook().getBookId().equals(currentBookId)) {
						if(currentBookId != null && !isCompletedBook(readedChapters, currentBookId, startChapterId, endChapterId)) {
							books.add(preBook);
						}
						currentBookId = cursor.getBook().getBookId();
						startChapterId = cursor.getChapterId();
					}
					
					preBook = cursor.getBook();
					endChapterId = cursor.getChapterId();
					cursor = addChapters(cursor, 1);
				}
			}
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		
		return books;
	}
	
	private Boolean isCompletedBook(List<BibleChapter> readedChapters, int book, int startChapter, int endChapter) {
		int counter = 0;
		
		for(int i = 0; i < readedChapters.size(); i++) {
			BibleChapter readedChapter = readedChapters.get(i);
			
			if(readedChapter.getBook().getBookId() == book && readedChapter.getChapterId() >= startChapter && readedChapter.getChapterId() <= endChapter) {
				counter++;
			}
		}
		
		return !(counter < (endChapter - startChapter + 1));
	}
	
	public List<BiblePerson> getPeople(String name) throws ServiceException {
		try {
			return biblePersonDAO.getPeople(name);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<BiblePerson> getPeople(BibleVerse verse)
			throws ServiceException {
		try {
			return biblePersonDAO.getPeople(verse);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<BiblePerson> getPeople(BibleChapter chapter)
			throws ServiceException {
		try {
			return biblePersonDAO.getPeople(chapter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<BiblePerson> getPeople(BibleBook book) throws ServiceException {
		try {
			return biblePersonDAO.getPeople(book);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}
	
	/**
	 * 取得信望愛的strong number
	 * @param version
	 * @param chineses
	 * @param chap
	 * @param sec
	 * @return
	 * @throws ServiceException
	 */
	public List<BibleWord> getFHLStrongNumber(String version, String chineses, int chap, int sec) throws ServiceException {
		List<BibleWord> words = new ArrayList<BibleWord>();
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		//下拉選單,選0表示查詢全部
		params.add(new BasicNameValuePair("version", version));
		params.add(new BasicNameValuePair("chineses", chineses));
		params.add(new BasicNameValuePair("chap", "" + chap));
		params.add(new BasicNameValuePair("sec", "" + sec));
		
		String raw = doPostForTextContent(context.getString(R.string.bibleReadingReferenceBookURL4), params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout))).split("<bible_text>")[1].split("</bible_text>")[0];
		
		String[] temp = raw.split(">");
		for(int i = 0; i < temp.length; i++) {
			BibleWord word = new BibleWordImpl();
			if(temp[i].indexOf("<") != -1) {
				String[] temp2 = temp[i].split("<");
				String text = temp2[0].replaceAll("\\{", "").replaceAll("\\}", "");
				
				String language = StrongNumber.LANGUAGE_GREEK;
				if(temp2[1].indexOf("H") != -1) {
					language = StrongNumber.LANGUAGE_HEBREW;
				}
				String type = StrongNumber.TYPE_COMMON;
				if(temp2[1].indexOf("T") != -1) {
					type = StrongNumber.TYPE_TENSE;
				}
				
				StrongNumber strongNumber = new StrongNumberImpl();
				strongNumber.setLanguage(language);
				strongNumber.setType(type);
				try {
					int number = Integer.parseInt(temp2[1].replaceAll("WG", "").replaceAll("WTG", "").replaceAll("WAG", "").replaceAll("WH", "").replaceAll("WTH", "").replaceAll("WAH", ""));
					strongNumber.setNumber(number);
				} catch(NumberFormatException e) {
					strongNumber.setNumber(-1);
					strongNumber.setDescription(temp2[1].replaceAll("WG", "").replaceAll("WTG", "").replaceAll("WAG", "").replaceAll("WH", "").replaceAll("WTH", "").replaceAll("WAH", ""));
				}
				
				word.setText(text);
				word.setStrongNumber(strongNumber);
			} else {
				word.setText(temp[i].replaceAll("\\{", "").replaceAll("\\}", ""));
			}
			
			words.add(word);
		}
		
		return words;
	}
	
	public String getChapterMP3Link(BibleChapter chapter) throws ServiceException {
		if(isValidChapter(chapter)) {
			return context.getString(R.string.bibleReadingAudioURL) + (chapter.getBook().getBookId() + 1) + '/' + (chapter.getBook().getBookId() + 1) + '_' + StringUtility.addZero(chapter.getChapterId(), 3) + ".mp3";
		} else {
			throw new ServiceException("Chapter not found!");
		}
	}

	public List<String> getOldTestamentLawBookNames() throws ServiceException {
		try {
			return bibleCatagoryDAO.getBookNames(0, OLD_TESTAMENT_LAW_BOOK_COUNT);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getOldTestamentHistoryBookNames()
			throws ServiceException {
		try {
			int start = OLD_TESTAMENT_LAW_BOOK_COUNT;
			int end = start + OLD_TESTAMENT_HISTORY_BOOK_COUNT;
			return bibleCatagoryDAO.getBookNames(start, end);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getOldTestamentWisdomBookNames()
			throws ServiceException {
		try {
			int start = OLD_TESTAMENT_LAW_BOOK_COUNT + OLD_TESTAMENT_HISTORY_BOOK_COUNT;
			int end = start + OLD_TESTAMENT_WISDOM_BOOK_COUNT;
			return bibleCatagoryDAO.getBookNames(start, end);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getOldTestamentProphetsBookNames()
			throws ServiceException {
		try {
			int start = OLD_TESTAMENT_LAW_BOOK_COUNT + OLD_TESTAMENT_HISTORY_BOOK_COUNT + OLD_TESTAMENT_WISDOM_BOOK_COUNT;
			int end = OLD_TESTAMENT_BOOK_COUNT;
			return bibleCatagoryDAO.getBookNames(start, end);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getNewTestamentGospelsBookNames()
			throws ServiceException {
		try {
			int start = OLD_TESTAMENT_BOOK_COUNT;
			int end = start + NEW_TESTAMENT_GOSPELS_BOOK_COUNT;
			return bibleCatagoryDAO.getBookNames(start, end);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getNewTestamentHistoryBookNames()
			throws ServiceException {
		try {
			int start = OLD_TESTAMENT_BOOK_COUNT + NEW_TESTAMENT_GOSPELS_BOOK_COUNT;
			int end = start + NEW_TESTAMENT_HISTORY_BOOK_COUNT;
			return bibleCatagoryDAO.getBookNames(start, end);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getNewTestamentPaulineEpistlesBookNames()
			throws ServiceException {
		try {
			int start = OLD_TESTAMENT_BOOK_COUNT + NEW_TESTAMENT_GOSPELS_BOOK_COUNT + NEW_TESTAMENT_HISTORY_BOOK_COUNT;
			int end = start + NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT;
			return bibleCatagoryDAO.getBookNames(start, end);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getNewTestamentGeneralEpistlesBookNames()
			throws ServiceException {
		try {
			int start = OLD_TESTAMENT_BOOK_COUNT + NEW_TESTAMENT_GOSPELS_BOOK_COUNT + NEW_TESTAMENT_HISTORY_BOOK_COUNT + NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT;
			int end = start + NEW_TESTAMENT_GENERAL_EPISTLES_BOOK_COUNT;
			return bibleCatagoryDAO.getBookNames(start, end);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<String> getNewTestamentApocalypseBookNames()
			throws ServiceException {
		try {
			int start = OLD_TESTAMENT_BOOK_COUNT + NEW_TESTAMENT_GOSPELS_BOOK_COUNT + NEW_TESTAMENT_HISTORY_BOOK_COUNT + NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT + NEW_TESTAMENT_GENERAL_EPISTLES_BOOK_COUNT;
			int end = BOOK_COUNT;
			return bibleCatagoryDAO.getBookNames(start, end);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	public List<BibleBookMark> getSPFCBookMarks() throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String json = doPostForJSON(context.getString(R.string.bookmarkURL), params, Integer.parseInt(context.getString(R.string.httpTimeout)));
		
		try {
			List<BibleBookMark> result = new ArrayList<BibleBookMark>();
			
			JSONObject jsonResult = new JSONObject(json);
			JSONArray jsonArray = jsonResult.getJSONArray("bookmarks");
			for(int i = 0; i < jsonArray.length(); i++) {
				BibleBookMark bookmark = new BibleBookMarkImpl();
				bookmark.setBookMarkName(new String(jsonArray.getJSONObject(i).getString("name").getBytes("ISO-8859-1"), "UTF-8"));
				
				BibleBook book = new BibleBookImpl();
				book.setBookId(jsonArray.getJSONObject(i).getInt("book"));
				BibleChapter chapter = new BibleChapterImpl();
				chapter.setBook(book);
				chapter.setChapterId(jsonArray.getJSONObject(i).getInt("chapter"));
				BibleVerse verse = new BibleVerseImpl();
				verse.setChapter(chapter);
				verse.setVerseId(jsonArray.getJSONObject(i).getInt("verse"));
				bookmark.setBookMark(verse);
				
				result.add(bookmark);
			}
			
			return result;
		} catch (JSONException e) {
			throw new ServiceException(e);
		} catch (UnsupportedEncodingException e) {
			throw new ServiceException(e);
		}
	}
}
