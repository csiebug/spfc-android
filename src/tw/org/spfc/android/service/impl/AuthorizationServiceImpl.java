package tw.org.spfc.android.service.impl;

import java.lang.reflect.Method;
import java.util.List;

import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.domain.Role;
import csiebug.domain.SecureMethod;
import csiebug.domain.SecureObject;
import csiebug.domain.User;
import csiebug.service.AuthorizationService;
import csiebug.service.ServiceException;

public class AuthorizationServiceImpl extends BaseServiceImpl implements AuthorizationService {
	public Boolean checkUserSession(String sessionId) throws ServiceException {
		//目前手機並沒有需要授權的功能,需要授權的功能,手機端都關掉不實作
		//所以也沒有需要檢查session是否過期,一律不過期
		return true;
	}
	
	public List<Role> getAuthorities(User user) throws ServiceException {
		//手機似乎無必要實作這功能
		return null;
	}
	
	public List<User> getAuthorizedUsers(Role role) throws ServiceException {
		//手機似乎無必要實作這功能
		return null;
	}

	public List<SecureMethod> getMethods(Role role) throws ServiceException {
		//手機似乎無必要實作這功能
		return null;
	}
	
	public Boolean hasPermission(User user, SecureObject object, Method method)
			throws ServiceException {
		//目前手機並沒有需要授權的功能,需要授權的功能,手機端都關掉不實作
		//所以權限不檢查,一律開放
		return true;
	}

	public Boolean hasPermission(Role role, SecureObject object, Method method)
			throws ServiceException {
		//目前手機並沒有需要授權的功能,需要授權的功能,手機端都關掉不實作
		//所以權限不檢查,一律開放
		return true;
	}
	
	public Boolean addPermission(Role role, SecureObject object, Method method)
			throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean deletePermission(Role role, SecureObject object,
			Method method) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean isUserEnabled(String sessionId) throws ServiceException {
		//目前手機並沒有需要授權的功能,需要授權的功能,手機端都關掉不實作
		//所以權限不檢查,一律開放
		return true;
	}

	public Boolean hasPermission(String sessionId, SecureObject object,
			Method method) throws ServiceException {
		//目前手機並沒有需要授權的功能,需要授權的功能,手機端都關掉不實作
		//所以權限不檢查,一律開放
		return true;
	}

	public List<SecureMethod> getAllMethods() throws ServiceException {
		//手機似乎無必要實作這功能
		return null;
	}

	public Boolean addMethod(SecureMethod method) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean deleteMethod(SecureMethod method) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}
	
	public Boolean addMethods(List<SecureMethod> vo) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean deleteMethods(List<SecureMethod> methods)
			throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean addAuthority(User user, Role role) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean deleteAuthority(User user, Role role)
			throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean addAuthorities(User user, List<Role> roles)
			throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean deleteAuthorities(User user, List<Role> roles)
			throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean addPermissions(Role role, SecureObject object,
			List<Method> methods) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean deletePermissions(Role role, SecureObject object,
			List<Method> methods) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean addAuthorities(Role role, List<User> users)
			throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean deleteAuthorities(Role role, List<User> users)
			throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean kickChannels(List<String> channelIds) throws ServiceException {
		//手機似乎無必要實作這功能
		return false;
	}

	public Boolean reconnectChannel(String sessionId) throws ServiceException {
		//手機似乎無必要實作這功能
		return null;
	}

	public List<String> getTimeoutChannelIds() throws ServiceException {
		//手機似乎無必要實作這功能
		return null;
	}

	public List<String> getTimeoutForgetPasswordChannelIds()
			throws ServiceException {
		//手機似乎無必要實作這功能
		return null;
	}

	public Boolean kickForgetPasswordChannels(List<String> channelIds)
			throws ServiceException {
		//手機似乎無必要實作這功能
		return null;
	}
}
