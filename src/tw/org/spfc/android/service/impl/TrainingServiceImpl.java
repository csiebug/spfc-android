package tw.org.spfc.android.service.impl;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import android.content.Context;
import tw.org.spfc.android.R;
import tw.org.spfc.android.service.TrainingService;
import tw.org.spfc.domain.BookingOrder;
import tw.org.spfc.domain.Class;
import tw.org.spfc.domain.Course;
import tw.org.spfc.domain.Person;
import tw.org.spfc.domain.Subject;
import tw.org.spfc.domain.TimeSchedule;
import tw.org.spfc.domain.pojoImpl.ClassImpl;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.service.ServiceException;
import csiebug.util.DateFormatException;
import csiebug.util.StringUtility;

public class TrainingServiceImpl extends BaseServiceImpl implements TrainingService {
	private Context context;
	
	public TrainingServiceImpl(Context context) {
		this.context = context;
	}

	public void cancelClass(Class arg0) {
		// TODO Auto-generated method stub
		
	}

	public Class createClass(Subject arg0, List<Person> arg1,
			List<BookingOrder> arg2) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public Course createCourse(String arg0, String arg1)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public Subject createSubject(Course arg0, String arg1, String arg2)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public Class modifyClass(Class arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Class> searchClasses(String arg0, String arg1, TimeSchedule arg2)
			throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Course> searchCourses(String arg0) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Subject> searchSubjects(String arg0) throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean signUpClass(Class arg0, Person arg1) throws ServiceException {
		// TODO Auto-generated method stub
		return false;
	}

	public List<Class> getClasses(int maxLength) throws ServiceException {
		try {
			return getRemoteClassList(maxLength);
		} catch (DateFormatException e) {
			throw new ServiceException(e);
		} catch (IOException e) {
			throw new ServiceException(e);
		}
	}

	public List<Class> searchClasses(List<String> keywords)
			throws ServiceException {
		List<Class> list = new ArrayList<Class>();
		
		//給一個極大數來實作無限大,用來取得所有的檔案列表
		int infinity = Integer.MAX_VALUE;
		List<Class> classes = getClasses(infinity);
		for(int i = 0; i < classes.size(); i++) {
			boolean addFlag = true;
			for(int j = 0; j < keywords.size(); j++) {
				if(classes.get(i).getName().indexOf(keywords.get(j)) == -1) {
					addFlag = false;
					break;
				}
			}
			
			if(addFlag) {
				list.add(classes.get(i));
			}
		}
		
		return list;
	}

	public Class getClassDetail(Class clazz) throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		
		String html = doPostForTextContent(clazz.getReferences().get(0), params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout)));
		
		String htmlPart = html.split("<td valign=\"top\">")[1].split("<object type=")[0];
		String audioURL = html.split("http://www.spfc.org.tw/j15/plugins/content/dewplayer.swf\\?son=")[1].split("\\.mp3")[0] + ".mp3";
     	
     	String pptURL = "";
     	if(html.indexOf("xmlDataPath") != -1) {
     		pptURL = html.split("xmlDataPath\", \"")[1].split("\"")[0];
     	}
     	
     	clazz.addReference(htmlPart);
     	clazz.addReference(audioURL);
     	clazz.addReference(pptURL);
     	
		return clazz;
	}
	
	private List<Class> getRemoteClassList(int maxLength) throws DateFormatException, IOException, ServiceException {
		//TODO: 先用parse html的方式取得顯示文字；教會網站改版時必須修改
		List<Class> list = new ArrayList<Class>();
		
		String html = getRemoteHTML();
		String[] htmlLines = html.split("\n");
		for(int i = 0; i < htmlLines.length; i++) {
			if(list.size() < maxLength) {
				if(StringUtility.ltrim(htmlLines[i]).startsWith("<a href=\"/j15/index.php/video")) {
					String url = "http://www.spfc.org.tw" + StringUtility.ltrim(htmlLines[i]).replace("<a href=\"", "").replace("\">", "");
					String name = StringUtility.ltrim(htmlLines[i+ 1]).replace("</a>", "");
					
					Class clazz = new ClassImpl();
					clazz.setName(name);
					clazz.addReference(url);
					
					list.add(clazz);
				}
			} else {
				break;
			}
		}
		
		return list;
	}
	
	private String getRemoteHTML() throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", "56"));
		params.add(new BasicNameValuePair("limit", "0"));
		params.add(new BasicNameValuePair("sectionid", "11"));
		params.add(new BasicNameValuePair("task", "category"));
		params.add(new BasicNameValuePair("filter_order", ""));
		params.add(new BasicNameValuePair("filter_order_Dir", ""));
		params.add(new BasicNameValuePair("limitstart", "0"));
		params.add(new BasicNameValuePair("viewcache", "0"));
		
		return doPostForTextContent(context.getString(R.string.sundaySchoolSearchURL), params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout)));
	}

	@SuppressWarnings("unchecked")
	public List<String> getClassPPT(Class clazz) throws ServiceException {
		if(!clazz.getReferences().get(3).equals("")) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			
			try {
				String xml = doPostForTextContent(clazz.getReferences().get(3), params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout)));
				SAXBuilder builder = new SAXBuilder();
				Document document = builder.build(new StringReader(xml));
	         	Element rootElement = (Element)document.getRootElement();
	         	String baseURL = rootElement.getAttributeValue("imagePath");
//	         	String thumbURL = rootElement.getAttributeValue("thumbPath");
	         	
	         	List<Element> imageNodes = rootElement.getChildren();
	         	List<String> images = new ArrayList<String>();
	         	for(int i = 0; i < imageNodes.size(); i++) {
	         		List<Element> imageAttributes = imageNodes.get(i).getChildren();
	         		for(int j = 0; j < imageAttributes.size(); j++) {
	         			if(imageAttributes.get(j).getName().equalsIgnoreCase("filename")) {
	         				images.add(baseURL + imageAttributes.get(j).getText());
	         			}
	         		}
	         	}
				
				return images;
			} catch (IOException e) {
				throw new ServiceException(e);
			} catch (JDOMException e) {
				throw new ServiceException(e);
			}
		} else {
			return new ArrayList<String>();
		}
	}
}
