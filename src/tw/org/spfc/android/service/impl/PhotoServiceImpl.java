package tw.org.spfc.android.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;
import tw.org.spfc.android.R;
import tw.org.spfc.domain.Album;
import tw.org.spfc.domain.Photo;
import tw.org.spfc.domain.pojoImpl.AlbumImpl;
import tw.org.spfc.domain.pojoImpl.PhotoImpl;
import tw.org.spfc.service.PhotoService;

public class PhotoServiceImpl extends BaseServiceImpl implements PhotoService {
	private Context context;
	
	public PhotoServiceImpl(Context context) {
		this.context = context;
	}
	
	public Album getNewsPhotos() throws ServiceException {
		//TODO: 教會網站改版時必須修改
		List<Photo> photos = new ArrayList<Photo>();
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String folderURL = context.getString(R.string.photoSlideShowFolder);
		String[] htmlLines = doPostForTextContent(folderURL, params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout))).split("\n");
		for(int i = 0; i < htmlLines.length; i++) {
			if(htmlLines[i].startsWith("imgName[")) {
				String name = htmlLines[i].split("] = '")[1].replaceAll("';", "");
				
				if(AssertUtility.isNotNullAndNotSpace(name)) {
					Photo photo = new PhotoImpl();
					photo.setSrc(folderURL + name.replaceAll(" ", "_") + ".jpg");
					photos.add(photo);
				}
			}
		}
		
		//TODO: 因為Server端不是用Domain model設計的,所以是parse html取得資料...
		//Album物件的其他屬性沒有辦法設定
		Album news = new AlbumImpl();
		news.setPhotos(photos);
		
		return news;
	}

	public Album getAlbum(String albumId) throws ServiceException {
		// TODO 這個還要跟server端討論配合,暫時不實作
		return null;
	}

	public Album getWallpapers() throws ServiceException {
		List<Photo> photos = new ArrayList<Photo>();
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		String folderURL = context.getString(R.string.wallpaperURL);
		
		try {
			JSONObject result = new JSONObject(doPostForJSON(folderURL, params, Integer.parseInt(context.getString(R.string.httpTimeout))));
			
			if(result.getInt("returnCode") == 1) {
				JSONArray wallpapers = result.getJSONObject("wallpapers").getJSONArray("photos");
				
				for(int i = 0; i < wallpapers.length(); i++) {
					Photo photo = new PhotoImpl();
					photo.setSrc(wallpapers.getJSONObject(i).getString("src"));
					photos.add(photo);
				}
			}
		} catch(JSONException e) {
			throw new ServiceException(e);
		}
		
		//TODO: 因為Server端不是用Domain model設計的,所以是parse html取得資料...
		//Album物件的其他屬性沒有辦法設定
		Album news = new AlbumImpl();
		news.setPhotos(photos);
		
		return news;
	}
}
