package tw.org.spfc.android.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;

import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.android.util.AndroidUtility;
import csiebug.domain.Application;
import csiebug.domain.Version;
import csiebug.service.ServiceException;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import tw.org.spfc.android.R;
import csiebug.service.VersionControlService;

public class VersionControlServiceImpl extends BaseServiceImpl implements VersionControlService {
	private Context context;
	
	public VersionControlServiceImpl(Context context) {
		this.context = context;
	}
	
	public String getRemoteVersion(String id) throws ServiceException {
		//TODO 這邊要改成接server api
		String releaseNote = getReleaseNote(id);
		return releaseNote.substring(0, releaseNote.indexOf(":"));
	}
	
	public String getReleaseNote(String id) throws ServiceException {
		//TODO 這邊要改成接server api
		if(AndroidUtility.isNetworkAvailable(context)) {
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			return doPostForTextContent(context.getString(R.string.versionControlFile), params, "big5", Integer.parseInt(context.getString(R.string.httpTimeout)));
		} else {
			throw new ServiceException("Network is unavailable!");
		}
	}
	
	public String getLocalVersion() throws ServiceException {
		try {
			return AndroidUtility.getVersion(context);
		} catch (NameNotFoundException e1) {
			throw new ServiceException(e1);
		}
	}
	
	/**
	 * 讀release note取得此軟體版本號
	 * @return
	 * @throws IOException
	 */
	@Deprecated
	public String getLocalVersionFromLocalReleaseNote() throws IOException {
		String version = "";
		
		InputStream inputStream = null;
		try {
			inputStream = context.getResources().openRawResource(R.raw.readme);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			version = reader.readLine().replace(":", "");
		} finally {
			if(inputStream != null) {
				inputStream.close();
			}
		}
		
		return version;
	}
	
	public String getLocalReleaseNote() throws ServiceException {
		StringBuilder result= new StringBuilder();
		
		InputStream inputStream = null;
		try {
			inputStream = context.getResources().openRawResource(R.raw.readme);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			
			String line = null;
	        while((line = reader.readLine()) != null){
	        	result.append(line + "\n");
	        }
		} catch (IOException e) {
			throw new ServiceException(e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new ServiceException(e);
				}
			}
		}
		
		return result.toString();
	}

	public Boolean hasNewVersion() throws ServiceException {
		if(AndroidUtility.isNetworkAvailable(context)) {
			try {
				String[] remoteVersions = getRemoteVersion(AndroidUtility.getPackageName(context)).split("\\.");
			
				String[] localVersions = getLocalVersion().split("\\.");
				for(int i = 1; i < remoteVersions.length; i++) {
					if(Integer.parseInt(remoteVersions[i]) > Integer.parseInt(localVersions[i])) {
						return true;
					}
				}
			} catch (NameNotFoundException e) {
				throw new ServiceException(e);
			}
		}
		
		return false;
	}

	public Boolean updateRemoteVersion(Application application, Version version) throws ServiceException {
		//手機端不實作此功能
		return null;
	}

}
