package tw.org.spfc.android.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import tw.org.spfc.android.R;
import tw.org.spfc.android.service.BibleGameService;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.service.ServiceException;

public class BibleGameServiceImpl extends BaseServiceImpl implements BibleGameService {
	private Context context;
	
	public BibleGameServiceImpl(Context context) {
		this.context = context;
	}

	public JSONArray getBibleGames(int book, int chapter, int verse)
			throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("book", "" + book));
		params.add(new BasicNameValuePair("chapter", "" + chapter));
		params.add(new BasicNameValuePair("verse", "" + verse));
		String json = doPostForJSON(context.getString(R.string.gameURL), params, Integer.parseInt(context.getString(R.string.httpTimeout)));
		
		try {
			JSONObject jsonResult = new JSONObject(json);
			return jsonResult.getJSONArray("games");
		} catch (JSONException e) {
			throw new ServiceException(e);
		}
	}

}
