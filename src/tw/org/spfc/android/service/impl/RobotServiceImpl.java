package tw.org.spfc.android.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import tw.org.spfc.android.R;
import tw.org.spfc.android.service.RobotService;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.service.ServiceException;

public class RobotServiceImpl extends BaseServiceImpl implements RobotService {
	private Context context;
	
	public RobotServiceImpl(Context context) {
		this.context = context;
	}
	
	public String talk(String message) throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("message", message));
		
		String json = doPostForJSON(context.getString(R.string.assistantURL), params, Integer.parseInt(context.getString(R.string.httpTimeout)));
		
		try {
			JSONObject jsonResult = new JSONObject(json);
			String reply = jsonResult.getString("message");
			return new String(reply.getBytes("ISO-8859-1"), "UTF-8");
		} catch (JSONException e) {
			throw new ServiceException(e);
		} catch (UnsupportedEncodingException e) {
			throw new ServiceException(e);
		}
	}

}
