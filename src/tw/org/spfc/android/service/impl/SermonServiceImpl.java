package tw.org.spfc.android.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;
import csiebug.util.DateFormatException;
import csiebug.util.DateFormatUtility;
import csiebug.util.FileUtility;
import csiebug.util.StringUtility;
import tw.org.spfc.android.R;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.domain.Sermon;
import tw.org.spfc.domain.pojoImpl.SermonImpl;
import tw.org.spfc.service.SermonService;

public class SermonServiceImpl extends BaseServiceImpl implements SermonService {
	private Context context;
	
	public SermonServiceImpl(Context context) {
		this.context = context;
	}
	
	public List<Sermon> getSermonFiles(int maxLength, boolean cache) throws ServiceException {
		try {
			//如果網路有通,則透過網路取得講道列表
			//如果網路沒通,也需要提供列表,以供離線撥放
			if(AndroidUtility.isNetworkAvailable(context)) {
				try {
					return getRemoteMP3List(maxLength, cache);
				} catch(Exception e) {
					//如果不是網路不通，而是因為網站掛掉的情況下，就要如此做
					Toast.makeText(context, context.getString(R.string.spfc_unavailable), Toast.LENGTH_LONG).show();
					
					List<Sermon> sermons = getLocalMP3List();
					Collections.sort(sermons, new Comparator<Sermon>() {

						public int compare(Sermon lhs, Sermon rhs) {
							String lName = lhs.getId().substring(0, 6);
							String rName = rhs.getId().substring(0, 6);
							
							return rName.compareTo(lName);
						}});
					
					return sermons;
				}
			} else {
				Toast.makeText(context, context.getString(R.string.offline_mode), Toast.LENGTH_LONG).show();
				
				List<Sermon> sermons = getLocalMP3List();
				Collections.sort(sermons, new Comparator<Sermon>() {

					public int compare(Sermon lhs, Sermon rhs) {
						String lName = lhs.getId().substring(0, 6);
						String rName = rhs.getId().substring(0, 6);
						
						return rName.compareTo(lName);
					}});
				
				return sermons;
			}
		} catch (Exception e) {
			//如果發生任何的Exception可能和cache檔案的內容有關,強迫刪除cache檔案(將檔案更名做debug用)
			//讓這個錯誤避免變成永遠無法使用的錯誤
			File cacheFile = SPFCFileSystem.getSermonListCacheFile(context);
			if(cacheFile.exists()) {
				cacheFile.renameTo(SPFCFileSystem.getSermonListCacheErrorFile(context));
			}
			
			throw new ServiceException(e);
		}
	}
	
	/**
	 * 取得sdcard的檔案列表
	 * @return
	 */
	private List<Sermon> getLocalMP3List() {
		List<Sermon> list = new ArrayList<Sermon>();
		
		File localSermonFolder = SPFCFileSystem.getSermonFolder(context);
		
		if(localSermonFolder != null && localSermonFolder.exists()) {
			String[] sermonFileNames = localSermonFolder.list();
			
			File cacheFile = SPFCFileSystem.getSermonListCacheFile(context);
			String html = "";
			
			if(cacheFile != null && cacheFile.exists()) {
				try {
					html = FileUtility.getTextFileContent(cacheFile, "UTF-8");
				} catch (IOException e) {
					//這是輔助功能為了在離線狀態可以產生講題和講員,所以發生Exception就不處理了
					Log.e(getId(), "Get cache file error!", e);
				}
			}
			
			for(int i = 0; i < sermonFileNames.length; i++) {
				Sermon sermon = new SermonImpl();
				String sermonExtentionFileName = context.getString(R.string.sermonExtensionFileName);
				
				if(sermonExtentionFileName.startsWith(".")) {
					sermon.setId(sermonFileNames[i].replace(context.getString(R.string.sermonExtensionFileName), ""));
				} else {
					sermon.setId(sermonFileNames[i].replace("." + context.getString(R.string.sermonExtensionFileName), ""));
				}
				
				if(AssertUtility.isNotNullAndNotSpace(html)) {
					sermon.setTitle(findNameFromHTML(html, sermon.getId()));
				}
				
				list.add(sermon);
			}
		}
		
		return list;
	}
	
	private String findNameFromHTML(String html, String sermonId) {
		//TODO: 用parse html的方式取得顯示文字
		String name = sermonId;
		
		String prefix = "20" + sermonId.substring(0, 2) + "." + sermonId.substring(2, 4) + "." + sermonId.substring(4, 6);
		String[] htmlLines = html.split("\n");
		for(int i = 0; i < htmlLines.length; i++) {
			if(StringUtility.ltrim(htmlLines[i]).startsWith("<a href=\"/j15/index.php/video")) {
				String result = StringUtility.ltrim(htmlLines[i+ 1]).replace("</a>", "");
				if(result.startsWith(prefix)) {
					return result;
				}
			}
		}
		
		return name;
	}
	
	private List<Sermon> getRemoteMP3List(int maxLength, boolean cache) throws DateFormatException, IOException, ServiceException {
		//TODO: 先用parse html的方式取得顯示文字；教會網站改版時必須修改
		List<Sermon> list = new ArrayList<Sermon>();
		
		String html = "";
		if(cache) {
			html = getHTMLWithCache();
		} else {
			html = getRemoteHTML();
		}
		String[] htmlLines = html.split("\n");
		for(int i = 0; i < htmlLines.length; i++) {
			if(list.size() < maxLength) {
				if(StringUtility.ltrim(htmlLines[i]).startsWith("<a href=\"/j15/index.php/video")) {
					String name = StringUtility.ltrim(htmlLines[i+ 1]).replace("</a>", "");
					
					try {
						Sermon sermon = new SermonImpl();
						sermon.setTitle(name);
						setSermonId(sermon, name, DateFormatUtility.toCalendar(name.substring(0, 10), 102));
						list.add(sermon);
					} catch(Exception e) {
						//網頁上有放日期格式錯誤的檔案,所以略過這種檔案
						Log.e(getId(), name + "日期格式錯誤", e);
					}
				}
			} else {
				break;
			}
		}
		
		return list;
	}
	
	private void setSermonId(Sermon sermon, String name, Calendar calendar) throws DateFormatException {
		String sunFooter = "_Sun";
		String satFooter = "_Sat";
		String friFooter = "_Fri";
		String thuFooter = "_Thu";
		String wedFooter = "_Wed";
		String tueFooter = "_Tue";
		String monFooter = "_Mon";
		
		String weeklyPaperBaseURL = "http://www.spfctw.org/image/pdf/";
		String weeklyPaperViceFileName = ".pdf";
		String pptBaseURL = "http://www.spfctw.org/image/messageoutline/";
		String pptViceFileName = "ppt.pdf";
		
		//TODO 因為教會網站空間變更,故檔案位置改變
		//目前新位置的最舊檔案是到2013/10/20
		String weeklyPaperBaseURLOld = "http://www.spfc.org.tw/j15/images/pdf/";
		String weeklyPaperBaseURLNew = "http://www.spfctw.org/image/pdf/";
//		"http://www.spfctw.org/image/messageoutline/20140713ppt.pdf";
		
		Calendar base = Calendar.getInstance();
		base.set(Calendar.YEAR, 2013);
		base.set(Calendar.MONTH, 9);
		base.set(Calendar.DAY_OF_MONTH, 19);
		if(calendar.before(base)) {
			weeklyPaperBaseURL = weeklyPaperBaseURLOld;
		} else {
			weeklyPaperBaseURL = weeklyPaperBaseURLNew;
		}
		
		String dateString = DateFormatUtility.getDisplayDate(calendar, 12);
		switch (calendar.get(Calendar.DAY_OF_WEEK)) {
			case Calendar.SUNDAY:
				setSermonId(sermon, dateString, sunFooter, name);
				sermon.addAttachedFile(weeklyPaperBaseURL + dateString.substring(0, 2) + "_" + dateString.substring(2, 6) + weeklyPaperViceFileName);
				String pptURL = pptBaseURL + calendar.get(Calendar.YEAR) + dateString.substring(2, 6) + pptViceFileName;
				sermon.addAttachedFile(pptURL);
				break;
			case Calendar.SATURDAY:
				setSermonId(sermon, dateString, satFooter, name);
				calendar.add(Calendar.DATE, 1);
				dateString = DateFormatUtility.getDisplayDate(calendar, 12);
				sermon.addAttachedFile(weeklyPaperBaseURL + dateString.substring(0, 2) + "_" + dateString.substring(2, 6) + weeklyPaperViceFileName);
				pptURL = pptBaseURL + calendar.get(Calendar.YEAR) + dateString.substring(2, 6) + pptViceFileName;
				sermon.addAttachedFile(pptURL);
				break;
			case Calendar.FRIDAY:
				setSermonId(sermon, dateString, friFooter, name);
				break;
			case Calendar.THURSDAY:
				setSermonId(sermon, dateString, thuFooter, name);
				break;
			case Calendar.WEDNESDAY:
				setSermonId(sermon, dateString, wedFooter, name);
				break;
			case Calendar.TUESDAY:
				setSermonId(sermon, dateString, tueFooter, name);
				break;
			case Calendar.MONDAY:
				setSermonId(sermon, dateString, monFooter, name);
				break;
			default:
				break;
		}
	}
	
	private void setSermonId(Sermon sermon, String dateString, String dateFooter, String name) {
		//TODO: 先用parse html的方式取得顯示文字
		//這裡要運作正確,必須要網址命名規則固定,且網頁上的文字規則也固定才行
		//判斷網頁文字規則,以便套用網址命名規則
		//以後諸如佈道會,退修會甚至分兩堂,三堂,四堂,都要靠固定下來的文字來做判斷
		String firstChapelHTMLText = "第一堂";
		String firstChapelFooter = "_1";
		String secondChapelHTMLText = "第二堂";
		String secondChapelFooter = "_2";
		String thirdChapelHTMLText = "第三堂";
		String thirdChapelFooter = "_3";
		String fourthChapelHTMLText = "第四堂";
		String fourthChapelFooter = "_4";
		String fifthChapelHTMLText = "第五堂";
		String fifthChapelFooter = "_5";
		String sixthChapelHTMLText = "第六堂";
		String sixthChapelFooter = "_6";
		String seventhChapelHTMLText = "第七堂";
		String seventhChapelFooter = "_7";
		String eighthChapelHTMLText = "第八堂";
		String eighthChapelFooter = "_8";
		String ninthChapelHTMLText = "第九堂";
		String ninthChapelFooter = "_9";
		String tenthChapelHTMLText = "第十堂";
		String tenthChapelFooter = "_10";
		String elevenChapelHTMLText = "第十一堂";
		String elevenChapelFooter = "_11";
		String twelfChapelHTMLText = "第十二堂";
		String twelfChapelFooter = "_12";
		String baptismHTMLText = "洗禮暨入會禮拜";
		String baptismFooter = "_BaptismInitiation";
		
		if(name.indexOf(firstChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + firstChapelFooter);
		} else if(name.indexOf(secondChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + secondChapelFooter);
		} else if(name.indexOf(thirdChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + thirdChapelFooter);
		} else if(name.indexOf(fourthChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + fourthChapelFooter);
		} else if(name.indexOf(fifthChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + fifthChapelFooter);
		} else if(name.indexOf(sixthChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + sixthChapelFooter);
		} else if(name.indexOf(seventhChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + seventhChapelFooter);
		} else if(name.indexOf(eighthChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + eighthChapelFooter);
		} else if(name.indexOf(ninthChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + ninthChapelFooter);
		} else if(name.indexOf(tenthChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + tenthChapelFooter);
		} else if(name.indexOf(elevenChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + elevenChapelFooter);
		} else if(name.indexOf(twelfChapelHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + twelfChapelFooter);
		} else if(name.indexOf(baptismHTMLText) != -1) {
			sermon.setId(dateString + dateFooter + baptismFooter);
		} else {
			sermon.setId(dateString + dateFooter);
		}
	}
	
	private String getHTMLWithCache() throws DateFormatException, IOException, ServiceException {
		String html = "";
		
		File cacheFolder = SPFCFileSystem.getCacheFolder(context);
		
		if(cacheFolder != null && (cacheFolder.exists() || cacheFolder.mkdirs())) {
			File cacheFile = SPFCFileSystem.getSermonListCacheFile(context);
			
			if(cacheFile != null) {
				SPFCPreference preference = new SPFCPreference(context);
				
				if(!shouldRefreshCache(cacheFile, preference)) {
					html = FileUtility.getTextFileContent(cacheFile, "UTF-8");
				} else {
					//將資料寫到local檔案當cache
					HttpResponse httpResponse = getRemoteResponse();
					if(httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						InputStream inputStream = null;
						FileOutputStream outputStream = null;
						
						try {
							if(cacheFile.exists()) {
								cacheFile.delete();
							}
							cacheFile.createNewFile();
							
							inputStream = httpResponse.getEntity().getContent();
							outputStream = new FileOutputStream(cacheFile);
							byte buffer[] = new byte[1];
							while(inputStream.read(buffer) != -1) {
								outputStream.write(buffer);
							}
						      
						    // 將緩衝區中的資料全部寫出 
						    outputStream.flush();
						    
						    //紀錄更新快取的時間
						    preference.setRefreshSermonListTime(DateFormatUtility.getDisplayDate(Calendar.getInstance(), Integer.parseInt(context.getString(R.string.dateFormat))));
						    
						    html = FileUtility.getTextFileContent(cacheFile, "UTF-8");
						} finally {
							if(inputStream != null) {
								inputStream.close();
							}
							
							if(outputStream != null) {
								outputStream.close();
							}
						}
				    } else {
						throw new RuntimeException(httpResponse.getStatusLine().getReasonPhrase());
					}
				}
			} else {
				html = getRemoteHTML();
			}
		} else {
			html = getRemoteHTML();
		}
		
		return html;
	}
	
	private boolean shouldRefreshCache(File file, SPFCPreference preference) throws DateFormatException {
		if(!file.exists()) {
			return true;
		} else {
			//如果快取時間大於等於7天,那就需要更新列表
			String refreshSermonListTime = preference.getRefreshSermonListTime();
			if(AssertUtility.isNotNullAndNotSpace(refreshSermonListTime)) {
				int now = Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
				int cacheTime = DateFormatUtility.toCalendar(preference.getRefreshSermonListTime(), Integer.parseInt(context.getString(R.string.dateFormat))).get(Calendar.DAY_OF_YEAR);
				
				return (now - cacheTime) >= 7;
			} else {
				return true;
			}
		}
	}
	
	private HttpResponse getRemoteResponse() throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		//下拉選單,選0表示查詢全部
		params.add(new BasicNameValuePair("limit", "0"));
		params.add(new BasicNameValuePair("id", "36"));
		params.add(new BasicNameValuePair("sectionid", "9"));
		params.add(new BasicNameValuePair("task", "category"));
		params.add(new BasicNameValuePair("filter_order", ""));
		params.add(new BasicNameValuePair("filter_order_Dir", ""));
		params.add(new BasicNameValuePair("limitstart", "0"));
		params.add(new BasicNameValuePair("viewcache", "0"));
		
		return doPost(context.getString(R.string.sermonSearchURL), params, Integer.parseInt(context.getString(R.string.httpTimeout)));
	}
	
	private String getRemoteHTML() throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		//下拉選單,選0表示查詢全部
		params.add(new BasicNameValuePair("limit", "0"));
		params.add(new BasicNameValuePair("id", "36"));
		params.add(new BasicNameValuePair("sectionid", "9"));
		params.add(new BasicNameValuePair("task", "category"));
		params.add(new BasicNameValuePair("filter_order", ""));
		params.add(new BasicNameValuePair("filter_order_Dir", ""));
		params.add(new BasicNameValuePair("limitstart", "0"));
		params.add(new BasicNameValuePair("viewcache", "0"));
		
		return doPostForTextContent(context.getString(R.string.sermonSearchURL), params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout)));
	}
	
	/**
	 * 用程式計算的檔案列表
	 * @param maxLength
	 * @return
	 * @throws DateFormatException
	 */
	@Deprecated
	public List<Sermon> getMP3List(int maxLength) throws DateFormatException {
		List<Sermon> list = new ArrayList<Sermon>();
		
		Calendar now = Calendar.getInstance();
		int thisYear = now.get(Calendar.YEAR);
		
		for(int i = thisYear; i >= 2010; i--) {
			List<Sermon> temp;
			if(i == thisYear) {
				temp = getAllYearMP3List(thisYear, now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
			} else {
				temp = getAllYearMP3List(i, Calendar.DECEMBER, 31);
			}
			
			if((list.size() + temp.size()) <= maxLength) {
				list.addAll(temp);
			} else {
				int remain = maxLength - list.size();
				for(int j = 0; j < remain; j++) {
					list.add(temp.get(j));
				}
				
				break;
			}
		}
		
		return list;
	}
	
	private List<Sermon> getAllYearMP3List(int year, int month, int day) throws DateFormatException {
		String sunFooter = "_Sun";
		String satFooter = "_Sat";
		List<Sermon> list = new ArrayList<Sermon>();
		
		Calendar lastDate = Calendar.getInstance();
		lastDate.set(Calendar.YEAR, year);
		lastDate.set(Calendar.MONTH, month);
		lastDate.set(Calendar.DAY_OF_MONTH, day);
		int dayOfYear = lastDate.get(Calendar.DAY_OF_YEAR);
		
		Calendar firstSermonDate = null;
		if(year == 2010) {
			firstSermonDate = Calendar.getInstance();
			lastDate.set(Calendar.YEAR, year);
			lastDate.set(Calendar.MONTH, Calendar.OCTOBER);
			lastDate.set(Calendar.DAY_OF_MONTH, 9);
		}
		
		Calendar cursor = Calendar.getInstance();
		cursor.set(Calendar.YEAR, year);
		for(int i = dayOfYear; i > 0; i--) {
			cursor.set(Calendar.DAY_OF_YEAR, i);
			
			if(year != 2010 || (cursor.get(Calendar.DAY_OF_YEAR) >= firstSermonDate.get(Calendar.DAY_OF_YEAR))) {
				Sermon sermon = new SermonImpl();
				switch (cursor.get(Calendar.DAY_OF_WEEK)) {
					case Calendar.SUNDAY:
						sermon.setId(DateFormatUtility.getDisplayDate(cursor, 12) + sunFooter);
						list.add(sermon);
						break;
					case Calendar.SATURDAY:
						sermon.setId(DateFormatUtility.getDisplayDate(cursor, 12) + satFooter);
						list.add(sermon);
						break;
					default:
						break;
				}
			} else {
				break;
			}
		}
		
		return list;
	}

	public List<Sermon> searchSermonFiles(String keyword)
			throws ServiceException {
		List<Sermon> list = new ArrayList<Sermon>();
		
		//給一個極大數來實作無限大,用來取得所有的檔案列表
		int infinity = Integer.MAX_VALUE;
		List<Sermon> sermons = getSermonFiles(infinity, false);
		for(int i = 0; i < sermons.size(); i++) {
			if(sermons.get(i).getTitle().indexOf(keyword) != -1) {
				list.add(sermons.get(i));
			}
		}
		
		return list;
	}

	public List<Sermon> searchSermonFiles(List<String> keywords)
			throws ServiceException {
		List<Sermon> list = new ArrayList<Sermon>();
		
		//給一個極大數來實作無限大,用來取得所有的檔案列表
		int infinity = Integer.MAX_VALUE;
		List<Sermon> sermons = getSermonFiles(infinity, false);
		for(int i = 0; i < sermons.size(); i++) {
			boolean addFlag = true;
			for(int j = 0; j < keywords.size(); j++) {
				if(sermons.get(i).getTitle().indexOf(keywords.get(j)) == -1) {
					addFlag = false;
					break;
				}
			}
			
			if(addFlag) {
				list.add(sermons.get(i));
			}
		}
		
		return list;
	}
}
