package tw.org.spfc.android.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import tw.org.spfc.android.R;
import tw.org.spfc.android.service.FacebookService;
import csiebug.android.service.impl.BaseServiceImpl;
import csiebug.service.ServiceException;

public class FacebookServiceImpl extends BaseServiceImpl implements FacebookService {
	private Context context;
	
	public FacebookServiceImpl(Context context) {
		this.context = context;
	}
	
	public String getPicture(String facebookType) throws ServiceException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("facebookType", facebookType));
		return doPostForTextContent(context.getString(R.string.facebookPictureURL), params, "UTF-8", Integer.parseInt(context.getString(R.string.httpTimeout)));
	}

}
