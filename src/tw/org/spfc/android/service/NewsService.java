package tw.org.spfc.android.service;

import java.util.List;

import tw.org.spfc.domain.News;
import csiebug.service.ServiceException;

public interface NewsService extends tw.org.spfc.service.NewsService {
	/**
	 * 教會服務中斷的緊急公告
	 * @return
	 * @throws ServiceException
	 */
	List<News> getEmergencyNews() throws ServiceException;
}
