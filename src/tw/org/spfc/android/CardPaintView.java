package tw.org.spfc.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.domain.Album;
import tw.org.spfc.service.PhotoService;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.google.analytics.tracking.android.EasyTracker;

import csiebug.android.ContextMethod;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.FileUtility;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

public class CardPaintView extends csiebug.android.PaintView {
	private Context context;
	private String exportFileName = "";
	private boolean facebookEnable = false;
	private String[] versionNames;
	private TextView[] verses;
	private String verse;
	private GoldenWordCardActivity activity;
	
	public CardPaintView(Context c, AttributeSet attrs) {
		super(c, attrs);
		context = c;
		
		initPaint();
	}
	
	public CardPaintView(Context c) {
		super(c);
		context = c;
		
		initPaint();
	}
	
	public void setExportFileName(String fileName) {
		exportFileName = fileName;
	}
	
	public void setFacebookEnable(boolean flag) {
		facebookEnable = flag;
		
		if(facebookEnable) {
			versionNames = new String[10];
		} else {
			versionNames = new String[9];
		}
		
		versionNames[0] = context.getString(R.string.choose_wallpaper);
		versionNames[1] = context.getString(R.string.verse_position);
		versionNames[2] = context.getString(R.string.verse_color);
		versionNames[3] = context.getString(R.string.clear_card);
		versionNames[4] = context.getString(R.string.export_card);
		versionNames[5] = context.getString(R.string.black_pen);
		versionNames[6] = context.getString(R.string.white_pen);
		versionNames[7] = context.getString(R.string.yellow_pen);
		versionNames[8] = context.getString(R.string.eraser);
		
		if(facebookEnable) {
			versionNames[9] = context.getString(R.string.share_to_facebook);
		}
	}
	
	public void setTextViews(TextView[] textViews) {
		this.verses = textViews;
	}
	
	public void setVerse(String verse) {
		this.verse = verse;
	}
	
	public void setActivity(GoldenWordCardActivity activity) {
		this.activity = activity;
	}

	@Override
	protected void onLongClickEvent() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    
		builder.setItems(versionNames, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(which == 0) {
					ArrayList<String> images = new ArrayList<String>();
					if(AndroidUtility.isNetworkAvailable(context)) {
						PhotoService photoService = ServiceProxyFactory.createPhotoService(context);
						try {
							Album album = photoService.getWallpapers();
							
							for(int i = 0; i < album.getPhotos().size(); i++) {
								String url = album.getPhotos().get(i).getSrc().replace("/wallpaper/", "/wallpaper/s/").replace(".jpg", "s.jpg");
								String[] urlPart = url.split("/");
								
								File s = SPFCFileSystem.getCardTemplateFile(context, urlPart[urlPart.length - 1]);
								if(s == null || !s.exists()) {
									InputStream inputStream = null;
									
							        try{
							            inputStream = (InputStream)new URL(url).getContent();
							            
							            FileUtility.writeFile(inputStream, SPFCFileSystem.getCardTemplateFile(context, urlPart[urlPart.length - 1]).getPath());
							        } catch (IOException e) {
							        	throw new RuntimeException(e);
									} finally {
							        	if(inputStream != null) {
							        		try {
												inputStream.close();
											} catch (IOException e) {
												throw new RuntimeException(e);
											}
							        	}
							        }
								}
								
								images.add(SPFCFileSystem.getCardTemplateFile(context, urlPart[urlPart.length - 1]).getPath());
								//images.add(album.getPhotos().get(i).getSrc().replace("/wallpaper/", "/wallpaper/s/").replace(".jpg", "s.jpg"));
								//images.add(album.getPhotos().get(i).getSrc());
							}
						} catch (ServiceException e) {
							throw new RuntimeException(e);
						}
					}
					
					Intent intent = new Intent(context, WallpaperChooseActivity.class);
					Bundle bundle = new Bundle();
					bundle.putStringArrayList("wallpapers", images);
					intent.putExtras(bundle);
					activity.startActivityForResult(intent, 1);
				} else if(which == 1) {
					String[] positions = new String[]{context.getString(R.string.top_left), context.getString(R.string.top_right), context.getString(R.string.bottom_left), context.getString(R.string.bottom_right)};
					AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
					builder2.setItems(positions, new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
							for(int i = 0; i < verses.length; i++) {
								if(i == which) {
									verses[i].setText(verse);
								} else {
									verses[i].setText("");
								}
							}
						}
					});
					
					builder2.create();
					
					builder2.show();
				} else if(which == 2) {
					String[] colors = new String[]{context.getString(R.string.white), context.getString(R.string.black)};
					AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
					builder2.setItems(colors, new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
							if(which == 0) {
								for(int i = 0; i < verses.length; i++) {
									verses[i].setTextColor(context.getResources().getColor(android.R.color.white));
								}
							} else if(which == 1) {
								for(int i = 0; i < verses.length; i++) {
									verses[i].setTextColor(context.getResources().getColor(android.R.color.black));
								}
							}
						}
					});
					
					builder2.create();
					
					builder2.show();
				} else if(which == 3) {
					EasyTracker.getTracker().sendEvent("paintCard", "clear", "", (long)0);
					
					clear();
				} else if(which == 4) {
					EasyTracker.getTracker().sendEvent("paintCard", "save", "", (long)0);
					
					try {
						File tempFile = saveBitmap(takeScreenshot());
						saveToGallery(tempFile);
						tempFile.deleteOnExit();
						//ContextMethod.toast(context, context.getString(R.string.writing_export_hint) + "(" + context.getString(R.string.note_file_in) + context.getString(R.string.bibleReadingNotesFolder) + ")");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				} else if(which == 5) {
					setEraseMode(false);
					setStrokeColor(Color.BLACK);
					setStrokeWidth(Float.parseFloat("4"));
				} else if(which == 6) {
					setEraseMode(false);
					setStrokeColor(Color.WHITE);
					setStrokeWidth(Float.parseFloat("4"));
				} else if(which == 7) {
					setEraseMode(false);
					setStrokeColor(Color.YELLOW);
					setStrokeWidth(Float.parseFloat("4"));
				} else if(which == 8) {
					setEraseMode(true);
					setStrokeWidth(Float.parseFloat("40"));
				} else if(which == 9) {
					Bitmap image = takeScreenshot();
		            Request request = Request.newUploadPhotoRequest(Session.getActiveSession(), image, new Request.Callback() {
		                public void onCompleted(Response response) {
		                    ContextMethod.toast(context, context.getString(R.string.facebook_photo_uploaded));
		                }
		            });
		            request.executeAsync();
				}
			}
	    });
	    builder.create();
	    
	    builder.show();
	}
	
	public void initPaint() {
		try {
			clear();
		} catch(Exception e) {
			
		}
		setEraseMode(false);
		setStrokeColor(Color.YELLOW);
		setStrokeWidth(Float.parseFloat("4"));
	}
	
	public Bitmap takeScreenshot() {
		View rootView = getRootView();
		rootView.setDrawingCacheEnabled(true);
		return rootView.getDrawingCache();
	}
	
	public File saveBitmap(Bitmap bitmap) throws IOException {
		if(exportFileName.equals("")) {
			exportFileName = Calendar.getInstance().getTimeInMillis() + ".png";
		}
		
		File cardFolder = SPFCFileSystem.getCardFolder(context);
		
		if(cardFolder != null && (cardFolder.exists() || cardFolder.mkdir())) {
			File imagePath = SPFCFileSystem.getCardFile(context, exportFileName);
		    FileOutputStream fos = null;
		    try {
		        fos = new FileOutputStream(imagePath);
		        bitmap.compress(CompressFormat.JPEG, 100, fos);
		        fos.flush();
		    } finally {
		    	if(fos != null) {
		    		fos.close();
		    	}
		    }
		    
		    return imagePath;
		} else {
			throw new RuntimeException("Can't create card folder!");
		}
	}
	
	public void saveToGallery(File file) {
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    Uri contentUri = Uri.fromFile(file);
	    mediaScanIntent.setData(contentUri);
	    context.sendBroadcast(mediaScanIntent);
	}
}
