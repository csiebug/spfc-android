package tw.org.spfc.android;

import java.util.Calendar;

import csiebug.android.AbstractBroadcastReceiver;
import android.app.Service;
import android.util.Log;

public class DownloadSermonReceiver extends AbstractBroadcastReceiver {

	@Override
	protected Class<? extends Service> getInvokeService() {
		Calendar now = Calendar.getInstance();
		Log.d(this.getClass().getName(), "DownloadSermonService invoke! at time = " + now.get(Calendar.YEAR) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.DAY_OF_MONTH) + " " + now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE) + ":" + now.get(Calendar.SECOND));
		return DownloadSermonService.class;
	}
	
}
