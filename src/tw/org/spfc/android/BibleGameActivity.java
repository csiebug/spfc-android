package tw.org.spfc.android;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import csiebug.android.util.AndroidUtility;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class BibleGameActivity extends BaseActivity {

	@Override
	protected int getLayout() {
		return R.layout.swipe_game;
	}

	@Override
	protected void setAdapters() {}

	@Override
	protected void setListeners() {}
	
	@Override
	protected void onCreateLogic() {
		Bundle bundle = getIntent().getExtras();
		
		String[] imageURLs = bundle.getStringArray("images");
		
		try {
			List<ImageView> imageViews = new ArrayList<ImageView>();
			for(int i = 0; i < imageURLs.length; i++) {
				Log.d(getClass().getName(), "url: " + imageURLs[i]);
				ImageView image = new ImageView(this);
				URL url = new URL(imageURLs[i]);
				URLConnection connection = url.openConnection();
				connection.connect();
				image.setImageBitmap(BitmapFactory.decodeStream(new BufferedInputStream(connection.getInputStream())));
				imageViews.add(image);
			}
			
			Gallery games = (Gallery)findViewById(R.id.swipe_game);
			Display display = AndroidUtility.getResolution(this);
			games.setAdapter(new ImageAdapter(imageViews, ScaleType.CENTER_INSIDE, display.getWidth(), display.getHeight()));
			
			toast(getString(R.string.swipe_hint));
		} catch(Exception e) {
			Log.e(getClass().getName(), "Get images error!", e);
		}
	}
}
