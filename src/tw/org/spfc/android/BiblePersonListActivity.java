package tw.org.spfc.android;

import java.util.List;
import java.util.Map;

import bible.domain.BibleBook;
import bible.domain.BiblePerson;
import bible.domain.pojoImpl.BibleBookImpl;
import csiebug.android.AbstractUIHandler;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.translate.Language;
import csiebug.util.translate.TranslateUtility;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.service.BibleService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BiblePersonListActivity extends BaseListActivity {
	private String[] names;
	
	private Handler handler = new AbstractUIHandler() {

		@Override
		public void debugLogic(Exception e) {
			BiblePersonListActivity.this.debugLogic(e);
		}

		@Override
		public void handlerLogic(Message msg) {
			SPFCPreference preference = new SPFCPreference(BiblePersonListActivity.this);
			ListAdapter adapter;
			
			int layout = FontSizeLayout.getLayoutForList(preference);
			
			adapter = new ArrayAdapter<String>(BiblePersonListActivity.this, layout, R.id.itemText, names);
			
			BiblePersonListActivity.this.getListView().setAdapter(adapter);
		}
		
	};
	
	@Override
	protected String[] getListItemTexts() {
		return names;
	}
	
	@Override
	protected Spanned[] getListItemSpanneds() {
		return null;
	}

	@Override
	protected String[] getContextMenuOptionsByArray() {
		return null;
	}

	@Override
	protected List<String> getContextMenuOptionsByList() {
		return null;
	}

	@Override
	protected Map<Integer, String> getContextMenuOptionsByMap() {
		return null;
	}

	@Override
	protected void contextItemSelectedLogic(MenuItem item) {
	}

	@Override
	protected String getContextMenuHeaderTitle() {
		return null;
	}

	@Override
	protected int getContextMenuHeaderIcon() {
		return 0;
	}

	@Override
	protected void putListItemToBundle(Bundle bundle, int position) {
	}

	@Override
	protected Class<?> getListItemEditActivityClass() {
		return null;
	}

	@Override
	protected int getRequestCode() {
		return 0;
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
	}

	@Override
	protected void setListeners() {
	}
	
	@Override
	protected Integer getLayout() {
		return R.layout.result_list;
	}
	
	@Override
	protected void onCreateLogic() {
		try {
			final int book = getIntent().getExtras().getInt("book");
			int chapter = getIntent().getExtras().getInt("chap");
			int verse = getIntent().getExtras().getInt("sec");
			boolean cacheHitFlag = false;
			//final Locale phoneLocale = getResources().getConfiguration().locale;
			final SPFCPreference preference = new SPFCPreference(this);
			final int version = preference.getBibleVersion();
			
			final BibleService service = ServiceProxyFactory.createBibleService(this, BibleDAOImpl.CHINESE_UNION_VERSION, BibleCatagoryDAOImpl.CHINESE_BIBLE);
			List<BiblePerson> people = service.getPeople(service.getVerse(book, chapter, verse));
			if(people.size() == 0) {
				people = service.getPeople(service.getChapter(book, chapter));
				
				if(people.size() == 0) {
					//搜尋本書聖經人物很耗時間,所以用快取把上次的查詢存起來
					if(book == preference.getBook() && preference.getBookBiblePeople().length != 0) {
						names = preference.getBookBiblePeople();
						cacheHitFlag = true;
					} else {
						Thread thread = new Thread(new BaseProgressDialogRunnable(BiblePersonListActivity.this, handler) {
							
							@Override
							protected void runLogic() {
								try {
									BibleBook voBook = new BibleBookImpl();
									voBook.setId("" + book);
									voBook.setBookId(book);
									List<BiblePerson> people = service.getPeople(voBook);
									
									names = new String[people.size()];
									for(int i = 0; i < people.size(); i++) {
//										if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//											names[i] = people.get(i).getName();
//										} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//											names[i] = people.get(i).getName();
//										} else {
//											names[i] = people.get(i).getId();
//										}
										if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
											names[i] = people.get(i).getName();
										} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
											names[i] = people.get(i).getName();
										} else if(version == BibleDAOImpl.AA && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.PORTUGUESE);
										} else if(version == BibleDAOImpl.DN1933 && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.DANISH);
										} else if(version == BibleDAOImpl.DNB1930 && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.NORWEGIAN);
										} else if(version == BibleDAOImpl.KAR && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.HUNGARIAN);
										} else if(version == BibleDAOImpl.LSG && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.FRENCH);
										} else if(version == BibleDAOImpl.LUTH1545 && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.GERMAN);
										} else if(version == BibleDAOImpl.R1933 && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.FINNISH);
										} else if(version == BibleDAOImpl.RMNN && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.ROMANIAN);
										} else if(version == BibleDAOImpl.RVA && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.SPANISH);
										} else if(version == BibleDAOImpl.SV1917 && AndroidUtility.isNetworkAvailable(BiblePersonListActivity.this)) {
											names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.SWEDISH);
										} else {
											names[i] = people.get(i).getId();
										}
									}
									
									//查詢結果存入快取
									preference.setBookBiblePeople(book, names);
									
									handler.sendEmptyMessage(0);
								} catch (ServiceException e) {
									throw new RuntimeException(e);
								}
							}
						});
						thread.start();
					}
					((TextView)findViewById(R.id.function_name)).setText(getString(R.string.book_people));
				} else {
					((TextView)findViewById(R.id.function_name)).setText(getString(R.string.chapter_people));
				}
			} else {
				((TextView)findViewById(R.id.function_name)).setText(getString(R.string.verse_people));
			}
			
			if(!cacheHitFlag) {
				names = new String[people.size()];
				for(int i = 0; i < people.size(); i++) {
//					if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//						names[i] = people.get(i).getName();
//					} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//						names[i] = people.get(i).getName();
//					} else {
//						names[i] = people.get(i).getId();
//					}
					if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
						names[i] = people.get(i).getName();
					} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
						names[i] = people.get(i).getName();
					} else if(version == BibleDAOImpl.AA && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.PORTUGUESE);
					} else if(version == BibleDAOImpl.DN1933 && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.DANISH);
					} else if(version == BibleDAOImpl.DNB1930 && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.NORWEGIAN);
					} else if(version == BibleDAOImpl.KAR && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.HUNGARIAN);
					} else if(version == BibleDAOImpl.LSG && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.FRENCH);
					} else if(version == BibleDAOImpl.LUTH1545 && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.GERMAN);
					} else if(version == BibleDAOImpl.R1933 && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.FINNISH);
					} else if(version == BibleDAOImpl.RMNN && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.ROMANIAN);
					} else if(version == BibleDAOImpl.RVA && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.SPANISH);
					} else if(version == BibleDAOImpl.SV1917 && AndroidUtility.isNetworkAvailable(this)) {
						names[i] = TranslateUtility.translate(people.get(i).getId(), Language.ENGLISH, Language.SWEDISH);
					} else {
						names[i] = people.get(i).getId();
					}
				}
			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected Integer getListItemLayout() {
		int layout = FontSizeLayout.getLayoutForList(new SPFCPreference(this));
		
		return layout;
	}
	
	@Override
	protected Integer getListItemTextViewID() {
		return R.id.itemText;
	}
	
	@Override
	protected void onListItemClickLogic(ListView l, View v, int position,
			long id) {
		if(!AndroidUtility.isNetworkAvailable(this)) {
			toast(getString(R.string.network_unavailable));
		} else {
			Intent wiki = new Intent(this, WikiActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("name", names[position]);
			wiki.putExtras(bundle);
			startActivity(wiki);
		}
	}
}
