package tw.org.spfc.android;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.EvernoteUtil;
import com.evernote.client.android.OnClientCallback;
import com.evernote.edam.type.Note;
import com.evernote.thrift.transport.TTransportException;
import com.facebook.FacebookRequestError;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;
import csiebug.util.FileUtility;
import csiebug.util.StringUtility;
import bible.domain.BibleVerse;
import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.FacebookService;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.service.BibleService;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class BibleNoteActivity extends BaseFacebookActivity {
	private BibleService service;
	private String engs;
	private int chapter;
	private int section;
	private Boolean lockFlag = false;
	
	@Override
	protected int getLayout() {
		return R.layout.bible_note;
	}

	@Override
	protected void setListeners() {
		findViewById(R.id.share_to_facebook).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
			@Override
			protected void onClickLogic(View arg0) {
				String verse = ((TextView)findViewById(R.id.function_name)).getText().toString();
				String note = ((TextView)findViewById(R.id.bible_note)).getText().toString();
				String pictureURL = getString(R.string.spfcAppIcon);
				try {
					pictureURL = ServiceProxyFactory.createFacebookService(BibleNoteActivity.this).getPicture(FacebookService.FACEBOOK_MY_BIBLE_NOTE);
				} catch (ServiceException e) {
					Log.e(getClass().getName(), "Get picture URL error!", e);
				}
				
				publishFeedDialog(getString(R.string.facebook_my_bible_note) + " - " + verse, "", note, getString(R.string.spfcHomepage), pictureURL);
			}
		});
		
		findViewById(R.id.share_to_twitter).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				SPFCPreference preference = new SPFCPreference(BibleNoteActivity.this);
				String oauthAccessToken = preference.getTwitterToken();
				String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

				ConfigurationBuilder confbuilder = new ConfigurationBuilder();
				Configuration conf = confbuilder
									.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
									.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
									.setOAuthAccessToken(oauthAccessToken)
									.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
									.setUseSSL(true)
									.build();
				
				Twitter twitter = new TwitterFactory(conf).getInstance();
				
				String verse = ((TextView)findViewById(R.id.function_name)).getText().toString();
				String note = ((TextView)findViewById(R.id.bible_note)).getText().toString();
				try {
					twitter.updateStatus(getString(R.string.facebook_my_bible_note) + " - " + verse + ":\n" + note);
					toast(getString(R.string.twitter_success));
				} catch (TwitterException e) {
					Log.e(getClass().getName(), "TwitterException", e);
				}
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
	}

	@Override
	protected void setAdapters() {
		((LoginButton) findViewById(R.id.login_button)).setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            public void onUserInfoFetched(GraphUser user) {
                setUser(user);
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
	}
	
	@Override
	protected void onPauseLogic() {
		//不知為何onPause會發生兩次,因為可能會存檔兩次的關係,所以做此判斷
		synchronized (lockFlag) {
			if(!lockFlag) {
				lockFlag = true;
				
				File noteFile = SPFCFileSystem.getBibleReadingNote(this, engs + StringUtility.addZero(chapter, 3) + StringUtility.addZero(section, 3) + ".txt");
				if(noteFile != null) {
					try {
						String note = ((TextView)findViewById(R.id.bible_note)).getText().toString();
						if(AssertUtility.isNotNullAndNotSpace(note)) {
							EvernoteSession evernoteSession = EvernoteSession.getInstance(this, getString(R.string.evernote_consumer_key), getString(R.string.evernote_consumer_secret), EvernoteSession.EvernoteService.PRODUCTION);
							if(evernoteSession.isLoggedIn()) {
								Note evernoteNote = new Note();
								String bookName = engs;
								List<String> fhl = service.getBookFHLAbbrNames();
								for(int i = 0; i < fhl.size(); i++) {
									if(fhl.get(i).equalsIgnoreCase(engs)) {
										bookName = service.getBookNames().get(i);
										break;
									}
								}
								
								evernoteNote.setTitle(bookName + ":" + chapter + ":" + section);
								evernoteNote.setContent(EvernoteUtil.NOTE_PREFIX + note + EvernoteUtil.NOTE_SUFFIX);
								
								List<String> tags = new ArrayList<String>();
								tags.add("SPFC");
								tags.add("Bible");
								tags.add(bookName);
								evernoteNote.setTagNames(tags);
								
								try {
									evernoteSession.getClientFactory().createNoteStoreClient().createNote(evernoteNote, new OnClientCallback<Note>() {
									    @Override
									    public void onSuccess(Note data) {
									      toast(getString(R.string.evernote_success));
									    }

									    @Override
									    public void onException(Exception exception) {
									      toast(getString(R.string.evernote_failure));
									    }
									});
								} catch (TTransportException e) {
									toast(getString(R.string.evernote_failure));
								}
							}
							
							if(noteFile.exists()) {
								noteFile.delete();
							}
							noteFile.createNewFile();
							
							FileUtility.writeFile(new ByteArrayInputStream(note.getBytes("UTF-8")), noteFile.getPath());
						} else {
							noteFile.delete();
						}
					} catch (IOException e) {
						toast(getString(R.string.write_exception));
					} catch (ServiceException e1) {
						//取對應語言的聖經書卷名錯誤,就不處理,用預設的書名
					}
				}
			}
		}
	}
	
	@Override
	protected void onCreateLogic() {
		if(AndroidUtility.isNetworkAvailable(this) && isLoggedTwitter()) {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = LayoutParams.WRAP_CONTENT;
			params.width = LayoutParams.WRAP_CONTENT;
			shareToTwitter.setLayoutParams(params);
		} else {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = 0;
			params.width = 0;
			shareToTwitter.setLayoutParams(params);
		}
		
		Bundle bundle = getIntent().getExtras();
		engs = bundle.getString("engs");
		chapter = bundle.getInt("chap");
		section = bundle.getInt("sec");
		((TextView)findViewById(R.id.function_name)).setText(bundle.getString("chapterName") + ":" + section);
		
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		SPFCPreference preference = new SPFCPreference(this);
		int version = preference.getBibleVersion();
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//		}
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
		}
		
		try {
			service = ServiceProxyFactory.createBibleService(this, version, language);
			BibleVerse verse = service.getVerse(bundle.getInt("book"), chapter, section);
			((TextView)findViewById(R.id.verse)).setText(verse.getVerse());
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		try {
			((TextView)findViewById(R.id.bible_note)).setText(getNote());
		} catch (IOException e) {
			toast(getString(R.string.read_exception));
		}
	}
	
	private String getNote() throws IOException {
		String note = "";
		File noteFolder = SPFCFileSystem.getBibleReadingNotesFolder(this);
		if(noteFolder != null && (noteFolder.exists() || noteFolder.mkdirs())) {
			File noteFile = SPFCFileSystem.getBibleReadingNote(this, engs + StringUtility.addZero(chapter, 3) + StringUtility.addZero(section, 3) + ".txt");
			
			if(noteFile != null && noteFile.exists()) {
				note = FileUtility.getTextFileContent(noteFile, "UTF-8");
			}
		}
		
		return note;
	}

	@Override
	protected void enableUI(GraphUser user) {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.WRAP_CONTENT;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.VISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void disableUI() {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = 0;
		params.width = 0;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.INVISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void showPublishResult(GraphObject result,
			FacebookRequestError error) {}

	@Override
	protected String getUserStatusMessage() {
		return null;
	}

	@Override
	protected Bitmap getUserPhoto() {
		return null;
	}
}
