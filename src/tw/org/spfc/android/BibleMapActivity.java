package tw.org.spfc.android;

import java.util.Calendar;
import java.util.List;

import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.service.BibleService;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import bible.domain.BibleBook;
import bible.domain.BibleChapter;
import bible.domain.BibleLocation;
import bible.domain.BibleVerse;
import bible.domain.pojoImpl.BibleBookImpl;
import bible.domain.pojoImpl.BibleChapterImpl;
import bible.domain.pojoImpl.BibleVerseImpl;

import com.google.android.maps.GeoPoint;

import csiebug.android.AbstractUIHandler;
import csiebug.android.google.map.LocationServiceUnavailableException;
import csiebug.service.ServiceException;
import csiebug.util.StringUtility;

public class BibleMapActivity extends BaseMapActivity {
	private int mapStatus = 0;
	
	private Handler handler = new AbstractUIHandler() {
		
		@Override
		public void handlerLogic(Message msg) {
			switch (mapStatus) {
			case 1:
				toast(getString(R.string.bible_chapter_map));
				break;
			case 2:
				toast(getString(R.string.bible_book_map));
				break;
			default:
				break;
			}
		}
		
		@Override
		public void debugLogic(Exception e) {
			BibleMapActivity.this.debugLogic(e);
		}
	};

	@Override
	protected void locationServiceUnavailableLogic(
			LocationServiceUnavailableException e) {
		//這支程式和定位無關,所以就不實作定位取不到的處理
	}

	@Override
	protected int getLayout() {
		return R.layout.map;
	}

	@Override
	protected void setListeners() {
		
	}

	@Override
	protected void setAdapters() {
		
	}

	@Override
	protected int getMapViewId() {
		return R.id.map;
	}
	
	@Override
	protected Double getDefaultLatitude() {
		//預設位置設在耶路撒冷
		return 31.777444;
	}
	
	@Override
	protected Double getDefaultLongitude() {
		//預設位置設在耶路撒冷
		return 35.234935;
	}
	
	@Override
	protected void onCreateLogic() {
		Thread thread = new Thread(new BaseProgressDialogRunnable(this, handler) {
			
			@Override
			protected void runLogic() {
				int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
				int version = BibleDAOImpl.CHINESE_UNION_VERSION;
				
				try {
					BibleService service = ServiceProxyFactory.createBibleService(BibleMapActivity.this, version, language);
					
					Bundle bundle = getIntent().getExtras();
					Integer book = bundle.getInt("book");
					Integer chapter = bundle.getInt("chap");
					Integer section = bundle.getInt("sec");
					String bookName = bundle.getString("chapterName").replaceAll("" + chapter, "");
					Boolean facebookEnable = bundle.getBoolean("facebookEnable");
					
					MapView mapView = (tw.org.spfc.android.MapView)getMapView();
					mapView.setEnableLongClick(true);
					mapView.setPaintView((MapPaintView)findViewById(R.id.paint_view));
					mapView.setExportFileName(service.getBookFHLAbbrNames().get(book) + StringUtility.addZero(chapter, 3) + StringUtility.addZero(section, 3) + Calendar.getInstance().getTimeInMillis() + ".png");
					mapView.setFacebookEnable(facebookEnable);
					mapView.setSatellite(true);
					
					BibleVerse bibleVerse = new BibleVerseImpl();
					bibleVerse.setVerseId(section);
					BibleChapter bibleChapter = new BibleChapterImpl();
					bibleChapter.setChapterId(chapter);
					bibleVerse.setChapter(bibleChapter);
					BibleBook bibleBook = new BibleBookImpl();
					bibleBook.setId("" + book);
					bibleBook.setBookId(book);
					bibleBook.setName(bookName);
					bibleChapter.setBook(bibleBook);
					
					List<BibleLocation> locations = service.getLocations(bibleVerse);
					if(locations.size() == 0) {
						locations = service.getLocations(bibleChapter);
						mapStatus = 1;
					}
					
					if(locations.size() == 0) {
						locations = service.getLocations(bibleBook);
						mapStatus = 2;
					}
					
					if(locations.size() > 0) {
						GeoPoint point = new GeoPoint((int)(locations.get(0).getLatitude() * 1E6), (int)(locations.get(0).getLongitude() * 1E6));
						BibleLocationOverlay overlay = new BibleLocationOverlay(getResources().getDrawable(R.drawable.pin_green), locations);
						overlay.setContext(BibleMapActivity.this);
						mapView.getOverlays().add(overlay);
						
						mapView.getController().animateTo(point);
						
						handler.sendEmptyMessage(0);
					} else {
						finish();
					}
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
		});
		thread.start();
	}
	
	@Override
	protected int getDefaultZoomLevel() {
		return 7;
	}
}
