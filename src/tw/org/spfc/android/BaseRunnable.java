package tw.org.spfc.android;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import csiebug.android.AbstractRunnable;

public abstract class BaseRunnable extends AbstractRunnable {

	public BaseRunnable(Context context, Handler handler) {
		super(context, handler);
	}
	
	@Override
	protected void handlerLogic(Message msg) {
		//預設不做事情,如果有畫面更新的處理才實作
		
	}
}
