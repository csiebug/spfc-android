package tw.org.spfc.android;

import com.google.analytics.tracking.android.EasyTracker;

import csiebug.android.ContextMethod;
import csiebug.android.google.map.AbstractDefaultMapActivity;

public abstract class BaseMapActivity extends AbstractDefaultMapActivity {
	@Override
	protected void debugLogic(Exception e, Object[] args) {
		ContextMethod.debugLogic(this, e, args, getString(R.string.exception), getString(R.string.send_mail_to_author), getString(R.string.close), getString(R.string.authorEmail));
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}
	
	@Override
	protected void onStopLogic() {
		super.onStopLogic();
		EasyTracker.getInstance().activityStop(this);
	}
}
