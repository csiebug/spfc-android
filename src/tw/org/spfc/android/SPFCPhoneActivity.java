package tw.org.spfc.android;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

public class SPFCPhoneActivity extends BaseActivity {

	@Override
	protected int getLayout() {
		return R.layout.phone;
	}

	@Override
	protected void setListeners() {
		findViewById(R.id.operator).setOnClickListener(new Operator(this, getString(R.string.spfcTelephone)));
		findViewById(R.id.n101).setOnClickListener(new Operator(this, getString(R.string.spfcTelephone) + ",101"));
		findViewById(R.id.n103).setOnClickListener(new Operator(this, getString(R.string.spfcTelephone) + ",103"));
		findViewById(R.id.n104).setOnClickListener(new Operator(this, getString(R.string.spfcTelephone) + ",104"));
		findViewById(R.id.n201).setOnClickListener(new Operator(this, getString(R.string.spfcTelephone) + ",201"));
		findViewById(R.id.n203).setOnClickListener(new Operator(this, getString(R.string.spfcTelephone) + ",203"));
		findViewById(R.id.n205).setOnClickListener(new Operator(this, getString(R.string.spfcTelephone) + ",205"));
		findViewById(R.id.n206).setOnClickListener(new Operator(this, getString(R.string.spfcTelephone) + ",206"));
	}
	
	private class Operator extends AbstractDefaultButtonListener {
		private String phoneNumber;
		
		public Operator(Context context, String phoneNumber) {
			super(context);
			this.phoneNumber = phoneNumber;
		}

		@Override
		protected void onClickLogic(DialogInterface dialog, int which) {}

		@Override
		protected void onClickLogic(View v) {
			//TODO 因為部分WIFI版平板,無撥號功能,會因為要求android.permission.CALL_PHONE,而無法在Google Play搜尋到此app,所以決定取消撥號功能
//			alert(getString(R.string.warning), getString(R.string.confirm_call), getString(R.string.ok), getString(R.string.cancel), new AbstractDefaultButtonListener(SPFCPhoneActivity.this) {
//				
//				@Override
//				protected void onClickLogic(View v) {}
//				
//				@Override
//				protected void onClickLogic(DialogInterface dialog, int which) {
//					Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
//					startActivity(intent);
//				}
//			}, new AbstractDefaultButtonListener(SPFCPhoneActivity.this) {
//				
//				@Override
//				protected void onClickLogic(View v) {}
//				
//				@Override
//				protected void onClickLogic(DialogInterface dialog, int which) {
//					dialog.dismiss();
//				}
//			});
			
			if(phoneNumber.split(",").length == 2) {
				alert("分機", phoneNumber.split(",")[1], getString(R.string.ok));
			} else {
				alert("總機", phoneNumber, getString(R.string.ok));
			}
		}
		
	}

	@Override
	protected void setAdapters() {
		
	}

}
