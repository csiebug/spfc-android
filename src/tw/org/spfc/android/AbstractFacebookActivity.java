package tw.org.spfc.android;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.facebook.FacebookException;

import csiebug.android.AbstractDefaultFragmentActivity;
import csiebug.android.ContextMethod;
import csiebug.util.AssertUtility;

public abstract class AbstractFacebookActivity extends AbstractDefaultFragmentActivity {
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private final String PENDING_ACTION_BUNDLE_KEY = "tw.org.spfc.android:PendingAction";
	private PendingAction pendingAction = PendingAction.NONE;
	private GraphUser user;
	
	private enum PendingAction {
        NONE,
        POST_PHOTO,
        POST_STATUS_UPDATE
    }
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new Session.StatusCallback() {
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
    
    @Override
	protected void debugLogic(Exception e, Object[] args) {
		ContextMethod.debugLogic(this, e, args, getString(R.string.exception), getString(R.string.send_mail_to_author), getString(R.string.close), getString(R.string.authorEmail));
	}
    
    protected GraphUser getUser() {
    	return user;
    }
    
    protected void setUser(GraphUser user) {
    	this.user = user;
    }
    
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (pendingAction != PendingAction.NONE &&
                (exception instanceof FacebookOperationCanceledException ||
                exception instanceof FacebookAuthorizationException)) {
        	pendingAction = PendingAction.NONE;
            handleFacebookPermissionNotGranted();
        } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
            handlePendingAction();
        }
        updateUI();
    }
    
    /**
     * facebook無權限操作的處理
     */
    abstract protected void handleFacebookPermissionNotGranted();
    
    protected void updateUI() {
        Session session = Session.getActiveSession();
        boolean enableButtons = (session != null && session.isOpened());
        
        if (enableButtons && user != null) {
            enableUI(user);
        } else {
            disableUI();
        }
    }
    
    /**
     * 登入facebook成功,要啟用某些UI
     * @param user
     */
    abstract protected void enableUI(GraphUser user);
    
    /**
     * 沒有登入facebook,要停用某些UI
     */
    abstract protected void disableUI();
    
    @SuppressWarnings("incomplete-switch")
    protected void handlePendingAction() {
        PendingAction previouslyPendingAction = pendingAction;
        // These actions may re-set pendingAction if they are still pending, but we assume they
        // will succeed.
        pendingAction = PendingAction.NONE;

        switch (previouslyPendingAction) {
            case POST_PHOTO:
                postPhoto();
                break;
            case POST_STATUS_UPDATE:
                postStatusUpdate();
                break;
        }
    }
    
    /**
     * post到facebook之後的處理
     * @param message
     * @param result
     * @param error
     */
    abstract protected void showPublishResult(GraphObject result, FacebookRequestError error);
    
    /**
     * 開啟feed dialog
     * @param name
     * @param caption
     * @param description
     * @param link
     * @param picture
     */
    protected void publishFeedDialog(String name, String caption, String description, String link, String picture) {
        Bundle params = new Bundle();
        if(AssertUtility.isNotNullAndNotSpace(name)) {
        	params.putString("name", name);
        }
        if(AssertUtility.isNotNullAndNotSpace(caption)) {
        	params.putString("caption", caption);
        }
        if(AssertUtility.isNotNullAndNotSpace(description)) {
        	params.putString("description", description);
        }
        if(AssertUtility.isNotNullAndNotSpace(link)) {
        	params.putString("link", link);
        }
        if(AssertUtility.isNotNullAndNotSpace(picture)) {
        	params.putString("picture", picture);
        }

        WebDialog feedDialog = (
            new WebDialog.FeedDialogBuilder(this,
                Session.getActiveSession(),
                params))
            .setOnCompleteListener(new OnCompleteListener() {

                public void onComplete(Bundle values,
                    FacebookException error) {
                    if (error == null) {
                        // When the story is posted, echo the success
                        // and the post Id.
                        final String postId = values.getString("post_id");
                        if (postId != null) {
                        	//toast("Posted story, id: "+postId);
                        } else {
                        	//toast("Publish cancelled");
                        }
                    } else if (error instanceof FacebookOperationCanceledException) {
                    	//toast("Publish cancelled");
                    } else {
                    	//toast("Error posting story");
                    }
                }

            })
            .build();
        feedDialog.show();
    }
    
    private void postStatusUpdate() {
        if (user != null && hasPublishPermission()) {
            final String message = getUserStatusMessage();
            Request request = Request
                    .newStatusUpdateRequest(Session.getActiveSession(), message, new Request.Callback() {
                        public void onCompleted(Response response) {
                            showPublishResult(response.getGraphObject(), response.getError());
                        }
                    });
            request.executeAsync();
        } else {
            pendingAction = PendingAction.POST_STATUS_UPDATE;
        }
    }
    
    /**
     * 取得要update到facebook的個人近況
     */
    abstract protected String getUserStatusMessage();
    
    private void postPhoto() {
        if (hasPublishPermission()) {
            Bitmap image = getUserPhoto();
            Request request = Request.newUploadPhotoRequest(Session.getActiveSession(), image, new Request.Callback() {
                public void onCompleted(Response response) {
                    showPublishResult(response.getGraphObject(), response.getError());
                }
            });
            request.executeAsync();
        } else {
            pendingAction = PendingAction.POST_PHOTO;
        }
    }
    
    /**
     * 取得要上傳的照片
     * @return
     */
    abstract protected Bitmap getUserPhoto();
    
    private boolean hasPublishPermission() {
        Session session = Session.getActiveSession();
        return session != null && session.getPermissions().contains("publish_actions");
    }
    
    protected void performPublish(PendingAction action) {
        Session session = Session.getActiveSession();
        if (session != null) {
            pendingAction = action;
            if (hasPublishPermission()) {
                // We can do the action right away.
                handlePendingAction();
            } else {
                // We need to get new permissions, then complete the action when we get called back.
                session.requestNewPublishPermissions(new Session.NewPermissionsRequest(this, PERMISSIONS));
            }
        }
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	try {
			//必要設定
	        super.onCreate(savedInstanceState);
	        uiHelper = new UiLifecycleHelper(this, callback);
	        uiHelper.onCreate(savedInstanceState);

	        if (savedInstanceState != null) {
	            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
	            pendingAction = PendingAction.valueOf(name);
	        }
	        
	        setContentView(getLayout());
	        
	        //客製設定
	        customSettings();
	        
	        //實作onCreate時的邏輯行為
	        restorePreference();
	        findViews();
	        
	        onCreateLogic();
	        
	        //Binding UI的邏輯行為
	        setListeners();
	        if(getRegisterViewForContextMenu() != null) {
	        	registerForContextMenu(getRegisterViewForContextMenu());
	        }
	        
	        //Binding UI的資料
	        setAdapters();
	    } catch(Exception e) {
			debugLogic(e);
		}
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();

        updateUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);

        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
		
    	uiHelper.onActivityResult(requestCode, resultCode, data);
    	
		try {
			onActivityResultLogic(requestCode, resultCode, data);
		} catch(Exception e) {
			Iterator<String> keys = data.getExtras().keySet().iterator();
			StringBuffer bundle = new StringBuffer("[");
			int i = 0;
			while(keys.hasNext()) {
				String key = keys.next();
				if(i == 0) {
					i = 1;
				} else {
					bundle.append(", ");
				}
				bundle.append(key + " = " + data.getExtras().get(key));
			}
			bundle.append("]");
			debugLogic(e, new Object[]{"requestCode = " + requestCode, "resultCode = " + resultCode, "data.getExtras = " + bundle.toString()});
		}
	}

    @Override
    public void onPause() {
    	super.onPause();
		
    	uiHelper.onPause();
    	
		try {
			//實作onPause時的邏輯行為
			savePreferenceOnPause();
			onPauseLogic();
		} catch(Exception e) {
			debugLogic(e);
		}
	}

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }
}
