package tw.org.spfc.android;

import java.util.List;

import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.service.BibleService;
import bible.domain.BibleLocation;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import csiebug.android.google.map.AbstractOverlay;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.translate.Language;
import csiebug.util.translate.TranslateUtility;

public class BibleLocationOverlay extends AbstractOverlay {
	private List<BibleLocation> bibleLocations;

	public BibleLocationOverlay(Drawable defaultMarker) {
		super(boundCenterBottom(defaultMarker));
	}
	
	public BibleLocationOverlay(Drawable defaultMarker, List<BibleLocation> bibleLocations) {
		super(boundCenterBottom(defaultMarker));
		this.bibleLocations = bibleLocations;
	}

	@Override
	protected void addAllPoints(Context context) {
		for(int i = 0; i < bibleLocations.size(); i++) {
			BibleLocation location = bibleLocations.get(i);
			addPoint(new GeoPoint((int)(location.getLatitude() * 1E6), (int)(location.getLongitude() * 1E6)), location.getName(), location.getDescription());
		}
	}

	@Override
	protected boolean onTapLogic(OverlayItem item) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
		
		try {
//			Locale phoneLocale = getContext().getResources().getConfiguration().locale;
//			int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
			
//			if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//				language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//			} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//				language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//			} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//				language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//			} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//				language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//			} else {
//				language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//			}
//			
//			if(language == BibleCatagoryDAOImpl.CHINESE_BIBLE) {
//				dialog.setTitle(item.getTitle());
//				dialog.setMessage(item.getSnippet());
//			} else {
//				List<String> chineseBooks;
//				List<String> targetBooks;
//				
//				BibleService service = ServiceProxyFactory.createBibleService(getContext(), BibleDAOImpl.CHINESE_UNION_VERSION, BibleCatagoryDAOImpl.CHINESE_BIBLE);
//				chineseBooks = service.getBookNames();
//				
//				service = ServiceProxyFactory.createBibleService(getContext(), BibleDAOImpl.CHINESE_UNION_VERSION, language);
//				targetBooks = service.getBookNames();
//				
//				if(language == BibleCatagoryDAOImpl.FRENCH_BIBLE) {
//					String english = item.getTitle().substring(0, item.getTitle().indexOf("("));
//					String target = TranslateUtility.translate(english, Language.ENGLISH, Language.FRENCH);
//					
//					dialog.setTitle(english + "(" + target + ")");
//				} else if(language == BibleCatagoryDAOImpl.GERMAN_BIBLE) {
//					String english = item.getTitle().substring(0, item.getTitle().indexOf("("));
//					String target = TranslateUtility.translate(english, Language.ENGLISH, Language.GERMAN);
//					
//					dialog.setTitle(english + "(" + target + ")");
//				} else {
//					dialog.setTitle(item.getTitle().substring(0, item.getTitle().indexOf("(")));
//				}
//				
//				String message = item.getSnippet();
//				for(int i=0; i < chineseBooks.size(); i++) {
//					message = message.replaceAll(chineseBooks.get(i), targetBooks.get(i));
//				}
//				dialog.setMessage(message);
//			}
			
			SPFCPreference preference = new SPFCPreference(getContext());
			int version = preference.getBibleVersion();
			
			if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
				dialog.setTitle(item.getTitle());
				dialog.setMessage(item.getSnippet());
			} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
				dialog.setTitle(item.getTitle());
				dialog.setMessage(item.getSnippet());
			} else if(version == BibleDAOImpl.AA) {
				translate(dialog, item, Language.PORTUGUESE, BibleCatagoryDAOImpl.PORTUGUESE_BIBLE);
			} else if(version == BibleDAOImpl.DN1933) {
				translate(dialog, item, Language.DANISH, BibleCatagoryDAOImpl.DANISH_BIBLE);
			} else if(version == BibleDAOImpl.DNB1930) {
				translate(dialog, item, Language.NORWEGIAN, BibleCatagoryDAOImpl.NORWEGIAN_BIBLE);
			} else if(version == BibleDAOImpl.KAR) {
				translate(dialog, item, Language.NORWEGIAN, BibleCatagoryDAOImpl.NORWEGIAN_BIBLE);
			} else if(version == BibleDAOImpl.LSG) {
				translate(dialog, item, Language.FRENCH, BibleCatagoryDAOImpl.FRENCH_BIBLE);
			} else if(version == BibleDAOImpl.LUTH1545) {
				translate(dialog, item, Language.GERMAN, BibleCatagoryDAOImpl.GERMAN_BIBLE);
			} else if(version == BibleDAOImpl.R1933) {
				translate(dialog, item, Language.FINNISH, BibleCatagoryDAOImpl.FINNISH_BIBLE);
			} else if(version == BibleDAOImpl.RMNN) {
				translate(dialog, item, Language.ROMANIAN, BibleCatagoryDAOImpl.ROMANIAN_BIBLE);
			} else if(version == BibleDAOImpl.RVA) {
				translate(dialog, item, Language.SPANISH, BibleCatagoryDAOImpl.SPANISH_BIBLE);
			} else if(version == BibleDAOImpl.SV1917) {
				translate(dialog, item, Language.SWEDISH, BibleCatagoryDAOImpl.SWEDISH_BIBLE);
			} else {
				translate(dialog, item, Language.ENGLISH, BibleCatagoryDAOImpl.ENGLISH_BIBLE);
			}
		} catch (ServiceException e) {
			Log.e(getClass().getName(), "i18n exception!", e);
			
			dialog.setTitle(item.getTitle());
			dialog.setMessage(item.getSnippet());
		}
		
		dialog.show();
		return true;
	}
	
	private void translate(AlertDialog.Builder dialog, OverlayItem item, Language language, int bible) throws ServiceException {
		List<String> chineseBooks;
		List<String> targetBooks;
		
		BibleService service = ServiceProxyFactory.createBibleService(getContext(), BibleDAOImpl.CHINESE_UNION_VERSION, BibleCatagoryDAOImpl.CHINESE_BIBLE);
		chineseBooks = service.getBookNames();
		
		service = ServiceProxyFactory.createBibleService(getContext(), BibleDAOImpl.CHINESE_UNION_VERSION, bible);
		targetBooks = service.getBookNames();
		
		String english = item.getTitle().substring(0, item.getTitle().indexOf("("));
		if(!language.equals(Language.ENGLISH) && AndroidUtility.isNetworkAvailable(getContext())) {
			String target = TranslateUtility.translate(english, Language.ENGLISH, language);
			dialog.setTitle(english + "(" + target + ")");
		} else {
			dialog.setTitle(english);
		}
		
		String message = item.getSnippet();
		for(int i=0; i < chineseBooks.size(); i++) {
			message = message.replaceAll(chineseBooks.get(i), targetBooks.get(i));
		}
		dialog.setMessage(message);
	}
}
