package tw.org.spfc.android;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.facebook.FacebookRequestError;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.analytics.tracking.android.EasyTracker;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.listener.AbstractDefaultMediaPlayerListener;
import tw.org.spfc.android.service.FacebookService;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.domain.Sermon;
import tw.org.spfc.service.SermonService;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import csiebug.android.MediaPlayerController;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;

public class SermonActivity extends BaseFacebookActivity {
	private MediaPlayerController player;
	private WebView webView;
	private boolean isNetworkAvailable = false;
	
	private Handler seekBarHandler = new Handler();
	private Runnable seekBarRunner = new BaseRunnable(SermonActivity.this, null) {
		
		@Override
		protected void runLogic() {
			//如果player在撥放,則每秒更新一次SeekBar位置
			if(player.isPlaying()) {
				((SeekBar)findViewById(R.id.seekBar)).setProgress(player.getCurrentPosition());
				seekBarHandler.postDelayed(seekBarRunner, 1000);
			} else {
				seekBarHandler.removeCallbacks(seekBarRunner);
			}
		}
	};
	
	private String[] sermonFileNames = new String[]{};
	private String[] sermonDisplays = new String[]{};
	private String[] weeklyPapers = new String[]{};
	private String[] sermonPPTs = new String[]{};
	
	@Override
	protected int getLayout() {
		return R.layout.sermon;
	}
	
	@Override
	protected void findViews() {
		webView = (WebView)findViewById(R.id.attach);
	}
	
	@Override
	public void onBackPressed() {
		LayoutParams params = webView.getLayoutParams();
		
		if(params.height == LayoutParams.FILL_PARENT) {
			params.height = 0;
			params.width = 0;
			webView.setLayoutParams(params);
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void setListeners() {
		AbstractDefaultMediaPlayerListener listener = new AbstractDefaultMediaPlayerListener(this, player, seekBarHandler, seekBarRunner) {

			@Override
			protected void onCompletionLogic(MediaPlayer mp) {
				EasyTracker.getTracker().sendEvent("sermon", "complete", "", (long)0);
				
				player.setMediaPlayerState(MediaPlayerController.PLAYER_PLAYBACK_COMPLETED);
				findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
				((SeekBar)findViewById(R.id.seekBar)).setProgress(0);
			}

			@Override
			protected void onErrorLogic(MediaPlayer mp, int what, int extra) {
				player.setMediaPlayerState(MediaPlayerController.PLAYER_ERROR);
				findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
				((SeekBar)findViewById(R.id.seekBar)).setProgress(0);
			}
		};
		player.setOnCompletionListener(listener);
		player.setOnErrorListener(listener);
		
		((SeekBar)findViewById(R.id.seekBar)).setOnSeekBarChangeListener(listener);
		
		((Spinner)findViewById(R.id.sermon_url)).setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				EasyTracker.getTracker().sendEvent("sermon", "change", "", (long)0);
				
				//換首時要將player stop,按鈕改成停止,SeekBar要歸零
				int mediaPlayerState = player.getMediaPlayerState();
				if(mediaPlayerState == MediaPlayerController.PLAYER_STARTED ||
				   mediaPlayerState == MediaPlayerController.PLAYER_PAUSED) {
					player.stop();
				}
				
				findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
				((SeekBar)findViewById(R.id.seekBar)).setProgress(0);
			}

			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0,
					View arg1, int arg2, long arg3) {
				return false;
			}

			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {}
		});
		
		findViewById(R.id.play).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("sermon", "play", "", (long)0);
				
				//如果是一開始撥放或是換首以後撥放,則需要做initial的動作
				int mediaPlayerState = player.getMediaPlayerState();
				
				if(mediaPlayerState == MediaPlayerController.PLAYER_PLAYBACK_COMPLETED) {
					/**
					 * http://developer.android.com/reference/android/media/MediaPlayer.html#StateDiagram
					 * http://developer.android.com/reference/android/media/MediaPlayer.html#Valid_and_Invalid_States
					 * 從android官方文件來看,onCompletion發生應該是進入playback_completed這個狀態
					 * 而且reset應該允許在playback_comleted執行
					 * 但是一直會發生illegalStateException
					 * 從logcat看到mediaplayer所記錄的是執行了release進入了end狀態
					 * 所以對於mediaplayer的狀態一直被官方文件誤導
					 * 此時必須要再重新instance mediaplayer才行
					 */
					player.create();
					mediaPlayerState = player.getMediaPlayerState();
				}
				
				if(mediaPlayerState == MediaPlayerController.PLAYER_IDLE ||
				   mediaPlayerState == MediaPlayerController.PLAYER_STOPED ||
				   mediaPlayerState == MediaPlayerController.PLAYER_END ||
				   mediaPlayerState == MediaPlayerController.PLAYER_ERROR) {
					String url = sermonFileNames[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()];
					initPlayer(url);
				}
				
				//不是撥放狀態才可以按下撥放鍵
				mediaPlayerState = player.getMediaPlayerState();
				
				if(mediaPlayerState == MediaPlayerController.PLAYER_PREPARED ||
				   mediaPlayerState == MediaPlayerController.PLAYER_PAUSED) {
					//如果是被暫停的情況下移動SeekBar的位置,要先將player的撥放位置移動以後再撥放
					SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar);
					if(seekBar.getProgress() < player.getDuration()) {
						player.seekTo(seekBar.getProgress());
					}
					
					//player播放,將按紐也改成撥放中按鈕
					player.start();
					findViewById(R.id.play).setBackgroundResource(R.drawable.play1_hot);
					
					//SeekBar要同步更新
					seekBarHandler.post(seekBarRunner);
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
			
			private void initPlayer(String url) {
				try {
					//player初始化,看要load cdcard檔案還是要load網路
					if(initPlayerForLocal(url)) {
						player.prepare();
					} else {
						//檢查網路是否有通才可以撥放
						if(isNetworkAvailable) {
							if(initPlayerForRemote(url)) {
								try {
									player.prepare();
								} catch (IOException e) {
									if(e.getMessage().startsWith("Prepare failed")) {
										//發生這個Exception通常是因為這個URL不存在
										player.reset();
										toast(getString(R.string.sermon_not_found));
									} else {						
										throw e;
									}
								}
							}
						} else {
							toast(getString(R.string.network_unavailable));
						}
					}
					
					if(player.getMediaPlayerState() == MediaPlayerController.PLAYER_PREPARED) {
						//SeekBar初始化
						int duration = player.getDuration();
						SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar);
						seekBar.setMax(duration);
						seekBar.setProgress(0);
					}
				} catch (Exception e) {
					player.release();
					throw new RuntimeException(e);
				}
			}
			
			private boolean initPlayerForLocal(String url) throws IllegalArgumentException, IllegalStateException, IOException {
				boolean playLocalFile = false;
				toast(url + getString(R.string.sermonExtensionFileName));	
				File localSermonFile = SPFCFileSystem.getSermonFile(SermonActivity.this, url + getString(R.string.sermonExtensionFileName));
				
				if(localSermonFile != null && localSermonFile.exists()) {
					player.reset();
					player.setDataSource(localSermonFile.getPath());
					playLocalFile = true;
				}
				
				return playLocalFile;
			}
			
			private boolean initPlayerForRemote(String url) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
				boolean playRemote = false;
				
				String realURL = getString(R.string.sermonFolder) + url + getString(R.string.sermonExtensionFileName);
				player.reset();
				player.setDataSource(Uri.parse(realURL));
				playRemote = true;
				
				return playRemote;
			}
		});
		
		findViewById(R.id.pause).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				//player暫停,把按鈕改成停止狀態
				player.pause();
				findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.update).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("sermon", "refresh", "", (long)0);
				
				File cacheFile = SPFCFileSystem.getSermonListCacheFile(SermonActivity.this);
				
				if(cacheFile != null && cacheFile.exists()) {
					cacheFile.delete();
				}
				
				refreshSermonList();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.weekly_paper).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@SuppressLint("SetJavaScriptEnabled")
			@Override
			protected void onClickLogic(View v) {
//				WebView message = new WebView(SermonActivity.this);
//				message.getSettings().setJavaScriptEnabled(true);
//				message.setWebViewClient(new WebViewClient() {
//					@Override
//					public boolean shouldOverrideUrlLoading(WebView view,
//							String url) {
//						view.loadUrl(url);
//						return true;
//					}
//				});
//				message.loadUrl("http://docs.google.com/gview?embedded=true&url=" + weeklyPapers[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()]);
//				alert(getString(R.string.weekly_paper), message, getString(R.string.ok));
				
//				Intent intent = new Intent(Intent.ACTION_VIEW);
//			 	Uri uri = Uri.parse("http://docs.google.com/gview?embedded=true&url=" + weeklyPapers[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()]);
//			 	intent.setData(uri);
//			 	startActivity(intent);
			 	
				webView.getSettings().setJavaScriptEnabled(true);
			 	webView.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading(WebView view,
							String url) {
						view.loadUrl(url);
						return true;
					}
					
					@Override
					public void onPageFinished(WebView view, String url) {
						LayoutParams params = view.getLayoutParams();
					 	params.height = LayoutParams.FILL_PARENT;
						params.width = LayoutParams.FILL_PARENT;
						view.setLayoutParams(params);
					}
				});
				webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + weeklyPapers[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()]);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.sermon_ppt).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@SuppressLint("SetJavaScriptEnabled")
			@Override
			protected void onClickLogic(View v) {
//				WebView message = new WebView(SermonActivity.this);
//				message.getSettings().setJavaScriptEnabled(true);
//				message.setWebViewClient(new WebViewClient() {
//					@Override
//					public boolean shouldOverrideUrlLoading(WebView view,
//							String url) {
//						view.loadUrl(url);
//						return true;
//					}
//				});
//				message.loadUrl("http://docs.google.com/gview?embedded=true&url=" + sermonPPTs[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()]);
//				alert(sermonDisplays[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()], message, getString(R.string.ok));
				
//				Intent intent = new Intent(Intent.ACTION_VIEW);
//			 	Uri uri = Uri.parse("http://docs.google.com/gview?embedded=true&url=" + sermonPPTs[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()]);
//			 	intent.setData(uri);
//			 	startActivity(intent);
				
				webView.getSettings().setJavaScriptEnabled(true);
				webView.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading(WebView view,
							String url) {
						view.loadUrl(url);
						return true;
					}
					
					@Override
					public void onPageFinished(WebView view, String url) {
						LayoutParams params = view.getLayoutParams();
					 	params.height = LayoutParams.FILL_PARENT;
						params.width = LayoutParams.FILL_PARENT;
						view.setLayoutParams(params);
					}
				});
				webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + sermonPPTs[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()]);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.latest_weekly_paper).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@SuppressLint("SetJavaScriptEnabled")
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("latest_weekly_paper", "read", "", (long)0);
				
//				WebView message = new WebView(SermonActivity.this);
//				message.getSettings().setJavaScriptEnabled(true);
//				message.setWebViewClient(new WebViewClient() {
//					@Override
//					public boolean shouldOverrideUrlLoading(WebView view,
//							String url) {
//						view.loadUrl(url);
//						return true;
//					}
//				});
//				message.loadUrl("http://docs.google.com/gview?embedded=true&url=http://www.spfctw.org/image/pdf/new.pdf");
//				alert(getString(R.string.latest_weekly_paper), message, getString(R.string.ok));
				
//				Intent intent = new Intent(Intent.ACTION_VIEW);
//			 	Uri uri = Uri.parse("http://docs.google.com/gview?embedded=true&url=http://www.spfctw.org/image/pdf/new.pdf");
//			 	intent.setData(uri);
//			 	startActivity(intent);
				
				webView.getSettings().setJavaScriptEnabled(true);
				webView.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading(WebView view,
							String url) {
						view.loadUrl(url);
						return true;
					}
					
					@Override
					public void onPageFinished(WebView view, String url) {
						LayoutParams params = view.getLayoutParams();
					 	params.height = LayoutParams.FILL_PARENT;
						params.width = LayoutParams.FILL_PARENT;
						view.setLayoutParams(params);
					}
				});
				webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=http://www.spfctw.org/image/pdf/new.pdf");
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.share_to_facebook).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				String pictureURL = getString(R.string.spfcAppIcon);
				try {
					pictureURL = ServiceProxyFactory.createFacebookService(SermonActivity.this).getPicture(FacebookService.FACEBOOK_MY_FAVOR_SERMON);
				} catch (ServiceException e) {
					Log.e(getClass().getName(), "Get picture URL error!", e);
				}
				
				String url = getString(R.string.sermonFolder) + sermonFileNames[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()] + getString(R.string.sermonExtensionFileName);
				String name = sermonDisplays[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()];
				
				publishFeedDialog(getString(R.string.facebook_my_favor_sermon), "", name, url, pictureURL);
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
		
		findViewById(R.id.share_to_twitter).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				SPFCPreference preference = new SPFCPreference(SermonActivity.this);
				String oauthAccessToken = preference.getTwitterToken();
				String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

				ConfigurationBuilder confbuilder = new ConfigurationBuilder();
				Configuration conf = confbuilder
									.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
									.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
									.setOAuthAccessToken(oauthAccessToken)
									.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
									.setUseSSL(true)
									.build();
				
				Twitter twitter = new TwitterFactory(conf).getInstance();
				
				String url = getString(R.string.sermonFolder) + sermonFileNames[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()] + getString(R.string.sermonExtensionFileName);
				String name = sermonDisplays[((Spinner)findViewById(R.id.sermon_url)).getSelectedItemPosition()];
				try {
					twitter.updateStatus(getString(R.string.facebook_my_favor_sermon) + ":\n" + name + "\n" + url);
					toast(getString(R.string.twitter_success));
				} catch (TwitterException e) {
					Log.e(getClass().getName(), "TwitterException", e);
				}
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
	}
	
	@Override
	protected void setAdapters() {
		((LoginButton) findViewById(R.id.login_button)).setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            public void onUserInfoFetched(GraphUser user) {
                setUser(user);
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
	}
	
	@Override
	protected void onCreateLogic() {
		if(AndroidUtility.isNetworkAvailable(this) && isLoggedTwitter()) {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = LayoutParams.WRAP_CONTENT;
			params.width = LayoutParams.WRAP_CONTENT;
			shareToTwitter.setLayoutParams(params);
		} else {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = 0;
			params.width = 0;
			shareToTwitter.setLayoutParams(params);
		}
		
		player = new MediaPlayerController(this);
		
		isNetworkAvailable = AndroidUtility.isNetworkAvailable(this);
		
		refreshSermonList();
		
		//如果進入撥放程式,則把通知取消掉
		((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE)).cancel(NotificationID.DOWNLOAD_SERMON_NOTIFICATION);
	}
	
	private void refreshSermonList() {
		List<Sermon> list;
		try {
			int maxLength = new SPFCPreference(this).getSermonListLength();
			SermonService sermonService = ServiceProxyFactory.createSermonService(this);
			list = sermonService.getSermonFiles(maxLength, true);
			
			if(list.size() == 0) {
				findViewById(R.id.play).setEnabled(false);
				findViewById(R.id.pause).setEnabled(false);
			}
			
			sermonFileNames = new String[list.size()];
			sermonDisplays = new String[list.size()];
			weeklyPapers = new String[list.size()];
			sermonPPTs = new String[list.size()];
			
			for(int i = 0; i < list.size(); i++) {
				Sermon sermon = list.get(i);
				
				sermonFileNames[i] = sermon.getId();
				sermonDisplays[i] = getDisplayText(sermon);
				weeklyPapers[i] = sermon.getAttachedFiles().get(0);
				sermonPPTs[i] = sermon.getAttachedFiles().get(1);
			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sermonDisplays);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		((Spinner)findViewById(R.id.sermon_url)).setAdapter(adapter);
	}
	
	private String getDisplayText(Sermon sermon) {
		//TODO: 因為還沒有接server所以不會有title和speaker資訊
//		return sermon.getTitle() + " - " + sermon.getSpeaker();
		
		if(AssertUtility.isNotNullAndNotSpace(sermon.getTitle())) {
			return sermon.getTitle();
		} else {
			return sermon.getId();
		}
	}
	
	@Override
	protected void onStopLogic() {
		player.release();
		seekBarHandler.removeCallbacks(seekBarRunner);
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		refreshSermonList();
	}
	
	@Override
	protected Integer getOptionMenuLayout() {
		return R.menu.sermon_setting_menu;
	}
	
	@Override
	protected void optionsItemSelectedLogic(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.settings:
				Intent settings = new Intent(this, SermonSettingsActivity.class);
				startActivityForResult(settings, 1);
				break;
			default:
				break;
		}
	}

	@Override
	protected void enableUI(GraphUser user) {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.WRAP_CONTENT;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.VISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void disableUI() {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = 0;
		params.width = 0;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.INVISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void showPublishResult(GraphObject result,
			FacebookRequestError error) {}

	@Override
	protected String getUserStatusMessage() {
		return null;
	}

	@Override
	protected Bitmap getUserPhoto() {
		return null;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	@Override
	protected void onPauseLogic() {
		// TODO Auto-generated method stub
		super.onPauseLogic();
	}
}
