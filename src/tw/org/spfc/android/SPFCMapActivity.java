package tw.org.spfc.android;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

import csiebug.android.google.map.LocationServiceUnavailableException;

public class SPFCMapActivity extends BaseMapActivity {

	@Override
	protected void locationServiceUnavailableLogic(
			LocationServiceUnavailableException e) {
		//這支程式和定位無關,所以就不實作定位取不到的處理
	}

	@Override
	protected int getLayout() {
		return R.layout.map;
	}

	@Override
	protected void setListeners() {
		
	}

	@Override
	protected void setAdapters() {
		
	}

	@Override
	protected int getMapViewId() {
		return R.id.map;
	}
	
	@Override
	protected Double getDefaultLatitude() {
		return Double.parseDouble(getString(R.string.spfcLatitude));
	}
	
	@Override
	protected Double getDefaultLongitude() {
		return Double.parseDouble(getString(R.string.spfcLongitude));
	}
	
	@Override
	protected void onCreateLogic() {
		MapView mapView = getMapView();
		mapView.setSatellite(false);
		GeoPoint point = new GeoPoint((int)(Double.parseDouble(getString(R.string.spfcLatitude)) * 1E6), (int)(Double.parseDouble(getString(R.string.spfcLongitude)) * 1E6));
		SPFCOverlay overlay = new SPFCOverlay(getResources().getDrawable(R.drawable.pin_green), point, getString(R.string.app_name) + "\n(" + getString(R.string.spfcAddress) + ")", getString(R.string.siteInfo));
		overlay.setContext(this);
		mapView.getOverlays().add(overlay);
		
		mapView.getController().animateTo(point);
	}
}
