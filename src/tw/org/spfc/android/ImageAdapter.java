package tw.org.spfc.android;

import java.util.List;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.SpinnerAdapter;

public class ImageAdapter extends BaseAdapter implements SpinnerAdapter {
	private List<ImageView> images;
	private ScaleType scaleType;
	private int width;
	private int height;
	
	public ImageAdapter(List<ImageView> images, ScaleType scaleType, int width, int height) {
		this.images = images;
		this.scaleType = scaleType;
		this.width = width;
		this.height = height;
	}
	
	public int getCount() {
		return images.size();
	}

	public Object getItem(int arg0) {
		return images.get(arg0);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView image = images.get(position);
		
		image.setScaleType(scaleType);
		image.setLayoutParams(new Gallery.LayoutParams(width, height));
		
		return image;
	}

}
