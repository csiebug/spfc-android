package tw.org.spfc.android;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import csiebug.android.AbstractProgressDialogRunnable;

public abstract class BaseProgressDialogRunnable extends AbstractProgressDialogRunnable {

	public BaseProgressDialogRunnable(Context context, Handler handler) {
		super(context, handler);
	}
	
	protected String getTitle() {
		return getContext().getString(R.string.loading);
	};
	
	@Override
	protected void handlerLogic(Message msg) {
		//預設不做事情,如果有畫面更新的處理才實作
		
	}
}
