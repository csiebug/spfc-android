package tw.org.spfc.android;

import android.os.Bundle;
import android.util.Log;

public class FHLActivity extends BaseWebActivity {
	
	@Override
	protected String getWebViewURL() {
		Bundle bundle = getIntent().getExtras();
		String book = bundle.getString("book");
		String engs = bundle.getString("engs");
		int chapter = bundle.getInt("chap");
		int section = bundle.getInt("sec");
		
		String link = "";
		if(book.equals("parsing")) {
			link = getString(R.string.bibleReadingReferenceBookURL2) + "?engs=" + engs + "&chap=" + chapter + "&sec=" + section + "&m=";
		} else if(book.equals("whparsing")) {
			link = getString(R.string.bibleReadingReferenceBookURL3) + "?engs=" + engs + "&chap=" + chapter + "&sec=" + section;
		} else if(book.equals("strong")) {
			if(bundle.getInt("strongNumber") != -1) {
				link = getString(R.string.bibleReadingReferenceBookURL5) + "?N=" + bundle.getString("language") + "&k=" + bundle.getInt("strongNumber") + "&m=";
			} else {
				link = getString(R.string.bibleReadingReferenceBookURL5) + "?N=" + bundle.getString("language") + "&k=" + bundle.getString("description") + "&m=";
			}
		} else {
			link = getString(R.string.bibleReadingReferenceBookURL) + "?book=" + book + "&engs=" + engs + "&chap=" + chapter + "&sec=" + section + "&m=";
		}
		
		Log.d(this.getClass().getName(), "FHL URL: " + link);
		
		return link;
	}
}
