package tw.org.spfc.android;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.listener.AbstractDefaultMediaPlayerListener;
import tw.org.spfc.android.service.FacebookService;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

import com.facebook.FacebookRequestError;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.analytics.tracking.android.EasyTracker;

import csiebug.android.MediaPlayerController;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.SeekBar;
import android.widget.TextView;

public class ClassDetailActivity extends BaseFacebookActivity {
	private MediaPlayerController player;
	private boolean isNetworkAvailable = false;
	private String name = "";
	private String referenceLink = "";
	private String audioURL = "";
	private List<String> imageURLs = new ArrayList<String>();
	
	private Handler seekBarHandler = new Handler();
	private Runnable seekBarRunner = new BaseRunnable(ClassDetailActivity.this, null) {
		
		@Override
		protected void runLogic() {
			//如果player在撥放,則每秒更新一次SeekBar位置
			if(player.isPlaying()) {
				((SeekBar)findViewById(R.id.seekBar)).setProgress(player.getCurrentPosition());
				seekBarHandler.postDelayed(seekBarRunner, 1000);
			} else {
				seekBarHandler.removeCallbacks(seekBarRunner);
			}
		}
	};
	
	private String getHTML() {
		Bundle bundle = getIntent().getExtras();
		String htmlPart = bundle.getString("htmlPart");
//		String audioURL = bundle.getString("audioURL");
		
		return "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body bgcolor=\"#EBEB99\">" + htmlPart + "</body></html>";
//		return "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body bgcolor=\"#EBEB99\">" + htmlPart + "<br><br><audio controls><source src=\"" + audioURL + "\" type=\"audio/mpeg\"/></audio></body></html>";
	}

	@Override
	protected void enableUI(GraphUser user) {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.WRAP_CONTENT;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.VISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void disableUI() {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = 0;
		params.width = 0;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.INVISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void showPublishResult(GraphObject result,
			FacebookRequestError error) {
		
	}

	@Override
	protected String getUserStatusMessage() {
		return null;
	}

	@Override
	protected Bitmap getUserPhoto() {
		return null;
	}

	@Override
	protected int getLayout() {
		return R.layout.sunday_school;
	}

	@Override
	protected void setAdapters() {
		((LoginButton) findViewById(R.id.login_button)).setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            public void onUserInfoFetched(GraphUser user) {
                setUser(user);
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
	}

	@Override
	protected void setListeners() {
		AbstractDefaultMediaPlayerListener listener = new AbstractDefaultMediaPlayerListener(this, player, seekBarHandler, seekBarRunner) {

			@Override
			protected void onCompletionLogic(MediaPlayer mp) {
				EasyTracker.getTracker().sendEvent("class", "complete", "", (long)0);
				
				player.setMediaPlayerState(MediaPlayerController.PLAYER_PLAYBACK_COMPLETED);
				findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
				((SeekBar)findViewById(R.id.seekBar)).setProgress(0);
			}

			@Override
			protected void onErrorLogic(MediaPlayer mp, int what, int extra) {
				player.setMediaPlayerState(MediaPlayerController.PLAYER_ERROR);
				findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
				((SeekBar)findViewById(R.id.seekBar)).setProgress(0);
			}
		};
		player.setOnCompletionListener(listener);
		player.setOnErrorListener(listener);
		
		((SeekBar)findViewById(R.id.seekBar)).setOnSeekBarChangeListener(listener);
		
		findViewById(R.id.play).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("sermon", "play", "", (long)0);
				
				//如果是一開始撥放或是換首以後撥放,則需要做initial的動作
				int mediaPlayerState = player.getMediaPlayerState();
				
				if(mediaPlayerState == MediaPlayerController.PLAYER_PLAYBACK_COMPLETED) {
					/**
					 * http://developer.android.com/reference/android/media/MediaPlayer.html#StateDiagram
					 * http://developer.android.com/reference/android/media/MediaPlayer.html#Valid_and_Invalid_States
					 * 從android官方文件來看,onCompletion發生應該是進入playback_completed這個狀態
					 * 而且reset應該允許在playback_comleted執行
					 * 但是一直會發生illegalStateException
					 * 從logcat看到mediaplayer所記錄的是執行了release進入了end狀態
					 * 所以對於mediaplayer的狀態一直被官方文件誤導
					 * 此時必須要再重新instance mediaplayer才行
					 */
					player.create();
					mediaPlayerState = player.getMediaPlayerState();
				}
				
				if(mediaPlayerState == MediaPlayerController.PLAYER_IDLE ||
				   mediaPlayerState == MediaPlayerController.PLAYER_STOPED ||
				   mediaPlayerState == MediaPlayerController.PLAYER_END ||
				   mediaPlayerState == MediaPlayerController.PLAYER_ERROR) {
					initPlayer(audioURL);
				}
				
				//不是撥放狀態才可以按下撥放鍵
				mediaPlayerState = player.getMediaPlayerState();
				
				if(mediaPlayerState == MediaPlayerController.PLAYER_PREPARED ||
				   mediaPlayerState == MediaPlayerController.PLAYER_PAUSED) {
					//如果是被暫停的情況下移動SeekBar的位置,要先將player的撥放位置移動以後再撥放
					SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar);
					if(seekBar.getProgress() < player.getDuration()) {
						player.seekTo(seekBar.getProgress());
					}
					
					//player播放,將按紐也改成撥放中按鈕
					player.start();
					findViewById(R.id.play).setBackgroundResource(R.drawable.play1_hot);
					
					//SeekBar要同步更新
					seekBarHandler.post(seekBarRunner);
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
			
			private void initPlayer(String url) {
				try {
					//檢查網路是否有通才可以撥放
					if(isNetworkAvailable) {
						if(initPlayerForRemote(url)) {
							try {
								player.prepare();
							} catch (IOException e) {
								if(e.getMessage().startsWith("Prepare failed")) {
									//發生這個Exception通常是因為這個URL不存在
									player.reset();
									toast(getString(R.string.sermon_not_found));
								} else {						
									throw e;
								}
							}
						}
					} else {
						toast(getString(R.string.network_unavailable));
					}
					
					if(player.getMediaPlayerState() == MediaPlayerController.PLAYER_PREPARED) {
						//SeekBar初始化
						int duration = player.getDuration();
						SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar);
						seekBar.setMax(duration);
						seekBar.setProgress(0);
					}
				} catch (Exception e) {
					player.release();
					throw new RuntimeException(e);
				}
			}
			
			private boolean initPlayerForRemote(String url) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
				boolean playRemote = false;
				
				player.reset();
				player.setDataSource(Uri.parse(audioURL));
				playRemote = true;
				
				return playRemote;
			}
		});
		
		findViewById(R.id.pause).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				//player暫停,把按鈕改成停止狀態
				player.pause();
				findViewById(R.id.play).setBackgroundResource(R.drawable.play1_disabled);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.share_to_facebook).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				String pictureURL = getString(R.string.spfcAppIcon);
				try {
					pictureURL = ServiceProxyFactory.createFacebookService(ClassDetailActivity.this).getPicture(FacebookService.FACEBOOK_MY_FAVOR_CLASS);
				} catch (ServiceException e) {
					Log.e(getClass().getName(), "Get picture URL error!", e);
				}
				
				publishFeedDialog(getString(R.string.facebook_my_favor_class), "", name, referenceLink, pictureURL);
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
		
		findViewById(R.id.share_to_twitter).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				SPFCPreference preference = new SPFCPreference(ClassDetailActivity.this);
				String oauthAccessToken = preference.getTwitterToken();
				String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

				ConfigurationBuilder confbuilder = new ConfigurationBuilder();
				Configuration conf = confbuilder
									.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
									.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
									.setOAuthAccessToken(oauthAccessToken)
									.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
									.setUseSSL(true)
									.build();
				
				Twitter twitter = new TwitterFactory(conf).getInstance();
				
				try {
					twitter.updateStatus(getString(R.string.facebook_my_favor_class) + ":\n" + name + "\n" + referenceLink);
					toast(getString(R.string.twitter_success));
				} catch (TwitterException e) {
					Log.e(getClass().getName(), "TwitterException", e);
				}
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
	}
	
	@Override
	protected void onCreateLogic() {
		Bundle bundle = getIntent().getExtras();
		name = bundle.getString("name");
		referenceLink = bundle.getString("referenceLink");
		audioURL = bundle.getString("audioURL");
		imageURLs = bundle.getStringArrayList("ppt");
		
		if(AndroidUtility.isNetworkAvailable(this) && isLoggedTwitter()) {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = LayoutParams.WRAP_CONTENT;
			params.width = LayoutParams.WRAP_CONTENT;
			shareToTwitter.setLayoutParams(params);
		} else {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = 0;
			params.width = 0;
			shareToTwitter.setLayoutParams(params);
		}
		
		player = new MediaPlayerController(this);
		
		isNetworkAvailable = AndroidUtility.isNetworkAvailable(this);
		
		WebView webView = (WebView)findViewById(R.id.web);
		Gallery ppt = (Gallery)findViewById(R.id.ppt);
		
		try {
			if(imageURLs.size() > 0) {
				ppt.setAdapter(new LazyLoadingImageAdapter(imageURLs, this));
				
				LayoutParams params = webView.getLayoutParams();
				params.height = 0;
				params.width = 0;
				webView.setLayoutParams(params);
			} else {
				webView.loadDataWithBaseURL(getString(R.string.spfcHomepage), getHTML(), "text/html", "UTF-8", null);
				
				LayoutParams params = ppt.getLayoutParams();
				params.height = 0;
				params.width = 0;
				ppt.setLayoutParams(params);
			}
		} catch(Exception e) {
			//講義壞掉就不管了,播放器還是要開出來
			webView.loadDataWithBaseURL(getString(R.string.spfcHomepage), getHTML(), "text/html", "UTF-8", null);
			
			LayoutParams params = ppt.getLayoutParams();
			params.height = 0;
			params.width = 0;
			ppt.setLayoutParams(params);
		}
		
		((TextView)findViewById(R.id.function_name)).setText(name);
	}
	
	@Override
	protected void onStopLogic() {
		player.release();
		seekBarHandler.removeCallbacks(seekBarRunner);
	}
}
