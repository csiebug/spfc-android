package tw.org.spfc.android.util;

import java.io.File;

import tw.org.spfc.android.R;
import csiebug.android.util.AndroidUtility;

import android.content.Context;

public class SPFCFileSystem {
	
	private static File getFolder(Context context, String settingsPath) {
		String externalStoragePath = AndroidUtility.getExternalStoragePath();
		if(externalStoragePath != null) {
			String folderPath = "";
			if(externalStoragePath.endsWith("/")) {
				folderPath = externalStoragePath.substring(0, externalStoragePath.length() - 1) + settingsPath;
			} else {
				folderPath = externalStoragePath + settingsPath;
			}
			
			return new File(folderPath);
		}
		
		return null;
	}
	
	private static File getFile(Context context, String settingsFolderPath, String fileName) {
		String externalStoragePath = AndroidUtility.getExternalStoragePath();
		if(externalStoragePath != null) {
			String folderPath = "";
			if(externalStoragePath.endsWith("/")) {
				folderPath = externalStoragePath.substring(0, externalStoragePath.length() - 1) + settingsFolderPath;
			} else {
				folderPath = externalStoragePath + settingsFolderPath;
			}
			
			String filePath = "";
			if(folderPath.endsWith("/")) {
				filePath = folderPath + fileName;
			} else {
				filePath = folderPath + "/" + fileName;
			}
			
			return new File(filePath);
		}
		
		return null;
	}
	
	/**
	 * 取得card template folder
	 * @param context
	 * @return
	 */
	public static File getCardTemplateFolder(Context context) {
		return getFolder(context, context.getString(R.string.cardTemplate));
	}
	
	/**
	 * 取得card template
	 * @param context
	 * @param fileName
	 * @return
	 */
	public static File getCardTemplateFile(Context context, String fileName) {
		return getFile(context, context.getString(R.string.cardTemplate), fileName);
	}
	
	/**
	 * 取得card folder
	 * @param context
	 * @return
	 */
	public static File getCardFolder(Context context) {
		return getFolder(context, context.getString(R.string.cardFolder));
	}
	
	/**
	 * 取得card file
	 * @param context
	 * @param fileName
	 * @return
	 */
	public static File getCardFile(Context context, String fileName) {
		return getFile(context, context.getString(R.string.cardFolder), fileName);
	}
	
	/**
	 * 取得temp folder
	 * @param context
	 * @return
	 */
	public static File getTempFolder(Context context) {
		return getFolder(context, context.getString(R.string.tempFolder));
	}
	
	/**
	 * 取得temp file
	 * @param context
	 * @param fileName
	 * @return
	 */
	public static File getTempFile(Context context, String fileName) {
		return getFile(context, context.getString(R.string.tempFolder), fileName);
	}
	
	/**
	 * 取得cache folder
	 * @param context
	 * @return
	 */
	public static File getCacheFolder(Context context) {
		return getFolder(context, context.getString(R.string.cacheFolder));
	}
	
	/**
	 * 取得sermon list cache file
	 * @param context
	 * @return
	 */
	public static File getSermonListCacheFile(Context context) {
		return getFile(context, context.getString(R.string.cacheFolder), context.getString(R.string.sermonListCache));
	}
	
	/**
	 * 取得sermon list cache error file,用來做debug用
	 * @param context
	 * @return
	 */
	public static File getSermonListCacheErrorFile(Context context) {
		return getFile(context, context.getString(R.string.cacheFolder), context.getString(R.string.sermonListCacheError));
	}
	
	/**
	 * 取得照片播放的網頁
	 * @param context
	 * @return
	 */
	public static File getPhotoCacheFile(Context context) {
		return getFile(context, context.getString(R.string.cacheFolder), context.getString(R.string.photoCache));
	}
	
	/**
	 * 取得sermon folder
	 * @param context
	 * @return
	 */
	public static File getSermonFolder(Context context) {
		return getFolder(context, context.getString(R.string.localSermonFolder));
	}
	
	/**
	 * 取得sermon file
	 * @param context
	 * @return
	 */
	public static File getSermonFile(Context context, String url) {
		return getFile(context, context.getString(R.string.localSermonFolder), url);
	}
	
	/**
	 * 取得apk folder
	 * @param context
	 * @return
	 */
	public static File getAPKFolder(Context context) {
		return getFolder(context, context.getString(R.string.localAPKFolder));
	}
	
	/**
	 * 取得apk file
	 * @param context
	 * @return
	 */
	public static File getAPKFile(Context context) {
		return getFile(context, context.getString(R.string.localAPKFolder), context.getString(R.string.APKFileName));
	}
	
	/**
	 * 取得bible reading notes folder
	 * @param context
	 * @return
	 */
	public static File getBibleReadingNotesFolder(Context context) {
		return getFolder(context, context.getString(R.string.bibleReadingNotesFolder));
	}
	
	/**
	 * 取得bible reading note
	 * @param context
	 * @return
	 */
	public static File getBibleReadingNote(Context context, String url) {
		return getFile(context, context.getString(R.string.bibleReadingNotesFolder), url);
	}
	
	/**
	 * 取得讀經計畫的folder
	 * @param context
	 * @return
	 */
	public static File getBibleReadingPlanFolder(Context context) {
		return getFolder(context, context.getString(R.string.bibleReadingPlanFolder));
	}
	
	/**
	 * 取得讀經計畫
	 * @param context
	 * @return
	 */
	public static File getBibleReadingPlan(Context context, String url) {
		return getFile(context, context.getString(R.string.bibleReadingPlanFolder), url);
	}
	
	/**
	 * 取得經文語音檔
	 * @param context
	 * @param url
	 * @return
	 */
	public static File getBibleReadingAudio(Context context, String url) {
		return getFile(context, context.getString(R.string.cacheFolder), url);
	}
}
