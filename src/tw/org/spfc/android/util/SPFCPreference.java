package tw.org.spfc.android.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.DecoderException;

import tw.org.spfc.android.R;
import tw.org.spfc.domain.News;
import tw.org.spfc.domain.Photo;

import csiebug.android.AbstractPreference;
import csiebug.util.AssertUtility;
import csiebug.util.DateFormatException;
import csiebug.util.DateFormatUtility;
import csiebug.util.DesCoder;
import csiebug.util.StringUtility;
import android.content.Context;

public class SPFCPreference extends AbstractPreference {
	public static final String PREFERENCE = "SPFC";
	public static final String PREFERENCE_SPFC_USER_ID = "SPFC_USER_ID";
	public static final String PREFERENCE_SPFC_USER_NAME = "SPFC_USER_NAME";
	public static final String PREFERENCE_SPFC_USER_PASSWORD = "SPFC_USER_PASSWORD";
	public static final String PREFERENCE_SPFC_USER_KEY = "SPFC_USER_KEY";
	public static final String PREFERENCE_GCM_ENABLE = "GCM_ENABLE";
	public static final String PREFERENCE_GCM_USER_ID = "GCM_USER_ID";
	public static final String PREFERENCE_GCM_USER_NAME = "GCM_USER_NAME";
	public static final String PREFERENCE_PRE_DOWNLOAD = "PREFERENCE_PRE_DOWNLOAD";
	public static final String PREFERENCE_PRE_DOWNLOAD_TIME = "PREFERENCE_PRE_DOWNLOAD_TIME";
	public static final String PREFERENCE_DOWNLOADING_SERMON = "PREFERENCE_DOWNLOADING_SERMON";
	public static final String PREFERENCE_START_DOWNLOAD_SERMON_TIME = "PREFERENCE_START_DOWNLOAD_SERMON_TIME";
	public static final String PREFERENCE_SERMON_LIST_LENGTH = "PREFERENCE_SERMON_LIST_LENGTH";
	public static final String PREFERENCE_LOCAL_SERMON_COUNT = "PREFERENCE_SERMON_COUNT";
	public static final String PREFERENCE_REFRESH_SERMON_LIST_TIME = "PREFERENCE_REFRESH_SERMON_LIST_TIME";
	public static final String PREFERENCE_BIBLE_READING_VERSION = "PREFERENCE_BIBLE_READING_VERSION";
	public static final String PREFERENCE_BIBLE_READING_BOOK = "PREFERENCE_BIBLE_READING_BOOK"; 
	public static final String PREFERENCE_BIBLE_READING_CHAPTER = "PREFERENCE_BIBLE_READING_CHAPTER";
	public static final String PREFERENCE_BIBLE_READING_VERSE = "PREFERENCE_BIBLE_READING_VERSE";
	public static final String PREFERENCE_BIBLE_READING_REFERENCE_VERSE = "PREFERENCE_BIBLE_READING_REFERENCE_VERSE";
	public static final String PREFERENCE_BIBLE_READING_TEXT_SIZE = "PREFERENCE_BIBLE_READING_TEXT_SIZE";
	public static final String PREFERENCE_BIBLE_READING_STYLE = "PREFERENCE_BIBLE_READING_STYLE";
	public static final String PREFERENCE_BIBLE_READING_PLAN = "PREFERENCE_BIBLE_READING_PLAN";
	public static final String PREFERENCE_MAIN_MENU_PHOTOS = "PREFERENCE_MAIN_MENU_PHOTOS";
	public static final String PREFERENCE_MAIN_MENU_GOLDEN_STRING = "PREFERENCE_MAIN_MENU_GOLDEN_STRING";
	public static final String PREFERENCE_MAIN_MENU_NEWS = "PREFERENCE_MAIN_MENU_NEWS";
	public static final String PREFERENCE_SPFC_AVAILABLE = "PREFERENCE_SPFC_AVAILABLE";
	public static final String PREFERENCE_BOOK = "PREFERENCE_BOOK";
	public static final String PREFERENCE_BOOK_BIBLE_PEOPLE = "PREFERENCE_BOOK_BIBLE_PEOPLE";
	public static final String PREFERENCE_TWITTER_TOKEN = "PREFERENCE_TWITTER_TOKEN";
	public static final String PREFERENCE_TWITTER_TOKEN_SECRET = "PREFERENCE_TWITTER_TOKEN_SECRET";
	public static final String PREFERENCE_WALLPAPER = "PREFERENCE_WALLPAPER";
	public static final String PREFERENCE_BIBLE_BOOKMARK = "PREFERENCE_BIBLE_BOOKMARK";
	public static final String PREFERENCE_BIBLE_VS = "PREFERENCE_BIBLE_VS";
	public static final String PREFERENCE_BIBLE_VS_VERSION = "PREFERENCE_BIBLE_VS_VERSION";
	public static final String PREFERENCE_SPFC_BOOKMARK_ENABLE = "PREFERENCE_SPFC_BOOKMARK_ENABLE";
	
	public SPFCPreference(Context context) {
		super(context, PREFERENCE);
	}
	
	/**
	 * 設定取得石牌信友堂崇拜經文的書籤
	 * @param enable
	 */
	public void setSPFCBookMarkEnable(Boolean enable) {
		getPreference().edit().
		putBoolean(PREFERENCE_SPFC_BOOKMARK_ENABLE, enable).
		commit();
	}
	
	/**
	 * 確認是否打開取得石牌信友堂崇拜經文的書籤
	 * @return
	 */
	public Boolean isSPFCBookMarkEnable() {
		return getBooleanValue(PREFERENCE_SPFC_BOOKMARK_ENABLE, false);
	}
	
	/**
	 * 設定經文對照功能的版本
	 * @param enable
	 * @param version
	 */
	public void setBibleVS(Boolean enable, int version) {
		getPreference().edit().
		putBoolean(PREFERENCE_BIBLE_VS, enable).
		putInt(PREFERENCE_BIBLE_VS_VERSION, version).
		commit();
	}
	
	/**
	 * 確認是否打開經文對照功能
	 * @return
	 */
	public Boolean isBibleVSEnable() {
		return getBooleanValue(PREFERENCE_BIBLE_VS, false);
	}
	
	/**
	 * 取得經文對照的版本
	 * @return
	 */
	public int getBibleVSVersion() {
		return getIntValue(PREFERENCE_BIBLE_VS_VERSION);
	}
	
	/**
	 * 加入一個書籤
	 * @param book
	 * @param chapter
	 */
	public void addBibleBookMark(int book, int chapter) {
		String addBookmark = StringUtility.addZero(book, 2) + ":" + StringUtility.addZero(chapter, 3);
		String allBookmarks = getStringValue(PREFERENCE_BIBLE_BOOKMARK);
		
		//表示沒有這個bookmark
		if(allBookmarks.indexOf(addBookmark) == -1) {
			if(allBookmarks.trim().equals("")) {
				getPreference().edit().putString(PREFERENCE_BIBLE_BOOKMARK, addBookmark).commit();
			} else {
				getPreference().edit().putString(PREFERENCE_BIBLE_BOOKMARK, allBookmarks + ";" + addBookmark).commit();
			}
		}
	}
	
	/**
	 * 刪除一個書籤
	 * @param book
	 * @param chapter
	 */
	public void removeBibleBookMark(int book, int chapter) {
		String removeBookmark = StringUtility.addZero(book, 2) + ":" + StringUtility.addZero(chapter, 3);
		String allBookmarks = getStringValue(PREFERENCE_BIBLE_BOOKMARK);
		
		//表示有這個bookmark
		if(allBookmarks.indexOf(removeBookmark) != -1) {
			if(allBookmarks.indexOf(removeBookmark + ";") != -1) {
				getPreference().edit().putString(PREFERENCE_BIBLE_BOOKMARK, allBookmarks.replace(removeBookmark + ";", "")).commit();
			} else if(allBookmarks.indexOf(";" + removeBookmark) != -1) {
				getPreference().edit().putString(PREFERENCE_BIBLE_BOOKMARK, allBookmarks.replace(";" + removeBookmark, "")).commit();
			} else {
				getPreference().edit().putString(PREFERENCE_BIBLE_BOOKMARK, allBookmarks.replace(removeBookmark, "")).commit();
			}
		}
	}
	
	/**
	 * 取得所有書籤
	 * @return
	 */
	public String[] getBibleBookMark() {
		String temp = getStringValue(PREFERENCE_BIBLE_BOOKMARK);
		if(temp.trim().equals("")) {
			return new String[0];
		} else {
			return getStringValue(PREFERENCE_BIBLE_BOOKMARK).split(";");
		}
	}
	
	public void setWallpaper(String url) {
		getPreference().edit().putString(PREFERENCE_WALLPAPER, url).commit();
	}
	
	public String getWallpaper() {
		return getStringValue(PREFERENCE_WALLPAPER);
	}
	
	public void loginTwitter(String token, String tokenSecret) {
		getPreference().edit().
		putString(PREFERENCE_TWITTER_TOKEN, token).
		putString(PREFERENCE_TWITTER_TOKEN_SECRET, tokenSecret).
		commit();
	}
	
	public void logoutTwitter() {
		getPreference().edit().
		putString(PREFERENCE_TWITTER_TOKEN, "").
		putString(PREFERENCE_TWITTER_TOKEN_SECRET, "").
		commit();
	}
	
	public String getTwitterToken() {
		return getStringValue(PREFERENCE_TWITTER_TOKEN);
	}
	
	public String getTwitterTokenSecret() {
		return getStringValue(PREFERENCE_TWITTER_TOKEN_SECRET);
	}
	
	public void registerSPFC(String id, String name, String password) {
		String key = StringUtility.makeRandomNumberKey(48);
		
		try {
			getPreference().edit().
			putString(PREFERENCE_SPFC_USER_ID, id).
			putString(PREFERENCE_SPFC_USER_NAME, name).
			putString(PREFERENCE_SPFC_USER_PASSWORD, DesCoder.encryptCode(password, key)).
			putString(PREFERENCE_SPFC_USER_KEY, key).
			commit();
		} catch (InvalidKeyException e) {
			throw new RuntimeException(e);
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (NoSuchPaddingException e) {
			throw new RuntimeException(e);
		} catch (InvalidAlgorithmParameterException e) {
			throw new RuntimeException(e);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		} catch (IllegalBlockSizeException e) {
			throw new RuntimeException(e);
		} catch (BadPaddingException e) {
			throw new RuntimeException(e);
		} catch (DecoderException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String getSPFCUserId() {
		return getStringValue(PREFERENCE_SPFC_USER_ID);
	}
	
	public String getSPFCUserName() {
		return getStringValue(PREFERENCE_SPFC_USER_NAME);
	}
	
	public String getSPFCUserPASSWORD() {
		return getStringValue(PREFERENCE_SPFC_USER_PASSWORD);
	}
	
	public String getSPFCUserKey() {
		return getStringValue(PREFERENCE_SPFC_USER_KEY);
	}
	
	public void registerGCM(String id, String name) {
		getPreference().edit().
		putBoolean(PREFERENCE_GCM_ENABLE, true).
		putString(PREFERENCE_GCM_USER_ID, id).
		putString(PREFERENCE_GCM_USER_NAME, name).
		commit();
	}
	
	public void unregisterGCM() {
		getPreference().edit().
		putBoolean(PREFERENCE_GCM_ENABLE, false).
		putString(PREFERENCE_GCM_USER_ID, "").
		putString(PREFERENCE_GCM_USER_NAME, "").
		commit();
	}
	
	public Boolean getGCMEnable() {
		return getBooleanValue(PREFERENCE_GCM_ENABLE, false);
	}
	
	public String getGCMUserId() {
		return getStringValue(PREFERENCE_GCM_USER_ID);
	}
	
	public String getGCMUserName() {
		return getStringValue(PREFERENCE_GCM_USER_NAME);
	}
	
	public Boolean isSPFCAvailable() {
		return getBooleanValue(PREFERENCE_SPFC_AVAILABLE, false);
	}
	
	public void setSPFCAvailable(Boolean available) {
		getPreference().edit().
		putBoolean(PREFERENCE_SPFC_AVAILABLE, available).
		commit();
	}
	
	/**
	 * 設定首頁的照片、金句、消息
	 * @param photos
	 * @param goldenString
	 * @param news
	 */
	public void setMainMenu(List<Photo> photos, String goldenString, List<News> news) {
		StringBuffer photoSrcs = new StringBuffer();
		for(int i = 0; i < photos.size(); i++) {
			if(i != 0) {
				photoSrcs.append(";");
			}
			photoSrcs.append(photos.get(i).getSrc());
		}
		
		StringBuffer newsContents = new StringBuffer();
		for(int i = 0; i < news.size(); i++) {
			if(i != 0) {
				newsContents.append(";");
			}
			newsContents.append(news.get(i).getContent());
		}
		
		if(news.size() == 0) {
			newsContents.append(getContext().getString(R.string.spfc_unavailable));
		}
		
		getPreference().edit().
		putString(PREFERENCE_MAIN_MENU_PHOTOS, photoSrcs.toString()).
		putString(PREFERENCE_MAIN_MENU_GOLDEN_STRING, goldenString).
		putString(PREFERENCE_MAIN_MENU_NEWS, newsContents.toString()).
		commit();
	}
	
	/**
	 * 取得首頁照片
	 * @return
	 */
	public String[] getMainMenuPhotos() {
		return getStringValue(PREFERENCE_MAIN_MENU_PHOTOS).split(";");
	}
	
	/**
	 * 取得首頁金句
	 * @return
	 */
	public String getMainMenuGoldenString() {
		return getStringValue(PREFERENCE_MAIN_MENU_GOLDEN_STRING);
	}
	
	/**
	 * 取得首頁消息
	 * @return
	 */
	public String[] getMainMenuNews() {
		return getStringValue(PREFERENCE_MAIN_MENU_NEWS).split(";");
	}
	
	/**
	 * 設定預先下載
	 * @param isChecked
	 * @param timeString
	 * @param localSermonCount
	 */
	public void setPreDownload(boolean isChecked, String timeString, int localSermonCount) {
		getPreference().edit().
		putBoolean(PREFERENCE_PRE_DOWNLOAD, isChecked).
		putString(PREFERENCE_PRE_DOWNLOAD_TIME, timeString).
		putInt(PREFERENCE_LOCAL_SERMON_COUNT, localSermonCount).
		commit();
	}
	
	/**
	 * 取得是否設定預先下載
	 * @return
	 */
	public boolean getPreDownload() {
		return getBooleanValue(PREFERENCE_PRE_DOWNLOAD, false);
	}
	
	/**
	 * 取得預先下載時間
	 * @return
	 */
	public String getPreDownloadTime() {
		return getStringValue(PREFERENCE_PRE_DOWNLOAD_TIME, getContext().getString(R.string.defaultPreDownloadTime));
	}
	
	/**
	 * 取得預先下載時間設定的時
	 * @return
	 */
	public int getPreDownloadHour() {
		return Integer.parseInt(getPreDownloadTime().split(":")[0]);
	}
	
	/**
	 * 取得預先下載時間設定的分
	 * @return
	 */
	public int getPreDownloadMinutes() {
		return Integer.parseInt(getPreDownloadTime().split(":")[1]);
	}
	
	/**
	 * 判斷是否正在下載講道
	 * (為了避免lock,所以不只判斷PREFERENCE_DOWNLOADING_SERMON的值,必須要有機制避免程式錯誤而此值永遠沒機會改變造成的lock)
	 * @return
	 * @throws DateFormatException 
	 * @throws NumberFormatException 
	 */
	public boolean isDownloadingSermon() throws NumberFormatException, DateFormatException {
		boolean downloading = getBooleanValue(PREFERENCE_DOWNLOADING_SERMON, false);
		String startDownloadSermonTime = getStartDownloadSermonTime();
		
		if(!AssertUtility.isNotNullAndNotSpace(startDownloadSermonTime)) {
			setDownloadingSermon(false);
			return false;
		} else {
			if(!downloading) {
				return false;
			} else {
				Calendar now = Calendar.getInstance();
				String date = startDownloadSermonTime.split(" ")[0];
				String time = startDownloadSermonTime.split(" ")[1];
				
				Calendar start = DateFormatUtility.toCalendar(date, time, Integer.parseInt(getContext().getString(R.string.dateFormat)));
				
				if((now.get(Calendar.DAY_OF_YEAR) - start.get(Calendar.DAY_OF_YEAR)) > 1) {
					//如果下載時間超過兩天,表示不合理,很有可能是程式發生錯誤
					setDownloadingSermon(false);
					return false;
				} else if((now.get(Calendar.DAY_OF_YEAR) - start.get(Calendar.DAY_OF_YEAR)) == 1) {
					//跨天的下載的話,計算下載時數,如果超過22小時,表示不合理,很有可能是程式發生錯誤
					if((now.get(Calendar.HOUR_OF_DAY) + (24 - start.get(Calendar.HOUR_OF_DAY))) > 22) {
						setDownloadingSermon(false);
						return false;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
		}
	}
	
	/**
	 * 設定下載講道狀態
	 * @param status
	 */
	public void setDownloadingSermon(boolean status) {
		getPreference().edit().
		putBoolean(PREFERENCE_DOWNLOADING_SERMON, status).
		commit();
	}
	
	/**
	 * 取得開始下載講道時間
	 * @return
	 */
	public String getStartDownloadSermonTime() {
		return getStringValue(PREFERENCE_START_DOWNLOAD_SERMON_TIME);
	}
	
	/**
	 * 設定開始下載講道時間
	 * @param time
	 */
	public void setStartDownloadSermonTime(String time) {
		getPreference().edit().
		putString(PREFERENCE_START_DOWNLOAD_SERMON_TIME, time).
		commit();
	}
	
	/**
	 * 取得上次更新講道列表快取的時間
	 * @return
	 */
	public String getRefreshSermonListTime() {
		return getStringValue(PREFERENCE_REFRESH_SERMON_LIST_TIME);
	}
	
	/**
	 * 設定更新講道列表快取的時間
	 * @param time
	 */
	public void setRefreshSermonListTime(String time) {
		getPreference().edit().
		putString(PREFERENCE_REFRESH_SERMON_LIST_TIME, time).
		commit();
	}
	
	/**
	 * 設定主日講道撥放列表最多顯示筆數
	 * @param length
	 */
	public void setSermonListLength(int length) {
		getPreference().edit().
		putInt(PREFERENCE_SERMON_LIST_LENGTH, length).
		commit();
	}
	
	/**
	 * 取得主日講道撥放列表最多顯示筆數
	 * @return
	 */
	public int getSermonListLength() {
		int value = getIntValue(PREFERENCE_SERMON_LIST_LENGTH);
		
		if(value == 0) {
			value = Integer.parseInt(getContext().getString(R.string.defaultSermonListLength));
			setSermonListLength(value);
		}
		
		return value;
	}
	
	/**
	 * 設定sdcard保留的講道檔案數量
	 * @param length
	 */
	public void setLocalSermonCount(int length) {
		getPreference().edit().
		putInt(PREFERENCE_LOCAL_SERMON_COUNT, length).
		commit();
	}
	
	/**
	 * 取得sdcard設定保留的講道檔案數量
	 * @return
	 */
	public int getLocalSermonCount() {
		int value = getIntValue(PREFERENCE_LOCAL_SERMON_COUNT);
		
		if(value == 0) {
			value = Integer.parseInt(getContext().getString(R.string.defaultLocalSermonFilesCount));
			setLocalSermonCount(value);
		}
		
		return value;
	}
	
	/**
	 * 設定本次讀經的聖經版本
	 * @param version
	 */
	public void setBibleVersion(int version) {
		getPreference().edit().putInt(PREFERENCE_BIBLE_READING_VERSION, version).commit();
	}
	
	/**
	 * 取得上次讀經的聖經版本
	 * @return
	 */
	public int getBibleVersion() {
		return getIntValue(PREFERENCE_BIBLE_READING_VERSION);
	}
	
	/**
	 * 設定本次讀經的書卷和章節
	 * @param book
	 * @param chapter
	 * @param verse
	 */
	public void setBibleReading(int book, int chapter, int verse) {
		getPreference().edit().
		putInt(PREFERENCE_BIBLE_READING_BOOK, book).
		putInt(PREFERENCE_BIBLE_READING_CHAPTER, chapter).
		putInt(PREFERENCE_BIBLE_READING_VERSE, verse).
		commit();
	}
	
	/**
	 * 取得上次讀經的書卷
	 * @return
	 */
	public int getBibleReadingBook() {
		return getIntValue(PREFERENCE_BIBLE_READING_BOOK);
	}
	
	/**
	 * 取得上次讀經的章
	 * @return
	 */
	public int getBibleReadingChapter() {
		int chapter = getIntValue(PREFERENCE_BIBLE_READING_CHAPTER);
		
		if(chapter == 0) {
			return 1;
		} else {
			return chapter;
		}
	}
	
	/**
	 * 取得上次讀經的節
	 * @return
	 */
	public int getBibleReadingVerse() {
		int verse = getIntValue(PREFERENCE_BIBLE_READING_VERSE);
		
		if(verse == 0) {
			return 1;
		} else {
			return verse;
		}
	}
	
	/**
	 * 設定本次處理的節
	 * @param verse
	 */
	public void setBibleReadingReferenceVerse(int verse) {
		getPreference().edit().
		putInt(PREFERENCE_BIBLE_READING_REFERENCE_VERSE, verse).
		commit();
	}
	
	/**
	 * 取得上次處理的節
	 * @return
	 */
	public int getBibleReadingReferenceVerse() {
		int verse = getIntValue(PREFERENCE_BIBLE_READING_REFERENCE_VERSE);
		
		if(verse == 0) {
			return 1;
		} else {
			return verse;
		}
	}
	
	/**
	 * 設定經文字體大小
	 * @param level
	 */
	public void setBibleReadingTextSize(int level) {
		getPreference().edit().
		putInt(PREFERENCE_BIBLE_READING_TEXT_SIZE, level).
		commit();
	}
	
	/**
	 * 取得經文字體大小
	 * @return
	 */
	public Integer getBibleReadingTextSize() {
		return getIntValue(PREFERENCE_BIBLE_READING_TEXT_SIZE);
	}
	
	/**
	 * 設定閱讀聖經的style
	 * @param level
	 */
	public void setBibleReadingStyle(int level) {
		getPreference().edit().
		putInt(PREFERENCE_BIBLE_READING_STYLE, level).
		commit();
	}
	
	/**
	 * 取得閱讀聖經的style
	 * @return
	 */
	public Integer getBibleReadingStyle() {
		return getIntValue(PREFERENCE_BIBLE_READING_STYLE);
	}
	
	/**
	 * 設定採用的讀經計畫
	 * @param isChecked
	 * @param timeString
	 * @param localSermonCount
	 */
	public void setBibleReadingPlan(String plan) {
		getPreference().edit().
		putString(PREFERENCE_BIBLE_READING_PLAN, plan).
		commit();
	}
	
	/**
	 * 取得採用的讀經計畫
	 * @return
	 */
	public String getBibleReadingPlan() {
		return getStringValue(PREFERENCE_BIBLE_READING_PLAN);
	}
	
	/**
	 * 快取本書聖經人物
	 * @param names
	 */
	public void setBookBiblePeople(int book, String[] names) {
		StringBuffer namesString = new StringBuffer();
		for(int i = 0; i < names.length; i++) {
			if(i != 0) {
				namesString.append(";");
			}
			namesString.append(names[i]);
		}
		
		getPreference().edit().
		putInt(PREFERENCE_BOOK, book).
		putString(PREFERENCE_BOOK_BIBLE_PEOPLE, namesString.toString()).
		commit();
	}
	
	/**
	 * 取得目前快取聖經人物的書卷為何
	 * @return
	 */
	public int getBook() {
		return getIntValue(PREFERENCE_BOOK);
	}
	
	/**
	 * 取得快取本書聖經人物
	 * @return
	 */
	public String[] getBookBiblePeople() {
		String namesString = getStringValue(PREFERENCE_BOOK_BIBLE_PEOPLE);
		
		if(AssertUtility.isNotNullAndNotSpace(namesString)) {
			return namesString.split(";");
		} else {
			return new String[0];
		}
	}
}
