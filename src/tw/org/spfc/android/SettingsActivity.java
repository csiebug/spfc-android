package tw.org.spfc.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.InvalidAuthenticationException;
import com.facebook.FacebookRequestError;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gcm.GCMRegistrar;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import csiebug.android.util.AndroidUtility;
import csiebug.service.VersionControlService;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import csiebug.service.ServiceException;
import csiebug.util.StringUtility;

public class SettingsActivity extends BaseFacebookActivity {
	private LoginButton loginButton;
	private EvernoteSession evernoteSession;
	
	@Override
	protected int getLayout() {
		return R.layout.settings;
	}

	@Override
	protected void setListeners() {
		findViewById(R.id.about_app).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("aboutApp", "click", "", (long)0);
				
				try {
					VersionControlService versionControlService = ServiceProxyFactory.createVersionControlService(SettingsActivity.this);
					alert(getString(R.string.aboutApp) + " (" + versionControlService.getLocalVersion()  + ")", versionControlService.getLocalReleaseNote(), getString(R.string.close), getString(R.string.help), null, new AbstractDefaultButtonListener(SettingsActivity.this) {
						
						@Override
						protected void onClickLogic(View v) {}
						
						@Override
						protected void onClickLogic(DialogInterface dialog, int which) {
						 	Intent intent = new Intent(Intent.ACTION_VIEW);
						 	Uri uri = Uri.parse("http://docs.google.com/gview?embedded=true&url=http://spfc-service.appspot.com/spfc-android.pdf");
						 	intent.setData(uri);
						 	startActivity(intent);
						}
					});
					//alert(getString(R.string.aboutApp) + " (" + versionControlService.getLocalVersion()  + ")", versionControlService.getLocalReleaseNote(), getString(R.string.close), null);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.suggestion).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("emailToAuthor", "click", "", (long)0);
				
				Intent notifyIntent = new Intent(Intent.ACTION_SEND);
				notifyIntent.setType("plain/text");
				notifyIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.authorEmail)});
				notifyIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.suggestion));
				notifyIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.author_word) + "\n--");
				SettingsActivity.this.startActivity(Intent.createChooser(notifyIntent, getString(R.string.suggestion)));
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.thanks).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("thanks", "click", "", (long)0);
				
				try {
					StringBuilder result= new StringBuilder();
					
					InputStream inputStream = null;
					try {
						inputStream = SettingsActivity.this.getResources().openRawResource(R.raw.thanks);
						BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
						
						String line = null;
				        while((line = reader.readLine()) != null){
				        	result.append(line + "\n");
				        }
					} catch (IOException e) {
						throw new ServiceException(e);
					} finally {
						if(inputStream != null) {
							try {
								inputStream.close();
							} catch (IOException e) {
								throw new ServiceException(e);
							}
						}
					}
					
					alert(getString(R.string.thanks), result.toString(), getString(R.string.close), null);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.rateAppOnGooglePlay).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
			@Override
			protected void onClickLogic(View arg0) {
				EasyTracker.getTracker().sendEvent("rateApp", "click", "", (long)0);
				
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=tw.org.spfc.android")));
			}
		});
		
		((CheckBox)findViewById(R.id.notify_enable)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(final CompoundButton buttonView, boolean isChecked) {
				final SPFCPreference preference = new SPFCPreference(SettingsActivity.this);
				
				GCMRegistrar.checkDevice(SettingsActivity.this);
				GCMRegistrar.checkManifest(SettingsActivity.this);
				
				if(isChecked) {
					AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
					builder.setMessage(getString(R.string.name) + ":");
					final EditText name = new EditText(SettingsActivity.this);
					builder.setView(name);
					builder.setPositiveButton(getString(R.string.ok), new OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
							//取消匿名註冊
							preference.unregisterGCM();
							GCMRegistrar.unregister(SettingsActivity.this);
							
							//真正註冊
							preference.registerGCM(StringUtility.makeRandomMixKey(8), name.getText().toString());
							GCMRegistrar.register(SettingsActivity.this, getString(R.string.google_api_project_id));
						}
					});
					builder.setNegativeButton(getString(R.string.cancel), new OnClickListener() {

						public void onClick(DialogInterface dialog,
								int which) {
							buttonView.setChecked(false);
						}
					});
					
					AlertDialog alert = builder.create();
					alert.show();
				} else {
					//取消註冊
					preference.unregisterGCM();
					GCMRegistrar.unregister(SettingsActivity.this);
					
					//匿名註冊(只能用來發送程式公告的訊息)
					preference.registerGCM(StringUtility.makeRandomMixKey(8), "anonymous");
					GCMRegistrar.register(SettingsActivity.this, getString(R.string.google_api_project_id));
				}
			}
		});
		
		findViewById(R.id.share_to_facebook).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				String pictureURL = getString(R.string.spfcAppIconForShareApp);
				String url = "https://play.google.com/store/apps/details?id=tw.org.spfc.android";
				
				publishFeedDialog(getString(R.string.facebook_share_app), "", getString(R.string.facebook_share_app), url, pictureURL);
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
		
		findViewById(R.id.share_to_twitter).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				SPFCPreference preference = new SPFCPreference(SettingsActivity.this);
				String oauthAccessToken = preference.getTwitterToken();
				String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

				ConfigurationBuilder confbuilder = new ConfigurationBuilder();
				Configuration conf = confbuilder
									.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
									.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
									.setOAuthAccessToken(oauthAccessToken)
									.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
									.setUseSSL(true)
									.build();
				
				Twitter twitter = new TwitterFactory(conf).getInstance();
				
				String url = "https://play.google.com/store/apps/details?id=tw.org.spfc.android";
				try {
					twitter.updateStatus(getString(R.string.facebook_share_app) + ":\n" + url);
					toast(getString(R.string.twitter_success));
				} catch (TwitterException e) {
					Log.e(getClass().getName(), "TwitterException", e);
				}
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
		
		findViewById(R.id.twitter).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
			@Override
			protected void onClickLogic(View arg0) {
				final SPFCPreference preference = new SPFCPreference(SettingsActivity.this);
				
				if(isLoggedTwitter()) {
					preference.logoutTwitter();
					((Button)findViewById(R.id.twitter)).setText(getString(R.string.login));
					
					Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
					LayoutParams params = shareToTwitter.getLayoutParams();
					params.height = 0;
					params.width = 0;
					shareToTwitter.setLayoutParams(params);
				} else {
					Intent intent = new Intent(SettingsActivity.this, TwitterOAuthActivity.class);
					SettingsActivity.this.startActivityForResult(intent, 1);
				}
			}
		});
		
		findViewById(R.id.evernote).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				if(evernoteSession.isLoggedIn()) {
					try {
						evernoteSession.logOut(getContext());
						
						Button evernoteButton = (Button)findViewById(R.id.evernote);
						evernoteButton.setText(getString(R.string.login));
					} catch (InvalidAuthenticationException e) {
						Log.e(getClass().getName(), "EvernoteException", e);
					}
				} else {
					evernoteSession.authenticate(getContext());
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {
			}
		});
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		switch (requestCode) {
	      //Update UI when oauth activity returns result
	      case EvernoteSession.REQUEST_CODE_OAUTH:
	        if(resultCode == Activity.RESULT_OK) {
	        	Button evernoteButton = (Button)findViewById(R.id.evernote);
	    		
	    		if(evernoteSession.isLoggedIn()) {
	    			evernoteButton.setText(getString(R.string.logout));
	    		} else {
	    			evernoteButton.setText(getString(R.string.login));
	    		}
	        }
	        break;
	      default:
	    	Button twitterButton = (Button)findViewById(R.id.twitter);
	  		if (isLoggedTwitter()) {
	  			twitterButton.setText(getString(R.string.logout));
	  			
	  			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
	  			LayoutParams params = shareToTwitter.getLayoutParams();
	  			params.height = LayoutParams.WRAP_CONTENT;
	  			params.width = LayoutParams.WRAP_CONTENT;
	  			shareToTwitter.setLayoutParams(params);
	  		} else {
	  			twitterButton.setText(getString(R.string.login));
	  			
	  			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
	  			LayoutParams params = shareToTwitter.getLayoutParams();
	  			params.height = 0;
	  			params.width = 0;
	  			shareToTwitter.setLayoutParams(params);
	  		}
	        break;
	    }
	}
	
	@Override
	protected void setAdapters() {
		loginButton.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            public void onUserInfoFetched(GraphUser user) {
                setUser(user);
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
	}
	
	@Override
	protected void onCreateLogic() {
		//設定UI初始值
		if(AndroidUtility.isNetworkAvailable(this) && isLoggedTwitter()) {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = LayoutParams.WRAP_CONTENT;
			params.width = LayoutParams.WRAP_CONTENT;
			shareToTwitter.setLayoutParams(params);
		} else {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = 0;
			params.width = 0;
			shareToTwitter.setLayoutParams(params);
		}
		
		SPFCPreference preference = new SPFCPreference(this);
		((CheckBox)findViewById(R.id.notify_enable)).setChecked(!preference.getGCMUserName().equalsIgnoreCase("anonymous"));
		
		loginButton = (LoginButton) findViewById(R.id.login_button);
		
		Button twitterButton = (Button)findViewById(R.id.twitter);
		if(isLoggedTwitter()) {
			twitterButton.setText(getString(R.string.logout));
		} else {
			twitterButton.setText(getString(R.string.login));
		}
		
		Button evernoteButton = (Button)findViewById(R.id.evernote);
		evernoteSession = EvernoteSession.getInstance(this, getString(R.string.evernote_consumer_key), getString(R.string.evernote_consumer_secret), EvernoteSession.EvernoteService.PRODUCTION);
		
		if(evernoteSession.isLoggedIn()) {
			evernoteButton.setText(getString(R.string.logout));
		} else {
			evernoteButton.setText(getString(R.string.login));
		}
	}
	
	@Override
	protected void enableUI(GraphUser user) {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.WRAP_CONTENT;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.VISIBLE);
	}

	@Override
	protected void disableUI() {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = 0;
		params.width = 0;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void showPublishResult(GraphObject result,
			FacebookRequestError error) {}

	@Override
	protected String getUserStatusMessage() {
		return null;
	}

	@Override
	protected Bitmap getUserPhoto() {
		return null;
	}
}
