package tw.org.spfc.android;

public class NotificationID {
	/**
	 * 講道下載完成通知
	 */
	public static final int DOWNLOAD_SERMON_NOTIFICATION = 0;
	/**
	 * 正在下載講道通知
	 */
	public static final int DOWNLOADING_SERMON_NOTIFICATION = 1;
}
