package tw.org.spfc.android;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.domain.Book;
import tw.org.spfc.domain.pojoImpl.BookImpl;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;

public class LibraryActivity extends BaseActivity {
	private List<ImageView> images = new ArrayList<ImageView>();
	private Gallery newBooks;
	private TextView newBookDescription;
	private List<Book> books;
	private boolean directionRight = true;
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message message) {
			if(newBooks.getSelectedItemPosition() == (images.size() - 1)) {
				directionRight = false;
			} else if(newBooks.getSelectedItemPosition() == 0) {
				directionRight = true;
			}
			
			if(directionRight) {
				newBookDescription.setText(books.get(newBooks.getSelectedItemPosition() + 1).getDescription());
				newBooks.onKeyDown(KeyEvent.KEYCODE_DPAD_RIGHT, new KeyEvent(0, 0));
			} else {
				newBookDescription.setText(books.get(newBooks.getSelectedItemPosition() - 1).getDescription());
				newBooks.onKeyDown(KeyEvent.KEYCODE_DPAD_LEFT, new KeyEvent(0, 0));
			}
		}
	};
	@Override
	protected int getLayout() {
		return R.layout.keyword_search;
	}

	@Override
	protected void setListeners() {
		findViewById(R.id.query).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				searchBook();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
			
			private void searchBook() {
				if(!AndroidUtility.isNetworkAvailable(LibraryActivity.this)) {
					toast(getString(R.string.network_unavailable));
					finish();
				} else {
					try {
						String keyword = ((TextView)findViewById(R.id.keyword)).getText().toString();
						Book valueObject = new BookImpl();
						valueObject.setName(keyword);
						List<Book> list = ServiceProxyFactory.createLibraryService(LibraryActivity.this).queryBook(valueObject);
						
						if(list.size() > 0) {
							int length = list.size();
							
							if(list.get(list.size() - 1) == null) {
								toast(getString(R.string.data_too_much));
								length = list.size() - 1;
							}
							
							String[] bookNames = new String[length];
							String[] bookLinks = new String[length];
							
							for(int i = 0; i < length; i++) {
								if(list.get(i) != null) {
									bookNames[i] = list.get(i).getName();
									bookLinks[i] = list.get(i).getReferenceLink();
								}
							}
							
							Intent intent = new Intent(LibraryActivity.this, BookListActivity.class);
							Bundle bundle = new Bundle();
							bundle.putString("keyword", keyword);
							bundle.putStringArray("bookNames", bookNames);
							bundle.putStringArray("bookLinks", bookLinks);
							intent.putExtras(bundle);
							
							startActivity(intent);
						} else {
							toast(getString(R.string.no_data));
						}
					} catch (ServiceException e) {
						throw new RuntimeException(e);
					}
				}
			}
		});
		
		newBooks.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				newBookDescription.setText(books.get(arg2).getDescription());
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
	}

	@Override
	protected void setAdapters() {
	}
	
	@Override
	protected void onCreateLogic() {
		((TextView)findViewById(R.id.keyword_label)).setText(R.string.term);
		
		try {
			books = ServiceProxyFactory.createLibraryService(LibraryActivity.this).getNewBooks();
			for(int i = 0; i < books.size(); i++) {
				Log.d(getClass().getName(), "url: " + books.get(i).getCoverLink());
				ImageView image = new ImageView(this);
				URL url = new URL(books.get(i).getCoverLink());
				URLConnection connection = url.openConnection();
				connection.connect();
				image.setImageBitmap(BitmapFactory.decodeStream(new BufferedInputStream(connection.getInputStream())));
				images.add(image);
			}
			
			newBookDescription = (TextView)findViewById(R.id.new_book_description);
			newBookDescription.setText(books.get(0).getDescription());
			
			newBooks = (Gallery)findViewById(R.id.new_books);
			newBooks.setAdapter(new ImageAdapter(images, ScaleType.FIT_XY, 90, 126));
			
			Thread thread = new Thread(new Runnable() {
				
				public void run() {
					try {
						while(true) {
							handler.sendEmptyMessage(0);
							Thread.sleep(5000);
						}
					} catch (Exception e) {
						//自動播放功能掛掉,不應影響整個功能
						Log.e(getClass().getName(), "Play photo error!", e);
					}
				}
			});
			thread.start();
		} catch (Exception e) {
			//新書介紹掛掉,不應影響圖書查詢功能,所以不處理錯誤只記錄log
			Log.e(getClass().getName(), "Get new books information error!", e);
		}
	}
}
