package tw.org.spfc.android;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.service.BibleService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import bible.domain.BibleVerse;

import com.facebook.FacebookRequestError;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;

import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.FileUtility;
import csiebug.util.StringUtility;

public class GoldenWordCardActivity extends BaseFacebookActivity {
	private CardPaintView paint;
	private TextView textView1;
	private TextView textView2;
	private TextView textView3;
	private TextView textView4;

	@Override
	protected int getLayout() {
		return R.layout.golden_word_card;
	}

	@Override
	protected void setListeners() {
	}

	@Override
	protected void setAdapters() {
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		SPFCPreference preference = new SPFCPreference(this);
		ImageView imageView = (ImageView)findViewById(R.id.card);
		
		switch (resultCode) {
			case 1:
				String fileName = preference.getWallpaper();
				
				File template = SPFCFileSystem.getCardTemplateFile(this, fileName);
				
				if(template != null && template.exists()) {
					try {
						imageView.setImageDrawable(loadImageFromFile(template));
					} catch (Exception e) {
						imageView.setImageResource(R.drawable.welcome);
					}
				} else {
					try {
						imageView.setImageDrawable(loadImageFromURL(getString(R.string.wallpaperFolderURL) + fileName));
					} catch (Exception e) {
						imageView.setImageResource(R.drawable.welcome);
					}
				}
				break;
			case 2:
				File tempImage = SPFCFileSystem.getTempFile(this, preference.getWallpaper());
				imageView.setImageURI(Uri.fromFile(tempImage));
				tempImage.deleteOnExit();
				
				break;
			default:
				break;
		}
	}
	
	private Drawable loadImageFromFile(File file) throws IOException {
		InputStream inputStream = null;
		
        try{
            inputStream = new FileInputStream(file);
            Drawable draw = Drawable.createFromStream(inputStream, "src");
            return draw;
        } finally {
        	if(inputStream != null) {
        		inputStream.close();
        	}
        }
	}
	
	private Drawable loadImageFromURL(String url) throws MalformedURLException, IOException{
		InputStream inputStream = null;
		
        try{
            inputStream = (InputStream)new URL(url).getContent();
            
            String[] urlPart = url.split("/");
            FileUtility.writeFile(inputStream, SPFCFileSystem.getCardTemplateFile(this, urlPart[urlPart.length - 1]).getPath());
            
            Drawable draw = Drawable.createFromStream(inputStream, "src");
            return draw;
        } finally {
        	if(inputStream != null) {
        		inputStream.close();
        	}
        }
    }
	
	@Override
	protected void onCreateLogic() {
		File cardTemplateFolder = SPFCFileSystem.getCardTemplateFolder(this);
		
		if(cardTemplateFolder != null && (cardTemplateFolder.exists() || cardTemplateFolder.mkdir())) {
			ImageView imageView = (ImageView)findViewById(R.id.card);
			
			File defaultWallpaper = SPFCFileSystem.getCardTemplateFile(this, getString(R.string.defaultWallpaper));
			
			if(defaultWallpaper != null && defaultWallpaper.exists()) {
				try {
					imageView.setImageDrawable(loadImageFromFile(defaultWallpaper));
				} catch (Exception e) {
					imageView.setImageResource(R.drawable.welcome);
				}
			} else {
				try {
					imageView.setImageDrawable(loadImageFromURL(getString(R.string.defaultWallpaperURL)));
				} catch (Exception e) {
					imageView.setImageResource(R.drawable.welcome);
				}
			}
		}
		
		Bundle bundle = getIntent().getExtras();
		Integer book = bundle.getInt("book");
		Integer chapter = bundle.getInt("chap");
		Integer section = bundle.getInt("sec");
		Boolean facebookEnable = bundle.getBoolean("facebookEnable");
		String verseText = bundle.getString("verse");
		
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		SPFCPreference preference = new SPFCPreference(this);
		int version = preference.getBibleVersion();
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//		}
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
		}
		
		try {
			BibleService service = ServiceProxyFactory.createBibleService(this, version, language);
			BibleVerse verse = service.getVerse(book, chapter, section);
			
			initTextView();
			
			textView2.setText(verse.getVerse());
			
			paint = (CardPaintView)findViewById(R.id.paint_view);
			paint.setExportFileName(service.getBookFHLAbbrNames().get(book) + StringUtility.addZero(chapter, 3) + StringUtility.addZero(section, 3) + Calendar.getInstance().getTimeInMillis() + ".png");
			paint.setFacebookEnable(facebookEnable);
			paint.setTextViews(new TextView[]{textView1, textView2, textView3, textView4});
			paint.setVerse(verse.getVerse());
			paint.setActivity(this);
		} catch (ServiceException e) {
			//throw new RuntimeException(e);
			initTextView();
			
			textView2.setText(verseText);
			
			paint = (CardPaintView)findViewById(R.id.paint_view);
			paint.setExportFileName("bibleSearch" + Calendar.getInstance().getTimeInMillis() + ".png");
			paint.setFacebookEnable(facebookEnable);
			paint.setTextViews(new TextView[]{textView1, textView2, textView3, textView4});
			paint.setVerse(verseText);
			paint.setActivity(this);
		}
		
		toast(getString(R.string.writing_hint));
	}
	
	private void initTextView() {
		textView1 = (TextView)findViewById(R.id.verse1);
		textView2 = (TextView)findViewById(R.id.verse2);
		textView3 = (TextView)findViewById(R.id.verse3);
		textView4 = (TextView)findViewById(R.id.verse4);
		
		LayoutParams params = textView1.getLayoutParams();
		params.width = AndroidUtility.getResolution(this).getWidth() * 2 / 5;
		textView1.setLayoutParams(params);
		
		params = textView2.getLayoutParams();
		params.width = AndroidUtility.getResolution(this).getWidth() * 2 / 5;
		textView2.setLayoutParams(params);
		
		params = textView3.getLayoutParams();
		params.width = AndroidUtility.getResolution(this).getWidth() * 2 / 5;
		textView3.setLayoutParams(params);
		
		params = textView4.getLayoutParams();
		params.width = AndroidUtility.getResolution(this).getWidth() * 2 / 5;
		textView4.setLayoutParams(params);
	}

	@Override
	protected void enableUI(GraphUser user) {}

	@Override
	protected void disableUI() {}

	@Override
	protected void showPublishResult(GraphObject result,
			FacebookRequestError error) {
	}

	@Override
	protected String getUserStatusMessage() {
		return null;
	}

	@Override
	protected Bitmap getUserPhoto() {
		return null;
	}
}
