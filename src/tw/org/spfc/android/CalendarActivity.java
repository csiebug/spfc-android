package tw.org.spfc.android;

public class CalendarActivity extends BaseWebActivity {
	
	@Override
	protected String getWebViewURL() {
		//因為google calendar的網址放進xml檔案裡面會造成xml格式錯誤(不曉得是eclipse誤判還是?),所以只好寫死在這裡
		//不是故意不放到settings.xml裡面
		return "https://www.google.com/calendar/embed?src=308ott35lf2qgpjnkeagqvqn08%40group.calendar.google.com&ctz=Asia/Taipei";
	}
}
