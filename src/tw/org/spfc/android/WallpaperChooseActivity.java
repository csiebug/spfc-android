package tw.org.spfc.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.android.util.SPFCPreference;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Gallery;

public class WallpaperChooseActivity extends BaseActivity {
	private Gallery wallpapers;
//	private Button camera;
	private List<String> imageURLs = new ArrayList<String>();
	
	@Override
	protected int getLayout() {
		return R.layout.wallpaper_choose;
	}

	@Override
	protected void setAdapters() {
		
	}

	@Override
	protected void setListeners() {
		wallpapers.setOnItemClickListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {
			}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				SPFCPreference preference = new SPFCPreference(getContext());
				String[] temp = imageURLs.get(arg2).split("/");
				preference.setWallpaper(temp[temp.length - 1].replace("s.jpg", ".jpg"));
				setResult(1);
				finish();
			}
		});
		
//		camera.setOnClickListener(new AbstractDefaultButtonListener(this) {
//			
//			@Override
//			protected void onClickLogic(View v) {
//				Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//			    if(takePictureIntent.resolveActivity(getPackageManager()) != null) {
//			    	String exportFileName = "temp.png";
//			    	
//			    	SPFCPreference preference = new SPFCPreference(getContext());
//					preference.setWallpaper(exportFileName);
//			    	
//					File tempFolder = SPFCFileSystem.getTempFolder(getContext());
//					if(tempFolder.exists() || tempFolder.mkdir()) {
//				    	File tempFile = SPFCFileSystem.getTempFile(getContext(), exportFileName);
//				    	if(!tempFile.exists()) {
//				    		try {
//								tempFile.createNewFile();
//							} catch (IOException e) {
//								Log.e(getClass().getName(), "Save bitmap error!", e);
//							}
//				    	}
//				    	
//				    	takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
//				        startActivityForResult(takePictureIntent, 1);
//					}
//			    }
//			}
//			
//			@Override
//			protected void onClickLogic(DialogInterface dialog, int which) {
//			}
//		});
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		switch (requestCode) {
			case 1:
				try {
					//這裡取出的是縮圖,故捨棄此做法
//					Bundle extras = data.getExtras();
//			        Bitmap imageBitmap = (Bitmap)extras.get("data");
//			        
//					String exportFileName = saveBitmap(imageBitmap);
//					
//					SPFCPreference preference = new SPFCPreference(this);
//					preference.setWallpaper(exportFileName);
					setResult(2);
					finish();
				} catch (Exception e) {
					Log.e(getClass().getName(), "Save bitmap error!", e);
				}
				break;
			default:
				break;
		}
	}
	
	public String saveBitmap(Bitmap bitmap) throws IOException {
		String exportFileName = Calendar.getInstance().getTimeInMillis() + ".png";
		
		File imagePath = SPFCFileSystem.getTempFile(this, exportFileName);
	    FileOutputStream fos = null;
	    try {
	        fos = new FileOutputStream(imagePath);
	        bitmap.compress(CompressFormat.JPEG, 100, fos);
	        fos.flush();
	    } finally {
	    	if(fos != null) {
	    		fos.close();
	    	}
	    }
	    
	    return exportFileName;
	}
	
	@Override
	protected void onCreateLogic() {
		Bundle bundle = getIntent().getExtras();
		imageURLs = bundle.getStringArrayList("wallpapers");
		
//		camera = (Button)findViewById(R.id.camera);
		wallpapers = (Gallery)findViewById(R.id.wallpapers);
		
		try {
			if(imageURLs.size() > 0) {
				LazyLoadingImageAdapter adapter = new LazyLoadingImageAdapter(imageURLs, this);
				adapter.setCacheSize(27);
				adapter.setLocalFlag(true);
				wallpapers.setAdapter(adapter);
			} else {
				LayoutParams params = wallpapers.getLayoutParams();
				params.height = 0;
				params.width = 0;
				wallpapers.setLayoutParams(params);
			}
		} catch(OutOfMemoryError e) {
			//某些手機因記憶體不足會造成App crash,就不再load其他背景圖讓他們選擇,只提供照相功能
			LayoutParams params = wallpapers.getLayoutParams();
			params.height = 0;
			params.width = 0;
			wallpapers.setLayoutParams(params);
		} catch(Exception e) {
			LayoutParams params = wallpapers.getLayoutParams();
			params.height = 0;
			params.width = 0;
			wallpapers.setLayoutParams(params);
		}
	}
}
