package tw.org.spfc.android;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.domain.Sermon;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.RemoteViews;
import csiebug.android.AbstractWIFILockService;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.DateFormatException;
import csiebug.util.DateFormatUtility;
import csiebug.util.StringUtility;

public class DownloadSermonService extends AbstractWIFILockService {
	NotificationManager notificationManager = null;
	Notification notification = null;
	int max = 0;
	int progress = 0;
	int read = 0;
	boolean exceptionFlag = false;
	File localSermonFile = null;
	public static boolean isDownloading = false;
	
	private Handler progressBarHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			notification.contentView.setProgressBar(R.id.downloading_progress, max, progress, false);
			notificationManager.notify(NotificationID.DOWNLOADING_SERMON_NOTIFICATION, notification);
		}
	};
	
	private Runnable downloadSermonRunner = new BaseRunnable(this, null) {

		@Override
		protected void runLogic() {
			try {
				SPFCPreference preference = new SPFCPreference(DownloadSermonService.this);
				isDownloading = preference.isDownloadingSermon();
				Log.d(this.getClass().getName(), "DownloadSermonService start!");
				Log.d(this.getClass().getName(), "isNetworkAvailable = " + AndroidUtility.isNetworkAvailable(DownloadSermonService.this));
				Log.d(this.getClass().getName(), "isDownloading = " + isDownloading);
				if(AndroidUtility.isNetworkAvailable(DownloadSermonService.this)) {
					if(!isDownloading) {
						isDownloading = true;
						preference.setDownloadingSermon(isDownloading);
						Calendar now = Calendar.getInstance();
						preference.setStartDownloadSermonTime(DateFormatUtility.getDisplayDate(now, Integer.parseInt(DownloadSermonService.this.getString(R.string.dateFormat))) + " " + StringUtility.addZero(now.get(Calendar.HOUR_OF_DAY), 2) + ":" + StringUtility.addZero(now.get(Calendar.MINUTE), 2));
						downloadProcess();
						isDownloading = false;
						preference.setDownloadingSermon(isDownloading);
					}
				} else {
					isDownloading = false;
					preference.setDownloadingSermon(isDownloading);
				}
			} catch (Exception e) {
				exceptionFlag = true;
				Log.e(this.getClass().getName(), "DownloadSermonService exception!", e);
				throw new RuntimeException(e);
			} finally {
				if(!isDownloading || exceptionFlag) {
					if(exceptionFlag) {
						Log.d(this.getClass().getName(), "Download incomplete!");
						if(localSermonFile != null && localSermonFile.exists()) {
							Log.d(this.getClass().getName(), "Delete file " + localSermonFile.getName());
							localSermonFile.delete();
						}
						
						PendingIntent appIntent = PendingIntent.getActivity(DownloadSermonService.this, 0, new Intent(), 0);
						notification = new Notification();
						notification.flags = Notification.FLAG_AUTO_CANCEL;
						notification.icon = R.drawable.spfc3;
						notification.tickerText = getString(R.string.download_sermon_failure);
						notification.defaults = Notification.DEFAULT_SOUND;
						notification.setLatestEventInfo(DownloadSermonService.this, getString(R.string.download_sermon_failure), "", appIntent);
						
						notificationManager.notify(NotificationID.DOWNLOAD_SERMON_NOTIFICATION, notification);
					}
					
					finallyProcess();
				}
				
				//要把下載中通知取消掉
				if(notificationManager == null) {
					notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
				}
				notificationManager.cancel(NotificationID.DOWNLOADING_SERMON_NOTIFICATION);
				
				Log.d(this.getClass().getName(), "DownloadSermonService finish!");
				
				Intent intent = new Intent(DownloadSermonService.this, DownloadSermonService.class);
				stopService(intent);
			}
		}
		
		private void downloadProcess() throws Exception {
			notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
			File localSermonFolder = SPFCFileSystem.getSermonFolder(DownloadSermonService.this);
			
			if(localSermonFolder != null && (localSermonFolder.exists() || localSermonFolder.mkdirs())) {
				String[] downloadFileNames = getLatestSermonFileName();
				boolean shouldDownload = false;
				
				for(int i = 0; i < downloadFileNames.length; i++) {
					localSermonFile = SPFCFileSystem.getSermonFile(DownloadSermonService.this, downloadFileNames[i]);
					
					Log.d(this.getClass().getName(), "Check " + downloadFileNames[i] + " exist...");
					
					//如果在本機的sdcard裡面不存在,就預備下載
					if(localSermonFile != null && !localSermonFile.exists()) {
						Log.d(this.getClass().getName(), "Start download file " + downloadFileNames[i]);
						shouldDownload = true;
						
						InputStream inputStream = null;
						FileOutputStream outputStream = null;
						
						try {
							URL downloadURL = new URL(getString(R.string.sermonFolder) + downloadFileNames[i]);
							
							Log.d(this.getClass().getName(), "Download URL: " + getString(R.string.sermonFolder) + downloadFileNames[i]);
							
							URLConnection connection = downloadURL.openConnection();
							connection.connect();
							inputStream = connection.getInputStream();
							outputStream = new FileOutputStream(localSermonFile);
							
							max = connection.getContentLength();
							progress = 0;
							
							notification = initNotifyProgress(localSermonFile.getName() + getString(R.string.downloading), max);
							notificationManager.notify(NotificationID.DOWNLOADING_SERMON_NOTIFICATION, notification);
							
							//連線中斷問題,不會丟出exception
							//所以要在while迴圈開始寫資料之前,先把此flag設為true
							//while迴圈結束後,把此flag設為false,這樣才能真正確保寫檔是完全完成的
							exceptionFlag = true;
							byte buffer[] = new byte[1];
							read = 0;
							while((read = inputStream.read(buffer)) != -1) {
								outputStream.write(buffer);
								
								progress = progress + read;
								if(refreshLogic()) {
									progressBarHandler.sendEmptyMessage(0);
								}
							}
						      
						    // 將緩衝區中的資料全部寫出 
						    outputStream.flush();
						    exceptionFlag = false;
						    
						    Log.d(this.getClass().getName(), "Download file " + downloadFileNames[i] + " success!");
						} catch(FileNotFoundException e) {
							//如果檔案還沒上傳,就不下載,也不管這個exception
							Log.d(this.getClass().getName(), "File " + downloadFileNames[i] + " is not exist on server!");
						} catch(Exception e) {
							exceptionFlag = true;
							Log.e(this.getClass().getName(), "Download file " + downloadFileNames[i] + " failure!", e);
							throw new RuntimeException(e);
						} finally {
							if(inputStream != null) {
								try {
									inputStream.close();
								} catch (IOException e) {
									throw new RuntimeException(e);
								}
							}
							
							if(outputStream != null) {
								try {
									outputStream.close();
								} catch (IOException e) {
									throw new RuntimeException(e);
								}
							}
							
							//發生任何Exception的情況,都要把檔案刪除,以免影響講道撥放
							if(exceptionFlag) {
								Log.d(this.getClass().getName(), "Download incomplete! Delete file " + downloadFileNames[i]);
								if(localSermonFile != null && localSermonFile.exists()) {
									localSermonFile.delete();
									
									PendingIntent appIntent = PendingIntent.getActivity(DownloadSermonService.this, 0, new Intent(), 0);
									notification = new Notification();
									notification.flags = Notification.FLAG_AUTO_CANCEL;
									notification.icon = R.drawable.spfc3;
									notification.tickerText = getString(R.string.download_sermon_failure);
									notification.defaults = Notification.DEFAULT_SOUND;
									notification.setLatestEventInfo(DownloadSermonService.this, getString(R.string.download_sermon_failure), "", appIntent);
									
									notificationManager.notify(NotificationID.DOWNLOAD_SERMON_NOTIFICATION, notification);
								}
							}
						}
					}
				}
				
				//如果應該Download(表示local不存在),而也成功結束Download,則應該發出通知
				if(shouldDownload && !exceptionFlag) {
					//檢查sdcard檔案數，必要時要執行刪除舊檔的動作
					SPFCPreference preference = new SPFCPreference(DownloadSermonService.this);
					int localSermonCount = preference.getLocalSermonCount();
					
					File[] sermonFiles = localSermonFolder.listFiles();
					if(sermonFiles.length > localSermonCount) {
						deleteOldSermonFiles(sermonFiles, sermonFiles.length - localSermonCount);
					}
					
					Intent notifyIntent = new Intent(DownloadSermonService.this, SermonActivity.class);
					notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					PendingIntent appIntent = PendingIntent.getActivity(DownloadSermonService.this, 0, notifyIntent, 0);
					
					notification = new Notification();
					notification.icon = R.drawable.spfc3;
					notification.tickerText = getString(R.string.download_sermon_success);
					notification.defaults = Notification.DEFAULT_SOUND;
					notification.setLatestEventInfo(DownloadSermonService.this, getString(R.string.download_sermon_success), getString(R.string.notify_to_play_sermon), appIntent);
					
					notificationManager.notify(NotificationID.DOWNLOAD_SERMON_NOTIFICATION, notification);
				}
			} else {
				throw new RuntimeException("Can't create sermon folder!");
			}
		}
		
		private boolean refreshLogic() {
			//更新100次
			return (progress * 100) % max == 0;
			
			//更新50次
			//return (progress * 50) % max == 0;
			
			//更新25次
			//return (progress * 25) % max == 0;
			
			//更新20次
			//return (progress * 20) % max == 0;
			
			//更新10次
			//return (progress * 10) % max == 0;
		}
		
		private Notification initNotifyProgress(String message, int max) {
			PendingIntent appIntent = PendingIntent.getActivity(DownloadSermonService.this, 0, new Intent(), 0);
			notification = new Notification(R.drawable.spfc3, message, System.currentTimeMillis());
			notification.sound = null;
			notification.flags = Notification.FLAG_ONGOING_EVENT;
			notification.contentView = new RemoteViews(getApplicationContext().getPackageName(), R.layout.downloading);
			notification.contentView.setImageViewResource(R.id.downloading_icon, R.drawable.spfc3);
			notification.contentView.setTextViewText(R.id.downloading_text, message);
			notification.contentView.setProgressBar(R.id.downloading_progress, max, 0, false);
			notification.contentIntent = appIntent;
			
			return notification;
		}
		
		private void deleteOldSermonFiles(File[] sermonFiles, int deleteCount) {
			Arrays.sort(sermonFiles, new Comparator<File>() {

				public int compare(File lhs, File rhs) {
					String lName = lhs.getName().substring(0, 6);
					String rName = rhs.getName().substring(0, 6);
					
					return rName.compareTo(lName);
				}
			});
			
			int i = 0;
			int lastIndex = sermonFiles.length - 1;
			while(i < deleteCount) {
				Log.d(this.getClass().getName(), "Delete old file " + sermonFiles[lastIndex - i].getName());
				
				sermonFiles[lastIndex - i].delete();
				i++;
			}
		}
		
		/**
		 * 取得最新要下載的講道檔名
		 * @return
		 * @throws DateFormatException 
		 * @throws ServiceException 
		 */
		private String[] getLatestSermonFileName() throws ServiceException {
			String[] realURL;
			
			List<Sermon> sermons = ServiceProxyFactory.createSermonService(DownloadSermonService.this).getSermonFiles(new SPFCPreference(DownloadSermonService.this).getLocalSermonCount(), false);
			realURL = new String[sermons.size()];
			for(int i = 0; i < sermons.size(); i++) {
				realURL[i] = sermons.get(i).getId() + getString(R.string.sermonExtensionFileName);
			}
			
			return realURL;
		}
	};
	
	private void finallyProcess() {
		//不管怎樣結束(正常或發生錯誤)都需要把Downloading狀態改成false,以避免lock
		isDownloading = false;
		SPFCPreference preference = new SPFCPreference(DownloadSermonService.this);
		preference.setDownloadingSermon(isDownloading);
	}
	
	@Override
	protected void onStartLogic(Intent intent, int startId) {
		Thread thread = new Thread(downloadSermonRunner);
		thread.start();
	}
}
