package tw.org.spfc.android;

import tw.org.spfc.android.util.SPFCPreference;
import android.app.Activity;
import android.content.Context;
import android.graphics.PointF;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.widget.ArrayAdapter;
import android.widget.Gallery;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BibleChapterListView extends ListView implements OnGestureListener, OnTouchListener {
	private static int MIN_DISTANCE = 120;
	private Context context;
	private SPFCPreference preference;
	private String[] verses;
	private GestureDetector detector;
	
	// We can be in one of these 3 states
	static final int NONE = 0;
	static final int DRAG = 1;
	static final int ZOOM = 2;
	int mode = NONE;
	
	// Remember some things for zooming
	PointF start = new PointF();
	PointF mid = new PointF();
	float oldDist = 1f;
	
	public BibleChapterListView(Context context, SPFCPreference preference, String[] verses) {
		super(context);
		this.context = context;
		this.preference = preference;
		this.verses = verses;
		detector = new GestureDetector(this);
		
		if(preference.getBibleReadingStyle() == null || preference.getBibleReadingStyle() == 0) {
			setBackgroundColor(getResources().getColor(R.drawable.bgColor2));
		} else {
			setBackgroundColor(getResources().getColor(android.R.color.black));
		}
		
		setFastScrollEnabled(true);
		setAdapter(null);
	}
	
	@Override
	public void setAdapter(ListAdapter adapter) {
		int layout = FontSizeLayout.getLayoutForList(preference);
		
		adapter = new ArrayAdapter<String>(context, layout, R.id.itemText, verses){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View view = super.getView(position, convertView, parent);
				
				SPFCPreference preference = new SPFCPreference(context);
				if(preference.getBibleReadingStyle() == null || preference.getBibleReadingStyle() == 0) {
					TextView text = (TextView)view.findViewById(R.id.itemText);
					text.setTextColor(getResources().getColor(android.R.color.black));
				} else {
					TextView text = (TextView)view.findViewById(R.id.itemText);
					text.setTextColor(getResources().getColor(android.R.color.white));
				}
				
				return view;
			}
		};
		
		super.setAdapter(adapter);
	}
	
	private void zoomIn() {
		int maxSize = FontSizeLayout.getMaxSizeForList();
		int minSize = FontSizeLayout.getMinSizeForList();
		int step = FontSizeLayout.getStepSize();
		
		//已經zoom in就不繼續zoom in
		if(preference.getBibleReadingTextSize() < maxSize) {
			if(preference.getBibleReadingTextSize() < minSize) {
				preference.setBibleReadingTextSize(minSize + step);
			} else {
				preference.setBibleReadingTextSize(preference.getBibleReadingTextSize() + step);
			}
			
			setAdapter(null);
			
			setSelection(preference.getBibleReadingVerse() - 1);
		}
	}
	
	private void zoomOut() {
		int minSize = FontSizeLayout.getMinSizeForList();
		int step = FontSizeLayout.getStepSize();
		
		//已經zoom out就不繼續zoom out
		if(preference.getBibleReadingTextSize() > minSize) {
			preference.setBibleReadingTextSize(preference.getBibleReadingTextSize() - step);
			setAdapter(null);
			
			setSelection(preference.getBibleReadingVerse() - 1);
		}
	}
	
	public boolean onDown(MotionEvent e) {
		return false;
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		Gallery gallery = (Gallery)((Activity)context).findViewById(R.id.bible_chapter);
		int position = gallery.getFirstVisiblePosition();
		
		//y軸移動量
		float y = e1.getY() - e2.getY();
		if(y < 0) {
			y = 0 - y;
		}
		
		//x軸移動量
		float x = e1.getX() - e2.getX();
		if(x < 0) {
			x = 0 - x;
		}
		
		boolean scrollFlag = y > x;
		
		//避免上下滾動的時候,不小心換頁(因為高解析度手機,120在上下滾動的時候會很靈敏的跳頁)
		if(!scrollFlag) {
			//左右換頁，把節歸1
			preference.setBibleReading(preference.getBibleReadingBook(), preference.getBibleReadingChapter(), 1);
			if (e1.getX() - e2.getX() > MIN_DISTANCE) {
				if(position < gallery.getCount() - 1) {
					gallery.setSelection(position + 1, true);
				}
			} else if (e1.getX() - e2.getX() < (0 - MIN_DISTANCE)) {
				if(position > 0) {
					gallery.setSelection(position - 1, true);
				}
			}
		}
		
		return true;
	}

	public void onLongPress(MotionEvent e) {
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return true;
	}

	public void onShowPress(MotionEvent e) {
	}

	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}
	 
	private float spacing(MotionEvent event) {
		float x = event.getX(0) - event.getX(1);
		float y = event.getY(0) - event.getY(1);
		return (float) Math.sqrt(x * x + y * y);
	}

	/** Calculate the mid point of the first two fingers */
	private void midPoint(PointF point, MotionEvent event) {
		float x = event.getX(0) + event.getX(1);
		float y = event.getY(0) + event.getY(1);
		point.set(x / 2, y / 2);
	}
	 
	public boolean onTouch(View v, MotionEvent event) {
		// Handle touch events here...
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
			case MotionEvent.ACTION_DOWN:
				start.set(event.getX(), event.getY());
				mode = DRAG;
				break;
			case MotionEvent.ACTION_POINTER_DOWN:
			   oldDist = spacing(event);
			   if (oldDist > 10f) {
				   midPoint(mid, event);
				   mode = ZOOM;
			   }
			   break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
				mode = NONE;
				break;
			case MotionEvent.ACTION_MOVE:
			   if (mode == DRAG) {
			   } else if (mode == ZOOM) {
				   float newDist = spacing(event);
				   if (newDist > 10f) {
					   float scale = newDist / oldDist;
					   if(scale > 1) {
						   zoomIn();
					   } else {
						   zoomOut();
					   }
				   }
			   }
			   break;
		}
		
		this.onTouchEvent(event);
		detector.onTouchEvent(event);
		return true;
	}
}
