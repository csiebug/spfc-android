package tw.org.spfc.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;

import com.google.android.gcm.GCMRegistrar;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.android.util.SPFCFileSystem;
import csiebug.service.VersionControlService;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.View;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.DateFormatException;
import csiebug.util.StringUtility;

public class SPFCActivity extends BaseActivity {
	@Override
	protected int getLayout() {
		return R.layout.welcome;
	}
	
	@Override
	protected void setListeners() {}
	
	@Override
	protected void setAdapters() {}
    
	@Override
	protected void onCreateLogic() {
		//TODO 現在上Google play版本檢查暫時不需要,可以加快速度,等server端實作VersionService(必要升級檢查),還需要加回來
//		try {
//			checkNewVersionAPK();
//		} catch(Exception e) {
//			//此功能是用來檢查有沒有新版,是輔助功能,就算出現exception也不該影響主要功能的使用,所以不處理exception
//			Log.e(getClass().getName(), "Check new version apk error!", e);
			otherInit();
//		}
		
		SPFCPreference preference = new SPFCPreference(this);
		GCMRegistrar.checkDevice(this);
		GCMRegistrar.checkManifest(this);
		String regId = GCMRegistrar.getRegistrationId(this);
		
		if(regId.equals("")) {
			//匿名註冊(只能用來發送程式公告的訊息)
			preference.registerGCM(StringUtility.makeRandomMixKey(8), "anonymous");
			GCMRegistrar.register(this, getString(R.string.google_api_project_id));
		}
	}
	
	private void otherInit() {
		try {
			initAlarmManager();
		} catch (NumberFormatException e) {
			throw new RuntimeException(e);
		} catch (DateFormatException e) {
			throw new RuntimeException(e);
		}
		
		Intent intent = new Intent(this, MainMenuActivity.class);
		startActivity(intent);
		
		finish();
	}
	
	@SuppressWarnings("unused")
	private void checkNewVersionAPK() throws NameNotFoundException {
		//檢查網路上的版本號與自己的版本號,通知是否有新版程式
		try {
			VersionControlService versionControlService = ServiceProxyFactory.createVersionControlService(this);
			
			//表示有新版
			if(versionControlService.hasNewVersion()) {
				alert(getString(R.string.new_version_apk), versionControlService.getReleaseNote(AndroidUtility.getPackageName(this)), getString(R.string.download_apk), getString(R.string.cancel), new AbstractDefaultButtonListener(this) {
					
					@Override
					protected void onClickLogic(View v) {}
					
					@Override
					protected void onClickLogic(DialogInterface dialog, int which) {
						dialog.dismiss();
						
						Thread thread = new Thread(new BaseProgressDialogRunnable(SPFCActivity.this, null) {
							
							@Override
							protected void runLogic() {
								try {
									downloadAPK();
									otherInit();
								} catch (IOException e) {
									throw new RuntimeException(e);
								}
							}

							@Override
							protected String getTitle() {
								return getString(R.string.downloading);
							}
						});
						
						thread.start();
					}
				}, new AbstractDefaultButtonListener(this) {
					
					@Override
					protected void onClickLogic(View v) {}
					
					@Override
					protected void onClickLogic(DialogInterface dialog, int which) {
						otherInit();
					}
				});
			} else {
				otherInit();
			}
		} catch (ServiceException e) {
			//如果是網路不通的話,當作不啟動版本檢查功能,所以不處理
			if(!e.getMessage().equals("Network is unavailable!")) {
				throw new RuntimeException(e);
			}
		}
	}
	
	private void downloadAPK() throws IOException {
		File localAPKFolder = SPFCFileSystem.getAPKFolder(this);
		
		if(localAPKFolder != null && (localAPKFolder.exists() || localAPKFolder.mkdirs())) {
			File localAPKFile = SPFCFileSystem.getAPKFile(this);
			
			InputStream inputStream = null;
			FileOutputStream outputStream = null;
			try {
				URL downloadURL = new URL(getString(R.string.downloadAPKURL));
				URLConnection connection = downloadURL.openConnection();
				connection.connect();
				inputStream = connection.getInputStream();
				outputStream = new FileOutputStream(localAPKFile);
				byte buffer[] = new byte[1];
				
				while(inputStream.read(buffer) != -1) { 
					outputStream.write(buffer); 
			    }
			      
			    // 將緩衝區中的資料全部寫出 
			    outputStream.flush();
			    
			    AndroidUtility.openFile(this, localAPKFile);
			} catch(IOException e) {
				//如果發生Exception的狀況，就把下載到一半的檔案砍掉
				localAPKFile.deleteOnExit();
				throw e;
			} finally {
				if(inputStream != null) {
					inputStream.close();
				}
				
				if(outputStream != null) {
					outputStream.close();
				}
			}
		} else {
			throw new RuntimeException("Can't create apk folder!");
		}
	}
	
	private void initAlarmManager() throws NumberFormatException, DateFormatException {
		AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
		
		installDownloadSermonService(alarmManager);
	}
	
	/**
	 * 安裝定時下載講道服務
	 * @param alarmManager
	 * @throws DateFormatException 
	 * @throws NumberFormatException 
	 */
	private void installDownloadSermonService(AlarmManager alarmManager) throws NumberFormatException, DateFormatException {
		SPFCPreference preference = new SPFCPreference(SPFCActivity.this);
		
		if(preference.getPreDownload()) {
			long repeatTime = 24 * 60 * 60 * 1000;
			Intent intent = new Intent(this, DownloadSermonReceiver.class);
			PendingIntent sender = PendingIntent.getBroadcast(this, 1, intent, 0);
			
			String preDownloadTimeString = preference.getPreDownloadTime();
			int hour = Integer.parseInt(preDownloadTimeString.split(":")[0]);
			int minute = Integer.parseInt(preDownloadTimeString.split(":")[1]);
			Calendar firstTriggerTime = Calendar.getInstance();
			firstTriggerTime.set(Calendar.HOUR_OF_DAY, hour);
			firstTriggerTime.set(Calendar.MINUTE, minute);
			firstTriggerTime.set(Calendar.SECOND, 0);
			
			if(preference.isDownloadingSermon()) {
				//如果已經正在下載了,就往後延一天
				firstTriggerTime.add(Calendar.DAY_OF_MONTH, 1);
			}
			
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, firstTriggerTime.getTimeInMillis(), repeatTime, sender);
		}
	}
}