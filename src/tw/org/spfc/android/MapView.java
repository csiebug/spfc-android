package tw.org.spfc.android;

import csiebug.android.ContextMethod;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;

public class MapView extends csiebug.android.MapView {
	private Context context;
	private MapPaintView paint;
	private String exportFileName;
	private boolean facebookEnable;
	private boolean enableLongClick = false;
	
	public MapView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}
	
	public MapView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
	}
	
	public MapView(Context context, String apiKey) {
		super(context, apiKey);
		this.context = context;
	}
	
	public void setPaintView(MapPaintView paint) {
		this.paint = paint;
	}
	
	public void setExportFileName(String fileName) {
		this.exportFileName = fileName;
	}
	
	public void setFacebookEnable(boolean flag) {
		this.facebookEnable = flag;
	}
	
	public void setEnableLongClick(boolean flag) {
		this.enableLongClick = flag;
	}

	@Override
	protected void onLongClickEvent() {
		if(enableLongClick) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
		    
			String[] menu = {context.getString(R.string.writing_enable)};
			builder.setItems(menu, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if(which == 0) {
						ContextMethod.toast(context, context.getString(R.string.writing_hint));
						android.view.ViewGroup.LayoutParams params = paint.getLayoutParams();
						params.height = LayoutParams.FILL_PARENT;
						params.width = LayoutParams.FILL_PARENT;
						paint.setLayoutParams(params);
						paint.setExportFileName(exportFileName);
						paint.setFacebookEnable(facebookEnable);
				    }
				}
		    });
		    builder.create();
		    
		    builder.show();
		}
	}
}
