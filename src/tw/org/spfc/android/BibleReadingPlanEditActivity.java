package tw.org.spfc.android;

import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;
import csiebug.util.DateFormatException;
import csiebug.util.DateFormatUtility;
import bible.domain.BibleBook;
import bible.domain.BibleChapter;
import bible.domain.pojoImpl.BibleBookImpl;
import bible.domain.pojoImpl.BibleChapterImpl;
import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.domain.BibleReadingPlan;
import tw.org.spfc.domain.BibleReadingPlanChapterInterval;
import tw.org.spfc.domain.pojoImpl.BibleReadingPlanChapterIntervalImpl;
import tw.org.spfc.domain.pojoImpl.BibleReadingPlanImpl;
import tw.org.spfc.domain.TimeSchedule;
import tw.org.spfc.service.BibleService;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;

public class BibleReadingPlanEditActivity extends BaseListActivity {
	private BibleReadingPlan plan;
	private BibleService service;
	
	private String[] intervals;
	private Integer[] startBooks;
	private Integer[] endBooks;
	private Integer[] startChapters;
	private Integer[] endChapters;
	
	private int editPosition = 0;
	
	@Override
	protected String[] getListItemTexts() {
		return intervals;
	}
	
	@Override
	protected Spanned[] getListItemSpanneds() {
		return null;
	}
	
	@Override
	protected View getRegisterViewForContextMenu() {
		return getListView();
	}

	@Override
	protected String[] getContextMenuOptionsByArray() {
		return null;
	}

	@Override
	protected List<String> getContextMenuOptionsByList() {
		return null;
	}

	@Override
	protected Map<Integer, String> getContextMenuOptionsByMap() {
		Map<Integer, String> menu = new LinkedHashMap<Integer, String>();
		menu.put(1, getString(R.string.delete));
		menu.put(2, getString(R.string.add));
		menu.put(3, getString(R.string.edit));
		
		return menu;
	}
	
	@Override
	protected void contextItemSelectedLogic(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
		try {
			switch (item.getItemId()) {
				case 1:
					plan.getReadings().remove(info.position);
					refreshListView();
					break;
				case 2:
					Intent addIntent = new Intent(this, BibleChapterIntervalChooseActivity.class);
					startActivityForResult(addIntent, 2);
					break;
				case 3:
					editPosition = info.position;
					
					Intent editIntent = new Intent(this, BibleChapterIntervalChooseActivity.class);
					Bundle bundle = new Bundle();
					bundle.putInt("startBook", startBooks[info.position]);
					bundle.putInt("startChapter", startChapters[info.position]);
					bundle.putInt("endBook", endBooks[info.position]);
					bundle.putInt("endChapter", endChapters[info.position]);
					editIntent.putExtras(bundle);
					startActivityForResult(editIntent, 3);
					break;
				default:
					break;
			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected String getContextMenuHeaderTitle() {
		return null;
	}

	@Override
	protected int getContextMenuHeaderIcon() {
		return 0;
	}

	@Override
	protected void putListItemToBundle(Bundle bundle, int position) {}

	@Override
	protected Class<?> getListItemEditActivityClass() {
		return null;
	}

	@Override
	protected int getRequestCode() {
		return 0;
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		if(data != null) {
			Bundle bundle = data.getExtras();
			BibleChapter startChapter = new BibleChapterImpl();
			BibleBook startBook = new BibleBookImpl();
			startBook.setId("" + bundle.getInt("startBook"));
			startBook.setBookId(bundle.getInt("startBook"));
			startChapter.setBook(startBook);
			startChapter.setChapterId(bundle.getInt("startChapter"));
			BibleChapter endChapter = new BibleChapterImpl();
			BibleBook endBook = new BibleBookImpl();
			endBook.setId("" + bundle.getInt("endBook"));
			endBook.setBookId(bundle.getInt("endBook"));
			endChapter.setBook(endBook);
			endChapter.setChapterId(bundle.getInt("endChapter"));
			try {
				BibleReadingPlanChapterInterval reading = new BibleReadingPlanChapterIntervalImpl(startChapter, endChapter);
				
				try {
					switch (requestCode) {
						case 2:
							plan.getReadings().add(reading);
							refreshListView();
							break;
						case 3:
							plan.getReadings().set(editPosition, reading);
							refreshListView();
							break;
						default:
							break;
					}
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			} catch(Exception e) {
				if(e.getMessage().equals("Data invalid!")) {
					toast(getString(R.string.interval_exception));
				} else {
					throw new RuntimeException(e);
				}
			}
		}
	}
	
	@Override
	protected Integer getOptionMenuLayout() {
		return R.menu.bible_reading_plan_edit_menu;
	}
	
	@Override
	protected void optionsItemSelectedLogic(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.add:
				Intent addIntent = new Intent(this, BibleChapterIntervalChooseActivity.class);
				startActivityForResult(addIntent, 2);
				break;
			default:
				break;
		}
	}

	@Override
	protected void setListeners() {
		findViewById(R.id.start_date).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				new DatePickerDialog(BibleReadingPlanEditActivity.this, new DatePickerDialog.OnDateSetListener() {
					
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						try {
							Calendar startDate = Calendar.getInstance();
							startDate.set(Calendar.YEAR, year);
							startDate.set(Calendar.MONTH, monthOfYear);
							startDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
							plan.getSchedule().setStartDatetime(startDate);
							((Button)findViewById(R.id.start_date)).setText(DateFormatUtility.getDisplayDate(plan.getSchedule().getStartDatetime(), Integer.parseInt(getString(R.string.dateFormat))));
						} catch (NumberFormatException e) {
							throw new RuntimeException(e);
						} catch (DateFormatException e) {
							throw new RuntimeException(e);
						}
					}
				}, plan.getSchedule().getStartDatetime().get(Calendar.YEAR), plan.getSchedule().getStartDatetime().get(Calendar.MONTH), plan.getSchedule().getStartDatetime().get(Calendar.DAY_OF_MONTH)).show();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		findViewById(R.id.end_date).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				new DatePickerDialog(BibleReadingPlanEditActivity.this, new DatePickerDialog.OnDateSetListener() {
					
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						try {
							Calendar endDate = Calendar.getInstance();
							endDate.set(Calendar.YEAR, year);
							endDate.set(Calendar.MONTH, monthOfYear);
							endDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
							plan.getSchedule().setEndDatetime(endDate);
							((Button)findViewById(R.id.end_date)).setText(DateFormatUtility.getDisplayDate(plan.getSchedule().getEndDatetime(), Integer.parseInt(getString(R.string.dateFormat))));
						} catch (NumberFormatException e) {
							throw new RuntimeException(e);
						} catch (DateFormatException e) {
							throw new RuntimeException(e);
						}
					}
				}, plan.getSchedule().getEndDatetime().get(Calendar.YEAR), plan.getSchedule().getEndDatetime().get(Calendar.MONTH), plan.getSchedule().getEndDatetime().get(Calendar.DAY_OF_MONTH)).show();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		findViewById(R.id.add).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent addIntent = new Intent(BibleReadingPlanEditActivity.this, BibleChapterIntervalChooseActivity.class);
				startActivityForResult(addIntent, 2);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
	}
	
	private void refreshListView() throws ServiceException {
		List<BibleReadingPlanChapterInterval> readings = plan.getReadings();
		intervals = new String[readings.size()];
		startBooks = new Integer[readings.size()];
		endBooks = new Integer[readings.size()];
		startChapters = new Integer[readings.size()];
		endChapters = new Integer[readings.size()];
		List<String> abbrNames = service.getBookAbbrNames();
		
		for(int i = 0; i < readings.size(); i++) {
			BibleReadingPlanChapterInterval reading = readings.get(i);
			startBooks[i] = reading.getStartChapter().getBook().getBookId();
			startChapters[i] = reading.getStartChapter().getChapterId();
			endBooks[i] = reading.getEndChapter().getBook().getBookId();
			endChapters[i] = reading.getEndChapter().getChapterId();
			
			intervals[i] = abbrNames.get(startBooks[i]) + ":" + startChapters[i] + " ~ " + abbrNames.get(endBooks[i]) + ":" + endChapters[i];
		}
		
		setAdapters();
	}
	
	@Override
	protected void onPauseLogic() {
		String planName = ((TextView)findViewById(R.id.plan_name)).getText().toString();
		if(AssertUtility.isNotNullAndNotSpace(planName)) {
			try {
				boolean mondayBreak = !((CheckBox)findViewById(R.id.monday)).isChecked();
				boolean tuesdayBreak = !((CheckBox)findViewById(R.id.tuesday)).isChecked();
				boolean wednesdayBreak = !((CheckBox)findViewById(R.id.wednesday)).isChecked();
				boolean thursdayBreak = !((CheckBox)findViewById(R.id.thursday)).isChecked();
				boolean fridayBreak = !((CheckBox)findViewById(R.id.friday)).isChecked();
				boolean saturdayBreak = !((CheckBox)findViewById(R.id.saturday)).isChecked();
				boolean sundayBreak = !((CheckBox)findViewById(R.id.sunday)).isChecked();
				Calendar startDate = DateFormatUtility.toCalendar(((Button)findViewById(R.id.start_date)).getText().toString(), Integer.parseInt(getString(R.string.dateFormat)));
				Calendar endDate = DateFormatUtility.toCalendar(((Button)findViewById(R.id.end_date)).getText().toString(), Integer.parseInt(getString(R.string.dateFormat)));
				plan.setName(planName);
				plan.getSchedule().setStartDatetime(startDate);
				plan.getSchedule().setEndDatetime(endDate);
				plan.getSchedule().setMondayBreak(mondayBreak);
				plan.getSchedule().setTuesdayBreak(tuesdayBreak);
				plan.getSchedule().setWednesdayBreak(wednesdayBreak);
				plan.getSchedule().setThursdayBreak(thursdayBreak);
				plan.getSchedule().setFridayBreak(fridayBreak);
				plan.getSchedule().setSaturdayBreak(saturdayBreak);
				plan.getSchedule().setSundayBreak(sundayBreak);
				
				service.addBibleReadingPlan(plan);
			} catch (DateFormatException e) {
				throw new RuntimeException(e);
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}
		}
		
		setResult(1);
	}
	
	@Override
	protected Integer getLayout() {
		return R.layout.bible_reading_plan_edit;
	}
	
	@Override
	protected void onCreateLogic() {
		String path = getIntent().getExtras().getString("path");
		
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//		int version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//			version = BibleDAOImpl.CHINA_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//			version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//			version = BibleDAOImpl.LSG;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//			version = BibleDAOImpl.LUTH1545;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//			version = BibleDAOImpl.KING_JAMES_VERSION;
//		}
		SPFCPreference preference = new SPFCPreference(this);
		int version = preference.getBibleVersion();
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
		}
		
		try {
			service = ServiceProxyFactory.createBibleService(this, version, language);
			
			//修改
			if(AssertUtility.isNotNullAndNotSpace(path)) {
				plan = service.getBibleReadingPlan(path);
				((TextView)findViewById(R.id.plan_name)).setText(plan.getName());
				((Button)findViewById(R.id.start_date)).setText(DateFormatUtility.getDisplayDate(plan.getSchedule().getStartDatetime(), Integer.parseInt(getString(R.string.dateFormat))));
				((Button)findViewById(R.id.end_date)).setText(DateFormatUtility.getDisplayDate(plan.getSchedule().getEndDatetime(), Integer.parseInt(getString(R.string.dateFormat))));
				((CheckBox)findViewById(R.id.monday)).setChecked(!plan.getSchedule().isMondayBreak());
				((CheckBox)findViewById(R.id.tuesday)).setChecked(!plan.getSchedule().isTuesdayBreak());
				((CheckBox)findViewById(R.id.wednesday)).setChecked(!plan.getSchedule().isWednesdayBreak());
				((CheckBox)findViewById(R.id.thursday)).setChecked(!plan.getSchedule().isThursdayBreak());
				((CheckBox)findViewById(R.id.friday)).setChecked(!plan.getSchedule().isFridayBreak());
				((CheckBox)findViewById(R.id.saturday)).setChecked(!plan.getSchedule().isSaturdayBreak());
				((CheckBox)findViewById(R.id.sunday)).setChecked(!plan.getSchedule().isSundayBreak());
				
				List<BibleReadingPlanChapterInterval> readings = plan.getReadings();
				intervals = new String[readings.size()];
				startBooks = new Integer[readings.size()];
				endBooks = new Integer[readings.size()];
				startChapters = new Integer[readings.size()];
				endChapters = new Integer[readings.size()];
				List<String> abbrNames = service.getBookAbbrNames();
				
				for(int i = 0; i < readings.size(); i++) {
					BibleReadingPlanChapterInterval reading = readings.get(i);
					startBooks[i] = reading.getStartChapter().getBook().getBookId();
					startChapters[i] = reading.getStartChapter().getChapterId();
					endBooks[i] = reading.getEndChapter().getBook().getBookId();
					endChapters[i] = reading.getEndChapter().getChapterId();
					
					intervals[i] = abbrNames.get(startBooks[i]) + ":" + startChapters[i] + " ~ " + abbrNames.get(endBooks[i]) + ":" + endChapters[i];
				}
			//新增
			} else {
				plan = new BibleReadingPlanImpl();
				Calendar now = Calendar.getInstance();
				TimeSchedule schedule = new TimeSchedule();
				schedule.setStartDatetime(now);
				schedule.setEndDatetime(now);
				plan.setSchedule(schedule);
				
				((Button)findViewById(R.id.start_date)).setText(DateFormatUtility.getDisplayDate(now, Integer.parseInt(getString(R.string.dateFormat))));
				((Button)findViewById(R.id.end_date)).setText(DateFormatUtility.getDisplayDate(now, Integer.parseInt(getString(R.string.dateFormat))));
				((CheckBox)findViewById(R.id.monday)).setChecked(true);
				((CheckBox)findViewById(R.id.tuesday)).setChecked(true);
				((CheckBox)findViewById(R.id.wednesday)).setChecked(true);
				((CheckBox)findViewById(R.id.thursday)).setChecked(true);
				((CheckBox)findViewById(R.id.friday)).setChecked(true);
				((CheckBox)findViewById(R.id.saturday)).setChecked(true);
				((CheckBox)findViewById(R.id.sunday)).setChecked(true);
				
				intervals = new String[]{};
				startBooks = new Integer[]{};
				endBooks = new Integer[]{};
				startChapters = new Integer[]{};
				endChapters = new Integer[]{};
			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		} catch (NumberFormatException e) {
			throw new RuntimeException(e);
		} catch (DateFormatException e) {
			throw new RuntimeException(e);
		}
	}
}
