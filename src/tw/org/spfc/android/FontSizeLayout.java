package tw.org.spfc.android;

import tw.org.spfc.android.util.SPFCPreference;

public class FontSizeLayout {
	public static int getMaxSizeForList() {
		return 90;
	}
	
	public static int getMinSizeForList() {
		return 30;
	}
	
	public static int getDefaultSizeForList() {
		return 50;
	}
	
	public static int getStepSize() {
		return 2;
	}
	
	public static int getLayoutForList(SPFCPreference preference) {
		int size = preference.getBibleReadingTextSize();
		
		int layout = R.layout.list_item_60;
		
		switch (size) {
		case 90:
			layout = R.layout.list_item_90;
			break;
		case 88:
			layout = R.layout.list_item_88;
			break;
		case 86:
			layout = R.layout.list_item_86;
			break;
		case 84:
			layout = R.layout.list_item_84;
			break;
		case 82:
			layout = R.layout.list_item_82;
			break;
		case 80:
			layout = R.layout.list_item_80;
			break;
		case 78:
			layout = R.layout.list_item_78;
			break;
		case 76:
			layout = R.layout.list_item_76;
			break;
		case 74:
			layout = R.layout.list_item_74;
			break;
		case 72:
			layout = R.layout.list_item_72;
			break;
		case 70:
			layout = R.layout.list_item_70;
			break;
		case 68:
			layout = R.layout.list_item_68;
			break;
		case 66:
			layout = R.layout.list_item_66;
			break;
		case 64:
			layout = R.layout.list_item_64;
			break;
		case 62:
			layout = R.layout.list_item_62;
			break;
		case 58:
			layout = R.layout.list_item_58;
			break;
		case 56:
			layout = R.layout.list_item_56;
			break;
		case 54:
			layout = R.layout.list_item_54;
			break;
		case 52:
			layout = R.layout.list_item_52;
			break;
		case 50:
			layout = R.layout.list_item_50;
			break;
		case 48:
			layout = R.layout.list_item_48;
			break;
		case 46:
			layout = R.layout.list_item_46;
			break;
		case 44:
			layout = R.layout.list_item_44;
			break;
		case 42:
			layout = R.layout.list_item_42;
			break;
		case 40:
			layout = R.layout.list_item_40;
			break;
		case 38:
			layout = R.layout.list_item_38;
			break;
		case 36:
			layout = R.layout.list_item_36;
			break;
		case 34:
			layout = R.layout.list_item_34;
			break;
		case 32:
			layout = R.layout.list_item_32;
			break;
		case 30:
			layout = R.layout.list_item_30;
			break;
		default:
			layout = R.layout.list_item_60;
			preference.setBibleReadingTextSize(60);
			break;
		}
		
		return layout;
	}
}
