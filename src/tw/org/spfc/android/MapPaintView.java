package tw.org.spfc.android;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.service.BibleService;

import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.EvernoteUtil;
import com.evernote.client.android.OnClientCallback;
import com.evernote.client.conn.mobile.FileData;
import com.evernote.edam.type.Note;
import com.evernote.edam.type.Resource;
import com.evernote.edam.type.ResourceAttributes;
import com.evernote.thrift.transport.TTransportException;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.google.analytics.tracking.android.EasyTracker;

import csiebug.android.ContextMethod;
import csiebug.service.ServiceException;
import csiebug.util.StringUtility;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

public class MapPaintView extends csiebug.android.PaintView {
	private Context context;
	private String exportFileName = "";
	private boolean facebookEnable = false;
	private String[] versionNames;
	
	public MapPaintView(Context c, AttributeSet attrs) {
		super(c, attrs);
		context = c;
		
		initPaint();
	}
	
	public MapPaintView(Context c) {
		super(c);
		context = c;
		
		initPaint();
	}
	
	public void setExportFileName(String fileName) {
		exportFileName = fileName;
	}
	
	public void setFacebookEnable(boolean flag) {
		facebookEnable = flag;
		
		if(facebookEnable) {
			versionNames = new String[7];
		} else {
			versionNames = new String[6];
		}
		
		versionNames[0] = context.getString(R.string.writing_disable);
		versionNames[1] = context.getString(R.string.clear_note);
		versionNames[2] = context.getString(R.string.export_note);
		versionNames[3] = context.getString(R.string.blue_pen);
		versionNames[4] = context.getString(R.string.red_pen);
		versionNames[5] = context.getString(R.string.eraser);
		
		if(facebookEnable) {
			versionNames[6] = context.getString(R.string.share_to_facebook);
		}
	}

	@Override
	protected void onLongClickEvent() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
	    
		builder.setItems(versionNames, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if(which == 0) {
					LayoutParams params = getLayoutParams();
					params.height = 1;
					params.width = 1;
					setLayoutParams(params);
					
					initPaint();
				} else if(which == 1) {
					EasyTracker.getTracker().sendEvent("paintBible", "clear", "", (long)0);
					
					clear();
				} else if(which == 2) {
					EasyTracker.getTracker().sendEvent("paintBible", "save", "", (long)0);
					
					try {
						File tempFile = saveBitmap(takeScreenshot());
						saveToGallery(tempFile);
						
						EvernoteSession evernoteSession = EvernoteSession.getInstance(context, context.getString(R.string.evernote_consumer_key), context.getString(R.string.evernote_consumer_secret), EvernoteSession.EvernoteService.PRODUCTION);
						if(evernoteSession.isLoggedIn()) {
							InputStream in = null;
							try {
								// Hash the data in the image file. The hash is used to reference the
						        // file in the ENML note content.
						        in = new BufferedInputStream(new FileInputStream(tempFile));
						        FileData data = new FileData(EvernoteUtil.hash(in), tempFile);
						        
						        // Create a new Resource
						        Resource resource = new Resource();
						        resource.setData(data);
						        resource.setMime("image/png");
						        ResourceAttributes attributes = new ResourceAttributes();
						        attributes.setFileName(exportFileName);
						        resource.setAttributes(attributes);
								
								Note evernoteNote = new Note();
								evernoteNote.addToResources(resource);
								
								int end = 0;
								for(int i = 0; i < exportFileName.length(); i++) {
									char c = exportFileName.charAt(i);
									if('0' == c || '1' == c) {
										end = i;
										break;
									}
								}
								
								int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
								SPFCPreference preference = new SPFCPreference(context);
								int version = preference.getBibleVersion();
//								Locale phoneLocale = getResources().getConfiguration().locale;
//								
//								if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//									language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//								} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//									language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//								} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//									language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//								} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//									language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//								} else {
//									language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//								}
								if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
									language = BibleCatagoryDAOImpl.CHINA_BIBLE;
								} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
									language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
								} else if(version == BibleDAOImpl.AA) {
									language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
								} else if(version == BibleDAOImpl.DN1933) {
									language = BibleCatagoryDAOImpl.DANISH_BIBLE;
								} else if(version == BibleDAOImpl.DNB1930) {
									language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
								} else if(version == BibleDAOImpl.KAR) {
									language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
								} else if(version == BibleDAOImpl.LSG) {
									language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
								} else if(version == BibleDAOImpl.LUTH1545) {
									language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
								} else if(version == BibleDAOImpl.R1933) {
									language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
								} else if(version == BibleDAOImpl.RMNN) {
									language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
								} else if(version == BibleDAOImpl.RVA) {
									language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
								} else if(version == BibleDAOImpl.SV1917) {
									language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
								} else {
									language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
								}
								
								String bookName = exportFileName.substring(0, end);
								
								try {
									BibleService service = ServiceProxyFactory.createBibleService(context, version, language);
									List<String> fhl = service.getBookFHLAbbrNames();
									for(int i = 0; i < fhl.size(); i++) {
										if(fhl.get(i).equalsIgnoreCase(bookName)) {
											bookName = service.getBookNames().get(i);
											break;
										}
									}
								} catch (ServiceException e) {
									throw new RuntimeException(e);
								}
								
								evernoteNote.setTitle(bookName + ":" + Integer.parseInt(exportFileName.substring(end, end + 3)));
								
								// Set the note's ENML content. Learn about ENML at
						        // http://dev.evernote.com/documentation/cloud/chapters/ENML.php
						        String content =
						            EvernoteUtil.NOTE_PREFIX +
						                bookName + ":" + Integer.parseInt(exportFileName.substring(end, end + 3)) +
						                EvernoteUtil.createEnMediaTag(resource) +
						                EvernoteUtil.NOTE_SUFFIX;

						        evernoteNote.setContent(content);
								
								List<String> tags = new ArrayList<String>();
								tags.add("SPFC");
								tags.add("Bible");
								tags.add(bookName);
								
								evernoteNote.setTagNames(tags);
								
								evernoteSession.getClientFactory().createNoteStoreClient().createNote(evernoteNote, new OnClientCallback<Note>() {
								    @Override
								    public void onSuccess(Note data) {
								      ContextMethod.toast(context, context.getString(R.string.evernote_success));
								    }

								    @Override
								    public void onException(Exception exception) {
								      ContextMethod.toast(context, context.getString(R.string.evernote_failure));
								    }
								});
							} catch (TTransportException e) {
								ContextMethod.toast(context, context.getString(R.string.evernote_failure));
							} finally {
								if(in != null) {
									in.close();
								}
							}
						}
						
						tempFile.deleteOnExit();
						//ContextMethod.toast(context, context.getString(R.string.writing_export_hint) + "(" + context.getString(R.string.note_file_in) + context.getString(R.string.bibleReadingNotesFolder) + ")");
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				} else if(which == 3) {
					setEraseMode(false);
					setStrokeColor(Color.BLUE);
					setStrokeWidth(Float.parseFloat("4"));
				} else if(which == 4) {
					setEraseMode(false);
					setStrokeColor(Color.RED);
					setStrokeWidth(Float.parseFloat("4"));
				} else if(which == 5) {
					setEraseMode(true);
					setStrokeWidth(Float.parseFloat("40"));
				} else if(which == 6) {
					Bitmap image = takeScreenshot();
		            Request request = Request.newUploadPhotoRequest(Session.getActiveSession(), image, new Request.Callback() {
		                public void onCompleted(Response response) {
		                    ContextMethod.toast(context, context.getString(R.string.facebook_photo_uploaded));
		                }
		            });
		            request.executeAsync();
				}
			}
	    });
	    builder.create();
	    
	    builder.show();
	}
	
	public void initPaint() {
		try {
			clear();
		} catch(Exception e) {
			
		}
		setEraseMode(false);
		setStrokeColor(Color.BLUE);
		setStrokeWidth(Float.parseFloat("4"));
	}
	
	public Bitmap takeScreenshot() {
		View rootView = getRootView();
		rootView.setDrawingCacheEnabled(true);
		return rootView.getDrawingCache();
	}
	
	public File saveBitmap(Bitmap bitmap) throws IOException {
		if(exportFileName.equals("")) {
			exportFileName = Calendar.getInstance().getTimeInMillis() + ".png";
		}
		
		File noteFolder = SPFCFileSystem.getBibleReadingNotesFolder(context);
		
		if(noteFolder != null && (noteFolder.exists() || noteFolder.mkdir())) {
			File imagePath = SPFCFileSystem.getBibleReadingNote(context, exportFileName);
		    FileOutputStream fos = null;
		    try {
		        fos = new FileOutputStream(imagePath);
		        bitmap.compress(CompressFormat.JPEG, 100, fos);
		        fos.flush();
		    } finally {
		    	if(fos != null) {
		    		fos.close();
		    	}
		    }
		    
		    return imagePath;
		} else {
			throw new RuntimeException("Can't create note folder!");
		}
	}
	
	public void saveToGallery(File file) {
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    Uri contentUri = Uri.fromFile(file);
	    mediaScanIntent.setData(contentUri);
	    context.sendBroadcast(mediaScanIntent);
	}
}
