package tw.org.spfc.android;

import java.util.List;

import bible.domain.BibleBook;
import bible.domain.BibleChapter;
import bible.domain.BibleVerse;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.StringUtility;
import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.domain.BibleBookMark;
import tw.org.spfc.domain.BibleReadingPlan;
import tw.org.spfc.service.BibleService;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;

public class BibleChapterChooseActivity extends BaseActivity {
	private int book;
	private int baseIndex = 0;
	private int bookmarkBook;
	private int bookmarkChapter;
	private int bookmarkVerse = 1;
	private String[] testaments;
	private String[] books;
	private String[] bookmarks;
	private String[] displayBookmarks;
	private String[] spfcBookmarks;
	private BibleService service;
	private SPFCPreference preference;
	private Spinner testamentSpinner;
	private Spinner bookSpinner;
	private Spinner bookmarkSpinner;
	
	@Override
	protected int getLayout() {
		return R.layout.bible_chapter_choose;
	}

	@Override
	protected void setListeners() {
		bookmarkSpinner.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				int index = bookmarkSpinner.getSelectedItemPosition() - 1;
				if(index > -1) {
					if(preference.isSPFCBookMarkEnable()) {
						if(index < spfcBookmarks.length) {
							bookmarkBook = Integer.parseInt(spfcBookmarks[index].split(":")[0]);
							bookmarkChapter = Integer.parseInt(spfcBookmarks[index].split(":")[1]);
							bookmarkVerse = Integer.parseInt(spfcBookmarks[index].split(":")[2]);
						} else {
							bookmarkBook = Integer.parseInt(bookmarks[index - spfcBookmarks.length].split(":")[0]);
							bookmarkChapter = Integer.parseInt(bookmarks[index - spfcBookmarks.length].split(":")[1]);
							bookmarkVerse = 1;
						}
					} else {
						bookmarkBook = Integer.parseInt(bookmarks[index].split(":")[0]);
						bookmarkChapter = Integer.parseInt(bookmarks[index].split(":")[1]);
						bookmarkVerse = 1;
					}
				}
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
		
		findViewById(R.id.reading).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				if(bookmarkSpinner.getSelectedItemPosition() > 0) {
					preference.setBibleReading(bookmarkBook, bookmarkChapter, bookmarkVerse);
					setResult(1);
					finish();
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.delete).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				int index = bookmarkSpinner.getSelectedItemPosition() - 1;
				
				if(index > -1) {
					if(preference.isSPFCBookMarkEnable()) {
						if(index >= spfcBookmarks.length) {
							String[] temp1 = new String[bookmarks.length - 1];
							String[] temp2 = new String[displayBookmarks.length - 1];
							
							for(int i = 0; i < temp2.length; i++) {
								if(i <= spfcBookmarks.length) {
									temp2[i] = displayBookmarks[i];
								} else {
									if(i <= index) {
										temp1[i - spfcBookmarks.length - 1] = bookmarks[i - spfcBookmarks.length - 1];
										temp2[i] = displayBookmarks[i];
									} else {
										temp1[i - spfcBookmarks.length - 1] = bookmarks[i - spfcBookmarks.length];
										temp2[i] = displayBookmarks[i + 1].replace((i - spfcBookmarks.length + 1) + ". ", (i - spfcBookmarks.length) + ". ");
									}
								}
							}
							
							preference.removeBibleBookMark(bookmarkBook, bookmarkChapter);
							
							bookmarks = temp1;
							displayBookmarks = temp2;
							
							setAdapters();
							
							toast(getString(R.string.delete_success));
						}
					} else {
						String[] temp1 = new String[bookmarks.length - 1];
						String[] temp2 = new String[displayBookmarks.length - 1];
						temp2[0] = displayBookmarks[0];
						
						for(int i = 0; i < temp1.length; i++) {
							if(i < index) {
								temp1[i] = bookmarks[i];
								temp2[i + 1] = displayBookmarks[i + 1];
							} else {
								temp1[i] = bookmarks[i + 1];
								temp2[i + 1] = displayBookmarks[i + 2].replace((i + 2) + ". ", (i + 1) + ". ");
							}
						}
						
						preference.removeBibleBookMark(bookmarkBook, bookmarkChapter);
						
						bookmarks = temp1;
						displayBookmarks = temp2;
						
						setAdapters();
						
						toast(getString(R.string.delete_success));
					}
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		testamentSpinner.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				try {
					List<String> list;
					switch (testamentSpinner.getSelectedItemPosition()) {
						case 0:
							baseIndex = 0;
							list = service.getBookNames();
							break;
						case 1:
							baseIndex = 0;
							list = service.getOldTestamentBookNames();
							break;
						case 2:
							baseIndex = BibleService.OLD_TESTAMENT_BOOK_COUNT;
							list = service.getNewTestamentBookNames();
							break;
						case 3:
							baseIndex = 0;
							list = service.getOldTestamentLawBookNames();
							break;
						case 4:
							baseIndex = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT;
							list = service.getOldTestamentHistoryBookNames();
							break;
						case 5:
							baseIndex = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT;
							list = service.getOldTestamentWisdomBookNames();
							break;
						case 6:
							baseIndex = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.OLD_TESTAMENT_WISDOM_BOOK_COUNT;
							list = service.getOldTestamentProphetsBookNames();
							break;
						case 7:
							baseIndex = BibleService.OLD_TESTAMENT_BOOK_COUNT;
							list = service.getNewTestamentGospelsBookNames();
							break;
						case 8:
							baseIndex = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT;
							list = service.getNewTestamentHistoryBookNames();
							break;
						case 9:
							baseIndex = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT;
							list = service.getNewTestamentPaulineEpistlesBookNames();
							break;
						case 10:
							baseIndex = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT;
							list = service.getNewTestamentGeneralEpistlesBookNames();
							break;
						case 11:
							baseIndex = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT + BibleService.NEW_TESTAMENT_GENERAL_EPISTLES_BOOK_COUNT;
							list = service.getNewTestamentApocalypseBookNames();
							break;
						default:
							list = service.getBookNames();
							break;
					}
					
					books = new String[list.size()];
					for(int i = 0; i < list.size(); i++) {
						books[i] = list.get(i);
					}
					ArrayAdapter<String> bookAdapter = new ArrayAdapter<String>(BibleChapterChooseActivity.this, android.R.layout.simple_spinner_item, books);
					bookAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					bookSpinner.setAdapter(bookAdapter);
					if((book - baseIndex) < list.size() && (book - baseIndex) >= 0) {
						bookSpinner.setSelection(book - baseIndex);
					} else {
						bookSpinner.setSelection(0);
					}
					
					try {
						setChaptersArray(baseIndex + bookSpinner.getSelectedItemPosition());
					} catch (ServiceException e) {
						throw new RuntimeException(e);
					}
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});

		bookSpinner.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				try {
					setChaptersArray(baseIndex + bookSpinner.getSelectedItemPosition());
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
		((CheckBox)findViewById(R.id.spfc_bookmark_enable)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(AndroidUtility.isNetworkAvailable(BibleChapterChooseActivity.this)) {
					Boolean original = preference.isSPFCBookMarkEnable();
					try {
						preference.setSPFCBookMarkEnable(isChecked);
						initBookmark(service);
						setAdapters();
					} catch (ServiceException e) {
						preference.setSPFCBookMarkEnable(original);
						buttonView.setChecked(original);
					}
				}
			}
		});
	}

	@Override
	protected void setAdapters() {
		ArrayAdapter<String> bookmarkAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, displayBookmarks);
		bookmarkAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		bookmarkSpinner.setAdapter(bookmarkAdapter);
		ArrayAdapter<String> testamentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, testaments);
		testamentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		testamentSpinner.setAdapter(testamentAdapter);
		ArrayAdapter<String> bookAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, books);
		bookAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		bookSpinner.setAdapter(bookAdapter);
		bookSpinner.setSelection(book);
	}
	
	private void setChaptersArray(int book) throws ServiceException {
		int length = service.getBookChapters(book);
		BibleReadingPlan plan = service.getBibleReadingPlan(preference.getBibleReadingPlan());
		
		LinearLayout chaptersGrid = (LinearLayout)findViewById(R.id.chaptersGrid);
		chaptersGrid.removeAllViews();
		LinearLayout chapterRow = new LinearLayout(this);
		for(int i = 0; i < length; i++) {
			if(i == 0 || (i >= 9 && i <= 99 && (i % 7) == 2) || (i >= 100 && (i % 6) == 4)) {
				chapterRow = new LinearLayout(this);
				chapterRow.setOrientation(LinearLayout.HORIZONTAL);
				LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
				chapterRow.setLayoutParams(params);
				chaptersGrid.addView(chapterRow);
			}
			
			final int chapter = i + 1; 
			Button button = new Button(this);
			button.setText("" + chapter);
			android.view.ViewGroup.LayoutParams params = new android.view.ViewGroup.LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
			button.setLayoutParams(params);
			if(plan != null && service.isReaded(null, plan, book, chapter)) {
				button.setBackgroundResource(R.drawable.orangebutton);
			} else {
				button.setBackgroundResource(R.drawable.greenbutton);
			}
			button.setOnClickListener(new AbstractDefaultButtonListener(this) {
				
				@Override
				protected void onClickLogic(View v) {
					preference.setBibleReading(baseIndex + bookSpinner.getSelectedItemPosition(), chapter, 1);
					setResult(1);
					finish();
				}
				
				@Override
				protected void onClickLogic(DialogInterface dialog, int which) {}
			});
			chapterRow.addView(button);
		}
	}
	
	@Override
	protected void findViews() {
		bookmarkSpinner = (Spinner)findViewById(R.id.bookmark);
		testamentSpinner = (Spinner)findViewById(R.id.testament);
		bookSpinner = (Spinner)findViewById(R.id.books);
	}
	
	@Override
	protected void onCreateLogic() {
		preference = new SPFCPreference(this);
		bookmarks = preference.getBibleBookMark();
		book = preference.getBibleReadingBook();
		testaments = new String[]{getString(R.string.all), getString(R.string.old_testament), getString(R.string.new_testament), getString(R.string.old_testament_law), getString(R.string.old_testament_history), getString(R.string.old_testament_wisdom), getString(R.string.old_testament_prophets), getString(R.string.new_testament_gospels), getString(R.string.new_testament_history), getString(R.string.new_testament_pauline_epistles), getString(R.string.new_testament_general_epistles), getString(R.string.new_testament_apocalypse)};
		
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//		int version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//			version = BibleDAOImpl.CHINA_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//			version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//			version = BibleDAOImpl.LSG;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//			version = BibleDAOImpl.LUTH1545;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//			version = BibleDAOImpl.KING_JAMES_VERSION;
//		}
		int version = preference.getBibleVersion();
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
		}
		
		try {
			service = ServiceProxyFactory.createBibleService(this, version, language);
			List<String> list = service.getBookNames();
			books = new String[list.size()];
			for(int i = 0; i < list.size(); i++) {
				books[i] = list.get(i);
			}
			
			((CheckBox)findViewById(R.id.spfc_bookmark_enable)).setChecked(preference.isSPFCBookMarkEnable());
			initBookmark(service);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void initBookmark(BibleService service) throws ServiceException {
		if(preference.isSPFCBookMarkEnable() && AndroidUtility.isNetworkAvailable(this)) {
			try {
				List<BibleBookMark> list2 = service.getSPFCBookMarks();
				spfcBookmarks = new String[list2.size()];
				
				displayBookmarks = new String[bookmarks.length + list2.size() + 1];
				displayBookmarks[0] = getString(R.string.choose_bookmark);
				for(int i = 0; i < bookmarks.length + list2.size(); i++) {
					if(i < list2.size()) {
						BibleVerse verse = list2.get(i).getBookMark();
						BibleChapter chapter = verse.getChapter();
						BibleBook book = chapter.getBook();
						spfcBookmarks[i] = StringUtility.addZero(book.getBookId(), 2) + ":" + StringUtility.addZero(chapter.getChapterId(), 3) + ":" + StringUtility.addZero(verse.getVerseId(), 3);
						
						displayBookmarks[i + 1] = list2.get(i).getBookMarkName();
					} else {
						int book = Integer.parseInt(bookmarks[i - list2.size()].split(":")[0]);
						int chapter = Integer.parseInt(bookmarks[i - list2.size()].split(":")[1]);
						displayBookmarks[i + 1] = (i - list2.size() + 1) + ". " + service.getBookNames().get(book) + chapter;
					}
				}
			} catch(Exception e) {
				spfcBookmarks = new String[0];
				
				displayBookmarks = new String[bookmarks.length + 1];
				displayBookmarks[0] = getString(R.string.choose_bookmark);
				for(int i = 0; i < bookmarks.length; i++) {
					int book = Integer.parseInt(bookmarks[i].split(":")[0]);
					int chapter = Integer.parseInt(bookmarks[i].split(":")[1]);
					displayBookmarks[i + 1] = (i + 1) + ". " + service.getBookNames().get(book) + chapter;
				}
			}
		} else {
			spfcBookmarks = new String[0];
			
			displayBookmarks = new String[bookmarks.length + 1];
			displayBookmarks[0] = getString(R.string.choose_bookmark);
			for(int i = 0; i < bookmarks.length; i++) {
				int book = Integer.parseInt(bookmarks[i].split(":")[0]);
				int chapter = Integer.parseInt(bookmarks[i].split(":")[1]);
				displayBookmarks[i + 1] = (i + 1) + ". " + service.getBookNames().get(book) + chapter;
			}
		}
	}
}
