package tw.org.spfc.android;

import tw.org.spfc.android.util.SPFCPreference;
import android.os.Bundle;

import com.facebook.FacebookException;
import com.facebook.FacebookOperationCanceledException;
import com.facebook.Session;
import com.facebook.widget.WebDialog;
import com.facebook.widget.WebDialog.OnCompleteListener;
import com.google.analytics.tracking.android.EasyTracker;

import csiebug.util.AssertUtility;

public abstract class BaseFacebookActivity extends AbstractFacebookActivity {
	protected void handleFacebookPermissionNotGranted() {
		alert(getString(R.string.warning), getString(R.string.facebook_permission_not_granted), getString(R.string.ok));
	};
	
	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}
	
	@Override
	protected void onStopLogic() {
		super.onStopLogic();
		EasyTracker.getInstance().activityStop(this);
	}
	
	@Override
	protected void publishFeedDialog(final String name, String caption,
			String description, String link, String picture) {
		//覆寫此功能，為了加上Google Analytics
		EasyTracker.getTracker().sendEvent("facebook", "click", "", (long)0);
		
		Bundle params = new Bundle();
        if(AssertUtility.isNotNullAndNotSpace(name)) {
        	params.putString("name", name);
        }
        if(AssertUtility.isNotNullAndNotSpace(caption)) {
        	params.putString("caption", caption);
        }
        if(AssertUtility.isNotNullAndNotSpace(description)) {
        	params.putString("description", description);
        }
        if(AssertUtility.isNotNullAndNotSpace(link)) {
        	params.putString("link", link);
        }
        if(AssertUtility.isNotNullAndNotSpace(picture)) {
        	params.putString("picture", picture);
        }

        WebDialog feedDialog = (
            new WebDialog.FeedDialogBuilder(this,
                Session.getActiveSession(),
                params))
            .setOnCompleteListener(new OnCompleteListener() {

                public void onComplete(Bundle values,
                    FacebookException error) {
                    if (error == null) {
                        // When the story is posted, echo the success
                        // and the post Id.
                        final String postId = values.getString("post_id");
                        if (postId != null) {
                        	//toast("Posted story, id: "+postId);
                        	EasyTracker.getTracker().sendEvent(name, "share", "", (long)0);
                        } else {
                        	//toast("Publish cancelled");
                        	EasyTracker.getTracker().sendEvent("facebook", "cancel", "", (long)0);
                        }
                    } else if (error instanceof FacebookOperationCanceledException) {
                    	//toast("Publish cancelled");
                    	EasyTracker.getTracker().sendEvent("facebook", "cancel", "", (long)0);
                    } else {
                    	//toast("Error posting story");
                    }
                }

            })
            .build();
        feedDialog.show();
	}
	
	/**
	 * 判斷是否已登入Twitter
	 * @return
	 */
	protected boolean isLoggedTwitter() {
		SPFCPreference preference = new SPFCPreference(this);
		return !preference.getTwitterToken().trim().equals("");
	}
}
