package tw.org.spfc.android;

import java.util.Calendar;

import com.google.analytics.tracking.android.EasyTracker;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.util.SPFCPreference;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.CompoundButton.OnCheckedChangeListener;
import csiebug.util.DateFormatException;
import csiebug.util.StringUtility;

public class SermonSettingsActivity extends BaseActivity {
	
	@Override
	protected int getLayout() {
		return R.layout.sermon_settings;
	}

	@Override
	protected void setListeners() {
		((CheckBox)findViewById(R.id.preDownload)).setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					EasyTracker.getTracker().sendEvent("preDownloadSermon", "use", "", (long)0);
				}
				
				((TextView)findViewById(R.id.preDownloadTime)).setClickable(isChecked);
				((EditText)findViewById(R.id.local_sermon_count)).setEnabled(isChecked);
			}
		});
		
		findViewById(R.id.preDownloadTime).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				SPFCPreference preference = new SPFCPreference(SermonSettingsActivity.this);
				new TimePickerDialog(SermonSettingsActivity.this, new TimePickerDialog.OnTimeSetListener() {
					
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						((TextView)findViewById(R.id.preDownloadTime)).setText(StringUtility.addZero(hourOfDay, 2) + ":" + StringUtility.addZero(minute, 2));
					}
				}, preference.getPreDownloadHour(), preference.getPreDownloadMinutes(), true).show();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
	}

	@Override
	protected void setAdapters() {
	}
	
	@Override
	protected void onCreateLogic() {
		//設定UI初始值
		SPFCPreference preference = new SPFCPreference(this);
		((EditText)findViewById(R.id.sermon_list_length)).setText("" + preference.getSermonListLength());
		CheckBox preDownload = (CheckBox)findViewById(R.id.preDownload);
		preDownload.setChecked(preference.getPreDownload());
		preDownload.setText(getString(R.string.pre_download) + "(" + getString(R.string.sermon_file_in) + getString(R.string.localSermonFolder) + ")");
		
		((TextView)findViewById(R.id.preDownloadTime)).setText(preference.getPreDownloadTime());
		findViewById(R.id.preDownloadTime).setClickable(preference.getPreDownload());
		((EditText)findViewById(R.id.local_sermon_count)).setText("" + preference.getLocalSermonCount());
	}
	
	@Override
	protected void onPauseLogic() {
		SPFCPreference preference = new SPFCPreference(SermonSettingsActivity.this);
		preference.setSermonListLength(Integer.parseInt(((TextView)findViewById(R.id.sermon_list_length)).getText().toString()));
		boolean preDownload = ((CheckBox)findViewById(R.id.preDownload)).isChecked();
		String preDownloadTimeString = ((TextView)findViewById(R.id.preDownloadTime)).getText().toString();
		int localSermonCount = Integer.parseInt(((TextView)findViewById(R.id.local_sermon_count)).getText().toString());
		preference.setPreDownload(preDownload, preDownloadTimeString, localSermonCount);
		
		try {
			updateAlarmManager(preDownload, preDownloadTimeString, preference.isDownloadingSermon());
		} catch (NumberFormatException e) {
			throw new RuntimeException(e);
		} catch (DateFormatException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void updateAlarmManager(boolean preDownload, String preDownloadTimeString, boolean isDownloading) {
		AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
		long repeatTime = 24 * 60 * 60 * 1000;
		Intent intent = new Intent(this, DownloadSermonReceiver.class);
		PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent, 0);
		
		if(preDownload) {
			//新增定時處理
			int hour = Integer.parseInt(preDownloadTimeString.split(":")[0]);
			int minute = Integer.parseInt(preDownloadTimeString.split(":")[1]);
			Calendar firstTriggerTime = Calendar.getInstance();
			firstTriggerTime.set(Calendar.HOUR_OF_DAY, hour);
			firstTriggerTime.set(Calendar.MINUTE, minute);
			firstTriggerTime.set(Calendar.SECOND, 0);
			if(isDownloading) {
				//如果已經正在下載了,就往後延一天
				firstTriggerTime.add(Calendar.DAY_OF_MONTH, 1);
			}
			
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, firstTriggerTime.getTimeInMillis(), repeatTime, sender);
		} else {
			//取消定時處理
			alarmManager.cancel(sender);
		}
	}
}
