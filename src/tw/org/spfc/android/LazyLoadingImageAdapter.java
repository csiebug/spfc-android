package tw.org.spfc.android;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;

public class LazyLoadingImageAdapter extends BaseAdapter implements SpinnerAdapter {
	// 當前要load進來的資料,儘量通過快取的方式減少IO的次數並且節省記憶體用量
	private Map<String, ImageView> caches = new HashMap<String, ImageView>();
	// 當前load進來的資料的開始索引數
	private int startIndex = -1;
	// 當前load進來的資料的結束索引數
	private int endIndex = -1;
	// cache的容量
	private int cacheSize = 9;
	// 是否是local檔案
	private boolean localFlag = false;
	
	private class ImagePreLoader implements Runnable {
		private int position;
		
		public ImagePreLoader(int position) {
			this.position = position;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			try {
				preloadPreviousImages(position);
				preloadNextImages(position);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	};
	
	private List<String> imageURLs;
	private Context context;
	
	public LazyLoadingImageAdapter(List<String> imageURLs, Context context) {
		this.imageURLs = imageURLs;
		this.context = context;
	}
	
	public void setCacheSize(int size) {
		this.cacheSize = size;
	}
	
	public void setLocalFlag(boolean flag) {
		this.localFlag = flag;
	}
	
	public int getCount() {
		return imageURLs.size();
	}
	public Object getItem(int position) {
		// 先檢查用戶請求的資料是否在快取中
		// 如果不在
		// 就先清除快取(為了節省記憶體)
		// 然後從IO中取得資料
		// 主要考慮速度的問題
		// 因為從快取中取得資料要比IO中取快得多
		if((position > endIndex) || (position < startIndex)) {
			caches.clear();
			
			try {
				loadImages();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			//開Thread來作cache loading,不要影響主線程(Adapter正常的切換)
			ImagePreLoader loader = new ImagePreLoader(position);
			Thread thread = new Thread(loader);
			thread.start();
		}
		
		return caches.get(Integer.toString(position));
	}
	public long getItemId(int position) {
		return position;
	}
	public View getView(int position, View convertView, ViewGroup parent) {
		// 先檢查用戶請求的資料是否在快取中
		// 如果不在
		// 就先清除快取(為了節省記憶體)
		// 然後從IO中取得資料
		// 主要考慮速度的問題
		// 因為從快取中取得資料要比IO中取快得多
		if((position > endIndex) || (position < startIndex)) {
			caches.clear();
			
			try {
				loadImages();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		} else {
			//開Thread來作cache loading,不要影響主線程(Adapter正常的切換)
			ImagePreLoader loader = new ImagePreLoader(position);
			Thread thread = new Thread(loader);
			thread.start();
		}
		
		return caches.get(Integer.toString(position));
	}
	
	private void preloadPreviousImages(int position) throws IOException {
		if(position < startIndex + (cacheSize / 3)) {
			int newStartIndex = startIndex - (cacheSize / 3);
			
			if(newStartIndex < 0) {
				newStartIndex = 0;
			}
			
			int newEndIndex = newStartIndex + cacheSize - 1;
			
			if(newEndIndex > imageURLs.size() - 1) {
				newEndIndex = imageURLs.size() - 1;
			}
			
			for(int i = newStartIndex; i <= newEndIndex; i++) {
				if(!caches.containsKey(Integer.toString(i))) {
					Log.d(getClass().getName(), "url: " + imageURLs.get(i));
					ImageView image = new ImageView(context);
					
					if(localFlag) {
						image.setImageBitmap(BitmapFactory.decodeFile(imageURLs.get(i)));
					} else {
						URL url = new URL(imageURLs.get(i));
						URLConnection connection = url.openConnection();
						connection.connect();
						image.setImageBitmap(BitmapFactory.decodeStream(new BufferedInputStream(connection.getInputStream())));
					}
					
					caches.put(Integer.toString(i), image);
				}
			}
			
			for(int i = newEndIndex + 1; i <= endIndex; i++) {
				caches.remove(Integer.toString(i));
			}
			
			startIndex = newStartIndex;
			endIndex = newEndIndex;
		}
	}
	
	private void preloadNextImages(int position) throws IOException {
		if(position > startIndex + ((cacheSize / 3) * 2) - 1) {
			int newStartIndex = startIndex + (cacheSize / 3);
			
			if(newStartIndex > imageURLs.size() - 1) {
				newStartIndex = imageURLs.size() - 1;
			}
			
			int newEndIndex = newStartIndex + cacheSize - 1;
			
			if(newEndIndex > imageURLs.size() - 1) {
				newEndIndex = imageURLs.size() - 1;
			}
			
			for(int i = newStartIndex; i <= newEndIndex; i++) {
				if(!caches.containsKey(Integer.toString(i))) {
					Log.d(getClass().getName(), "url: " + imageURLs.get(i));
					ImageView image = new ImageView(context);
					
					if(localFlag) {
						image.setImageBitmap(BitmapFactory.decodeFile(imageURLs.get(i)));
					} else {
						URL url = new URL(imageURLs.get(i));
						URLConnection connection = url.openConnection();
						connection.connect();
						image.setImageBitmap(BitmapFactory.decodeStream(new BufferedInputStream(connection.getInputStream())));
					}
					caches.put(Integer.toString(i), image);
				}
			}
			
			for(int i = startIndex; i <= newStartIndex - 1; i++) {
				caches.remove(Integer.toString(i));
			}
			
			startIndex = newStartIndex;
			endIndex = newEndIndex;
		}
	}
	
	private void loadImages() throws IOException {
		if(startIndex == -1) {
			startIndex = 0;
			endIndex = startIndex + cacheSize - 1;
			
			if(endIndex > imageURLs.size() - 1) {
				endIndex = imageURLs.size() - 1;
			}
		} else {
			startIndex = endIndex + 1;
			
			if(startIndex > imageURLs.size() - 1) {
				startIndex = imageURLs.size() - 1;
			}
			
			endIndex = startIndex + cacheSize - 1;
			
			if(endIndex > imageURLs.size() - 1) {
				endIndex = imageURLs.size() - 1;
			}
		}
		
		for(int i = startIndex; i <= endIndex; i++) {
			Log.d(getClass().getName(), "url: " + imageURLs.get(i));
			ImageView image = new ImageView(context);
			
			if(localFlag) {
				image.setImageBitmap(BitmapFactory.decodeFile(imageURLs.get(i)));
			} else {
				URL url = new URL(imageURLs.get(i));
				URLConnection connection = url.openConnection();
				connection.connect();
				image.setImageBitmap(BitmapFactory.decodeStream(new BufferedInputStream(connection.getInputStream())));
			}
			
			caches.put(Integer.toString(i), image);
		}
	}
}
