package tw.org.spfc.android;

import java.util.List;

import csiebug.service.ServiceException;
import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.service.BibleService;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class BibleChapterIntervalChooseActivity extends BaseActivity {
	private int startBook = 0;
	private int startChapter = 1;
	private int endBook = 0;
	private int endChapter = 1;
	private String[] books;
	private Integer[] chapters;
	private BibleService service;
	private Spinner startBookSpinner;
	private Spinner startChapterSpinner;
	private Spinner endBookSpinner;
	private Spinner endChapterSpinner;
	
	@Override
	protected int getLayout() {
		return R.layout.bible_chapter_interval_choose;
	}

	@Override
	protected void setListeners() {
		startBookSpinner.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				try {
					setChaptersArray(startBookSpinner.getSelectedItemPosition());
					ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(BibleChapterIntervalChooseActivity.this, android.R.layout.simple_spinner_item, chapters);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					startChapterSpinner.setAdapter(adapter);
					startChapterSpinner.setSelection(0);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
		
		endBookSpinner.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				try {
					setChaptersArray(endBookSpinner.getSelectedItemPosition());
					ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(BibleChapterIntervalChooseActivity.this, android.R.layout.simple_spinner_item, chapters);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					endChapterSpinner.setAdapter(adapter);
					endChapterSpinner.setSelection(0);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
		
		findViewById(R.id.ok).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent data = new Intent();
				Bundle bundle = new Bundle();
				bundle.putInt("startBook", startBookSpinner.getSelectedItemPosition());
				bundle.putInt("startChapter", startChapterSpinner.getSelectedItemPosition() + 1);
				bundle.putInt("endBook", endBookSpinner.getSelectedItemPosition());
				bundle.putInt("endChapter", endChapterSpinner.getSelectedItemPosition() + 1);
				data.putExtras(bundle);
				setResult(RESULT_OK, data);
				finish();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
	}
	
	@Override
	protected void setAdapters() {
		ArrayAdapter<String> startBookAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, books);
		startBookAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		startBookSpinner.setAdapter(startBookAdapter);
		startBookSpinner.setSelection(startBook);
		
		ArrayAdapter<String> endBookAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, books);
		endBookAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		endBookSpinner.setAdapter(endBookAdapter);
		endBookSpinner.setSelection(endBook);
		try {
			setChaptersArray(startBook);
			ArrayAdapter<Integer> startChapterAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, chapters);
			startChapterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			startChapterSpinner.setAdapter(startChapterAdapter);
			startChapterSpinner.setSelection(startChapter - 1);
			
			setChaptersArray(endBook);
			ArrayAdapter<Integer> endChapterAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_spinner_item, chapters);
			endChapterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			endChapterSpinner.setAdapter(endChapterAdapter);
			endChapterSpinner.setSelection(endChapter - 1);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void setChaptersArray(int book) throws ServiceException {
		int length = service.getBookChapters(book);
		chapters = new Integer[length];
		for(int i = 0; i < length; i++) {
			chapters[i] = i + 1;
		}
	}
	
	@Override
	protected void findViews() {
		startBookSpinner = (Spinner)findViewById(R.id.start_book);
		startChapterSpinner = (Spinner)findViewById(R.id.start_chapter);
		endBookSpinner = (Spinner)findViewById(R.id.end_book);
		endChapterSpinner = (Spinner)findViewById(R.id.end_chapter);
	}
	
	@Override
	protected void onCreateLogic() {
		Bundle bundle = getIntent().getExtras();
		
		if(bundle != null) {
			startBook = bundle.getInt("startBook");
			startChapter = bundle.getInt("startChapter");
			endBook = bundle.getInt("endBook");
			endChapter = bundle.getInt("endChapter");
		}
		
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//		int version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//			version = BibleDAOImpl.CHINA_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//			version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//			version = BibleDAOImpl.LSG;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//			version = BibleDAOImpl.LUTH1545;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//			version = BibleDAOImpl.KING_JAMES_VERSION;
//		}
		SPFCPreference preference = new SPFCPreference(this);
		int version = preference.getBibleVersion();
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
		}
		
		try {
			service = ServiceProxyFactory.createBibleService(this, version, language);
			List<String> list = service.getBookNames();
			books = new String[list.size()];
			for(int i = 0; i < list.size(); i++) {
				books[i] = list.get(i);
			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
}
