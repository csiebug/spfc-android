package tw.org.spfc.android;

import csiebug.android.AbstractWebViewHTMLViewer;
import tw.org.spfc.android.util.SPFCPreference;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.util.Log;
import android.webkit.WebView;

public class TwitterOAuthActivity extends BaseWebActivity {
	private static Twitter twitter;
	private static RequestToken requestToken;

	@Override
	protected String getWebViewURL() {
		ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setOAuthConsumerKey(getString(R.string.twitter_consumer_key));
		configurationBuilder.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret));
		configurationBuilder.setUseSSL(true);
		Configuration configuration = configurationBuilder.build();
		twitter = new TwitterFactory(configuration).getInstance();

		try {
			requestToken = twitter.getOAuthRequestToken("");
			String authenticationURL = requestToken.getAuthenticationURL().replace("http", "https");
			
			return authenticationURL;
		} catch (TwitterException e) {
			Log.e(getClass().getName(), "TwitterException", e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void customWebViewSettings(final WebView webView) {
		webView.addJavascriptInterface(new CallbackHandler(), "HtmlViewer");
	}
	
	class CallbackHandler extends AbstractWebViewHTMLViewer {
        public void showHTML(String html) {
        	if(html.indexOf("<code>") != -1) {
        		String verifier = html.substring(html.indexOf("<code>") + 6, html.indexOf("</code>"));
        		
        		try {
        			SPFCPreference preference = new SPFCPreference(TwitterOAuthActivity.this);
        			
        			AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier); 
                    preference.loginTwitter(accessToken.getToken(), accessToken.getTokenSecret());
                } catch (TwitterException e) {
                	Log.e(getClass().getName(), "TwitterException", e);
				}
        		
        		finish();
        	}
        }
    }
}
