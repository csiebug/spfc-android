package tw.org.spfc.android;

import com.facebook.FacebookRequestError;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.service.FacebookService;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.domain.Book;
import tw.org.spfc.domain.pojoImpl.BookImpl;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TextView;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;

public class BookDetailActivity extends BaseFacebookActivity {
	@Override
	protected int getLayout() {
		return R.layout.library_query_detail;
	}

	@Override
	protected void setListeners() {
		findViewById(R.id.share_to_facebook).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
			@Override
			protected void onClickLogic(View arg0) {
				Bundle bundle = getIntent().getBundleExtra(BookListActivity.class.getName() + ".bundle");
				String pictureURL = getString(R.string.spfcAppIcon);
				try {
					pictureURL = ServiceProxyFactory.createFacebookService(BookDetailActivity.this).getPicture(FacebookService.FACEBOOK_MY_FAVOR_BOOK);
				} catch (ServiceException e) {
					Log.e(getClass().getName(), "Get picture URL error!", e);
				}
				
				publishFeedDialog(getString(R.string.facebook_my_favor_book), "", bundle.getString("name"), bundle.getString("link"), pictureURL);
			}
		});
		
		findViewById(R.id.share_to_twitter).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				SPFCPreference preference = new SPFCPreference(BookDetailActivity.this);
				String oauthAccessToken = preference.getTwitterToken();
				String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

				ConfigurationBuilder confbuilder = new ConfigurationBuilder();
				Configuration conf = confbuilder
									.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
									.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
									.setOAuthAccessToken(oauthAccessToken)
									.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
									.setUseSSL(true)
									.build();
				
				Twitter twitter = new TwitterFactory(conf).getInstance();
				
				Bundle bundle = getIntent().getBundleExtra(BookListActivity.class.getName() + ".bundle");
				try {
					twitter.updateStatus(getString(R.string.facebook_my_favor_book) + ":\n" + bundle.getString("name") + "\n" + bundle.getString("link"));
					toast(getString(R.string.twitter_success));
				} catch (TwitterException e) {
					Log.e(getClass().getName(), "TwitterException", e);
				}
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
	}

	@Override
	protected void setAdapters() {
		((LoginButton) findViewById(R.id.login_button)).setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            public void onUserInfoFetched(GraphUser user) {
                setUser(user);
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
	}
	
	@Override
	protected void onCreateLogic() {
		if(!AndroidUtility.isNetworkAvailable(this)) {
			toast(getString(R.string.network_unavailable));
			finish();
		} else {
			if(isLoggedTwitter()) {
				Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
				LayoutParams params = shareToTwitter.getLayoutParams();
				params.height = LayoutParams.WRAP_CONTENT;
				params.width = LayoutParams.WRAP_CONTENT;
				shareToTwitter.setLayoutParams(params);
			} else {
				Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
				LayoutParams params = shareToTwitter.getLayoutParams();
				params.height = 0;
				params.width = 0;
				shareToTwitter.setLayoutParams(params);
			}
			
			Bundle bundle = getIntent().getBundleExtra(BookListActivity.class.getName() + ".bundle");
			((TextView)findViewById(R.id.function_name)).setText(bundle.getString("name"));
			
			Book valueObject = new BookImpl();
			valueObject.setReferenceLink(bundle.getString("link"));
			try {
				Book book = ServiceProxyFactory.createLibraryService(this).getBookDetail(valueObject);
				((TextView)findViewById(R.id.isbn_label)).setText(getString(R.string.isbn) + " : " + book.getId());
				((TextView)findViewById(R.id.author_name_label)).setText(getString(R.string.author_name) + " : " + book.getAuthorName());
				((TextView)findViewById(R.id.publisher_name_label)).setText(getString(R.string.publisher_name) + " : " + book.getPublisherName());
				((TextView)findViewById(R.id.class_name_label)).setText(getString(R.string.class_name) + " : " + book.getClassName());
			} catch (ServiceException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@Override
	protected void enableUI(GraphUser user) {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.WRAP_CONTENT;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.VISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void disableUI() {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = 0;
		params.width = 0;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.INVISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void showPublishResult(GraphObject result,
			FacebookRequestError error) {}

	@Override
	protected String getUserStatusMessage() {
		return null;
	}

	@Override
	protected Bitmap getUserPhoto() {
		return null;
	}
}
