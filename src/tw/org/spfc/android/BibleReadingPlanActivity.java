package tw.org.spfc.android;

import java.io.File;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;
import csiebug.util.FileUtility;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.domain.BibleReadingPlan;
import tw.org.spfc.domain.pojoImpl.BibleReadingPlanImpl;
import tw.org.spfc.service.BibleService;

public class BibleReadingPlanActivity extends BaseActivity {
	private SPFCPreference preference;
	private BibleService service;
	private String[] names;
	private String[] paths;
	private Spinner planSpinner;
	private BibleReadingPlan plan;
	
	@Override
	protected int getLayout() {
		return R.layout.bible_reading_plan;
	}

	@Override
	protected void setListeners() {
		planSpinner.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				if(planSpinner.getSelectedItemPosition() == 0) {
					((TextView)findViewById(R.id.edit)).setText(R.string.add);
				} else {
					((TextView)findViewById(R.id.edit)).setText(R.string.edit);
				}
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
		findViewById(R.id.ok).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("bibleReadingPlan", "use", "", (long)0);
				
				preference.setBibleReadingPlan(paths[planSpinner.getSelectedItemPosition()]);
				setResult(2);
				finish();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		findViewById(R.id.edit).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("bibleReadingPlan", "edit", "", (long)0);
				
				Intent editPlan = new Intent(BibleReadingPlanActivity.this, BibleReadingPlanEditActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("path", paths[planSpinner.getSelectedItemPosition()]);
				editPlan.putExtras(bundle);
				startActivityForResult(editPlan, 0);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		findViewById(R.id.delete).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("bibleReadingPlan", "delete", "", (long)0);
				
				BibleReadingPlan plan = new BibleReadingPlanImpl();
				plan.setName(paths[planSpinner.getSelectedItemPosition()]);
				
				try {
					if(service.deleteBibleReadingPlan(plan)) {
						setAdapters();
					}
				} catch (ServiceException e) {
					//刪除讀經計畫失敗,不讓他當掉,只記log
					Log.e(getClass().getName(), "Delete Reading Plan error!", e);
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		findViewById(R.id.clear_record).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				alert(getString(R.string.warning), getString(R.string.confirm_reset), getString(R.string.ok), getString(R.string.cancel), new AbstractDefaultButtonListener(BibleReadingPlanActivity.this) {
					
					@Override
					protected void onClickLogic(View v) {}
					
					@Override
					protected void onClickLogic(DialogInterface dialog, int which) {
						try {
							EasyTracker.getTracker().sendEvent("bibleReadingRecord", "reset", "", (long)0);
							
							int totalShouldRead = service.getPlanChapterSize(plan);
							int shouldReaded = service.getShouldReadChapterSize(plan);
							//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
							service.clearReadedRecord(null, plan);
							((TextView)findViewById(R.id.bible_reading_record)).setText(getString(R.string.readed) + ": 0 / " + getString(R.string.should_read) + ": " + shouldReaded + " / " + getString(R.string.total_should_read) + ": " + totalShouldRead);
							
							//TODO 要取得未完成的書卷,最差的情況會每一章都要測試是不是閱讀過,可能讀得越多,會越跑越慢,所以取消這功能
//							List<BibleBook> list = service.getUnCompletedBooks(null, plan);
//							if(list != null && list.size() > 0) {
//								((TextView)findViewById(R.id.unCompletedBooks)).setText(service.getBookNames().get(list.get(0).getBookId()) + getString(R.string.uncompleted) + "(" + getString(R.string.query_result_count).replaceAll("var.count", "" + list.size()) + ")");
//							}
							((TextView)findViewById(R.id.unCompletedBooks)).setText("");
							
							if(AndroidUtility.isNetworkAvailable(BibleReadingPlanActivity.this) && totalShouldRead != 0) {
								renderChartSimple(0, totalShouldRead, shouldReaded);
							}
							
							setResult(3);
						} catch (ServiceException e) {
							throw new RuntimeException(e);
						}
					}
				}, new AbstractDefaultButtonListener(BibleReadingPlanActivity.this) {
					
					@Override
					protected void onClickLogic(View v) {}
					
					@Override
					protected void onClickLogic(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		switch (resultCode) {
			case 1:
				setAdapters();
				break;
			default:
				break;
		}
	}
	
	@Override
	protected void setAdapters() {
		String path = preference.getBibleReadingPlan();
		
		File planFolder = SPFCFileSystem.getBibleReadingPlanFolder(this);
		
		try {
			if(planFolder != null && (planFolder.exists() || planFolder.mkdirs())) {
				File[] plans = planFolder.listFiles();
				names = new String[plans.length + 1];
				paths = new String[plans.length + 1];
				names[0] = "";
				paths[0] = "";
				int matchIndex = 0;
				for(int i = 0; i < plans.length; i++) {
					File plan = plans[i];
					
					paths[i + 1] = plan.getName();
					
					if(path.equalsIgnoreCase(paths[i + 1])) {
						matchIndex = i + 1;
					}
					
					JSONObject object = new JSONObject(FileUtility.getTextFileContent(plan, "big5"));
					names[i + 1] = object.getString("name");
				}
				
				planSpinner = (Spinner)findViewById(R.id.plan);
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, names);
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				planSpinner.setAdapter(adapter);
				planSpinner.setSelection(matchIndex);
				
				if(matchIndex == 0) {
					((TextView)findViewById(R.id.edit)).setText(R.string.add);
				} else {
					((TextView)findViewById(R.id.edit)).setText(R.string.edit);
				}
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void findViews() {
		planSpinner = (Spinner)findViewById(R.id.plan);
	}
	
	@Override
	protected void onCreateLogic() {
		preference = new SPFCPreference(this);
		
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		int version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//			version = BibleDAOImpl.CHINA_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//			version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//			version = BibleDAOImpl.LSG;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//			version = BibleDAOImpl.LUTH1545;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//			version = BibleDAOImpl.KING_JAMES_VERSION;
//		}
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
		}
		
		try {
			service = ServiceProxyFactory.createBibleService(BibleReadingPlanActivity.this, version, language);
			
			if(AssertUtility.isNotNullAndNotSpace(preference.getBibleReadingPlan())) {
				plan = service.getBibleReadingPlan(preference.getBibleReadingPlan());
				if(plan != null) {
					int totalShouldRead = service.getPlanChapterSize(plan);
					int shouldReaded = service.getShouldReadChapterSize(plan);
					//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
					int readed = service.readedSize(null, plan);
					((TextView)findViewById(R.id.bible_reading_record)).setText(getString(R.string.readed) + ": " + readed + " / " + getString(R.string.should_read) + ": " + shouldReaded + " / " + getString(R.string.total_should_read) + ": " + totalShouldRead);
					
					//TODO 要取得未完成的書卷,最差的情況會每一章都要測試是不是閱讀過,可能讀得越多,會越跑越慢,所以取消這功能
//					List<BibleBook> list = service.getUnCompletedBooks(null, plan);
//					if(list != null && list.size() > 0) {
//						((TextView)findViewById(R.id.unCompletedBooks)).setText(service.getBookNames().get(list.get(0).getBookId()) + getString(R.string.uncompleted) + "(" + getString(R.string.query_result_count).replaceAll("var.count", "" + list.size()) + ")");
//					}
					((TextView)findViewById(R.id.unCompletedBooks)).setText("");
					
					if(AndroidUtility.isNetworkAvailable(this) && totalShouldRead != 0) {
						renderChartSimple(readed, totalShouldRead, shouldReaded);
					} else {
						findViewById(R.id.reading_chart).setVisibility(View.INVISIBLE);
					}
				} else {
					findViewById(R.id.bible_reading_record).setVisibility(View.INVISIBLE);
					findViewById(R.id.clear_record).setVisibility(View.INVISIBLE);
					findViewById(R.id.reading_chart).setVisibility(View.INVISIBLE);
				}
			} else {
				findViewById(R.id.bible_reading_record).setVisibility(View.INVISIBLE);
				findViewById(R.id.clear_record).setVisibility(View.INVISIBLE);
				findViewById(R.id.reading_chart).setVisibility(View.INVISIBLE);
			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void renderChartSimple(int readed, int totalShouldRead, int shouldReaded) throws ServiceException {
		String chd;
		String chl;
		String chs = "320x120";
		
		int shouldReadButNotRead = shouldReaded - readed;
		
		//進度正常或超前
		if(shouldReadButNotRead <= 0) {
			chd = (readed * 100 / totalShouldRead) + "," + (totalShouldRead - readed) * 100 / totalShouldRead;
			chl = getString(R.string.readed) + " " + readed + "|" + getString(R.string.total_should_read) + " " + totalShouldRead;
		} else {
		//進度落後
			chd = (readed * 100 / totalShouldRead) + "," + (shouldReadButNotRead * 100 / totalShouldRead)  + "," + (totalShouldRead - shouldReaded) * 100 / totalShouldRead;
			chl = getString(R.string.readed) + " " + readed + "|" + getString(R.string.should_read_but_not_read) + " " + shouldReadButNotRead + "|" + getString(R.string.total_should_read) + " " + totalShouldRead;
		}
		
		((WebView)findViewById(R.id.reading_chart)).loadUrl(getString(R.string.googleChartAPI) + "?cht=p3&chs=" + chs + "&chd=t:" + chd.toString() + "&chl=" + chl.toString());
	}
}
