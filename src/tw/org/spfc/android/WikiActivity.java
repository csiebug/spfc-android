package tw.org.spfc.android;

import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.util.SPFCPreference;
import android.os.Bundle;

public class WikiActivity extends BaseWebActivity {

	@Override
	protected String getWebViewURL() {
		Bundle bundle = getIntent().getExtras();
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			return "http://zh.wikipedia.org/zh-cn/" + bundle.getString("name") + "?useformat=mobile";
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			return "http://zh.wikipedia.org/zh-tw/" + bundle.getString("name") + "?useformat=mobile";
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			return "http://fr.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			return "http://de.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
//		} else {
//			return "http://en.wikipedia.org/wiki/" + bundle.getString("name") + "?useformat=mobile";
//		}
		SPFCPreference preference = new SPFCPreference(this);
		int version = preference.getBibleVersion();
		
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			return "http://zh.wikipedia.org/zh-cn/" + bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			return "http://zh.wikipedia.org/zh-tw/" + bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.AA) {
			return "http://pt.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.DN1933) {
			return "http://da.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.DNB1930) {
			return "http://no.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.KAR) {
			return "http://hu.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.LSG) {
			return "http://fr.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.LUTH1545) {
			return "http://de.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.R1933) {
			return "http://fi.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.RMNN) {
			return "http://ro.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.RVA) {
			return "http://es.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else if(version == BibleDAOImpl.SV1917) {
			return "http://sv.wikipedia.org/wiki/"+ bundle.getString("name") + "?useformat=mobile";
		} else {
			return "http://en.wikipedia.org/wiki/" + bundle.getString("name") + "?useformat=mobile";
		}
	}

}
