package tw.org.spfc.android;

import java.util.List;

import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.listener.AbstractDefaultListItemListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.domain.BibleSearchResult;
import tw.org.spfc.service.BibleService;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import csiebug.service.ServiceException;
import csiebug.util.AssertUtility;
import csiebug.util.StringUtility;
import bible.domain.BibleVerse;

public class BibleSearchActivity extends BaseActivity {
	private String[] testaments;
	private String[] books;
	private String[] chapters;
	private BibleService service;
	private Spinner testamentSpinner;
	private Spinner bookSpinner;
	private Spinner chapterSpinner;
	
	@Override
	protected int getLayout() {
		return R.layout.bible_search;
	}

	@Override
	protected void setListeners() {
		testamentSpinner.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				try {
					List<String> list;
					switch (testamentSpinner.getSelectedItemPosition()) {
						case 0:
							list = service.getBookNames();
							break;
						case 1:
							list = service.getOldTestamentBookNames();
							break;
						case 2:
							list = service.getNewTestamentBookNames();
							break;
						case 3:
							list = service.getOldTestamentLawBookNames();
							break;
						case 4:
							list = service.getOldTestamentHistoryBookNames();
							break;
						case 5:
							list = service.getOldTestamentWisdomBookNames();
							break;
						case 6:
							list = service.getOldTestamentProphetsBookNames();
							break;
						case 7:
							list = service.getNewTestamentGospelsBookNames();
							break;
						case 8:
							list = service.getNewTestamentHistoryBookNames();
							break;
						case 9:
							list = service.getNewTestamentPaulineEpistlesBookNames();
							break;
						case 10:
							list = service.getNewTestamentGeneralEpistlesBookNames();
							break;
						case 11:
							list = service.getNewTestamentApocalypseBookNames();
							break;
						default:
							list = service.getBookNames();
							break;
					}
					
					books = new String[list.size() + 1];
					books[0] = getString(R.string.all);
					for(int i = 0; i < list.size(); i++) {
						books[i + 1] = list.get(i);
					}
					ArrayAdapter<String> bookAdapter = new ArrayAdapter<String>(BibleSearchActivity.this, android.R.layout.simple_spinner_item, books);
					bookAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					bookSpinner.setAdapter(bookAdapter);
					bookSpinner.setSelection(0);
					
					setChaptersArray(-1);
					ArrayAdapter<String> chapterAdapter = new ArrayAdapter<String>(BibleSearchActivity.this, android.R.layout.simple_spinner_item, chapters);
					chapterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					chapterSpinner.setAdapter(chapterAdapter);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});
		
		bookSpinner.setOnItemSelectedListener(new AbstractDefaultListItemListener(this) {
			
			@Override
			protected void onNothingSelectedLogic(AdapterView<?> arg0) {}
			
			@Override
			protected void onItemSelectedLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				try {
					switch (testamentSpinner.getSelectedItemPosition()) {
						case 0:
							setChaptersArray(bookSpinner.getSelectedItemPosition() - 1);
							break;
						case 1:
							setChaptersArray(bookSpinner.getSelectedItemPosition() - 1);
							break;
						case 2:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						case 3:
							setChaptersArray(bookSpinner.getSelectedItemPosition() - 1);
							break;
						case 4:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						case 5:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						case 6:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.OLD_TESTAMENT_WISDOM_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						case 7:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						case 8:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						case 9:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						case 10:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						case 11:
							if(bookSpinner.getSelectedItemPosition() != 0) {
								setChaptersArray(BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT + BibleService.NEW_TESTAMENT_GENERAL_EPISTLES_BOOK_COUNT + bookSpinner.getSelectedItemPosition() - 1);
							} else {
								setChaptersArray(-1);
							}
							break;
						default:
							setChaptersArray(bookSpinner.getSelectedItemPosition() - 1);
							break;
					}
					
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(BibleSearchActivity.this, android.R.layout.simple_spinner_item, chapters);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					chapterSpinner.setAdapter(adapter);
				} catch (ServiceException e) {
					throw new RuntimeException(e);
				}
			}
			
			@Override
			protected boolean onItemLongClickLogic(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				return false;
			}
			
			@Override
			protected void onItemClickLogic(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {}
		});

		findViewById(R.id.query).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				searchVerses();
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
			
			private void searchVerses() {
				String keyword = ((TextView)findViewById(R.id.keyword)).getText().toString();
				//允許使用者輸入空白來隔開多個keyword
				List<String> keywords = StringUtility.parseKeyword(keyword, " ");
				if(AssertUtility.isNotNullAndNotSpace(keyword)) {
					try {
						int testament = testamentSpinner.getSelectedItemPosition() - 1;
						int book = bookSpinner.getSelectedItemPosition() - 1;
						int chapter = chapterSpinner.getSelectedItemPosition();
						
						BibleSearchResult result = null;
						if(testament == -1) {
							if(book == -1) {
								result = service.searchVerses(keywords);
							} else {
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 0) {
							if(book == -1) {
								result = service.searchVerses(keywords, testament);
							} else {
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 1) {
							if(book == -1) {
								result = service.searchVerses(keywords, testament);
							} else {
								book = BibleService.OLD_TESTAMENT_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 2) {
							if(book == -1) {
								int startBook = 0;
								int startChapter = 1;
								int endBook = startBook + BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 3) {
							if(book == -1) {
								int startBook = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT;
								int startChapter = 1;
								int endBook = startBook + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								book = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 4) {
							if(book == -1) {
								int startBook = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT;
								int startChapter = 1;
								int endBook = startBook + BibleService.OLD_TESTAMENT_WISDOM_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								book = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 5) {
							if(book == -1) {
								int startBook = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.OLD_TESTAMENT_WISDOM_BOOK_COUNT;
								int startChapter = 1;
								int endBook = BibleService.OLD_TESTAMENT_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								book = BibleService.OLD_TESTAMENT_LAW_BOOK_COUNT + BibleService.OLD_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.OLD_TESTAMENT_WISDOM_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 6) {
							if(book == -1) {
								int startBook = BibleService.OLD_TESTAMENT_BOOK_COUNT;
								int startChapter = 1;
								int endBook = startBook + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								book = BibleService.OLD_TESTAMENT_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 7) {
							if(book == -1) {
								int startBook = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT;
								int startChapter = 1;
								int endBook = startBook + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								book = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 8) {
							if(book == -1) {
								int startBook = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT;
								int startChapter = 1;
								int endBook = startBook + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								book = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 9) {
							if(book == -1) {
								int startBook = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT;
								int startChapter = 1;
								int endBook = startBook + BibleService.NEW_TESTAMENT_GENERAL_EPISTLES_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								book = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						} else if(testament == 10) {
							if(book == -1) {
								int startBook = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT + BibleService.NEW_TESTAMENT_GENERAL_EPISTLES_BOOK_COUNT;
								int startChapter = 1;
								int endBook = startBook + BibleService.NEW_TESTAMENT_APOCALYPSE_BOOK_COUNT - 1;
								int endChapter = service.getBookChapters(endBook);
								result = service.searchVerses(keywords, startBook, startChapter, endBook, endChapter);
							} else {
								book = BibleService.OLD_TESTAMENT_BOOK_COUNT + BibleService.NEW_TESTAMENT_GOSPELS_BOOK_COUNT + BibleService.NEW_TESTAMENT_HISTORY_BOOK_COUNT + BibleService.NEW_TESTAMENT_PAULINE_EPISTLES_BOOK_COUNT + BibleService.NEW_TESTAMENT_GENERAL_EPISTLES_BOOK_COUNT + book;
								if(chapter == 0) {
									result = service.searchVerses(keywords, testament, book);
								} else {
									result = service.searchVerses(keywords, testament, book, chapter);
								}
							}
						}
						
						if(result != null && result.size() > 0) {
							String[] verses = new String[result.size()];
							String[] indexes = new String[result.size()];
							
							List<BibleVerse> list = result.getVerses();
							for(int i = 0; i < result.size(); i++) {
								verses[i] = list.get(i).getVerse();
								indexes[i] = list.get(i).getChapter().getBook().getBookId() + ":" + list.get(i).getChapter().getChapterId() + ":" + list.get(i).getVerseId();
							}
							
							Intent intent = new Intent(BibleSearchActivity.this, BibleVerseListActivity.class);
							Bundle bundle = new Bundle();
							bundle.putString("keyword", keyword);
							bundle.putStringArray("verses", verses);
							bundle.putStringArray("indexes", indexes);
							intent.putExtras(bundle);
							
							startActivityForResult(intent, 1);
						} else {
							toast(getString(R.string.no_data));
						}
					} catch (ServiceException e) {
						throw new RuntimeException(e);
					}
				} else {
					toast(getString(R.string.keyword_null));
				}
			}
		});
	}

	@Override
	protected void setAdapters() {
		ArrayAdapter<String> testamentAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, testaments);
		testamentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		testamentSpinner.setAdapter(testamentAdapter);
		ArrayAdapter<String> bookAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, books);
		bookAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		bookSpinner.setAdapter(bookAdapter);
		try {
			setChaptersArray(-1);
			ArrayAdapter<String> chapterAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, chapters);
			chapterAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			chapterSpinner.setAdapter(chapterAdapter);
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	private void setChaptersArray(int book) throws ServiceException {
		if(book == -1) {
			chapters = new String[0];
		} else {
			int length = service.getBookChapters(book);
			chapters = new String[length + 1];
			for(int i = 0; i <= length; i++) {
				if(i == 0) {
					chapters[i] = getString(R.string.all);
				} else {
					chapters[i] = "" + i;
				}
			}
		}
	}
	
	@Override
	protected void findViews() {
		testamentSpinner = (Spinner)findViewById(R.id.testament);
		bookSpinner = (Spinner)findViewById(R.id.books);
		chapterSpinner = (Spinner)findViewById(R.id.chapters);
	}
	
	@Override
	protected void onCreateLogic() {
		((TextView)findViewById(R.id.keyword_label)).setText(R.string.keyword);
		
		testaments = new String[]{getString(R.string.all), getString(R.string.old_testament), getString(R.string.new_testament), getString(R.string.old_testament_law), getString(R.string.old_testament_history), getString(R.string.old_testament_wisdom), getString(R.string.old_testament_prophets), getString(R.string.new_testament_gospels), getString(R.string.new_testament_history), getString(R.string.new_testament_pauline_epistles), getString(R.string.new_testament_general_epistles), getString(R.string.new_testament_apocalypse)};
		
		int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//		int version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		Locale phoneLocale = getResources().getConfiguration().locale;
//		
//		if(phoneLocale.equals(Locale.CHINA) || phoneLocale.equals(Locale.PRC) || phoneLocale.equals(Locale.SIMPLIFIED_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
//			version = BibleDAOImpl.CHINA_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
//			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
//			version = BibleDAOImpl.CHINESE_UNION_VERSION;
//		} else if(phoneLocale.equals(Locale.FRANCE) || phoneLocale.equals(Locale.FRENCH) || phoneLocale.equals(Locale.CANADA_FRENCH)) {
//			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
//			version = BibleDAOImpl.LSG;
//		} else if(phoneLocale.equals(Locale.GERMAN) || phoneLocale.equals(Locale.GERMANY)) {
//			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
//			version = BibleDAOImpl.LUTH1545;
//		} else {
//			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
//			version = BibleDAOImpl.KING_JAMES_VERSION;
//		}
		SPFCPreference preference = new SPFCPreference(this);
		int version = preference.getBibleVersion();
		if(version == BibleDAOImpl.CHINA_UNION_VERSION || version == BibleDAOImpl.CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINA_BIBLE;
		} else if(version == BibleDAOImpl.CHINESE_UNION_VERSION || version == BibleDAOImpl.CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
		} else if(version == BibleDAOImpl.AA) {
			language = BibleCatagoryDAOImpl.PORTUGUESE_BIBLE;
		} else if(version == BibleDAOImpl.DN1933) {
			language = BibleCatagoryDAOImpl.DANISH_BIBLE;
		} else if(version == BibleDAOImpl.DNB1930) {
			language = BibleCatagoryDAOImpl.NORWEGIAN_BIBLE;
		} else if(version == BibleDAOImpl.KAR) {
			language = BibleCatagoryDAOImpl.HUNGARIAN_BIBLE;
		} else if(version == BibleDAOImpl.LSG) {
			language = BibleCatagoryDAOImpl.FRENCH_BIBLE;
		} else if(version == BibleDAOImpl.LUTH1545) {
			language = BibleCatagoryDAOImpl.GERMAN_BIBLE;
		} else if(version == BibleDAOImpl.R1933) {
			language = BibleCatagoryDAOImpl.FINNISH_BIBLE;
		} else if(version == BibleDAOImpl.RMNN) {
			language = BibleCatagoryDAOImpl.ROMANIAN_BIBLE;
		} else if(version == BibleDAOImpl.RVA) {
			language = BibleCatagoryDAOImpl.SPANISH_BIBLE;
		} else if(version == BibleDAOImpl.SV1917) {
			language = BibleCatagoryDAOImpl.SWEDISH_BIBLE;
		} else {
			language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
		}
		
		try {
			service = ServiceProxyFactory.createBibleService(this, version, language);
			List<String> list = service.getBookNames();
			books = new String[list.size() + 1];
			books[0] = getString(R.string.all);
			for(int i = 0; i < list.size(); i++) {
				books[i + 1] = list.get(i);
			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		switch (resultCode) {
		case 1:
			setResult(1);
			finish();
			break;

		default:
			break;
		}
	}
}
