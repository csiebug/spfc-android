package tw.org.spfc.android;

public class RoomBookActivity extends BaseWebActivity {

	@Override
	protected String getWebViewURL() {
		return getString(R.string.roomBookURL);
	}
}
