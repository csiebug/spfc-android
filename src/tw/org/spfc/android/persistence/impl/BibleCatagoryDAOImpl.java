package tw.org.spfc.android.persistence.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import csiebug.persistence.DAOException;
import bible.persistence.BibleCatagoryDAO;

import tw.org.spfc.android.R;

public class BibleCatagoryDAOImpl implements BibleCatagoryDAO {
	/**
	 * 中文聖經目錄
	 */
	public static final int CHINESE_BIBLE = R.raw.chinese_bible_catagory;
	/**
	 * 英文聖經目錄
	 */
	public static final int ENGLISH_BIBLE = R.raw.english_bible_catagory;
	/**
	 * 中文聖經縮寫目錄
	 */
	public static final int CHINESE_ABBR_BIBLE = R.raw.chinese_abbr_bible_catagory;
	/**
	 * 英文聖經縮寫目錄
	 */
	public static final int ENGLISH_ABBR_BIBLE = R.raw.english_abbr_bible_catagory;
	/**
	 * 信望愛英文聖經縮寫目錄
	 */
	public static final int FHL_ABBR_BIBLE = R.raw.fhl_abbr_bible_catagory;
	
	/**
	 * 中文簡體聖經目錄
	 */
	public static final int CHINA_BIBLE = R.raw.china_bible_catagory;
	/**
	 * 中文簡體聖經縮寫目錄
	 */
	public static final int CHINA_ABBR_BIBLE = R.raw.china_abbr_bible_catagory;
	/**
	 * 丹麥文聖經目錄
	 */
	public static final int DANISH_BIBLE = R.raw.danish_bible_catagory;
	/**
	 * 芬蘭聖經目錄
	 */
	public static final int FINNISH_BIBLE = R.raw.finnish_bible_catagory;
	/**
	 * 法文聖經目錄
	 */
	public static final int FRENCH_BIBLE = R.raw.french_bible_catagory;
	/**
	 * 德文聖經目錄
	 */
	public static final int GERMAN_BIBLE = R.raw.german_bible_catagory;
	/**
	 * 匈牙利文聖經目錄
	 */
	public static final int HUNGARIAN_BIBLE = R.raw.hungarian_bible_catagory;
	/**
	 * 挪威文聖經目錄
	 */
	public static final int NORWEGIAN_BIBLE = R.raw.norwegian_bible_catagory;
	/**
	 * 葡萄牙文聖經目錄
	 */
	public static final int PORTUGUESE_BIBLE = R.raw.portuguese_bible_catagory;
	/**
	 * 羅馬尼亞文聖經目錄
	 */
	public static final int ROMANIAN_BIBLE = R.raw.romanian_bible_catagory;
	/**
	 * 俄文聖經目錄
	 */
//	public static final int RUSSIAN_BIBLE = R.raw.russian_bible_catagory;
	/**
	 * 西班牙文聖經目錄
	 */
	public static final int SPANISH_BIBLE = R.raw.spanish_bible_catagory;
	/**
	 * 瑞典聖經目錄
	 */
	public static final int SWEDISH_BIBLE = R.raw.swedish_bible_catagory;
	/**
	 * 越南文聖經目錄
	 */
//	public static final int VIETNAMESE_BIBLE = R.raw.vietnamese_bible_catagory;
	
	private Context context;
	private List<String> bookNames;
	private List<String> bookAbbrNames;
	private List<String> bookFHLAbbrNames;
	
	public BibleCatagoryDAOImpl(Context context, int language) throws DAOException {
		this.context = context;
		bookNames = new ArrayList<String>();
		bookAbbrNames = new ArrayList<String>();
		bookFHLAbbrNames = new ArrayList<String>();
		setDatasource(language);
	}
	
	public void setDatasource(int language) throws DAOException {
		InputStream inputStream = null;
		InputStream inputStream2 = null;
		InputStream inputStream3 = null;
		try {
			inputStream = context.getResources().openRawResource(language);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			
			String line = null;
	        while((line = reader.readLine()) != null){
	        	bookNames.add(line);
	        }
	        
	        switch (language) {
				case CHINESE_BIBLE:
					inputStream2 = context.getResources().openRawResource(CHINESE_ABBR_BIBLE);
					break;
				case ENGLISH_BIBLE:
					inputStream2 = context.getResources().openRawResource(ENGLISH_ABBR_BIBLE);
					break;
				case CHINA_BIBLE:
					inputStream2 = context.getResources().openRawResource(CHINA_ABBR_BIBLE);
					break;
				default:
					inputStream2 = context.getResources().openRawResource(ENGLISH_ABBR_BIBLE);
					break;
			}
	        
	        reader = new BufferedReader(new InputStreamReader(inputStream2, "UTF-8"));
	        
			line = null;
	        while((line = reader.readLine()) != null){
	        	bookAbbrNames.add(line);
	        }
	        
	        inputStream3 = context.getResources().openRawResource(FHL_ABBR_BIBLE);
			reader = new BufferedReader(new InputStreamReader(inputStream3, "UTF-8"));
			
			line = null;
	        while((line = reader.readLine()) != null){
	        	bookFHLAbbrNames.add(line);
	        }
        } catch (IOException e) {
			throw new DAOException(e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new DAOException(e);
				}
			}
			
			if(inputStream2 != null) {
				try {
					inputStream2.close();
				} catch (IOException e) {
					throw new DAOException(e);
				}
			}
			
			if(inputStream3 != null) {
				try {
					inputStream3.close();
				} catch (IOException e) {
					throw new DAOException(e);
				}
			}
		}
	}

	public String getBookName(int index) {
		return bookNames.get(index);
	}

	public List<String> getBookNames(int startIndex, int endIndex) {
		return bookNames.subList(startIndex, endIndex);
	}

	public String getBookAbbrName(int index) throws DAOException {
		return bookAbbrNames.get(index);
	}

	public List<String> getBookAbbrNames(int start, int end)
			throws DAOException {
		return bookAbbrNames.subList(start, end);
	}

	public List<String> getBookFHLAbbrNames(int start, int end)
			throws DAOException {
		return bookFHLAbbrNames.subList(start, end);
	}

}
