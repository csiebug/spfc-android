package tw.org.spfc.android.persistence.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import tw.org.spfc.android.R;
import tw.org.spfc.service.BibleService;

import csiebug.persistence.DAOException;
import csiebug.util.StringUtility;
import android.content.Context;
import bible.domain.BibleBook;
import bible.domain.BibleChapter;
import bible.domain.BiblePerson;
import bible.domain.BibleVerse;
import bible.domain.pojoImpl.BibleBookImpl;
import bible.domain.pojoImpl.BibleChapterImpl;
import bible.domain.pojoImpl.BiblePersonImpl;
import bible.persistence.BibleDAO;
import bible.persistence.BiblePersonDAO;

public class BiblePersonDAOImpl implements BiblePersonDAO {
	private Context context;
	private BibleDAO bibleDAO;
	
	public BiblePersonDAOImpl(Context context) throws DAOException {
		this.context = context;
		this.bibleDAO = new BibleDAOImpl(context, BibleDAOImpl.KING_JAMES_VERSION);
	}
	
	private List<BiblePerson> getAllPeople() throws DAOException {
		List<BiblePerson> people = new ArrayList<BiblePerson>();
		InputStream inputStream = null;
		try {
			inputStream = context.getResources().openRawResource(R.raw.people);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			String line = null;
			while((line = reader.readLine()) != null){
				BiblePerson person = new BiblePersonImpl();
				//和合本的名字
				person.setName(line.split("\\.")[2]);
				//NIV的名字
				person.setId(line.split("\\.")[1]);
				//維基百科的資料
				person.setDescription("http://zh.wikipedia.org/zh-tw/" + person.getName());
				people.add(person);
			}
		} catch (IOException e) {
			throw new DAOException(e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new DAOException(e);
				}
			}
		}
		
		return people;
	}
	
	public List<BiblePerson> getPeople(String name) throws DAOException {
		List<BiblePerson> people = new ArrayList<BiblePerson>();
		
		List<BiblePerson> all = getAllPeople();
		for(int i = 0; i < all.size(); i++) {
			if(all.get(i).getName().indexOf(name) != -1) {
				people.add(all.get(i));
			}
		}
		
		return people;
	}
	
	private boolean contains(List<BiblePerson> people, BiblePerson person) {
		for(int i = 0; i < people.size(); i++) {
			if(people.get(i).getName().equals(person.getName())) {
				return  true;
			}
		}
		return false;
	}

	public List<BiblePerson> getPeople(BibleVerse vo) throws DAOException {
		List<BiblePerson> people = new ArrayList<BiblePerson>();
		
		String verseText = bibleDAO.getVerse(getVerseIndex(vo.getChapter().getBook().getBookId(), vo.getChapter().getChapterId(), vo.getVerseId())).getVerse();
		List<BiblePerson> all = getAllPeople();
		for(int i = 0; i < all.size(); i++) {
			if(verseText.indexOf(all.get(i).getId()) != -1) {
				if(!contains(people, all.get(i))) {
					people.add(all.get(i));
				}
			}
		}
		
		return people;
	}

	public List<BiblePerson> getPeople(BibleChapter vo)
			throws DAOException {
		List<BiblePerson> people = new ArrayList<BiblePerson>();
		
		BibleChapter chapter = getChapter(vo.getBook().getBookId(), vo.getChapterId());
		
		for(int j = 0; j < chapter.getVerses().size(); j++) {
			String verseText = chapter.getVerses().get(j).getVerse();
			
			List<BiblePerson> all = getAllPeople();
			for(int i = 0; i < all.size(); i++) {
				if(verseText.indexOf(all.get(i).getId()) != -1) {
					if(!contains(people, all.get(i))) {
						people.add(all.get(i));
					}
				}
			}
		}
		
		return people;
	}

	public List<BiblePerson> getPeople(BibleBook vo) throws DAOException {
		List<BiblePerson> people = new ArrayList<BiblePerson>();
		
		//TODO 有效能問題,需要refactor(目前的解決方式如下程式碼)
//		List<BibleChapter> chapters = new ArrayList<BibleChapter>();
//		int chapterSize = getBookChapters(vo.getBookId());
//		for(int i = 0; i < chapterSize; i++) {
//			chapters.add(getChapter(vo.getBookId(), i + 1));
//		}
//		
//		for(int k = 0; k < chapters.size(); k++) {
//			for(int j = 0; j < chapters.get(k).getVerses().size(); j++) {
//				String verseText = chapters.get(k).getVerses().get(j).getVerse();
//				
//				List<BiblePerson> all = getAllPeople();
//				for(int i = 0; i < all.size(); i++) {
//					if(verseText.indexOf(all.get(i).getId()) != -1) {
//						if(!contains(people, all.get(i))) {
//							people.add(all.get(i));
//						}
//					}
//				}
//			}
//		}
		
		//使用預先查好的查表方式,整個把搜尋時間省掉(用記憶體空間換取時間)
		InputStream inputStream = null;
		try {
			inputStream = context.getResources().openRawResource(R.raw.people_by_chapter);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			
			String line = null;
			int index = 0;
			while((line = reader.readLine()) != null){
				if(index == vo.getBookId()) {
					JSONObject object = new JSONObject(line);
					JSONArray array = object.getJSONArray("result");
					
					for(int i = 0; i < array.length(); i++) {
						JSONObject jsonPerson = array.getJSONObject(i);
						
						BiblePerson person = new BiblePersonImpl();
						//和合本的名字
						person.setName(jsonPerson.getString("name"));
						//NIV的名字
						person.setId(jsonPerson.getString("id"));
						//維基百科的資料
						person.setDescription(jsonPerson.getString("description"));
						people.add(person);
					}
					
					break;
				}
				
				index++;
			}
		} catch (IOException e) {
			throw new DAOException(e);
		} catch (JSONException e) {
			throw new DAOException(e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new DAOException(e);
				}
			}
		}
		
		return people;
	}
	
	//以下method是抄自BibleServiceImpl
	private int getVerseIndex(int book, int chapter, int verse) throws DAOException {
		if(isValidVerse(book, chapter, verse)) {
			return bibleDAO.getChapterStartVerseIndex()[getChapterIndex(book, chapter)] + verse - 1;
		} else {
			throw new DAOException("Verse not found!");
		}
	}
	
	private boolean isValidVerse(int book, int chapter, int verse) throws DAOException {
		if(!isValidChapter(book, chapter)) {
			return false;
		}
		
		if(verse <= 0) {
			return false;
		}
		
		int chapterIndex = getChapterIndex(book, chapter);
		int baseVerseIndex = bibleDAO.getChapterStartVerseIndex()[chapterIndex];
		int nextBaseVerseIndex = bibleDAO.getChapterStartVerseIndex()[chapterIndex + 1];
		
		return ((baseVerseIndex + verse - 1) < nextBaseVerseIndex);
	}
	
	private boolean isValidChapter(int book, int chapter) {
		if(book < 0 || book > (BibleService.BOOK_COUNT - 1)) {
			return false;
		}
		
		if(chapter <= 0) {
			return false;
		}
		
		int baseChapterIndex = BibleService.bookChapters[book];
		int nextBaseChapterIndex = BibleService.bookChapters[book + 1];
		
		return ((baseChapterIndex + chapter - 1) < nextBaseChapterIndex);
	}
	
	private int getChapterIndex(int book, int chapter) throws DAOException {
		if(isValidChapter(book, chapter)) {
			return BibleService.bookChapters[book] + chapter - 1;
		} else {
			throw new DAOException("Chapter not found!");
		}
	}
	
	private BibleChapter getChapter(int book, int chapter) throws DAOException {
		int startIndex = getVerseIndex(book, chapter, 1);
		int endIndex = startIndex + getChapterVerses(book, chapter);
		
		BibleChapter bibleChapter = new BibleChapterImpl();
		bibleChapter.setId(StringUtility.addZero(book, 2) + StringUtility.addZero(chapter, 3));
		BibleBook bibleBook = new BibleBookImpl();
		bibleBook.setId("" + book);
		bibleBook.setBookId(book);
		bibleChapter.setBook(bibleBook);
		bibleChapter.setChapterId(chapter);
		bibleChapter.setVerses(bibleDAO.getVerses(startIndex, endIndex));
		
		return bibleChapter;
	}
	
	private int getChapterVerses(int book, int chapter) throws DAOException {
		if(isValidChapter(book, chapter)) {
			int chapterIndex = getChapterIndex(book, chapter);
			return bibleDAO.getChapterStartVerseIndex()[chapterIndex + 1] - bibleDAO.getChapterStartVerseIndex()[chapterIndex];
		} else {
			throw new DAOException("Chapter not found!");
		}
	}
	
	@SuppressWarnings("unused")
	private int getBookChapters(int book) throws DAOException {
		return BibleService.bookChapters[book + 1] - BibleService.bookChapters[book];
	}
}
