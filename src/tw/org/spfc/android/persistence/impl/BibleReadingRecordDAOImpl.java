package tw.org.spfc.android.persistence.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import csiebug.persistence.DAOException;
import csiebug.util.AssertUtility;
import csiebug.util.DateFormatException;
import csiebug.util.FileUtility;
import csiebug.util.StringUtility;
import bible.domain.BibleBook;
import bible.domain.BibleChapter;
import bible.domain.pojoImpl.BibleBookImpl;
import bible.domain.pojoImpl.BibleChapterImpl;
import tw.org.spfc.android.R;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.domain.BibleReadingPlan;
import tw.org.spfc.domain.User;
import tw.org.spfc.domain.pojoImpl.BibleReadingPlanConverter;
import tw.org.spfc.persistence.BibleReadingRecordDAO;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class BibleReadingRecordDAOImpl extends BaseDAOImpl implements BibleReadingRecordDAO {
	private String tableName = "bibleReadingRecord";
	public BibleReadingRecordDAOImpl(Context context) {
		super(context);
	}

	public void insertRecord(User user, BibleReadingPlan plan, BibleChapter chapter) throws DAOException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		ContentValues params = new ContentValues();
		params.put("_ID", plan.getId() + StringUtility.addZero(chapter.getBook().getBookId(), 2) + StringUtility.addZero(chapter.getChapterId(), 3));
		params.put("plan", plan.getId());
		params.put("book", chapter.getBook().getBookId());
		params.put("chapter", chapter.getChapterId());
		
		insert(tableName, null, params);
	}

	public void deleteRecords(User user, BibleReadingPlan plan) throws DAOException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		delete(tableName, "plan = ?", new String[]{plan.getId()});
	}

	public void loadRecords(User user, BibleReadingPlan plan) throws DAOException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		Cursor cursor = query("select book, chapter from " + tableName + " where plan = ?", new String[]{plan.getId()});
		int count = cursor.getCount();
		
		for(int i = 0; i < count; i++) {
			cursor.moveToPosition(i);
			int book = cursor.getInt(0);
			int chapter = cursor.getInt(1);
			
			BibleChapter bibleChapter = new BibleChapterImpl();
			BibleBook bibleBook = new BibleBookImpl();
			bibleBook.setId("" + book);
			bibleBook.setBookId(book);
			bibleChapter.setBook(bibleBook);
			bibleChapter.setChapterId(chapter);
			
			plan.getOwner().addReadedChapter(bibleChapter);
		}
	}
	
	public Boolean isReaded(User user, BibleReadingPlan plan, int book, int chapter) throws DAOException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		Cursor cursor = query("select book, chapter from " + tableName + " where plan = ? and book = ? and chapter = ?", new String[]{plan.getId(), "" + book, "" + chapter});
		
		return (cursor.getCount() > 0);
	}
	
	public Boolean isCompletedBook(User user, BibleReadingPlan plan, int book, int startChapter, int endChapter) throws DAOException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		Cursor cursor = query("select book, chapter from " + tableName + " where plan = ? and book = ? and chapter >= ? and chapter <= ?", new String[]{plan.getId(), "" + book, "" + startChapter, "" + endChapter});
		
		return !(cursor.getCount() < (endChapter - startChapter + 1));
	}
	
	public List<BibleChapter> getReadedChapters(User user, BibleReadingPlan plan)
			throws DAOException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		List<BibleChapter> readedChapters = new ArrayList<BibleChapter>();
		
		Cursor cursor = query("select book, chapter from " + tableName + " where plan = ?", new String[]{plan.getId()});
		int count = cursor.getCount();
		
		for(int i = 0; i < count; i++) {
			cursor.moveToPosition(i);
			int book = cursor.getInt(0);
			int chapter = cursor.getInt(1);
			
			BibleChapter bibleChapter = new BibleChapterImpl();
			BibleBook bibleBook = new BibleBookImpl();
			bibleBook.setId("" + book);
			bibleBook.setBookId(book);
			bibleChapter.setBook(bibleBook);
			bibleChapter.setChapterId(chapter);
			
			readedChapters.add(bibleChapter);
		}
		
		return readedChapters;
	}

	public int readedSize(User user, BibleReadingPlan plan) throws DAOException {
		//TODO 因為目前讀經計畫的設計都在手機端,沒有上server,所以就不管user這個參數
		Cursor cursor = query("select book, chapter from " + tableName + " where plan = ?", new String[]{plan.getId()});
		
		return cursor.getCount();
	}

	public Boolean addBibleReadingPlan(BibleReadingPlan vo) throws DAOException {
		try {
			String planFileContent = BibleReadingPlanConverter.toJSONString(vo, Integer.parseInt(getContext().getString(R.string.dateFormat)));
			
			File planFolder = SPFCFileSystem.getBibleReadingPlanFolder(getContext());
			
			if(planFolder != null && (planFolder.exists() || planFolder.mkdirs())) {
				File planFile = null;
				if(AssertUtility.isNotNullAndNotSpace(vo.getId())) {
					planFile = SPFCFileSystem.getBibleReadingPlan(getContext(), vo.getId());
				} else if(AssertUtility.isNotNullAndNotSpace(vo.getName())){
					planFile = SPFCFileSystem.getBibleReadingPlan(getContext(), vo.getName() + ".txt");
				}
				
				if(planFile != null) {
					if(planFile.exists()) {
						planFile.delete();
					}
					planFile.createNewFile();
					
					FileUtility.writeTextFile(planFileContent, planFile.getPath(), "big5");
				}
			}
			
			return true;
		} catch (IOException e) {
			throw new DAOException(e);
		} catch (NumberFormatException e) {
			throw new DAOException(e);
		} catch (JSONException e) {
			throw new DAOException(e);
		} catch (DateFormatException e) {
			throw new DAOException(e);
		}
	}

	public Boolean addBibleReadingPlans(List<BibleReadingPlan> vo)
			throws DAOException {
		for(int i = 0; i < vo.size(); i++) {
			if(!addBibleReadingPlan(vo.get(i))) {
				return false;
			}
		}
		
		return true;
	}

	public Boolean modifyBibleReadingPlan(BibleReadingPlan plan)
			throws DAOException {
		return addBibleReadingPlan(plan);
	}

	public Boolean modifyBibleReadingPlans(List<BibleReadingPlan> plans)
			throws DAOException {
		for(int i = 0; i < plans.size(); i++) {
			if(!modifyBibleReadingPlan(plans.get(i))) {
				return false;
			}
		}
		
		return true;
	}

	public Boolean deleteBibleReadingPlan(BibleReadingPlan plan)
			throws DAOException {
		File planFile = SPFCFileSystem.getBibleReadingPlan(getContext(), plan.getName());
		return (planFile != null && planFile.delete());
	}

	public Boolean deleteBibleReadingPlans(List<BibleReadingPlan> plans)
			throws DAOException {
		for(int i = 0; i < plans.size(); i++) {
			if(!deleteBibleReadingPlan(plans.get(i))) {
				return false;
			}
		}
		
		return true;
	}

	public List<BibleReadingPlan> searchBibleReadingPlans(BibleReadingPlan vo)
			throws DAOException {
		
		
		if(vo != null) {
			List<BibleReadingPlan> plans = new ArrayList<BibleReadingPlan>();
			
			BibleReadingPlan plan = getBibleReadingPlan(vo.getId());
			if(plan != null) {
				plans.add(plan);
			}
			
			return plans;
		} else {
			return getAllBibleReadingPlans();
		}
	}
	
	private BibleReadingPlan getBibleReadingPlan(String id) throws DAOException {
		if(AssertUtility.isNotNullAndNotSpace(id)) {
			File planFile = SPFCFileSystem.getBibleReadingPlan(getContext(), id);
			
			if(planFile != null && planFile.exists()) {
				try {
					JSONObject object = new JSONObject(FileUtility.getTextFileContent(planFile, "big5"));
					BibleReadingPlan plan = BibleReadingPlanConverter.toBibleReadingPlan(object, planFile.getName(), Integer.parseInt(getContext().getString(R.string.dateFormat)));
					plan.setId(planFile.getName());
					
					return plan;
				} catch (Exception e) {
					//如果發生任何的Exception可能和cache檔案的內容有關,強迫刪除cache檔案(將檔案更名做debug用)
					//讓這個錯誤避免變成永遠無法使用的錯誤
					if(planFile.exists()) {
						planFile.renameTo(SPFCFileSystem.getBibleReadingPlan(getContext(), id.replaceAll(".txt", "") + ".err"));
					}
					
					throw new DAOException(e);
				}
			}
		}
		
		return null;
	}
	
	private List<BibleReadingPlan> getAllBibleReadingPlans() throws DAOException {
		List<BibleReadingPlan> plans = new ArrayList<BibleReadingPlan>();
		
		File planFolder = SPFCFileSystem.getBibleReadingPlanFolder(getContext());
		
		if(planFolder != null && (planFolder.exists() || planFolder.mkdirs())) {
			File[] planFiles = planFolder.listFiles();
			
			for(int i = 0; i < planFiles.length; i++) {
				File planFile = planFiles[i];
				
				BibleReadingPlan plan = getBibleReadingPlan(planFile.getName());
				
				if(plan != null) {
					plans.add(plan);
				}
			}
		}
		
		return plans;
	}
}
