package tw.org.spfc.android.persistence.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import tw.org.spfc.android.R;

import csiebug.persistence.DAOException;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import bible.domain.BibleBook;
import bible.domain.BibleChapter;
import bible.domain.BibleLocation;
import bible.domain.BibleVerse;
import bible.domain.pojoImpl.BibleLocationImpl;
import bible.persistence.BibleLocationDAO;

public class BibleLocationDAOImpl implements BibleLocationDAO {
	private Context context;
	private Element rootElement;
	private Element rootElement1;
	private Element rootElement2;
	private Element rootElement3;
	private Element rootElement4;
	private Element rootElement5;
	private Element rootElement6;
	private Element rootElement7;
	
	public BibleLocationDAOImpl(Context context) {
		this.context = context;
	}
	
	private void setDatasource(int segment) throws NotFoundException, JDOMException, IOException {
		SAXBuilder builder = new SAXBuilder();
		Document document = null;
		
		switch (segment) {
		case 0:
			document = builder.build(context.getResources().openRawResource(R.raw.location_0));
			rootElement = (Element)document.getRootElement().getChildren().get(0);
			break;
		case 1:
			document = builder.build(context.getResources().openRawResource(R.raw.location_1));
			rootElement1 = (Element)document.getRootElement().getChildren().get(0);
			break;
		case 2:
			document = builder.build(context.getResources().openRawResource(R.raw.location_2));
			rootElement2 = (Element)document.getRootElement().getChildren().get(0);
			break;
		case 3:
			document = builder.build(context.getResources().openRawResource(R.raw.location_3));
			rootElement3 = (Element)document.getRootElement().getChildren().get(0);
			break;
		case 4:
			document = builder.build(context.getResources().openRawResource(R.raw.location_4));
			rootElement4 = (Element)document.getRootElement().getChildren().get(0);
			break;
		case 5:
			document = builder.build(context.getResources().openRawResource(R.raw.location_5));
			rootElement5 = (Element)document.getRootElement().getChildren().get(0);
			break;
		case 6:
			document = builder.build(context.getResources().openRawResource(R.raw.location_6));
			rootElement6 = (Element)document.getRootElement().getChildren().get(0);
			break;
		case 7:
			document = builder.build(context.getResources().openRawResource(R.raw.location_7));
			rootElement7 = (Element)document.getRootElement().getChildren().get(0);
			break;
		default:
			document = builder.build(context.getResources().openRawResource(R.raw.location_0));
			rootElement = (Element)document.getRootElement().getChildren().get(0);
			document = builder.build(context.getResources().openRawResource(R.raw.location_1));
			rootElement1 = (Element)document.getRootElement().getChildren().get(0);
			document = builder.build(context.getResources().openRawResource(R.raw.location_2));
			rootElement2 = (Element)document.getRootElement().getChildren().get(0);
			document = builder.build(context.getResources().openRawResource(R.raw.location_3));
			rootElement3 = (Element)document.getRootElement().getChildren().get(0);
			document = builder.build(context.getResources().openRawResource(R.raw.location_4));
			rootElement4 = (Element)document.getRootElement().getChildren().get(0);
			document = builder.build(context.getResources().openRawResource(R.raw.location_5));
			rootElement5 = (Element)document.getRootElement().getChildren().get(0);
			document = builder.build(context.getResources().openRawResource(R.raw.location_6));
			rootElement6 = (Element)document.getRootElement().getChildren().get(0);
			document = builder.build(context.getResources().openRawResource(R.raw.location_7));
			rootElement7 = (Element)document.getRootElement().getChildren().get(0);
			break;
		}
	}
	
	private int whichSegment(BibleBook book) {
		int bookId = book.getBookId();
		
		if(bookId < 5) {
			return 0;
		} else if(bookId < 10) {
			return 1;
		} else if(bookId < 17) {
			return 2;
		} else if(bookId < 22) {
			return 3;
		} else if(bookId < 27) {
			return 4;
		} else if(bookId < 39) {
			return 5;
		} else if(bookId < 44) {
			return 6;
		} else {
			return 7;
		}
	}
	
	public List<BibleLocation> getLocations(String name) throws DAOException {
		try {
			setDatasource(-1);
			
			List<BibleLocation> locations = new ArrayList<BibleLocation>();
			
			searchForName(rootElement, locations, name);
			searchForName(rootElement1, locations, name);
			searchForName(rootElement2, locations, name);
			searchForName(rootElement3, locations, name);
			searchForName(rootElement4, locations, name);
			searchForName(rootElement5, locations, name);
			searchForName(rootElement6, locations, name);
			searchForName(rootElement7, locations, name);
			
			return locations;
		} catch (NotFoundException e) {
			throw new DAOException(e);
		} catch (JDOMException e) {
			throw new DAOException(e);
		} catch (IOException e) {
			throw new DAOException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void searchForName(Element rootElement, List<BibleLocation> locations, String name) {
		List<Element> books = rootElement.getChildren();
		for(int i = 0; i < books.size(); i++) {
			if(books.get(i).getName().equals("Folder")) {
				List<Element> chapters = books.get(i).getChildren();
				for(int j = 0; j < chapters.size(); j++) {
					if(chapters.get(j).getName().equals("Folder")) {
						List<Element> placemarks = chapters.get(j).getChildren();
						for(int k = 0; k < placemarks.size(); k++) {
							if(placemarks.get(k).getName().equals("Placemark")) {
								String nameText = ((Element)placemarks.get(k).getChildren().get(0)).getText();
								String descriptionText = ((Element)placemarks.get(k).getChildren().get(2)).getText();
								String[] coordinates = ((Element)((Element)placemarks.get(k).getChildren().get(4)).getChildren().get(0)).getText().split(",");
								if(nameText.indexOf(name) != -1) {
									BibleLocation location = new BibleLocationImpl();
									location.setName(nameText);
									location.setDescription(descriptionText);
									location.setLongitude(Double.parseDouble(coordinates[0]));
									location.setLatitude(Double.parseDouble(coordinates[1]));
									
									locations.add(location);
								}
							}
						}
					}
				}
			}
		}
	}

	public List<BibleLocation> getLocations(BibleVerse verse)
			throws DAOException {
		try {
			int segment = whichSegment(verse.getChapter().getBook());
			setDatasource(segment);
				
			List<BibleLocation> locations = new ArrayList<BibleLocation>();
			
			switch (segment) {
			case 0:
				searchForVerse(rootElement, locations, verse);
				break;
			case 1:
				searchForVerse(rootElement1, locations, verse);
				break;
			case 2:
				searchForVerse(rootElement2, locations, verse);
				break;
			case 3:
				searchForVerse(rootElement3, locations, verse);
				break;
			case 4:
				searchForVerse(rootElement4, locations, verse);
				break;
			case 5:
				searchForVerse(rootElement5, locations, verse);
				break;
			case 6:
				searchForVerse(rootElement6, locations, verse);
				break;
			case 7:
				searchForVerse(rootElement7, locations, verse);
				break;
			default:
				searchForVerse(rootElement, locations, verse);
				searchForVerse(rootElement1, locations, verse);
				searchForVerse(rootElement2, locations, verse);
				searchForVerse(rootElement3, locations, verse);
				searchForVerse(rootElement4, locations, verse);
				searchForVerse(rootElement5, locations, verse);
				searchForVerse(rootElement6, locations, verse);
				searchForVerse(rootElement7, locations, verse);
				break;
			}
			
			return locations;
		} catch (NotFoundException e) {
			throw new DAOException(e);
		} catch (JDOMException e) {
			throw new DAOException(e);
		} catch (IOException e) {
			throw new DAOException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void searchForVerse(Element rootElement, List<BibleLocation> locations, BibleVerse verse) {
		List<Element> books = rootElement.getChildren();
		for(int i = 0; i < books.size(); i++) {
			if(books.get(i).getName().equals("Folder")) {
				List<Element> chapters = books.get(i).getChildren();
				for(int j = 0; j < chapters.size(); j++) {
					if(chapters.get(j).getName().equals("Folder")) {
						List<Element> placemarks = chapters.get(j).getChildren();
						for(int k = 0; k < placemarks.size(); k++) {
							if(placemarks.get(k).getName().equals("Placemark")) {
								String nameText = ((Element)placemarks.get(k).getChildren().get(0)).getText();
								String descriptionText = ((Element)placemarks.get(k).getChildren().get(2)).getText();
								String[] coordinates = ((Element)((Element)placemarks.get(k).getChildren().get(4)).getChildren().get(0)).getText().split(",");
								
								String compare = verse.getChapter().getBook().getName() + " " + verse.getChapter().getChapterId() + ":" + verse.getVerseId();
								if((descriptionText.indexOf(",") != -1 && descriptionText.indexOf(compare + ",") != -1) || descriptionText.equals(compare)) {
									BibleLocation location = new BibleLocationImpl();
									location.setName(nameText);
									location.setDescription(descriptionText);
									location.setLongitude(Double.parseDouble(coordinates[0]));
									location.setLatitude(Double.parseDouble(coordinates[1]));
									
									locations.add(location);
								}
							}
						}
					}
				}
			}
		}
	}

	public List<BibleLocation> getLocations(BibleChapter chapter)
			throws DAOException {
		try {
			int segment = whichSegment(chapter.getBook());
			setDatasource(segment);
				
			List<BibleLocation> locations = new ArrayList<BibleLocation>();
			
			switch (segment) {
			case 0:
				searchForChapter(rootElement, locations, chapter);
				break;
			case 1:
				searchForChapter(rootElement1, locations, chapter);
				break;
			case 2:
				searchForChapter(rootElement2, locations, chapter);
				break;
			case 3:
				searchForChapter(rootElement3, locations, chapter);
				break;
			case 4:
				searchForChapter(rootElement4, locations, chapter);
				break;
			case 5:
				searchForChapter(rootElement5, locations, chapter);
				break;
			case 6:
				searchForChapter(rootElement6, locations, chapter);
				break;
			case 7:
				searchForChapter(rootElement7, locations, chapter);
				break;
			default:
				searchForChapter(rootElement, locations, chapter);
				searchForChapter(rootElement1, locations, chapter);
				searchForChapter(rootElement2, locations, chapter);
				searchForChapter(rootElement3, locations, chapter);
				searchForChapter(rootElement4, locations, chapter);
				searchForChapter(rootElement5, locations, chapter);
				searchForChapter(rootElement6, locations, chapter);
				searchForChapter(rootElement7, locations, chapter);
				break;
			}
			
			return locations;
		} catch (NotFoundException e) {
			throw new DAOException(e);
		} catch (JDOMException e) {
			throw new DAOException(e);
		} catch (IOException e) {
			throw new DAOException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void searchForChapter(Element rootElement, List<BibleLocation> locations, BibleChapter chapter) {
		List<Element> books = rootElement.getChildren();
		for(int i = 0; i < books.size(); i++) {
			if(books.get(i).getName().equals("Folder")) {
				List<Element> chapters = books.get(i).getChildren();
				for(int j = 0; j < chapters.size(); j++) {
					if(chapters.get(j).getName().equals("Folder") && ((Element)chapters.get(j).getChildren().get(0)).getText().equals(chapter.getBook().getName() + " " + chapter.getChapterId())) {
						List<Element> placemarks = chapters.get(j).getChildren();
						for(int k = 0; k < placemarks.size(); k++) {
							if(placemarks.get(k).getName().equals("Placemark")) {
								String nameText = ((Element)placemarks.get(k).getChildren().get(0)).getText();
								String descriptionText = ((Element)placemarks.get(k).getChildren().get(2)).getText();
								String[] coordinates = ((Element)((Element)placemarks.get(k).getChildren().get(4)).getChildren().get(0)).getText().split(",");
								
								BibleLocation location = new BibleLocationImpl();
								location.setName(nameText);
								location.setDescription(descriptionText);
								location.setLongitude(Double.parseDouble(coordinates[0]));
								location.setLatitude(Double.parseDouble(coordinates[1]));
								
								locations.add(location);
							}
						}
					}
				}
			}
		}
	}

	public List<BibleLocation> getLocations(BibleBook book) throws DAOException {
		try {
			int segment = whichSegment(book);
			setDatasource(segment);
			
			List<BibleLocation> locations = new ArrayList<BibleLocation>();
			
			switch (segment) {
			case 0:
				searchForBook(rootElement, locations, book);
				break;
			case 1:
				searchForBook(rootElement1, locations, book);
				break;
			case 2:
				searchForBook(rootElement2, locations, book);
				break;
			case 3:
				searchForBook(rootElement3, locations, book);
				break;
			case 4:
				searchForBook(rootElement4, locations, book);
				break;
			case 5:
				searchForBook(rootElement5, locations, book);
				break;
			case 6:
				searchForBook(rootElement6, locations, book);
				break;
			case 7:
				searchForBook(rootElement7, locations, book);
				break;
			default:
				searchForBook(rootElement, locations, book);
				searchForBook(rootElement1, locations, book);
				searchForBook(rootElement2, locations, book);
				searchForBook(rootElement3, locations, book);
				searchForBook(rootElement4, locations, book);
				searchForBook(rootElement5, locations, book);
				searchForBook(rootElement6, locations, book);
				searchForBook(rootElement7, locations, book);
				break;
			}
			
			return locations;
		} catch (NotFoundException e) {
			throw new DAOException(e);
		} catch (JDOMException e) {
			throw new DAOException(e);
		} catch (IOException e) {
			throw new DAOException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void searchForBook(Element rootElement, List<BibleLocation> locations, BibleBook book) {
		List<Element> books = rootElement.getChildren();
		for(int i = 0; i < books.size(); i++) {
			if(books.get(i).getName().equals("Folder") && ((Element)books.get(i).getChildren().get(0)).getText().equals(book.getName())) {
				List<Element> chapters = books.get(i).getChildren();
				for(int j = 0; j < chapters.size(); j++) {
					if(chapters.get(j).getName().equals("Folder")) {
						List<Element> placemarks = chapters.get(j).getChildren();
						for(int k = 0; k < placemarks.size(); k++) {
							if(placemarks.get(k).getName().equals("Placemark")) {
								String nameText = ((Element)placemarks.get(k).getChildren().get(0)).getText();
								String descriptionText = ((Element)placemarks.get(k).getChildren().get(2)).getText();
								String[] coordinates = ((Element)((Element)placemarks.get(k).getChildren().get(4)).getChildren().get(0)).getText().split(",");
								
								BibleLocation location = new BibleLocationImpl();
								location.setName(nameText);
								location.setDescription(descriptionText);
								location.setLongitude(Double.parseDouble(coordinates[0]));
								location.setLatitude(Double.parseDouble(coordinates[1]));
								
								locations.add(location);
							}
						}
					}
				}
			}
		}
	}
}
