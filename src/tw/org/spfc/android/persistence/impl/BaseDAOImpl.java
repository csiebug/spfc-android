package tw.org.spfc.android.persistence.impl;

import csiebug.android.persistence.sqliteimpl.DatabaseHelper;
import tw.org.spfc.android.R;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import csiebug.persistence.BaseDAO;
import csiebug.persistence.DAOException;

/**
 * BaseDAO實作
 * @author George_Tsai
 *
 */
public abstract class BaseDAOImpl implements BaseDAO {
	private String databaseName;
	private Integer databaseVersion;
	private Context context;
	private String[] tables;
	private String[] createTableSQLs;
	private String[] createInitialData;
	private DatabaseHelper databaseHelper;
	private SQLiteDatabase database;
	
	public BaseDAOImpl(Context context) {
		this.context = context;
		databaseName = context.getString(R.string.database_name);
		databaseVersion = Integer.parseInt(context.getString(R.string.database_version));
		this.tables = context.getResources().getStringArray(R.array.tables);
		this.createTableSQLs= context.getResources().getStringArray(R.array.create_table_sqls);
		this.createInitialData = context.getResources().getStringArray(R.array.create_initial_data);
	}
	
	public Context getContext() {
		return context;
	}
	
	private void assertOpen() throws DAOException {
		if(database == null) {
			throw new DAOException("SQLiteDatabase is null! open() should be executed first!");
		}
	}
	
	public void open() {
		databaseHelper = new DatabaseHelper(context, databaseName, databaseVersion);
		if(tables != null) {
			databaseHelper.setTables(tables);
		}
		if(createTableSQLs != null) {
			databaseHelper.setCreateTableSQLs(createTableSQLs);
		}
		
		if(createInitialData != null) {
			databaseHelper.setCreateInitialData(createInitialData);
		}
		
		database = databaseHelper.getWritableDatabase();
	}
	
	public void close() {
		if(database != null && database.isOpen()) {
			database.close();
		}
		
		if(databaseHelper != null) {
			databaseHelper.close();
		}
	}
	
	public void beginTransaction() throws DAOException {
		assertOpen();
		
		database.beginTransaction();
	}
	
	public void commit() throws DAOException {
		assertOpen();
		
		if(database.inTransaction()) {
			database.setTransactionSuccessful();
		}
	}
	
	public void rollback() throws DAOException {
		//這裡不需要實作
	}
	
	public void endTransaction() throws DAOException {
		assertOpen();
		
		if(database.inTransaction()) {
			database.endTransaction();
		}
	}
	
	protected int delete(String table, String whereClause, String[] whereArgs) throws DAOException {
		assertOpen();
		
		return database.delete(table, whereClause, whereArgs);
	}
	
	protected int update(String table, ContentValues values, String whereClause, String[] whereArgs) throws DAOException {
		assertOpen();
		
		return database.update(table, values, whereClause, whereArgs);
	}
	
	protected long insert(String table, String nullColumnHack, ContentValues values) throws DAOException {
		assertOpen();
		
		return database.insert(table, nullColumnHack, values);
	}
	
	protected Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) throws DAOException {
		assertOpen();
		
		return database.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
	}
	
	protected Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) throws DAOException {
		assertOpen();
		
		return database.query(table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
	}
	
	protected Cursor query(boolean distinct, String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit) throws DAOException {
		assertOpen();
		
		return database.query(distinct, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
	}
	
	protected Cursor query(String sql, String[] selectionArgs) throws DAOException {
		assertOpen();
		
		return database.rawQuery(sql, selectionArgs);
	}
	
	protected void execSQL(String sql) throws DAOException {
		assertOpen();
		
		database.execSQL(sql);
	}
	
	protected void execSQL(String sql, Object[] bindArgs) throws DAOException {
		assertOpen();
		
		database.execSQL(sql, bindArgs);
	}
}
