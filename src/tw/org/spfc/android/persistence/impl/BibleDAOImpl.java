package tw.org.spfc.android.persistence.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sourceforge.pinyin4j.PinyinHelper;
import android.content.Context;
import android.util.Log;
import csiebug.persistence.DAOException;
import bible.domain.BibleVerse;
import bible.domain.pojoImpl.BibleVerseImpl;
import bible.persistence.BibleDAO;
import tw.org.spfc.android.R;

public class BibleDAOImpl implements BibleDAO {
	/**
	 * 目前共安裝了幾個版本的聖經
	 */
	public static final int VERSIONS = 17;
	
	/**
	 * 中文繁體和合本聖經
	 */
	public static final int CHINESE_UNION_VERSION = 0;
	
	/**
	 * 英王欽定本聖經
	 */
	public static final int KING_JAMES_VERSION = 1;
	
	/**
	 * 中文簡體和合本聖經
	 */
	public static final int CHINA_UNION_VERSION = 2;

	/**
	 * 丹麥文聖經
	 */
	public static final int DN1933 = 3;

	/**
	 * 芬蘭文聖經
	 */
	public static final int R1933 = 4;

	/**
	 * 法文聖經
	 */
	public static final int LSG = 5;

	/**
	 * 德文聖經
	 */
	public static final int LUTH1545 = 6;

	/**
	 * 匈牙利聖經
	 */
	public static final int KAR = 7;

	/**
	 * 挪威聖經
	 * (詩歌智慧書和先知書有缺,會造成程式錯誤,在前端需要把它拿掉)
	 */
	public static final int DNB1930 = 8;

	/**
	 * 葡萄牙文聖經
	 */
	public static final int AA = 9;

	/**
	 * 羅馬尼亞聖經
	 */
	public static final int RMNN = 10;

	/**
	 * 俄文聖經
	 */
	public static final int RUSV = 11;

	/**
	 * 西班牙文聖經
	 */
	public static final int RVA = 12;

	/**
	 * 瑞典聖經
	 */
	public static final int SV1917 = 13;

	/**
	 * 越南聖經
	 */
	public static final int VIET = 14;
	
	/**
	 * 中文繁體和合本聖經(加上難字注音)
	 */
	public static final int CHINESE_UNION_VERSION_WITH_HARDWORD_HELP = 15;
	
	/**
	 * 中文簡體和合本聖經(加上漢語拼音)
	 */
	public static final int CHINA_UNION_VERSION_WITH_HARDWORD_HELP = 16;
	
	//預設和合本
	private int version_0_start = 0; //index=0
	private int version_1_start = 5852; //index=187
	private int version_2_start = 8718; //index=291
	private int version_3_start = 12870; //index=436
	private int version_4_start = 17655; //index=679
	private int version_5_start = 22095; //index=862
	private int version_6_start = 23145; //index=929
	private int version_7_start = 27931; //index=1046
	private int version_7_end = Integer.MAX_VALUE;
	
	// 當前要load進來的資料,儘量通過快取的方式減少IO的次數並且節省記憶體用量
	private Map<String, BibleVerse> caches = new HashMap<String, BibleVerse>();
	// 當前load進來的資料的開始索引數
	private int startIndex = -1;
	// 當前load進來的資料的結束索引數
	private int endIndex = -1;
	
	private int version;
	
	private Context context;
	private int[] testaments = new int[8];
	//預設和合本
	private int versionIndex = R.raw.chinese_union_version_index;
	private int[] chapterStartVerseIndex = new int[1191];
	private List<String> bibleHardwords = new ArrayList<String>();
	
	public BibleDAOImpl(Context context, int version) throws DAOException {
		this.context = context;
		setDatasource(version);
	}
	
	public void setDatasource(int version) throws DAOException {
		//因為(android 2.1以下?)有限制raw檔案不能超過1MB,不然load進來會有IOException
		//所以需要將檔案拆解開來
		switch (version) {
			case CHINESE_UNION_VERSION:
				testaments[0] = R.raw.chinese_union_version_0;
				testaments[1] = R.raw.chinese_union_version_1;
				testaments[2] = R.raw.chinese_union_version_2;
				testaments[3] = R.raw.chinese_union_version_3;
				testaments[4] = R.raw.chinese_union_version_4;
				testaments[5] = R.raw.chinese_union_version_5;
				testaments[6] = R.raw.chinese_union_version_6;
				testaments[7] = R.raw.chinese_union_version_7;
				versionIndex = R.raw.chinese_union_version_index;
				break;
			case KING_JAMES_VERSION:
				testaments[0] = R.raw.king_james_version_0;
				testaments[1] = R.raw.king_james_version_1;
				testaments[2] = R.raw.king_james_version_2;
				testaments[3] = R.raw.king_james_version_3;
				testaments[4] = R.raw.king_james_version_4;
				testaments[5] = R.raw.king_james_version_5;
				testaments[6] = R.raw.king_james_version_6;
				testaments[7] = R.raw.king_james_version_7;
				versionIndex = R.raw.king_james_version_index;
				break;
			case CHINA_UNION_VERSION:
				testaments[0] = R.raw.china_union_version_0;
				testaments[1] = R.raw.china_union_version_1;
				testaments[2] = R.raw.china_union_version_2;
				testaments[3] = R.raw.china_union_version_3;
				testaments[4] = R.raw.china_union_version_4;
				testaments[5] = R.raw.china_union_version_5;
				testaments[6] = R.raw.china_union_version_6;
				testaments[7] = R.raw.china_union_version_7;
				versionIndex = R.raw.china_union_version_index;
				break;
			case DN1933:
				testaments[0] = R.raw.dn1933_0;
				testaments[1] = R.raw.dn1933_1;
				testaments[2] = R.raw.dn1933_2;
				testaments[3] = R.raw.dn1933_3;
				testaments[4] = R.raw.dn1933_4;
				testaments[5] = R.raw.dn1933_5;
				testaments[6] = R.raw.dn1933_6;
				testaments[7] = R.raw.dn1933_7;
				versionIndex = R.raw.dn1933_index;
				break;
			case R1933:
				testaments[0] = R.raw.r1933_0;
				testaments[1] = R.raw.r1933_1;
				testaments[2] = R.raw.r1933_2;
				testaments[3] = R.raw.r1933_3;
				testaments[4] = R.raw.r1933_4;
				testaments[5] = R.raw.r1933_5;
				testaments[6] = R.raw.r1933_6;
				testaments[7] = R.raw.r1933_7;
				versionIndex = R.raw.r1933_index;
				break;
			case LSG:
				testaments[0] = R.raw.lsg_0;
				testaments[1] = R.raw.lsg_1;
				testaments[2] = R.raw.lsg_2;
				testaments[3] = R.raw.lsg_3;
				testaments[4] = R.raw.lsg_4;
				testaments[5] = R.raw.lsg_5;
				testaments[6] = R.raw.lsg_6;
				testaments[7] = R.raw.lsg_7;
				versionIndex = R.raw.lsg_index;
				break;
			case LUTH1545:
				testaments[0] = R.raw.luth1545_0;
				testaments[1] = R.raw.luth1545_1;
				testaments[2] = R.raw.luth1545_2;
				testaments[3] = R.raw.luth1545_3;
				testaments[4] = R.raw.luth1545_4;
				testaments[5] = R.raw.luth1545_5;
				testaments[6] = R.raw.luth1545_6;
				testaments[7] = R.raw.luth1545_7;
				versionIndex = R.raw.luth1545_index;
				break;
			case KAR:
				testaments[0] = R.raw.kar_0;
				testaments[1] = R.raw.kar_1;
				testaments[2] = R.raw.kar_2;
				testaments[3] = R.raw.kar_3;
				testaments[4] = R.raw.kar_4;
				testaments[5] = R.raw.kar_5;
				testaments[6] = R.raw.kar_6;
				testaments[7] = R.raw.kar_7;
				versionIndex = R.raw.kar_index;
				break;
//			case DNB1930:
//				testaments[0] = R.raw.dnb1930_0;
//				testaments[1] = R.raw.dnb1930_1;
//				testaments[2] = R.raw.dnb1930_2;
//				testaments[3] = R.raw.dnb1930_3;
//				testaments[4] = R.raw.dnb1930_4;
//				testaments[5] = R.raw.dnb1930_5;
//				testaments[6] = R.raw.dnb1930_6;
//				testaments[7] = R.raw.dnb1930_7;
//				break;
			case AA:
				testaments[0] = R.raw.aa_0;
				testaments[1] = R.raw.aa_1;
				testaments[2] = R.raw.aa_2;
				testaments[3] = R.raw.aa_3;
				testaments[4] = R.raw.aa_4;
				testaments[5] = R.raw.aa_5;
				testaments[6] = R.raw.aa_6;
				testaments[7] = R.raw.aa_7;
				versionIndex = R.raw.aa_index;
				break;
			case RMNN:
				testaments[0] = R.raw.rmnn_0;
				testaments[1] = R.raw.rmnn_1;
				testaments[2] = R.raw.rmnn_2;
				testaments[3] = R.raw.rmnn_3;
				testaments[4] = R.raw.rmnn_4;
				testaments[5] = R.raw.rmnn_5;
				testaments[6] = R.raw.rmnn_6;
				testaments[7] = R.raw.rmnn_7;
				versionIndex = R.raw.rmnn_index;
				break;
//			case RUSV:
//				testaments[0] = R.raw.rusv_0;
//				testaments[1] = R.raw.rusv_1;
//				testaments[2] = R.raw.rusv_2;
//				testaments[3] = R.raw.rusv_3;
//				testaments[4] = R.raw.rusv_4;
//				testaments[5] = R.raw.rusv_5;
//				testaments[6] = R.raw.rusv_6;
//				testaments[7] = R.raw.rusv_7;
//				break;
			case RVA:
				testaments[0] = R.raw.rva_0;
				testaments[1] = R.raw.rva_1;
				testaments[2] = R.raw.rva_2;
				testaments[3] = R.raw.rva_3;
				testaments[4] = R.raw.rva_4;
				testaments[5] = R.raw.rva_5;
				testaments[6] = R.raw.rva_6;
				testaments[7] = R.raw.rva_7;
				versionIndex = R.raw.rva_index;
				break;
			case SV1917:
				testaments[0] = R.raw.sv1917_0;
				testaments[1] = R.raw.sv1917_1;
				testaments[2] = R.raw.sv1917_2;
				testaments[3] = R.raw.sv1917_3;
				testaments[4] = R.raw.sv1917_4;
				testaments[5] = R.raw.sv1917_5;
				testaments[6] = R.raw.sv1917_6;
				testaments[7] = R.raw.sv1917_7;
				versionIndex = R.raw.sv1917_index;
				break;
//			case VIET:
//				testaments[0] = R.raw.viet_0;
//				testaments[1] = R.raw.viet_1;
//				testaments[2] = R.raw.viet_2;
//				testaments[3] = R.raw.viet_3;
//				testaments[4] = R.raw.viet_4;
//				testaments[5] = R.raw.viet_5;
//				testaments[6] = R.raw.viet_6;
//				testaments[7] = R.raw.viet_7;
//				break;
			case CHINESE_UNION_VERSION_WITH_HARDWORD_HELP:
				testaments[0] = R.raw.chinese_union_version_0;
				testaments[1] = R.raw.chinese_union_version_1;
				testaments[2] = R.raw.chinese_union_version_2;
				testaments[3] = R.raw.chinese_union_version_3;
				testaments[4] = R.raw.chinese_union_version_4;
				testaments[5] = R.raw.chinese_union_version_5;
				testaments[6] = R.raw.chinese_union_version_6;
				testaments[7] = R.raw.chinese_union_version_7;
				versionIndex = R.raw.chinese_union_version_index;
				break;
			case CHINA_UNION_VERSION_WITH_HARDWORD_HELP:
				testaments[0] = R.raw.china_union_version_0;
				testaments[1] = R.raw.china_union_version_1;
				testaments[2] = R.raw.china_union_version_2;
				testaments[3] = R.raw.china_union_version_3;
				testaments[4] = R.raw.china_union_version_4;
				testaments[5] = R.raw.china_union_version_5;
				testaments[6] = R.raw.china_union_version_6;
				testaments[7] = R.raw.china_union_version_7;
				versionIndex = R.raw.china_union_version_index;
				break;
			default:
				break;
		}
		
		InputStream inputStream = null;
		try {
			inputStream = context.getResources().openRawResource(versionIndex);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			
			String line = null;
			
			int index = 0;
			while((line = reader.readLine()) != null){
				chapterStartVerseIndex[index] = Integer.parseInt(line);
				index++;
	        }
		} catch (IOException e) {
			throw new DAOException(e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new DAOException(e);
				}
			}
		}
		
		version_0_start = chapterStartVerseIndex[0]; //index=0
		version_1_start = chapterStartVerseIndex[187]; //index=187
		version_2_start = chapterStartVerseIndex[291]; //index=291
		version_3_start = chapterStartVerseIndex[436]; //index=436
		version_4_start = chapterStartVerseIndex[679]; //index=679
		version_5_start = chapterStartVerseIndex[862]; //index=862
		version_6_start = chapterStartVerseIndex[929]; //index=929
		version_7_start = chapterStartVerseIndex[1046]; //index=1046
		
		if(this.version != version) {
			// 轉換聖經版本時,記得把cache全部清掉,避免拿錯版本的經文
			caches.clear();
			startIndex = -1;
			endIndex = -1;
		}
		
		this.version = version;
	}
	
	private void loadTestament(int testamentIndex) throws DAOException {
		switch (testamentIndex) {
		case 0:
			startIndex = version_0_start;
			endIndex = version_1_start - 1;
			break;
		case 1:
			startIndex = version_1_start;
			endIndex = version_2_start - 1;
			break;
		case 2:
			startIndex = version_2_start;
			endIndex = version_3_start - 1;
			break;
		case 3:
			startIndex = version_3_start;
			endIndex = version_4_start - 1;
			break;
		case 4:
			startIndex = version_4_start;
			endIndex = version_5_start - 1;
			break;
		case 5:
			startIndex = version_5_start;
			endIndex = version_6_start - 1;
			break;
		case 6:
			startIndex = version_6_start;
			endIndex = version_7_start - 1;
			break;
		case 7:
			startIndex = version_7_start;
			endIndex = version_7_end;
			break;
		default:
			break;
		}
		
		InputStream inputStream = null;
		try {
			inputStream = context.getResources().openRawResource(testaments[testamentIndex]);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			
			String line = null;
			
			int i = startIndex;
			while((line = reader.readLine()) != null){
				BibleVerse verse = new BibleVerseImpl();
				verse.setVerse(line);
				caches.put(Integer.toString(i), verse);
				i++;
	        }
		} catch (IOException e) {
			throw new DAOException(e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new DAOException(e);
				}
			}
		}
	}
	
	public BibleVerse getVerse(int index) throws DAOException {
		// 先檢查用戶請求的資料是否在快取中
		// 如果不在
		// 就先清除快取(為了節省記憶體)
		// 然後從IO中取得資料
		// 主要考慮速度的問題
		// 因為從快取中取得資料要比IO中取快得多
		if((index > this.endIndex) || (index < startIndex)) {
			caches.clear();
			
			if(index < version_1_start) {
				loadTestament(0);
			} else if(index < version_2_start) {
				loadTestament(1);
			} else if(index < version_3_start) {
				loadTestament(2);
			} else if(index < version_4_start) {
				loadTestament(3);
			} else if(index < version_5_start) {
				loadTestament(4);
			} else if(index < version_6_start) {
				loadTestament(5);
			} else if(index < version_7_start) {
				loadTestament(6);
			} else {
				loadTestament(7);
			}
		}
		
		BibleVerse verse = caches.get(Integer.toString(index));
		if(version == CHINESE_UNION_VERSION_WITH_HARDWORD_HELP) {
			String verseHardwords = "";
			try {
				loadHardWords(version);
				try {
					verseHardwords = bibleHardwords.get(index);
				} catch(Exception e) {
					Log.d("hard word", "" + index);
				}
				
				if(!verseHardwords.equals("")) {
					String[] hardwords = verseHardwords.split(";");
					for(int j = 0; j < hardwords.length; j++) {
						if(verse.getVerse().indexOf(hardwords[j].split(" ")[0] + "(" + hardwords[j].split(" ")[1] + ")") == -1) {
							verse.setVerse(verse.getVerse().replaceAll(hardwords[j].split(" ")[0], hardwords[j].split(" ")[0] + "(" + hardwords[j].split(" ")[1] + ")"));
						}
					}
				}
			} catch (IOException e) {
				throw new DAOException(e);
			}
		} else if(version == CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
			String verseHardwords = "";
			try {
				loadHardWords(version);
				try {
					verseHardwords = bibleHardwords.get(index);
				} catch(Exception e) {
					Log.d("hard word", "" + index);
				}
				
				if(!verseHardwords.equals("")) {
					String[] hardwords = verseHardwords.split(";");
					for(int j = 0; j < hardwords.length; j++) {
						String hardword = hardwords[j].split(" ")[0];
						try {
							String pinyin = PinyinHelper.toHanyuPinyinStringArray(hardword.toCharArray()[0])[0];
							
							if(verse.getVerse().indexOf(hardword + "(" + pinyin + ")") == -1) {
								verse.setVerse(verse.getVerse().replaceAll(hardword, hardword + "(" + pinyin + ")"));
							}
						} catch(Exception e) {
							Log.d("hard word", hardword);
						}
					}
				}
			} catch (IOException e) {
				throw new DAOException(e);
			}
		}
		
		return verse;
	}
	
	private void loadHardWords(int version) throws IOException {
		if(bibleHardwords.size() == 0) {
			InputStream inputStream = null;
			try {
				if(version == CHINA_UNION_VERSION_WITH_HARDWORD_HELP) {
					inputStream = context.getResources().openRawResource(R.raw.hardword3);
				} else {
					inputStream = context.getResources().openRawResource(R.raw.hardword2);
				}
				BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
				
				String line = null;
				while((line = reader.readLine()) != null){
					bibleHardwords.add(line);
				}
			} finally {
				if(inputStream != null) {
					inputStream.close();
				}
			}
		}
	}

	public List<BibleVerse> getVerses(int startIndex, int endIndex) throws DAOException {
		List<BibleVerse> verses = new ArrayList<BibleVerse>();
		for(int i = startIndex; i < endIndex; i++) {
			verses.add(getVerse(i));
		}
		
		return verses;
	}
	
	/**
	 * 所有安裝的聖經版本的對應名稱
	 * @param version
	 * @return
	 */
	public static String getVersionName(int version) {
		String[] versionNames = {"CUVT", "KJV", "CUVS", "DN1933", "R1933", "LSG", "LUTH1545", "KAR", "DNB1930", "AA", "RMNN", "RUSV", "RVA", "SV1917", "VIET", "CUVTH", "CUVSH"};
		return versionNames[version];
	}

	public int[] getChapterStartVerseIndex() throws DAOException {
		return chapterStartVerseIndex;
	}
}
