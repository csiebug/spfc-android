package tw.org.spfc.android;


public class DailyBreadActivity extends BaseActivity {
	
	@Override
	protected int getLayout() {
		return R.layout.daily_bread;
	}

	@Override
	protected void setListeners() {
		
	}

	@Override
	protected void setAdapters() {
	}
	
	@Override
	protected void onCreateLogic() {
		
	}
}
