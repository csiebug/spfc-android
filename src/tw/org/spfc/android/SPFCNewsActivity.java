package tw.org.spfc.android;

import java.io.UnsupportedEncodingException;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.facebook.FacebookRequestError;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.analytics.tracking.android.EasyTracker;

import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.domain.News;
import tw.org.spfc.domain.pojoImpl.UserImpl;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class SPFCNewsActivity extends BaseFacebookActivity {
	@Override
	protected void enableUI(GraphUser user) {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = LayoutParams.WRAP_CONTENT;
		params.width = LayoutParams.WRAP_CONTENT;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.VISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void disableUI() {
		Button shareToFacebook = (Button)findViewById(R.id.share_to_facebook);
		LayoutParams params = shareToFacebook.getLayoutParams();
		params.height = 0;
		params.width = 0;
		shareToFacebook.setLayoutParams(params);
		findViewById(R.id.share_to_facebook).setVisibility(View.INVISIBLE);
		findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
	}

	@Override
	protected void showPublishResult(GraphObject result,
			FacebookRequestError error) {}

	@Override
	protected String getUserStatusMessage() {
		return null;
	}

	@Override
	protected Bitmap getUserPhoto() {
		return null;
	}

	@Override
	protected int getLayout() {
		return R.layout.news;
	}

	@Override
	protected void setAdapters() {
		((LoginButton) findViewById(R.id.login_button)).setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
            public void onUserInfoFetched(GraphUser user) {
                setUser(user);
                updateUI();
                // It's possible that we were waiting for this.user to be populated in order to post a
                // status update.
                handlePendingAction();
            }
        });
	}

	@Override
	protected void setListeners() {
		findViewById(R.id.pray).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@SuppressLint("SetJavaScriptEnabled")
			@Override
			protected void onClickLogic(View v) {
				EasyTracker.getTracker().sendEvent("prayer_letter", "read", "", (long)0);
				
				WebView message = new WebView(SPFCNewsActivity.this);
				message.getSettings().setJavaScriptEnabled(true);
				message.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading(WebView view,
							String url) {
						view.loadUrl(url);
						return true;
					}
				});
				message.loadUrl("http://docs.google.com/gview?embedded=true&url=http://www.spfctw.org/image/pdf/prayer/new.pdf");
				alert(getString(R.string.pray), message, getString(R.string.ok));
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});

		findViewById(R.id.share_to_facebook).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
			@Override
			protected void onClickLogic(View arg0) {
				try {
					//TODO: 目前authentication和authorization還沒有server端服務,所以這邊只好先暫時傳null使用者
					UserImpl user = new UserImpl();
					user.enable(true);
					
					// TODO description目前放的是facebook所用的縮圖
					List<News> news = ServiceProxyFactory.createNewsService(SPFCNewsActivity.this, user, null).getPromotedNews();
					if(news.size() > 0) {
						publishFeedDialog(getString(R.string.facebook_spfc_news), "", new String(news.get(0).getContent().getBytes("ISO-8859-1"), "UTF-8"), news.get(0).getURL(), news.get(0).getDescription());
					}
				} catch (ServiceException e) {
					Log.e(getClass().getName(), "Get news error!", e);
				} catch (UnsupportedEncodingException e) {
					Log.e(getClass().getName(), e.getMessage(), e);
				}
			}
		});
		
		findViewById(R.id.share_to_twitter).setOnClickListener(new AbstractDefaultButtonListener(this) {

			@Override
			protected void onClickLogic(View arg0) {
				try {
					//TODO: 目前authentication和authorization還沒有server端服務,所以這邊只好先暫時傳null使用者
					UserImpl user = new UserImpl();
					user.enable(true);
					
					List<News> news = ServiceProxyFactory.createNewsService(SPFCNewsActivity.this, user, null).getPromotedNews();
					if(news.size() > 0) {
						SPFCPreference preference = new SPFCPreference(SPFCNewsActivity.this);
						String oauthAccessToken = preference.getTwitterToken();
						String oAuthAccessTokenSecret = preference.getTwitterTokenSecret();

						ConfigurationBuilder confbuilder = new ConfigurationBuilder();
						Configuration conf = confbuilder
											.setOAuthConsumerKey(getString(R.string.twitter_consumer_key))
											.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret))
											.setOAuthAccessToken(oauthAccessToken)
											.setOAuthAccessTokenSecret(oAuthAccessTokenSecret)
											.setUseSSL(true)
											.build();
						
						Twitter twitter = new TwitterFactory(conf).getInstance();
						
						twitter.updateStatus(news.get(0).getURL() + "\n" + new String(news.get(0).getContent().getBytes("ISO-8859-1"), "UTF-8"));
						toast(getString(R.string.twitter_success));
					}
				} catch (ServiceException e) {
					Log.e(getClass().getName(), "Get news error!", e);
				} catch (UnsupportedEncodingException e) {
					Log.e(getClass().getName(), e.getMessage(), e);
				} catch (TwitterException e) {
					Log.e(getClass().getName(), "TwitterException", e);
				}
			}

			@Override
			protected void onClickLogic(DialogInterface arg0, int arg1) {}
			
		});
	}
	
	@Override
	protected void onCreateLogic() {
		if(AndroidUtility.isNetworkAvailable(this) && isLoggedTwitter()) {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = LayoutParams.WRAP_CONTENT;
			params.width = LayoutParams.WRAP_CONTENT;
			shareToTwitter.setLayoutParams(params);
		} else {
			Button shareToTwitter = (Button)findViewById(R.id.share_to_twitter);
			LayoutParams params = shareToTwitter.getLayoutParams();
			params.height = 0;
			params.width = 0;
			shareToTwitter.setLayoutParams(params);
		}
		
		WebView webView = (WebView)findViewById(R.id.web);
		webView.getSettings().setSupportZoom(true);
		webView.getSettings().setBuiltInZoomControls(true);
		webView.loadDataWithBaseURL(getString(R.string.spfcHomepage), getHTML(), "text/html", "UTF-8", null);
	}
	
	private String getHTML() {
		StringBuffer html = new StringBuffer();
		html.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body bgcolor='#EBEB99'>\n");
		
		SPFCPreference preference = new SPFCPreference(this);
    	
    	String[] newsContents = preference.getMainMenuNews();
        
        for(int i = 0; i < newsContents.length; i++) {
        	html.append(newsContents[i] + "\n");
        }
        
        html.append("</body></html>");
        
    	return html.toString();
	}
}
