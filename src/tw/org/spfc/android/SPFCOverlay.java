package tw.org.spfc.android;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import csiebug.android.google.map.AbstractOverlay;

public class SPFCOverlay extends AbstractOverlay {
	private GeoPoint point;
	private String name;
	private String address;
	
	public SPFCOverlay(Drawable defaultMarker) {
		super(boundCenterBottom(defaultMarker));
	}
	
	public SPFCOverlay(Drawable defaultMarker, GeoPoint point, String name, String address) {
		super(boundCenterBottom(defaultMarker));
		this.point = point;
		this.name = name;
		this.address = address;
	}

	@Override
	protected void addAllPoints(Context context) {
		addPoint(point, name, address);
	}

	@Override
	protected boolean onTapLogic(OverlayItem item) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
		dialog.setTitle(item.getTitle());
		dialog.setMessage(item.getSnippet());
		dialog.show();
		return true;
	}

}
