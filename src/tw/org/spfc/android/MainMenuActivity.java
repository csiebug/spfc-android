package tw.org.spfc.android;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.google.analytics.tracking.android.EasyTracker;

import bible.domain.BibleVerse;
import tw.org.spfc.android.listener.AbstractDefaultButtonListener;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.persistence.impl.BibleDAOImpl;
import tw.org.spfc.android.service.RobotService;
import tw.org.spfc.android.service.impl.ServiceProxyFactory;
import tw.org.spfc.android.util.SPFCFileSystem;
import tw.org.spfc.android.util.SPFCPreference;
import tw.org.spfc.domain.BibleSearchResult;
import tw.org.spfc.domain.News;
import tw.org.spfc.domain.Photo;
import tw.org.spfc.domain.pojoImpl.UserImpl;
import tw.org.spfc.service.BibleService;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import csiebug.util.FileUtility;

public class MainMenuActivity extends BaseActivity {
	private char[] chineseNumber = {'零', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十', '百'};
	
	private int toNumberForBible(String number, boolean chapter) throws Exception {
		//getLogger(getClass().getName()).warning("chineseNumber = " + number);
		
		//檢查所有字元
		for(int i = 0; i < number.length(); i++) {
			if(!isValid(number.toCharArray()[i])) {
				throw new Exception("Wrong number!");
			}
		}
		
		int result = 0;
		if(number.indexOf(chineseNumber[11]) == -1 && number.indexOf(chineseNumber[10]) == -1) {
			if(number.length() == 1) {
				result = toDecimal(number.toCharArray()[0]);
			} else if(number.length() == 2) {
				result = toDecimal(number.toCharArray()[0]) * 10 + toDecimal(number.toCharArray()[1]);
			} else if(number.length() == 3) {
				result = toDecimal(number.toCharArray()[0]) * 100 + toDecimal(number.toCharArray()[1]) * 10 + toDecimal(number.toCharArray()[2]);
			} else {
				throw new Exception("Wrong number!");
			}
		} else if(number.indexOf(chineseNumber[11]) == -1 && number.indexOf(chineseNumber[10]) != -1) {
			if(number.length() == 3) {
				result = toDecimal(number.split("" + chineseNumber[10])[0].toCharArray()[0]) * 10 + toDecimal(number.split("" + chineseNumber[10])[1].toCharArray()[0]);
			} else if(number.length() == 2 && number.startsWith("" + chineseNumber[10])) {
				result = 10 + toDecimal(number.split("" + chineseNumber[10])[1].toCharArray()[0]);
			} else if(number.length() == 2 && number.endsWith("" + chineseNumber[10])) {
				result = toDecimal(number.split("" + chineseNumber[10])[0].toCharArray()[0]) * 10;
			} else if(number.length() == 1) {
				result = 10;
			} else {
				throw new Exception("Wrong number!");
			}
		} else if(number.indexOf(chineseNumber[11]) != -1 && number.indexOf(chineseNumber[10]) == -1) {
			if(number.length() == 4) {
				result = toDecimal(number.split("" + chineseNumber[11] + chineseNumber[0])[0].toCharArray()[0]) * 100 + toDecimal(number.split("" + chineseNumber[11] + chineseNumber[0])[1].toCharArray()[0]);
			} else if(number.length() == 2) {
				result = toDecimal(number.split("" + chineseNumber[11])[0].toCharArray()[0]) * 100;
			} else {
				throw new Exception("Wrong number!");
			}
		} else if(number.indexOf(chineseNumber[11]) != -1 && number.indexOf(chineseNumber[10]) != -1) {
			String temp1 = number.split("" + chineseNumber[11])[0];
			String temp2 = number.split("" + chineseNumber[11])[1];
			String temp3 = temp2.split("" + chineseNumber[10])[0];
			
			if(number.length() == 5) {
				String temp4 = temp2.split("" + chineseNumber[10])[1];
				result = toDecimal(temp1.toCharArray()[0]) * 100 + toDecimal(temp3.toCharArray()[0]) * 10 + toDecimal(temp4.toCharArray()[0]);
			} else if(number.length() == 4) {
				result = toDecimal(temp1.toCharArray()[0]) * 100 + toDecimal(temp3.toCharArray()[0]) * 10;
			} else {
				throw new Exception("Wrong number!");
			}
		}
		
		//magic number for聖經最多章和最多節
		int max = 150;
		if(!chapter) {
			max = 176;
		}
		
		if(result <= max) {
			return result;
		} else {
			throw new Exception("Wrong number!");
		}
	}
	
	private boolean isValid(char number) {
		if(chineseNumber[10] == number) {
			return true;
		} else if(chineseNumber[11] == number) {
			return true;
		}
		
		try {
			toDecimal(number);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	private int toDecimal(char number) throws Exception {
		for(int i = 0; i < 10; i++) {
			if(chineseNumber[i] == number) {
				return i;
			}
		}
		
		throw new Exception("Wrong number!");
	}
	
	@Override
	protected int getLayout() {
		return R.layout.main;
	}
	
	@Override
	protected void setListeners() {
		findViewById(R.id.sermon_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent sermonIntent = new Intent();
				sermonIntent.setClass(MainMenuActivity.this, SermonActivity.class);
				
				startActivity(sermonIntent);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		//TODO: 功能太多，版面有限，故捨棄此功能
//		findViewById(R.id.room_book_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
//			@Override
//			protected void onClickLogic(View v) {
//				if(!AndroidUtility.isNetworkAvailable(MainMenuActivity.this)) {
//					toast(getString(R.string.network_unavailable));
//				} else {
//					Intent roomBookIntent = new Intent();
//					roomBookIntent.setClass(MainMenuActivity.this, RoomBookActivity.class);
//					
//					startActivity(roomBookIntent);
//				}
//			}
//			
//			@Override
//			protected void onClickLogic(DialogInterface dialog, int which) {}
//		});
		
		findViewById(R.id.sunday_school_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				if(!AndroidUtility.isNetworkAvailable(MainMenuActivity.this)) {
					toast(getString(R.string.network_unavailable));
				} else {
					Intent sundaySchoolIntent = new Intent();
					sundaySchoolIntent.setClass(MainMenuActivity.this, SundaySchoolActivity.class);
					
					startActivity(sundaySchoolIntent);
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.library_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				if(!AndroidUtility.isNetworkAvailable(MainMenuActivity.this)) {
					toast(getString(R.string.network_unavailable));
				} else {
					Intent libraryIntent = new Intent();
					libraryIntent.setClass(MainMenuActivity.this, LibraryActivity.class);
					
					startActivity(libraryIntent);
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});

		findViewById(R.id.calendar_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			@Override
			protected void onClickLogic(View v) {
				if(!AndroidUtility.isNetworkAvailable(MainMenuActivity.this)) {
					toast(getString(R.string.network_unavailable));
				} else {
					alertSingleChoiceList(MainMenuActivity.this, "", new String[]{getString(R.string.calendar_1), getString(R.string.calendar_2)}, new AbstractDefaultButtonListener(MainMenuActivity.this) {

						@Override
						protected void onClickLogic(DialogInterface dialog,
								int which) {
							Intent calendarIntent = new Intent();
							
							if(which == 0) {
								calendarIntent.setClass(MainMenuActivity.this, CalendarActivity.class);
							} else {
								calendarIntent.setClass(MainMenuActivity.this, WorshipTimeActivity.class);
							}
							
							startActivity(calendarIntent);
							dialog.cancel();
						}

						@Override
						protected void onClickLogic(View v) {}
					});
				}
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.bible_reading_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent bibleReadingIntent = new Intent();
				bibleReadingIntent.setClass(MainMenuActivity.this, BibleReadingActivity.class);
				
				startActivity(bibleReadingIntent);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.phone_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent intent = new Intent(MainMenuActivity.this, SPFCContactActivity.class);
				startActivity(intent);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.map_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent intent = new Intent(MainMenuActivity.this, SPFCMapActivity.class);
				startActivity(intent);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});

		findViewById(R.id.settings_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent settingsIntent = new Intent();
				settingsIntent.setClass(MainMenuActivity.this, SettingsActivity.class);
				
				startActivity(settingsIntent);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
		
		findViewById(R.id.news_button).setOnClickListener(new AbstractDefaultButtonListener(this) {
			
			@Override
			protected void onClickLogic(View v) {
				Intent newsIntent = new Intent();
				newsIntent.setClass(MainMenuActivity.this, SPFCNewsActivity.class);
				
				startActivity(newsIntent);
			}
			
			@Override
			protected void onClickLogic(DialogInterface dialog, int which) {}
		});
	}
	
	@Override
	protected Integer getOptionMenuLayout() {
		return R.menu.main_menu;
	}
	
	@Override
	protected void optionsItemSelectedLogic(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.assistant:
				Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
			    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
			    intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.talk));
			    startActivityForResult(intent, 1);
			   break;
			default:
				break;
		}
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
		switch (resultCode) {
			case RESULT_OK:
				ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				String firstMatch = (String)result.get(0);
				
				EasyTracker.getTracker().sendEvent("asistant", "keyword", firstMatch, (long)0);
				
				if(firstMatch.equalsIgnoreCase(getString(R.string.library))) {
					findViewById(R.id.library_button).performClick();
				} else if(firstMatch.equalsIgnoreCase(getString(R.string.calendar))) {
					findViewById(R.id.calendar_button).performClick();
				} else if(firstMatch.equalsIgnoreCase(getString(R.string.news))) {
					findViewById(R.id.news_button).performClick();
				} else if(firstMatch.equalsIgnoreCase(getString(R.string.sermon))) {
					findViewById(R.id.sermon_button).performClick();
				} else if(firstMatch.equalsIgnoreCase(getString(R.string.bible_reading))) {
					findViewById(R.id.bible_reading_button).performClick();
				} else if(firstMatch.equalsIgnoreCase(getString(R.string.sunday_school))) {
					findViewById(R.id.sunday_school_button).performClick();
				} else if(firstMatch.equalsIgnoreCase(getString(R.string.contact))) {
					findViewById(R.id.phone_button).performClick();
				} else if(firstMatch.equalsIgnoreCase(getString(R.string.church_address))) {
					findViewById(R.id.map_button).performClick();
				} else if(firstMatch.equalsIgnoreCase(getString(R.string.settings))) {
					findViewById(R.id.settings_button).performClick();
				} else {
					try {
						int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
						int version = BibleDAOImpl.CHINESE_UNION_VERSION;
						
						BibleService service = ServiceProxyFactory.createBibleService(this, version, language);
						int bookId = isBibleBook(firstMatch);
						
						if(bookId != -1) {
							int chapterId = 0;
							int verseId = 0;
							
							if(firstMatch.indexOf("節") != -1) {
								String chapterUnit = "章";
								if(firstMatch.indexOf(chapterUnit) == -1) {
									chapterUnit = "篇";
									firstMatch = firstMatch.replaceFirst("詩篇", "");
								}
								
								try {
									chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 3, firstMatch.indexOf(chapterUnit)));
								} catch(Exception e) {
									try {
										chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 2, firstMatch.indexOf(chapterUnit)));
									} catch(Exception e2) {
										try {
											chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 1, firstMatch.indexOf(chapterUnit)));
										} catch(Exception e3) {
											try {
												chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 5, firstMatch.indexOf(chapterUnit)), true);
											} catch (Exception e4) {
												try {
													chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 4, firstMatch.indexOf(chapterUnit)), true);
												} catch (Exception e5) {
													try {
														chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 3, firstMatch.indexOf(chapterUnit)), true);
													} catch (Exception e6) {
														try {
															chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 2, firstMatch.indexOf(chapterUnit)), true);
														} catch (Exception e7) {
															try {
																chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 1, firstMatch.indexOf(chapterUnit)), true);
															} catch (Exception e8) {
																
															}
														}
													}
												}
											}
										}
									}
								}
								
								try {
									verseId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf("節") - 3, firstMatch.indexOf("節")));
								} catch(Exception e) {
									try {
										verseId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf("節") - 2, firstMatch.indexOf("節")));
									} catch(Exception e2) {
										try {
											verseId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf("節") - 1, firstMatch.indexOf("節")));
										} catch(Exception e3) {
											try {
												verseId = toNumberForBible(firstMatch.substring(firstMatch.indexOf("節") - 5, firstMatch.indexOf("節")), false);
											} catch (Exception e4) {
												try {
													verseId = toNumberForBible(firstMatch.substring(firstMatch.indexOf("節") - 4, firstMatch.indexOf("節")), false);
												} catch (Exception e5) {
													try {
														verseId = toNumberForBible(firstMatch.substring(firstMatch.indexOf("節") - 3, firstMatch.indexOf("節")), false);
													} catch (Exception e6) {
														try {
															verseId = toNumberForBible(firstMatch.substring(firstMatch.indexOf("節") - 2, firstMatch.indexOf("節")), false);
														} catch (Exception e7) {
															try {
																verseId = toNumberForBible(firstMatch.substring(firstMatch.indexOf("節") - 1, firstMatch.indexOf("節")), false);
															} catch (Exception e8) {
																
															}
														}
													}
												}
											}
										}
									}
								}
							//可能是要取某章經文
							} else if(firstMatch.indexOf("章") != -1 || firstMatch.indexOf("篇") != -1) {
								String chapterUnit = "章";
								if(firstMatch.indexOf(chapterUnit) == -1) {
									chapterUnit = "篇";
									firstMatch = firstMatch.replaceFirst("詩篇", "");
								}
								
								try {
									chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 3, firstMatch.indexOf(chapterUnit)));
								} catch(Exception e) {
									try {
										chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 2, firstMatch.indexOf(chapterUnit)));
									} catch(Exception e2) {
										try {
											chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 1, firstMatch.indexOf(chapterUnit)));
										} catch(Exception e3) {
											try {
												chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 5, firstMatch.indexOf(chapterUnit)), true);
											} catch (Exception e4) {
												try {
													chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 4, firstMatch.indexOf(chapterUnit)), true);
												} catch (Exception e5) {
													try {
														chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 3, firstMatch.indexOf(chapterUnit)), true);
													} catch (Exception e6) {
														try {
															chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 2, firstMatch.indexOf(chapterUnit)), true);
														} catch (Exception e7) {
															try {
																chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(chapterUnit) - 1, firstMatch.indexOf(chapterUnit)), true);
															} catch (Exception e8) {
																
															}
														}
													}
												}
											}
										}
									}
								}
							//可能是要取某章經文
							} else {
								String bookName = service.getBookNames().get(bookId);
								
								try {
									chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(bookName) + bookName.length(), firstMatch.indexOf(bookName) + bookName.length() + 3));
								} catch(Exception e) {
									try {
										chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(bookName) + bookName.length(), firstMatch.indexOf(bookName) + bookName.length() + 2));
									} catch(Exception e2) {
										try {
											chapterId = Integer.parseInt(firstMatch.substring(firstMatch.indexOf(bookName) + bookName.length(), firstMatch.indexOf(bookName) + bookName.length() + 1));
										} catch(Exception e3) {
											try {
												chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(bookName) + bookName.length(), firstMatch.indexOf(bookName) + bookName.length() + 5), true);
											} catch (Exception e4) {
												try {
													chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(bookName) + bookName.length(), firstMatch.indexOf(bookName) + bookName.length() + 4), true);
												} catch (Exception e5) {
													try {
														chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(bookName) + bookName.length(), firstMatch.indexOf(bookName) + bookName.length() + 3), true);
													} catch (Exception e6) {
														try {
															chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(bookName) + bookName.length(), firstMatch.indexOf(bookName) + bookName.length() + 2), true);
														} catch (Exception e7) {
															try {
																chapterId = toNumberForBible(firstMatch.substring(firstMatch.indexOf(bookName) + bookName.length(), firstMatch.indexOf(bookName) + bookName.length() + 1), true);
															} catch (Exception e8) {
																
															}
														}
													}
												}
											}
										}
									}
								}
							}
							
							if(chapterId != 0) {
								SPFCPreference preference = new SPFCPreference(this);
								if(verseId != 0) {
									preference.setBibleReading(bookId, chapterId, verseId);
								} else {
									preference.setBibleReading(bookId, chapterId, 1);
								}
								preference.setBibleVersion(BibleDAOImpl.CHINESE_UNION_VERSION);
								findViewById(R.id.bible_reading_button).performClick();
							} else {
								alert(getString(R.string.assistant), getString(R.string.no_data), getString(R.string.ok));
							}
						} else if(firstMatch.indexOf("經文") != -1) {
							String keyword = firstMatch.split("的經文")[0];
							
							BibleSearchResult searchResult = service.searchVerses(keyword);
							
							if(searchResult != null && searchResult.size() > 0) {
								String[] verses = new String[searchResult.size()];
								
								List<BibleVerse> list = searchResult.getVerses();
								for(int i = 0; i < searchResult.size(); i++) {
									verses[i] = list.get(i).getVerse();
								}
								
								Intent intent = new Intent(this, BibleVerseListActivity.class);
								Bundle bundle = new Bundle();
								bundle.putString("keyword", keyword);
								bundle.putStringArray("verses", verses);
								intent.putExtras(bundle);
								
								startActivity(intent);
							} else {
								alert(getString(R.string.assistant), getString(R.string.no_data), getString(R.string.ok));
							}
						} else {
							RobotService robotService = ServiceProxyFactory.createRobotService(this);
							String reply = "";
							try {
								reply = robotService.talk(firstMatch);
							} catch (ServiceException e) {
								//輔助功能,產生錯誤就不丟出exception了
								Log.e(getClass().getName(), "RobotService error!", e);
							}
							
							Log.d(getClass().getName(), "First Match=" + firstMatch);
							alert(getString(R.string.assistant), reply, getString(R.string.ok));
						}
					} catch (ServiceException e1) {
						Log.w(getClass().getName(), e1);
						alert(getString(R.string.assistant), getString(R.string.no_data), getString(R.string.ok));
					}
				}
				break;
			default:
				break;
		}
	}
	
	/**
	 * 看看文字中是否含有聖經書卷的名稱,傳回書卷的編號
	 * @param message
	 * @return
	 * @throws ServiceException
	 */
	private int isBibleBook(String message) throws ServiceException {
		BibleService service = ServiceProxyFactory.createBibleService(this, BibleDAOImpl.CHINESE_UNION_VERSION, BibleCatagoryDAOImpl.CHINESE_BIBLE);
		List<String> names = service.getBookNames();
		for(int i = 0; i < names.size(); i++) {
			if(message.indexOf(names.get(i)) != -1) {
				return i;
			}
		}
		
		return -1;
	}
	
	@Override
	protected void setAdapters() {}
    
	@Override
	protected void onCreateLogic() {
		try {
			getWebViewData();
			
			WebView webView = (WebView)findViewById(R.id.spfc_news);
			if(AndroidUtility.isNetworkAvailable(this) && AndroidUtility.getExternalStoragePath() != null) {
				webView.getSettings().setJavaScriptEnabled(true);
				webView.getSettings().setSupportZoom(true);
				webView.getSettings().setBuiltInZoomControls(true);
				
				//教會網頁的照片slide show設定的寬度是472px,所以hard code這個magic number在這邊
				webView.setInitialScale(AndroidUtility.getScale(this, 472));
				
				webView.setWebViewClient(new WebViewClient() {
					@Override
				    public boolean shouldOverrideUrlLoading(WebView view, String url) {
				        view.loadUrl(url);
				        return true;
				    }
					
					public void onReceivedSslError(WebView view,
							SslErrorHandler handler, SslError error) {
						//handler.proceed();
						handler.cancel();
					}
				});
				
				TextView footer = (TextView)findViewById(R.id.footer);
				footer.setText("");
				footer.setHeight(0);
				
				loadHTML();
			} else {
				webView.setVisibility(View.INVISIBLE);
			}
		} catch(Exception e) {
			//輔助功能,產生錯誤就不丟出exception了
			Log.e(getClass().getName(), "Read html error!", e);
		}
	}
	
	private void loadHTML() throws IOException, ServiceException {
		WebView webView = (WebView)findViewById(R.id.spfc_news);
		
		String html = getSlideShowHTML();
		File cacheFolder = SPFCFileSystem.getCacheFolder(this);
		if(cacheFolder != null && (cacheFolder.exists() || cacheFolder.mkdirs())) {
			File htmlFile = SPFCFileSystem.getPhotoCacheFile(this);
			if(htmlFile != null) {
				if(htmlFile.exists()) {
					htmlFile.delete();
				}
				htmlFile.createNewFile();
				
				FileUtility.writeTextFile(html, htmlFile.getPath(), "UTF-8");
				webView.loadUrl("file://" + htmlFile.getPath());
			}
		}
	}
	
	private String getSlideShowHTML() throws IOException, ServiceException {
		StringBuilder result= new StringBuilder();
		
		InputStream inputStream = null;
		try {
			inputStream = getResources().openRawResource(R.raw.spfc);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			
			String line = null;
	        while((line = reader.readLine()) != null){
	        	if(line.startsWith("<div class=\"tog\"><h3></h3></div>")) {
	        		result.append(line.replaceAll("<div class=\"tog\"><h3></h3></div>", "<div class=\"tog\"><h3>" + getString(R.string.news) + "</h3></div>") + "\n");
	        	} else if(line.startsWith("<td align=\"center\"></td>")) {
	        		result.append(line.replaceAll("<td align=\"center\"></td>", "<td align=\"center\">" + getString(R.string.footer).replaceAll("\n", "<br>\n") + "</td>"));
	        	} else {
	        		result.append(line + "\n");
	        	}
	        	
	        	SPFCPreference preference = new SPFCPreference(this);
	        	
	        	if(line.startsWith("<ul id=\"briask-iss52\" class=\"briask-iss\" style=\"width:472px;height:300px\">")) {
	        		String[] photoSrcs = preference.getMainMenuPhotos();
		        	
		        	for(int i = 0; i < photoSrcs.length; i++) {
		        		result.append("<li><img src=\"" + photoSrcs[i] + "\" alt=\"An Image Slideshow\" /></li>\n");
		        	}
	        	}
	        	
	        	if(line.startsWith("<div style=\"overflow: auto; position: absolute; margin: 0px;\" id=\"vmarquee\">")) {
	        		//加入本日金句
	        		result.append("<p>【" + getString(R.string.golden_word) + "】" + preference.getMainMenuGoldenString() + "</p>\n");
		        	result.append("<p> </p>\n");
	        		
		        	String[] newsContents = preference.getMainMenuNews();
		        	
		        	for(int i = 0; i < newsContents.length; i++) {
		        		result.append(newsContents[i] + "\n");
		        	}
	        	}
	        }
		} finally {
			if(inputStream != null) {
				inputStream.close();
			}
		}
		
		return result.toString();
	}
	
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			try {
				loadHTML();
			} catch (Exception e) {
				//輔助功能,產生錯誤就不丟出exception了
				Log.e(getClass().getName(), "Read html error!", e);
			}
		}
	};
	
	private void getWebViewData() {
		if(AndroidUtility.isNetworkAvailable(this)) {
			final SPFCPreference preference = new SPFCPreference(MainMenuActivity.this);
			
			//先設成false,如果教會網站服務是正常的,會在thread裡面被設為true
			//這是為了避免ANR,造成程式當掉,且造成使用者久候不耐
			preference.setSPFCAvailable(false);
			
			Thread thread = new Thread(new Runnable() {
				
				public void run() {
					List<Photo> photos = new ArrayList<Photo>();
					try {
						photos = ServiceProxyFactory.createPhotoService(MainMenuActivity.this).getNewsPhotos().getPhotos();
					} catch (Exception e) {
						//相片的service如果有問題,也不要影響其他東西的顯示
						//只記log就好
						Log.e(getClass().getName(), "Photo service error!", e);
					}
					
					String goldenString = "";
					try {
						goldenString = ServiceProxyFactory.createBibleService(MainMenuActivity.this, BibleDAOImpl.CHINESE_UNION_VERSION, BibleCatagoryDAOImpl.CHINESE_BIBLE).getGoldenWord();
					} catch (Exception e) {
						//金句的service如果有問題,也不要影響其他東西的顯示
						//只記log就好
						Log.e(getClass().getName(), "Golden string service error!", e);
					}
					
					//TODO: 目前authentication和authorization還沒有server端服務,所以這邊只好先暫時傳null使用者
					UserImpl user = new UserImpl();
					user.enable(true);
					List<News> news = new ArrayList<News>();
					try {
						news = ServiceProxyFactory.createNewsService(MainMenuActivity.this, user, null).getNews();
						
						preference.setSPFCAvailable(true);
					} catch (Exception e) {
						//消息代禱的service如果有問題,也不要影響其他東西的顯示
						//只記log就好
						Log.e(getClass().getName(), "News service error!", e);
						try {
							news = ServiceProxyFactory.createNewsService(MainMenuActivity.this, user, null).getEmergencyNews();
						} catch (Exception e1) {
							//消息代禱的service如果有問題,也不要影響其他東西的顯示
							//只記log就好
							Log.e(getClass().getName(), "News service error!", e);
						}
					}
					
					preference.setMainMenu(photos, goldenString, news);
					
					handler.sendEmptyMessage(0);
				}
			});
			thread.start();
		}
	}
}
