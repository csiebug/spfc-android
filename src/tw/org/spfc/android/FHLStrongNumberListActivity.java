package tw.org.spfc.android;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import bible.domain.BibleWord;
import bible.domain.StrongNumber;
import csiebug.android.util.AndroidUtility;
import csiebug.service.ServiceException;
import tw.org.spfc.android.persistence.impl.BibleCatagoryDAOImpl;
import tw.org.spfc.android.service.impl.BibleServiceImpl;
import tw.org.spfc.android.util.SPFCPreference;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class FHLStrongNumberListActivity extends BaseListActivity {
	private String[] words;
	private StrongNumber[] strongNumbers;
	
	@Override
	protected String[] getListItemTexts() {
		return words;
	}
	
	@Override
	protected Spanned[] getListItemSpanneds() {
		return null;
	}

	@Override
	protected String[] getContextMenuOptionsByArray() {
		return null;
	}

	@Override
	protected List<String> getContextMenuOptionsByList() {
		return null;
	}

	@Override
	protected Map<Integer, String> getContextMenuOptionsByMap() {
		return null;
	}

	@Override
	protected void contextItemSelectedLogic(MenuItem item) {
	}

	@Override
	protected String getContextMenuHeaderTitle() {
		return null;
	}

	@Override
	protected int getContextMenuHeaderIcon() {
		return 0;
	}

	@Override
	protected void putListItemToBundle(Bundle bundle, int position) {
	}

	@Override
	protected Class<?> getListItemEditActivityClass() {
		return null;
	}

	@Override
	protected int getRequestCode() {
		return 0;
	}
	
	@Override
	protected void onActivityResultLogic(int requestCode, int resultCode,
			Intent data) {
	}

	@Override
	protected void setListeners() {
	}
	
	@Override
	protected Integer getLayout() {
		return R.layout.result_list;
	}
	
	@Override
	protected void onCreateLogic() {
		try {
			((TextView)findViewById(R.id.function_name)).setText(getString(R.string.note_fhl_strong_number));
			
			String fhlVersion = getIntent().getExtras().getString("version");
			String chineses = getIntent().getExtras().getString("chineses");
			int chapter = getIntent().getExtras().getInt("chap");
			int verse = getIntent().getExtras().getInt("sec");
			
			int language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
			int version = new SPFCPreference(this).getBibleVersion();
			
			Locale phoneLocale = getResources().getConfiguration().locale;
			
			if(phoneLocale.equals(Locale.CHINESE) || phoneLocale.equals(Locale.TAIWAN) || phoneLocale.equals(Locale.TRADITIONAL_CHINESE)) {
				language = BibleCatagoryDAOImpl.CHINESE_BIBLE;
			} else {
				language = BibleCatagoryDAOImpl.ENGLISH_BIBLE;
			}
			
			BibleServiceImpl service = new BibleServiceImpl(this, version, language);
			List<BibleWord> bibleWords = service.getFHLStrongNumber(fhlVersion, chineses, chapter, verse);
			
			words = new String[bibleWords.size()];
			strongNumbers = new StrongNumber[bibleWords.size()];
			for(int i = 0; i < bibleWords.size(); i++) {
				if(bibleWords.get(i).getStrongNumber() != null) {
					if(bibleWords.get(i).getStrongNumber().getNumber() != -1) {
						words[i] = bibleWords.get(i).getText() + "<" + bibleWords.get(i).getStrongNumber().getNumber() + ">";
					} else {
						words[i] = bibleWords.get(i).getText() + "<" + bibleWords.get(i).getStrongNumber().getDescription() + ">";
					}
				} else {
					words[i] = bibleWords.get(i).getText();
				}
				strongNumbers[i] = bibleWords.get(i).getStrongNumber();
			}
		} catch (ServiceException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected Integer getListItemLayout() {
		int layout = FontSizeLayout.getLayoutForList(new SPFCPreference(this));
		
		return layout;
	}
	
	@Override
	protected Integer getListItemTextViewID() {
		return R.id.itemText;
	}
	
	@Override
	protected void onListItemClickLogic(ListView l, View v, int position,
			long id) {
		if(strongNumbers[position] != null) {
			if(!AndroidUtility.isNetworkAvailable(this)) {
				toast(getString(R.string.network_unavailable));
			} else {
				Intent fhl = new Intent(this, FHLActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("book", "strong");
				if(strongNumbers[position].getLanguage().equalsIgnoreCase(StrongNumber.LANGUAGE_HEBREW)) {
					bundle.putString("language", "1");
				} else {
					bundle.putString("language", "0");
				}
				
				if(strongNumbers[position].getNumber() != -1) {
					bundle.putInt("strongNumber", strongNumbers[position].getNumber());
				} else {
					bundle.putInt("strongNumber", -1);
					bundle.putString("description", strongNumbers[position].getDescription());
				}
				
				fhl.putExtras(bundle);
				startActivity(fhl);
			}
		}
	}
}
