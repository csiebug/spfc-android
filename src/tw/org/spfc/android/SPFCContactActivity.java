package tw.org.spfc.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class SPFCContactActivity extends BaseListActivity {
	private static final String OPERATOR = "0228977702";
	private List<String> coworkers = new ArrayList<String>();
	private String[] emails;
	
	@Override
	protected Spanned[] getListItemSpanneds() {
		emails = new String[coworkers.size() + 1];
		Spanned[] contactList = new Spanned[coworkers.size() + 1];
		emails[0] = "";
		contactList[0] = Html.fromHtml("總機: " + OPERATOR);
		for(int i = 0; i < coworkers.size(); i++) {
			String name = coworkers.get(i).split(";")[0];
			String callNumber = coworkers.get(i).split(";")[1];
			String email = coworkers.get(i).split(";")[2];
			
			emails[i + 1] = email;
			if(callNumber.indexOf(',') == -1) {
				contactList[i + 1] = Html.fromHtml(name + "<br>" + email);
			} else if(callNumber.indexOf(OPERATOR) != -1) {
				contactList[i + 1] = Html.fromHtml(name + "<br>分機: " + callNumber.split(",")[1] + "<br>" + email);
			} else {
				contactList[i + 1] = Html.fromHtml(name + "<br>榮總分機: " + callNumber.split(",")[1] + "<br>" + email);
			}
		}
		
		return contactList;
	}

	@Override
	protected String[] getListItemTexts() {
		return null;
	}

	@Override
	protected void contextItemSelectedLogic(MenuItem arg0) {
		
	}

	@Override
	protected int getContextMenuHeaderIcon() {
		return 0;
	}

	@Override
	protected String getContextMenuHeaderTitle() {
		return null;
	}

	@Override
	protected String[] getContextMenuOptionsByArray() {
		return null;
	}

	@Override
	protected List<String> getContextMenuOptionsByList() {
		return null;
	}

	@Override
	protected Map<Integer, String> getContextMenuOptionsByMap() {
		return null;
	}

	@Override
	protected Class<?> getListItemEditActivityClass() {
		return null;
	}

	@Override
	protected int getRequestCode() {
		return 0;
	}

	@Override
	protected void onActivityResultLogic(int arg0, int arg1, Intent arg2) {
		
	}

	@Override
	protected void putListItemToBundle(Bundle arg0, int arg1) {
		
	}

	@Override
	protected void setListeners() {
		
	}
	
	@Override
	protected Integer getLayout() {
		return R.layout.result_list;
	}
	
	@Override
	protected Integer getListItemTextViewID() {
		return R.id.itemText;
	}
	
	@Override
	protected void onCreateLogic() {
		((TextView)findViewById(R.id.function_name)).setText(R.string.contact);
		
		InputStream inputStream = null;
		try {
			inputStream = getResources().openRawResource(R.raw.contact);
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			
			String line = null;
			while((line = reader.readLine()) != null){
	        	coworkers.add(line);
	        }
	    } catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}
	
	@Override
	protected void onListItemClickLogic(ListView l, View v, int position,
			long id) {
		if(position > 0) {
			Intent notifyIntent = new Intent(Intent.ACTION_SEND);
			notifyIntent.setType("plain/text");
			notifyIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emails[position]});
			startActivity(Intent.createChooser(notifyIntent, getString(R.string.contact)));
		}
	}
}
