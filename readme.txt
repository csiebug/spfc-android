關於個人資料之蒐集與運用的宣告:
本App使用Google Analytics記錄使用者在App的瀏覽行為等資料，這些資料係供本App管理流量和行為調查進行總量分析，以利於提昇本App的服務品質。
本App並不會蒐集任何有關個人身份的資料。
本App僅對全體使用者行為總和進行分析，並不會對個別使用者進行分析。

--
v1.16.19
2017/2/22
1.手寫筆記儲存錯誤的bugfix

--
v1.16.18
2016/12/24
1.增加修改電子聖經顏色的功能，可改為黑底白字

--
v1.16.17
2016/4/30
1.改善讀經進度提示

--
v1.16.16
2016/2/26
1.修正SSL問題

--
v1.16.15
2016/2/19
1.修正SSL問題

--
v1.16.14
2016/1/27
1.更新教會同工通訊資訊

--
v1.16.13
2015/8/23
1.修正查詢部分Strong Number的錯誤

--
v1.16.12
2015/7/30
1.編輯讀經計劃增加新增經文範圍按鈕
2.更新辦公室同工聯絡資訊

--
v1.16.11
2015/1/29
1.修正主日信息中的週報、投影片顯示功能
2.更新教會同工通訊資訊

--
v1.16.10
2014/11/25
聖經經文查詢功能長按經文可呼叫以下功能:
1.直接跳躍至聖經閱讀
2.複製經文到剪貼簿
3.製作金句卡
4.分享此經文至Facebook塗鴉牆
5.分享此經文至Twitter

--
v1.16.9
2014/10/27
1.因應Strong's Number功能改版的修正

--
v1.16.8
2014/9/23
1.增加團契小組聚會時間表
2.更新同工聯絡資訊

--
v1.16.7
2014/8/27
1.聖經增加石牌信友堂崇拜書籤

--
v1.16.6
2014/8/14
1.在教會消息功能加上觀看信友代禱誌功能
2.為支援更多機型，取消製作金句卡中拍照功能

--
v1.16.5
2014/7/31
1.因應Strong's Number功能改版的修正

--
v1.16.4
2014/7/20
1.修正多個版本聖經的閱讀錯誤。　

--
v1.16.3
2014/7/15
1.主日信息增加週報、講道投影片(若講員有提供的話)以及最新的週報檔案

--
v1.16.2
2014/6/29
1.聖經人物多國語bug fix

--
v1.16.1
2014/5/24
1.經文對照bug fix

--
v1.16.0
2014/5/20
1.聖經增加不同版本經文對照功能

--
v1.15.1
2014/5/18
1.經文搜尋bug fix
2.經文長按增加經文複製功能，可以在任意APP(如gmail等)的文字輸入框貼上

--
v1.15.0
2014/4/30
1.電子聖經加上書籤功能，方便快速切換聖經章節
2.調整電子聖經功能layout，新增menu按鍵讓使用者方便使用經文查詢與讀經計劃等功能；聖經版本切換功能也整合到menu裡

--
v1.14.9
2014/4/18
1.因應網站搬移，網站服務不穩定的情況，補上一些錯誤處理

--
v1.14.8
2014/4/6
1.網站搬移,因應成人主日學檔案路徑修改做修正

--
v1.14.7
2014/3/31
1.網站搬移,因應講道檔案路徑修改做修正

--
v1.14.5
2014/3/20
1.經文搜尋增加查詢條件,可以在更精確的範圍搜尋關鍵字
2.聖經書卷切換,增加條件,可減少滾動搜尋書卷的時間,可更快速切換書卷

--
v1.14.4
2014/3/15
1.經文搜尋bug fix

--
v1.14.3
2014/3/14
1.增加聖經度量衡換算(在補充圖表功能)

--
v1.14.2
2014/3/5
1.聖經筆記Evernote上傳修正
2.教會網頁服務中斷時的錯誤處理
(因教會網站空間要做轉換，教會網站也正在開發新版，所以需要因應此過渡時期可能服務暫停)

--
v1.14.1
2014/2/26
1.聖經bug fix
2.增加操作說明文件(設定/關於本軟體)

--
v1.14.0
2014/2/21
1.筆記功能整合Evernote(需於設定那裏登入授權給APP)
2.修正金句卡拍照功能

--
v1.13.3
2014/2/13
1.Twitter登入與推文問題修正

--
v1.13.2
2014/2/8
1.自製金句卡記憶體與效能調整

--
v1.13.1
2014/2/6
1.自製金句卡的bug fix

--
v1.13.0
2014/2/5
1.聖經經文增加自製金句卡功能

--
v1.12.2
2014/1/17
1.手寫筆記效能調整

--
v1.12.1
2014/1/15
1.聖經地圖增加手寫筆記的功能
2.手寫筆記效能調整

--
v1.12.0
2014/1/15
1.聖經增加手寫筆記的功能

--
v1.11.1
2014/1/11
1.語音搜尋助理可以搜尋經文,也可直接跳到聖經指定的章節

--
v1.11.0
2014/1/7
1.簡體中文聖經增加難字漢語拼音版本
2.增加語音搜尋助理的功能(APP首頁按menu鍵即可叫出)

--
v1.10.4
2013/12/7
1.改善聖經上下左右滑動的效能,減少因上下滑動而誤換頁的機率

--
v1.10.3
2013/12/4
1.聖經地圖與聖經人物資料可隨著使用者切換聖經版本,而跑出對應語言(新增10種語言)的資料
2.聖經人物的Wiki也隨著使用者切換聖經版本,連到對應語言(新增10種語言)的Wiki

除了中文和英文,其他語言都是即時透過Google翻譯,理論上有可能會稍慢
因此本來是不打算做多國版本
但用HTC Butterfly S加威寶的行動網路測試,速度其實還蠻快的
就把這部分加上
另外,我已將此App的Source Code放到網路上
希望可以有人幫忙把程式界面多國語化
網址:
https://bitbucket.org/csiebug/spfc-android/

--
v1.10.2
2013/11/12
1.改善主日學投影片載入的效能

--
v1.10.1
2013/11/9
1.因應最近流行的5吋高解析度機種(解析度1920x1080),聖經字體可以放更大

--
v1.10.0
2013/7/26
1.聖經加上輔助圖表功能,可供查經參考

--
v1.9.4
2013/6/26
1.聖經人物查詢可根據使用者手機語系,連至繁體中文、簡體中文或英文的Wiki

--
v1.9.3
2013/6/11
1.繁體和合本聖經多加了難字注音的功能，方便弟兄姊妹朗讀聖經

--
v1.9.2
2013/6/4
1.主日學加上投影片的功能

--
v1.9.0
2013/5/31
1.增加石牌信友堂主日學服務

--
v1.8.1
2013/5/27
1.增加11本Public Domain聖經(簡體中文、法、德、西、葡等版本聖經)

--
v1.8.0
2013/5/22
1.整合Twitter推文分享

--
v1.7.5
2013/5/2
1.加上講道列表與讀經計劃檔案錯誤的防範

--
v1.7.4
2013/4/24
1.修改教會發佈訊息通知的邏輯

--
v1.7.3
2013/4/19
1.加上教會發佈訊息通知的功能

--
v1.7.2
2013/3/29
1.加上Google Analytics分析

--
v1.7.1
2013/3/28
1.修正facebook分享教會消息的bug,現已可顯示活動縮圖
2.修正部分機型教會消息顯示為亂碼的問題

--
v1.7.0
2013/3/27
1.增加獨立的教會消息功能,此功能也整合facebook,可在此功能將教會欲宣傳的活動分享至facebook塗鴉牆
2.讀經筆記也增加分享至facebook塗鴉牆的功能

--
v1.6.0
2013/3/25
1.整合facebook,可分享石牌信友堂講道、圖書、聖經經文以及此App至facebook塗鴉牆
因整合facebook的關係，此版之後不支援Android 2.1以下的機種

--
v1.5.7
2013/3/14
1.修正設定讀經進度的bug
2.首頁教會消息加上zoom-in/zoom-out控制

--
v1.5.6
2013/3/3
1.更新教會新書介紹的內容

--
v1.5.5
2013/2/27
1.修正新書介紹文字對應錯誤

--
v1.5.4
2013/2/26
1.修正新書介紹文字對應錯誤

--
v1.5.3
2013/2/19
1.更新教會同工通訊資料
2.新書介紹增加文字介紹

--
v1.5.2
2013/1/11
1.修正讀經進度圓餅圖顯示錯誤

--
v1.5.1
2013/1/7
1.修正經文換章的錯誤

--
v1.5.0
2013/1/2
1.新增新書介紹功能(介紹以琳書房書籍)
2.修正讀經計劃到期後的統計錯誤
3.修正記錄上次讀經位置的錯誤

--
v1.4.4
2012/12/20
1.修正套用尚未設定經文範圍的讀經計劃,造成設定讀經計劃的功能無法使用的問題
2.修改讀經進度圓餅圖顯示,考慮on schedule以及進度超前的顯示

這兩個bug我要感謝我親愛的太太
因為她不太會用電腦和3C產品,反而讓我找到了如此的bug
她看到我用手機進行讀經計劃,就叫我也教她用

她設定了一個沒有經文範圍的讀經計劃並且套用了,要再進入讀經計劃的功能都會有程式錯誤
要解決這個問題,可以到管理應用程式,找到石牌信友堂點進去,然後按清除資料
這樣就會清掉這個app的preference資料了
不過此版本已經考慮了此問題,因此也不會再有程式錯誤產生

然後她設了一個短期的讀經計劃,而且她的進度已經超前了
她show給我看圓餅圖,我才看到會顯示負數
哈哈...因為我從年中開始全年讀經計劃,所以我都一直是落後的狀態
我好像也直覺覺得讀經計劃落後很正常,完全沒考慮進度超前的問題

--
v1.4.3
2012/12/19
1.修正記錄讀經進度的bug

我從年中左右開始全年讀經計劃,所以不是從創世紀開始讀經
所以跑到現在才發現這個bug
今天讀了利未記發現從11章開始無法記錄
查了一下發現我紀錄讀經進度的資料,key值重複的關係,因為是用書卷的index再加上章而成
利未記11章就會是211,剛好會和雅歌第1章一樣也是211
把key值規則重新設計補零解決此問題

--
v1.4.2
2012/11/27
1.讀經進度功能暫時取消顯示未讀完書卷功能,以提升效能

--
v1.4.1
2012/10/18
1.修正聖經最後一章往後滑動造成程式當掉的錯誤
2.修正某些切換狀況下，記憶上次讀經位置的錯誤

--
v1.4.0
2012/10/11
1.修改教會電話功能,補上新進的同工聯絡資訊,並加上寄送email功能
2.更換icon圖示

--
v1.3.1
2012/9/29
1.增加聖經經文字體縮放的功能,用雙指放大和縮小字體(原本只有兩種大小可選擇)

--
v1.2.4
2012/9/9
1.修正希臘文查詢Strong's Number的問題

正在讀以西結書,所以沒有察覺希臘文查詢有問題
查到的是希伯來文的原文
今天講道的時候講羅馬書才發現這問題
希望這次徹底解決這問題

--
v1.2.3
2012/9/2
1.修正查詢Strong's Number的問題

--
v1.2.2
2012/9/1
1.修正查詢Strong's Number的問題

--
v1.2.1
2012/8/31
1.修正部分希臘字查詢Strong's Number的問題

--
v1.2.0
2012/8/31
1.修改錯字ISBN
2.經文新增查詢Strong's Number功能

--
v1.1.7
2012/7/24
1.拿掉撥教會電話功能,改show分機號碼

今天在找為何有平板無法搜尋到此app的問題
順便就修正撥教會分機的功能
不過後來找到原因了,原因就是因為這個app有要求撥號的權限
而有些平板只有WIFI版,沒有3G版
所以無法撥號的平板在Google Play是無法搜尋到此app
為了支援更多機器,使更多人能使用
及評估撥號的功能使用的機率
決定在此版拿掉撥號功能

--
v1.1.6
2012/7/4
1.修正讀經紀錄的錯誤,造成經文閱讀無法使用的問題

--
v1.1.5
2012/6/29
1.自動下載講道的預設值設為關閉(應該對大部分使用者來說,不是需要的,所以把預設下載改掉,有需要再自己打開)
2.講道撥放功能可按功能鍵進行講道相關設定
3.在書卷章節切換功能上面加上橘色按鈕,可辨識目前讀經進度已讀的章

我已經把設定頁,有關講道的設定,移到講道撥放功能那裡去做設定
這是為了之後預備,設定頁以後會做其他功能設定之用
等我開發更多雲端服務後,就可以開放app申請帳號

之前在讀經計畫設定那裡已經加了提示哪卷書未完成
不過還是覺得不太夠
想到可以在書卷章節那裡用不同顏色的按鈕,這樣就可以讓使用者一目了然,知道本書的進度如何
這應該跟其他聖經軟體比起來是很獨特的設計吧?! :P

--
v1.1.4
2012/5/28
1.聖經人物書卷查詢功能效能修改

--
v1.1.3
2012/5/26
1.聖經人物書卷查詢功能加上cache

聖經人物書卷查詢,會比較耗時(等之後有時間再refactor這塊)
所以加上cache功能,如果上次查過同一卷書的話,就會引用cache裡面的資料
速度會差很多喔

--
v1.1.2
2012/5/25
1.新增聖經人物查詢以章和書卷查詢的功能

v1.1.1
2012/5/25
1.新增聖經人物查詢（資料來源為wikipedia）,若本節查無人名,則列出所有的人名

雖然wiki的資料正確性不是太可靠
不過資料多，而且目前沒有找到具有公信力的聖經人物的網路服務
所以還是使用wiki供查經的時候參考用
我想也是很方便的功能

目前我所整理的人名列表只有三百多筆
所以若有缺漏,可以讓我知道,我可以補上資料

--
v1.0.23
2012/5/21
1.繼續修正下載講道服務

--
v1.0.21
2012/5/19
1.修正經文版本切換的章節定位問題
2.讀經進度增加未完成書卷提醒(當進度逼近的時候,卻不知還有哪章未讀時會有用)

--
v1.0.20
2012/5/18
1.繼續修正下載講道的程式碼

--
v1.0.19
2012/5/16
1.加上讀經進度與外撥電話的confirm,防止誤用
2.繼續修正下載講道的程式碼

自己也已經用這程式的讀經進度在走,已經讀了一百多章
想到如果不小心按下重設,那讀經進度就消失了
所以想想還是加上confirm對使用者比較保險
教會電話也加上confirm,以免不小心就撥打電話出去了

--
v1.0.18
2012/5/15
1.修正Android 4.0以上下載講道發生錯誤而下載不完全的情況
2.加寬讀經進度圓餅圖的寬度

我的sensation升上ICS之後發現這幾週講道都是0 bytes
所以講道撥放沒辦法撥,要把sdcard的講道mp3砍掉才可以撥
(因為預設sdcard有檔案就會撥放sdcard檔案,沒有才會連上網線上收聽)
後來才知道有一個寫法在以前的Android版本允許,但Android4.0以上不行
希望可以徹底解決問題

--
v1.0.17
2012/4/26
1.聖經字詞查詢,允許輸入多個keyword,用空白隔開
2.修正和合本聖經有些單字無法顯現問題
3.教會位置補上交通方式的資訊

我想這功能應該可以幫助弟兄姊妹查經文更方便
比如某節經文很有印象卻不知道出現在哪裡
背不完全但是知道多個關鍵字,這樣的功能就很方便了

--
v1.0.15
2012/4/24
1.繼續改善載入讀經進度的速度以及記憶體用量

--
v1.0.14
2012/4/22
1.修正載入讀經計畫影響聖經閱讀功能的載入速度

因為程式寫的有瑕疵
導致跑的很沒有效率，而且記憶體用量驚人
本來寫出來只是稍微點個幾章做測試，功能正常
當我想用自己的程式開始自己的讀經計畫的時候
才發現開經文越來越慢了
不過當自己也變成深度使用者的時候，才可以先發現一些問題可以修正
--
v1.0.12
2012/4/19
1.增加Sensation升級ICS後的解析度
2.讀經進度加上圓餅圖報表

昨天我等了好久的官方ICS終於出來了
迫不及待的把自己的Sensation升級為ICS
不過我心裡也籌算著,可能會有和Galaxy Nexus一樣的解析度問題
升完後,馬上打開石牌信友堂app,果不其然,它載入的是低解析度的layout
app沒辦法判斷成原本的960x540,雖然打開程式,程式畫面還是會佔滿整個螢幕
看起來以後要針對ICS的機種要額外增加解析度的支援了

至於讀經進度,我想之後可能會加入還有哪些書未完成的資訊
這樣才有提醒作用,也才有機會去閱讀和點選已閱讀按鈕
--
v1.0.9
2012/4/16
1.增加聖經地圖的提示
2.讀經進度增加到目前為止應讀的章數

--
v1.0.8
2012/4/14
1.增加Galaxy Nexus解析度的支援

其實我有做1280x720解析度的設計,在模擬器上面有跑過這個解析度
結果遇到Galaxy Nexus實機跑出來的結果畫面卻很奇怪
網路上查硬體規格也的確是1280x720
後來看到這篇文章才知道原來真的是Galaxy Nexus特殊的問題(HTC One應該也會一樣,可能是ICS的問題了?)
http://www.mobileai.tw/2012/01/27/google-galaxy-nexus/
所以我做了1184x720的解析度,希望Galaxy Nexus跑起來是比較正常的

所以有實機上面發現任何問題,還是要告訴我喔
不然我還真的無法測出這些問題呢

--
v1.0.7
2012/4/13
1.修正讀經的閱讀按鈕的bug

因為一直在擴充功能和改善程式碼
結果把原本做好的功能改壞掉都不知道
沒有測試team的話,只能等自己有天用到才會發現了

--
v1.0.6
2012/4/13
1.將自動下載時的電源管理層級修改

Android預設螢幕關閉時會將數據網路也關閉,這是為了省電
所以自動下載功能還是會因為網路斷線而終止
但是不會產生Exception,所以對程式來說沒有發生錯誤,但下載其實已經被中止了
上方通知列我就沒辦法用程式判斷把它關掉

若覺得上方通知列一直呈現下載中,但其實已經很久沒動的情況下
請到設定->應用程式->管理應用程式->強制停止

若您不在乎省電問題,可以到設定->電源->讓數據連線進入休眠取消掉

若您希望維持您手機原本的省電設定
那就是出現下載異常時,手動強制停止程式

若您不需要下載講道檔案,那就到程式中的設定把定時預先下載取消掉

--
v1.0.5
2012/4/9
1.修正部分機型記憶體限制,造成聖經地圖與King James Version切換產生的錯誤

--
v1.0.4
2012/4/7
1.修正一些聖經地理資料
2.將聖經地圖起始位置設在耶路撒冷
3.修正尋找聖經地圖的邏輯
4.修改部分程式碼增快執行速度

--
v1.0.0
2012/4/6
1.加上聖經地圖的功能

加上聖經地圖功能
不過資料是否正確不太確定,如果有用此功能也可以幫忙校對資料喔

主要功能目前已經算是穩定
所以決定將版號升為1.0
希望大家多多支持

--
v0.2.22
2012/4/2
1.修改載入聖經的程式碼,使之更有效率避免錯誤

--
v0.2.21
2012/4/1
1.新增King James Version版本聖經,可供中英文版本聖經的對照

--
v0.2.19
2012/4/1
1.補上一些解析度的支援

--
v0.2.18
2012/3/30
1.補上聖經放大縮小時,停留在當節經文的功能

不過這在"某些"操作的狀況會失靈
因為手勢可以引發的動作太多
手勢之間的差異很小
各種手勢的動作會有重疊效果等等
這個有空我還會繼續做加強
我想這已經很方便,再加上之前做出來的fast scroll
應該可以讓我慢慢考慮處理這問題吧?!

--
v0.2.17
2012/3/27
1.加上fast scroll,讓讀經可以更方便

--
v0.2.16:
2012/3/26
1.設定讀經進度加上防呆

--
v0.2.15:
2012/3/26
1.修改選取書卷和章節的UI

之前是用選單來跳章,如果遇到如詩篇這種,要跳到想讀的章節就會很麻煩
相信改成這樣應該會更方便大家閱讀聖經

--
v0.2.14:
2012/3/23
1.修正撥放講道時進入休眠模式的問題

--
v0.2.13:
2012/3/23
1.修正因為手機休眠造成的網路斷線問題，將定時下載講道功能回復

這個bug找好久，連接在電腦上都debug不出來
但是在手機上跑下載就是會停住，原來是Android進入休眠會把cpu、螢幕、網路都關掉
而連接電源的時候手機不會進入休眠，難怪無法debug出來
這次改完後用實機測過兩次(開始下載後就不管手機了)，各下載了五個講道(共約200-250MB)，用威寶的3G的網路
兩次都順利下載完畢，這次應該是沒問題了
定時下載講道功能，因為講道檔案不會太小，所以建議盡量使用WIFI
(雖然3G測過也OK，不過既然要用定時下載，我想應該都是設定在家裡睡覺時，用家裡的網路吧?!)

--
v0.2.12:
2012/3/20
1.暫時將自動下載講道功能取消
2.增加首頁loading速度

看起來好像短期要解決講道下載斷線的問題很難
notification會一直存在,會造成使用者困擾
所以先拿掉此功能

--
v0.2.11:
2012/3/19
1.修正某些狀況下取教會消息產生的錯誤

--
v0.2.10:
2012/3/16
1.增加320X240解析度的支援

終於上架了,也代表挑戰開始
因為可能會有更多的人會去下載使用
也會直接面對意見
今天就有第一個下載,沒想到居然是galaxy mini
剛好就是我沒去測試的解析度
我猜下載的使用者看到那畫面一定很囧
為了不讓使用者失望,只好馬上著手支援320X240
祈禱那位下載的使用者沒有移除,有機會可以更新到這個版本
不然就損失了一個使用者了

--
v0.2.9:

2012/3/14
1.加入撥打教會電話的功能
2.加入在Google Map標出教會位置的功能

我想,既然是手機app,就可以做點PC和網頁做不到的事情
那就是打電話出去
透過app打教會電話和分機,應該是挺方便的事情
應該是比打開通訊錄尋找快多了

在app可以直接show出教會位置,應該在與人分享上面可以很直接和方便

--
v0.2.8:

2012/3/13
1.修正部份程式效能

--
v0.2.7:

2012/3/5
1.新增loading畫面

一開app會去跑檢查新版本等連線的程式碼(如果手機有開網路的話)
因為連線網路的關係,所以進到主畫面之前會有一段時間要跑
此時畫面是整片黃色的(這是我設定的背景顏色),很容易被人認為是當掉還是怎樣(我也覺得有點醜)
所以把loading和主畫面程式分開,並且讓loading的時候有漂亮的背景圖

--
v0.2.6:

2012/2/10
1.修正首頁消息的部分程式碼

偶然發現首頁的下方那塊全白了,所以知道有發生錯誤
教會消息的部分是沒有錯的,原因是我去向信望愛的金句服務要金句產生錯誤
所以造成整塊都不顯示
所以加強了程式碼,這部分獨立開來
現在就可以看到教會消息,看看哪天信望愛的金句服務會弄好

Android的開發改版目前是比較緩慢的,我都沒有在理Android的開發
我知道自動下載講道好像還是有問題,不過這個要很專心來debug才行阿

過去的時間我在忙2/10開始的電信展公司要展出的app
有空的時間我在另外寫Google App Engine的基礎建設(已經快搞定了)
這樣我就可以寫很多雲端服務的程式
然後讓手機的app可以使用
我之前在煩惱的沒有server端平台的事情就沒問題了
甚至金句服務都可以有我們自己教會客製的服務
大家敬請期待吧

--
v0.2.5:

2012/1/10
1.修改下載講道的bug(存檔檔名取錯的問題,會造成離線撥放功能無法進行)
2.下載講道的notification增加progress bar可以看到下載進度

--
v0.2.4:

2011/12/30
1.修改下載講道的Service(還未測試)

隔了蠻多天都完全沒碰這個app了
其實我早就知道定期下載的Service有bug
不過沒時間去看
希望可以運作成功

--
v0.2.3:

2011/12/11
1.已閱讀按鈕的bugfix
2.寫檔的bugfix

傷腦筋...因為寫程式的過程中,已經建立一些sdcard的目錄
所以沒有注意到一些寫檔的問題
今天起了新的模擬器在跑就發現有問題
這下就提醒我要測試完全新的安裝的問題

--
v0.2.2:

2011/12/10
1.menu上面增加icon,button的style也更改

--
v0.2.1:

2011/12/08
1.已閱讀按鈕的bugfix

做refactory的時候有點改壞,所以上一版新的

--
v0.2.0:

2011/12/07
1.加上紀錄讀經的功能,在未讀過的章上面有"已閱讀"按鈕,按下去表示已經閱讀過本章
2.在"設定讀經計畫"上面可以看到自己目前達成的讀經進度

這樣大家就不用在紙上(讀經表)做記號,紀錄自己每天讀經
而且這功能我故意做在每章經文的上面,就是將讀經表和經文連在一起
這樣使用者至少要點到那章經文,才能按到那章經文的已閱讀按鈕
這是紙本的讀經表做不到的事情

再來的feature work
就是看看有沒更好的讀經進度顯示方式,看看是否要弄出圖表之類的
因為只是列出目前讀完多少章的確畫面上是不夠漂亮
(只是要為此做報表,我是蠻懶的啦)

--
v0.1.6:

2011/12/06
1.補上搜尋結果的輔助功能,如keyword和筆數的提示;經文搜尋也在經文上面將keyword用紅色字標起來

--
v0.1.5:

2011/12/05
1.版面調整
2.加上每日金句功能

--
v0.1.3:

2011/12/05
1.補上教會消息的功能,拿掉相片播放功能(覺得這樣版面比較好看,比較不會花)

想說其實還是可以parse html得到最新的教會消息,所以還是做了
這樣只要教會網站的公告事項有更新,同樣的在app上也會看到最新的消息

--
v0.1.2:

2011/12/04
1.在主選單下方加上了教會照片

昨天把這個程式也分享給若鵬哥
若鵬哥一開啟就很直覺地用雙指把它放大,但這不是網頁也不是圖片啊...XD
不過這表示我留白太多了啦
對大螢幕來講主選單下面空白真的太大(我的sensation也是,不過我的CHT8000+看起來就剛剛好)
其實我應該把圖示放大,不過這些icon我都是抓現成的,所以放大可能會很難看,還是需要人來設計不同大小的icon

不過大螢幕的優點就是有很多空間可以放東西,比起為了CHT8000+設計畫面要調東調西的要輕鬆很多
想到就在畫面下面加上照片播放功能(這個功能在CHT8000+就沒有囉)

本來也想抓教會最新消息放上去,不過這個就要維仁那邊支援了,因為真的要web server那邊提供我API

--
v0.1.1:

2011/12/04
1.修改離線版和合本聖經切換字形大小的方式,拿掉切換大小字型的按鈕,換成使用兩指控制縮放

不過我只有預備兩種字的size,所以不會像縮放照片一樣會漸漸放大或縮小
沒有多點觸控的螢幕可能就沒有效了
(現在應該沒有智慧型手機沒多點觸控吧?! 我的CHT8000+就沒有...哭哭...)

--
v0.1.0:

2011/12/03
1.修改離線版和合本聖經的換頁方式,拿掉左右按鈕,換成用滑動的方式換頁

大家可以看看這樣的操作方式是否好用,給我點意見
因為功能已經某個程度完整,所以把版本號升上一級

--
v0.0.29:

2011/12/01
1.新增了讀經進度編輯器,可以使用方便的UI來設計讀經進度

--
v0.0.28:

2011/11/30
1.一些功能弄了橫式畫面可以用

--
v0.0.27:

2011/11/30
1.載入讀經進度完成,按下android的menu鍵,即可進入,載入後,會在上面多一行文字提醒今天進度

我在https://sites.google.com/site/csiebug/fen-xiang-dang-an-gui
放了一些讀經進度檔的範例,下載以後放到/sdcard/spfc/bibleReadingPlan/裡面
就可以載入讀經進度檔
程式會照你設定的日期範圍和經文範圍,用平均的方式算出一天的讀經範圍
餘數的話會在前面的日期每天多讀一章
進度檔可以自行設定開始和結束日期 ,自訂禮拜幾要休息,自訂經文範圍(可以指定多個範圍)
聖經書卷是從0開始,章則是由1開始,這點是要稍微說明一下
詳細可以看那幾個範例
可以自行修改,放入指定目錄即可

因為這個程式很複雜,所以也不知道算出來的進度正不正確
因為我做的很彈性
所以有發現不對的地方就提醒我囉

--
v0.0.26:

2011/11/29
1.離線和合本聖經新增查詢功能,按下android的menu鍵,即可進入查詢界面

--
v0.0.25:

2011/11/28
1.修正查詢注釋串珠等查詢條件

--
v0.0.24:

2011/11/28
這次是新增離線版和合本聖經的功能,對著單節經文長按會出現以下新增功能
1.筆記功能
2.信望愛注釋
3.串珠
4.原文

其中2-4的資料來源都是來自信望愛網站
我還沒有寫信過去問,可不可以讓我引用
因為我自己覺得這功能實用,就先寫了
若不能引用,我就會把程式碼拿掉,讓新版的程式沒有此功能
也請試用的人更新新版程式,或是移除程式
謝謝

--
v0.0.23:

2011/11/28
1.修正離線聖經單節經文太長被截掉的問題
2.新增離線聖經放大字體功能

原本ListView的item是用android default的layout
在CHT8000+跑是不會被截掉,但HTC Sensation會
所以懷疑是HTC的kernel或是Android版本不同,所以造成同樣的layout有不同的表現
現在只好改成自己自訂layout,就沒有這問題了

--
v0.0.22:

2011/11/26
1.修正離線聖經程式bug

--
v0.0.20:

2011/11/26
1.修正離線聖經程式讀取聖經目錄的bug

--
v0.0.19:

2011/11/26
1.送大家一本離線的和合本聖經 :)

未來計畫:
1.在上面可以做字詞搜尋
2.在經節上面可以做筆記
3.可以下載讀經進度(由教會或團契提供,比如教會全年讀經,學青靈修組自定讀經計劃等),可以選擇載入那個計劃,然後在工具列上面顯示今天進度
這是我打算做的,不過不知道有沒有時間可以做

--
v0.0.18:

2011/11/25
1.定時下載bugfix

--
v0.0.17:

2011/11/25
1.將480x320設定頁的抽屜拉吧縮小,不至於擋住清除快取按鈕

--
v0.0.16:

2011/11/24
1. 離線播放時,若sdcard中有cache存在的話,也讓離線播放的檔案列表可以有中文標題

--
v0.0.15:

2011/11/24
1.播放器bugfix

--
v0.0.14:

2011/11/24
1.修改自動下載講道的邏輯
原本為自動抓去上個禮拜六日的檔案(有可能同工還沒放上去,如果是隔天的話)
現在改成去查網頁上面有無新的檔案,然後保持抓滿sdcard保留檔案數(此設定值在設定頁可設定)

--
v0.0.13:

2011/11/24
1.線上收聽講道列表快取功能完成,會讀取快取並且判斷太舊的快取(大於等於7天),會自動更新
2.在設定頁也可以清除快取,播放程式會自動去更新

--
v0.0.12:

2011/11/24
1.線上收聽講道的列表,已經可以顯示中文名稱和講員

接下來的工作應該是會去做cache
因為這和其他查詢功能不同,列表只會在特定時間增加,不會修改和刪除
所以不需要每次都renew
我還沒有做這個,所以目前每次開啟都要等一下下,會有點慢

--
v0.0.11:

2011/11/24
1.加上場地查詢功能

沒有什麼適當和現成的icon可以用
有誰美工強的可以來幫忙阿
需要一整套的icon讓有整體性和美觀協調

--
v0.0.10:

2011/11/23
1.加上教會圖書查詢功能

希望做了這個功能大家可以多多查詢教會的書
然後可以來借教會的書籍去看

--
v0.0.9:

2011/11/23
1. 針對解析度480x320的螢幕調整"設定頁",將某些UI收入抽屜中

我只有CHT 8000+和HTC Sensation兩支實機可以測試
一支解析度為480x320
一支解析度為960x540

如果有畫面美觀或操控問題的話
可以利用程式發建議給我
若可以註明機型甚至解析度的話就更好了

--
v0.0.8:

2011/11/22
1. DownloadSermonService的bugfix
2. 自動刪除舊檔功能
3. 使用者評分與建議功能

Mobile程式開發需要的是創意和貼近使用者
所以做了使用者建議的功能
這樣我就可以有很多想法可以做進去我的程式裡面
評分功能也是希望自己可以看看別人的評價
評分功能是Mobile開發者繼續開發的動力
希望自己是可以被鼓勵的...:P

評分與建議因為沒有server端環境可以讓我寫程式存起來
所以目前都是直接寄e-mail給我本人
沒辦法做的像market一樣,可以直接在手機上看到別人給的評價
這可以列入未來的feature
等我survey看看有沒有合適的方案(有免費的地方嗎?)

--
v0.0.7:

2011/11/22
1. DownloadSermonService的bugfix
2. 下載講道檔案出現提示訊息

發現有好多bug還要繼續改呢...改得很頭大...orz
軟體初期開發就是這樣,所以版本更新會很頻繁
所以才把自訂版本定為第0版
有bug請見諒啊

--
v0.0.6:

2011/11/20
1. DownloadSermonService的bugfix

--
v0.0.5:

2011/11/18
這次新增功能有:
1. 教會行事曆功能完成(可以zoom-in,zoom-out喔)

--
v0.0.4:

2011/11/18
這次新增功能有:
1. 版本檢查與軟體自動下載完成後自動呼叫安裝程式完成

可以到分享檔案櫃中,下載SPFC-v.0.0.2.apk來安裝試驗
應該會自動下載v0.04版來更新
自動升級軟體功能總算完成了...耶...

--
v0.0.3:

2011/11/17
這次新增功能有:
1.版本檢查與軟體自動下載(存放在/sdcard/spfc/apk/),更新(還沒完成,現在需要手動去安裝下載下來的apk)
2.設定下載講道數,超過自動刪除舊檔(還未測試)

可以到分享檔案櫃中,下載SPFC-v.0.0.2.apk來安裝
在執行程式的時候就會去檢查這邊有沒有新軟體,點選下載可能會出現錯誤訊息
因為我有嘗試直接自動安裝下載的apk
但是sdcard下載下來的apk沒有問題,可以手動安裝
有bug大家在回覆給我

--
v0.0.2:

2011/11/16
這次新增功能有:
1. 撥放器新增了seek功能,可以讓使用者用seekbar來決定從哪個時間開始撥放
2. 撥放器會自動選擇即時網路撥放或是從sdcard撥放定時下載的檔案
3. 定時下載檔案功能(每天定時會去檢查上個星期六日的講道檔案並且下載)

雖然我自己實驗用威寶3G線上收聽是不會有任何lag的
用seek功能也只停一下下就可以繼續撥放(完全沒有任何不悅感)
不過或許別家電信業者的網路可能會塞車吧 :P
所以還是提供了自動下載以及自動選擇撥放sdcard檔案的功能

大家在幫忙看看bug吧